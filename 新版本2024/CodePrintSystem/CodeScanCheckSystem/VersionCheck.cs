﻿using AutoUpdaterDotNET;
using CodePrintSystem.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodeScanCheckSystem
{
    
    public class VersionCheck
    {
        public static void AutoUpdate(Form form)
        {
            AutoUpdater.Start(AppsettingsConfig.UpdaterConfigFileAddress);
            System.Timers.Timer timer = new System.Timers.Timer
            {
                Interval = 2 * 60 * 1000,
                SynchronizingObject = form
            };
            timer.Elapsed += delegate
            {
                //更新的包只能是ZIP文件，包直接解压到当前目录（包解压后的目录要和要更新的保持一样）
                AutoUpdater.Start(AppsettingsConfig.UpdaterConfigFileAddress);
            };
            timer.Start();
        }
    }
}
