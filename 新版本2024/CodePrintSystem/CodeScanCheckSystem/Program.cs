using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using CodePrintSystem.Core;
using CodePrintSystem.Utility;
using Microsoft.Extensions.DependencyInjection;

namespace CodeScanCheckSystem
{
    static class Program
    {

        #region 捕获所有未经捕获的异常


        #region 多线程异常
        /// <summary>
        /// 多线程异常
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {

            string str = "";
            //父方法
            System.Diagnostics.StackTrace ss = new System.Diagnostics.StackTrace(true);
            System.Reflection.MethodBase mb = ss.GetFrame(1).GetMethod();

            //取得父方法命名空间    
            str += mb.DeclaringType.Namespace + ".";

            //取得父方法类名    
            str += mb.DeclaringType.Name + ".";

            //取得父方法类全名    
            str += mb.DeclaringType.FullName + ".";

            //取得父方法名    
            str += mb.Name;


            Exception ex = e.ExceptionObject as Exception;

            string EXMessage = JsonHelper.SerializeObject(ex);

            //记录异常日志
            WriteLogHelper.WriteLogsAsync("子线程异常[" + str + "]\r\n" + ex.Message + "\r\n INFO:" + EXMessage + "\r\n\r\n", "UnhandledException");
        }
        #endregion

        #region UI线程异常
        /// <summary>
        /// UI线程异常
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {

            string str = "";
            //父方法
            System.Diagnostics.StackTrace ss = new System.Diagnostics.StackTrace(true);
            System.Reflection.MethodBase mb = ss.GetFrame(1).GetMethod();

            //取得父方法命名空间    
            str += mb.DeclaringType.Namespace + ".";

            //取得父方法类名    
            str += mb.DeclaringType.Name + ".";

            //取得父方法类全名    
            str += mb.DeclaringType.FullName + ".";

            //取得父方法名    
            str += mb.Name;

            //记录异常日志
            WriteLogHelper.WriteLogsAsync("UI线程异常[" + e.Exception.TargetSite.DeclaringType.FullName + "]\r\n" + e.Exception.Message + "\r\n" + e.Exception.StackTrace + "\r\r" + e.Exception.ToString(), "UIThreadException");
        }

        #endregion
        #endregion
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //UI线程异常
            Application.ThreadException += Application_ThreadException;

            //多线程异常
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            var services = new ServiceCollection();
            //添加服务注册
            RegisterService.RegisterComponents(services);
            var serviceProvider = services.BuildServiceProvider();
            //// 插入日志例子
            //LogModel logModel = new LogModel();
            //logModel.LogMessage = "test";
            //var logger = serviceProvider.GetRequiredService<TPLLogger>();
            //logger.InsertAsync(logModel);

            string version = Application.ProductVersion.ToString(); //System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            CurrentUserConfig.Version = version;


            AppsettingsConfig.ServiceProvider = serviceProvider;
            AppsettingsConfig.DefaultConnectionString = ConfigurationManager.AppSettings["ConnectionString"];
            AppsettingsConfig.PrintInterval = Convert.ToInt32(ConfigurationManager.AppSettings["PrintInterval"]);
            AppsettingsConfig.PrintTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["PrintTimeout"]);
            AppsettingsConfig.PrinterName = ConfigurationManager.AppSettings["PrinterName"];
            AppsettingsConfig.ExportExcelPath = ConfigurationManager.AppSettings["ExportExcelPath"];
            AppsettingsConfig.IsFingerprint = Convert.ToBoolean(ConfigurationManager.AppSettings["IsFingerprint"]);
            AppsettingsConfig.IsCanPasswordUnLock = Convert.ToBoolean(ConfigurationManager.AppSettings["IsCanPasswordUnLock"]);
            AppsettingsConfig.UpdaterConfigFileAddress = ConfigurationManager.AppSettings["UpdaterConfigFileAddress"];
            AppsettingsConfig.DatabaseType = ConfigurationManager.AppSettings["DatabaseType"];
            ///CreateScope 每次请求都是一个新的实例，不然一直都是同一个实例(请参考上面的例子)
            using (var scope = serviceProvider.CreateScope())
            {
                var form = scope.ServiceProvider.GetRequiredService<Login>();
                Application.Run(form);
            }

        }
    }
}
