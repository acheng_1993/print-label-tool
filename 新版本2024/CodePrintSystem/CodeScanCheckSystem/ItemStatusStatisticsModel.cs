﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeScanCheckSystem
{
    internal class ItemStatusStatisticsModel
    {
        public int Count { get; set; }
        public int Status { get; set; }
    }
}
