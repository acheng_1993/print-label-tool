﻿using CodePrintSystem.Basic.Abstractions;
using CodePrintSystem.Core;
using CodeScanCheckSystem.Forms;
using CodePrintSystem.Utility;
using Microsoft.Extensions.DependencyInjection;
using Sunny.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodeScanCheckSystem
{
    public partial class MainForm : UIHeaderMainFooterFrame, ISelfSingletonAutoInject
    {
        ScanForm scanForm;
        PersonSet  personSet;

        IServiceProvider serviceProvider;
        public MainForm(
             IServiceProvider serviceProvider, ScanForm scanForm, PersonSet personSet)
        {
            this.scanForm = scanForm;
            this.personSet = personSet;
            InitializeComponent();
            this.serviceProvider = serviceProvider;
            LBL_Version.Text = CurrentUserConfig.Version;
            this.Text = "标签扫描系统";
            this.uiNavBar1.NodeMouseClick += UiNavBar1_NodeMouseClick;


        }
        /// <summary>
        /// 初始化主界面登录信息
        /// </summary>
        public void InitLoginInfo()
        {
            this.lblUserName.Text = CurrentUserConfig.User.UserName;
        }

        public void AddAdminMenu()
        {
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("人员设置");
            
            System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("指纹解锁设置", new System.Windows.Forms.TreeNode[] {
                treeNode2,
               });
            treeNode2.Name = "PersonSet";
            treeNode2.Text = "人员设置";
            this.uiNavBar1.Nodes.Insert(1, treeNode7);
        }
        public void InitPage()
        {
            if (CurrentUserConfig.User.IsAdmin.HasValue && CurrentUserConfig.User.IsAdmin.Value)
            {
                AddAdminMenu();
                AddPage(personSet, (int)SystemMenuEnum.PersonSet);
            }
            AddPage(scanForm, (int)SystemMenuEnum.CodeScan);
            SelectPage((int)SystemMenuEnum.CodeScan);
        }


        private async void UiNavBar1_NodeMouseClick(TreeNode node, int menuIndex, int pageIndex)
        {

            if (node.Name == SystemMenuEnum.ExitSystem.ToString())
            {
                if (UIMessageBox.ShowAsk("确认退出？"))
                {

                    Application.Exit();
                }
            }
            if(node.Name==SystemMenuEnum.PersonSet.ToString())
            {
                SelectPage((int)SystemMenuEnum.PersonSet);

            }
            if (node.Name == SystemMenuEnum.CodeScan.ToString())
            {
                SelectPage((int)SystemMenuEnum.CodeScan);

            }
            if (node.Name == SystemMenuEnum.PasswordChange.ToString())
            {
                UIEditOption option = new UIEditOption();
                option.AutoLabelWidth = true;
                option.Text = "密码修改";
                option.AddText("OldPwd", "原密码", "", true, true);
                option.AddText("NewPwd", "新密码", "", true, true);
                option.AddText("CheckNewPwd", "确认新密码", "", true, true);

                UIEditForm frm = new UIEditForm(option);
                frm.ShowDialog();

                if (frm.IsOK)
                {
                    if (!frm["CheckNewPwd"].ToString().Equals(frm["NewPwd"].ToString()))
                    {
                        UIMessageTip.ShowError("两次密码输入不一致！");
                        return;
                    }
                    var data = new ChangePwdModel()
                    {
                        Id = CurrentUserConfig.User.Id,
                        OldPwd = frm["OldPwd"].ToString(),
                        NewPwd = frm["NewPwd"].ToString()
                    };

                    var result = await CommonRequestHelper.GetService<ICommandUserService>().ChangePasswordAsync(
                         data);
                    CommonRequestHelper.RequestTip(result);
                }
            }
            //  BatchGenerateCodeAsync();

        }



        private void MainContainer_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            var time = DateTime.Now;
            var date = time.ToString("yyyy-MM-dd HH:mm:ss");
            var dayOfWeek = CommonHelper.GetWeek(time.DayOfWeek);
            var week = CommonHelper.GetYearWeekDayByTime(time);

            this.lblTime.Text = $"{date} 第{week.weekOfYear}周  {dayOfWeek}";
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
