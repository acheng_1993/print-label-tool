﻿using CodePrintSystem.AppLayer.Basic.ViewObject;
using CodePrintSystem.Basic.Abstractions;
using CodePrintSystem.Core;
using CodePrintSystem.Utility;
using Gst.Rtsp;
using Sunny.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ToolKitCore.MyNPOIExtension;

namespace CodeScanCheckSystem.Forms
{
    public partial class ScanForm : UITitlePage, ISelfSingletonAutoInject
    {
        public ScanForm()
        {
            InitializeComponent();
            this.Text = "标签扫描";

            cbxItem.ValueMember = "Code";
            cbxItem.DisplayMember = "Code";
            cbxStatus.ValueMember = "Code";
            cbxStatus.DisplayMember = "Name";
            this.dateStart.Value = DateTime.Now.AddDays(-1);
            this.dateEnd.Value = DateTime.Now.AddDays(1);
            cbxItem.TextUpdate += CbxItem_TextUpdate;
            cbxItem.Leave += CbxItem_Leave;
            cbxItem.SelectedIndexChanged += CbxItem_SelectedIndexChanged;
            uiDataGridView1.AutoGenerateColumns = false;
            uiDataGridView1.AllowUserToAddRows = false;
            InitData();
            InitDataGrid();
        }
        private void InitDataGrid()
        {
            uiPagination1.ActivePage = 1;
            uiPagination1.PageSize = 20;
            uiDataGridView1.ReadOnly = false;
            uiPagination1.PageChanged += UiPagination1_PageChanged;

            uiDataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;//自适应
            //uiDataGridView1.se;
            uiDataGridView1.AddColumn("条码", "Code");//.SetFixedMode(200);
            uiDataGridView1.AddColumn("产品", "ItemCode");
            uiDataGridView1.AddColumn("生产制令号", "ProductionCode");
            uiDataGridView1.AddColumn("结果", "StatusName");
            uiDataGridView1.AddColumn("扫码时间", "CreateTime");
            uiDataGridView1.AddColumn("操作人", "CreaterName");//.SetFixedMode(200);
            //  uiDataGridView1.AddColumn("版本", "VersionCode");//.SetFixedMode(100);
          //  LoadDataGrid();
        }

        List<CodeScanHistoryView> listData = new List<CodeScanHistoryView>();
        private async Task LoadDataGrid()
        {
            var condition = new BaseCodeScanHistoryCondition()
            {
                Code = this.txtBarCode.Text.Trim(),
                ItemCode = txtItemCode.Text.Trim(),
                CreateTimeStart = dateStart.Value,
                CreateTimeEnd = dateEnd.Value,
                ProductionCode = txtProduceNum.Text.Trim(),
                CreaterName = txtOperater.Text.Trim(),

            };
            if (cbxStatus.SelectedItem != null)
            {
                condition.Status = ((StatusModel)cbxStatus.SelectedItem).Code;
            }
            WriteLogHelper.WriteLogsAsync("开始", "获取数据");
            var data = await CommonRequestHelper.GetService<IQueryCodeScanHistoryService>().GetListPagedAsync<CodeScanHistoryView>(
         uiPagination1.ActivePage, uiPagination1.PageSize,
       condition, "", "createTime desc"
            );
            data.Data.ForEach(x =>
            {
                x.StatusName = ((ScanStatusEnum)x.Status).GetDescription();
            });
            listData = data.Data;
            WriteLogHelper.WriteLogsAsync("结束", "获取数据");
            this.uiDataGridView1.DataSource = listData;
            this.uiPagination1.TotalCount = data.Total;
        }
        private void UiPagination1_PageChanged(object sender, object pagingSource, int pageIndex, int count)
        {
            LoadDataGrid();
        }


        /// <summary>
        /// 初始化数据
        /// </summary>
        /// <returns></returns>
        private async Task InitData()
        {
            var list = await CommonRequestHelper.GetService<IQueryItemInfoService
                       >().GetListPagedAsync<ItemInfoEntity>
                  (1, 20, new BaseItemInfoCondition()
                  {
                  });
            currentItemList = list.Data;
            if (currentItemList.Any())
            {
                cbxItem.Items.Clear();
                cbxItem.Items.AddRange(currentItemList.ToArray());
            }

            cbxStatus.Items.Add(new StatusModel() { Code = 0, Name = "全部" });
            cbxStatus.Items.Add(new StatusModel() { Code = 1, Name = "PASS" });
            cbxStatus.Items.Add(new StatusModel() { Code = 2, Name = "重码" });
            cbxStatus.Items.Add(new StatusModel() { Code = 3, Name = "错码" });

        }

        private async Task GetCountAsync()
        {

            Task.Run(async () =>
            {
                if (currentItem != null && !string.IsNullOrEmpty(currentItem.Code))
                {
                    WriteLogHelper.WriteLogsAsync("开始", "统计数据");
                    var statisticsDatas = await CommonRequestHelper.GetService<IQueryCodeScanHistoryService
                           >().GetStatusCountGroupByItemCodeAsync<ItemStatusStatisticsModel>
                      (new BaseCodeScanHistoryCondition()
                      {
                          ItemCode = currentItem.Code,
                          //  ProductionCode = txtProcNumber.Text,
                          CreateTimeStart = Convert.ToDateTime(DateTime.Now.ToString("D").ToString()),
                          CreateTimeEnd = Convert.ToDateTime(DateTime.Now.AddDays(1).ToString("D").ToString()).AddSeconds(-1),
                          CreateId = CurrentUserConfig.User.Id
                      });
                    this.Invoke(new Action(() =>
                    {

                        lblTotal.Text = statisticsDatas.Sum(t => t.Count).ToString();
                        lblPass.Text = statisticsDatas.SingleOrDefault(x => x.Status == ScanStatusEnum.Pass.ToInt())?.Count.ToString() ?? "0";
                        lblRepeatCount.Text = statisticsDatas.SingleOrDefault(x => x.Status == ScanStatusEnum.Repeat.ToInt())?.Count.ToString() ?? "0";
                        lblErrorCount.Text = statisticsDatas.SingleOrDefault(x => x.Status == ScanStatusEnum.Error.ToInt())?.Count.ToString() ?? "0";
                    }));

                    var itemCode = currentItem.Code;
                    if (!string.IsNullOrEmpty(itemCode))
                    {
                        var itemCountInfos = await CommonRequestHelper.GetService<IQueryItemScanCountService>().GetListAsync<ItemScanCountEntity>(new BaseItemScanCountCondition() { UserId = CurrentUserConfig.User.Id, ItemCode = itemCode });

                        // var itemCountInfo = await CommonRequestHelper.GetService<IQueryItemScanCountService>().GetSingleByItemCodeAsync<ItemScanCountEntity>(itemCode);
                        if (!itemCountInfos.IsListNullOrEmpty())
                        {
                            var itemCountInfo = itemCountInfos.First();
                            this.Invoke(new Action(() =>
                            {
                                Lbl_ScanCount.Text = itemCountInfo.Count.ToString();
                            }));
                        }
                        else
                        {
                            this.Invoke(new Action(() =>
                            {
                                Lbl_ScanCount.Text = "0";
                            }));
                        }
                    }
                    else
                    {
                        this.Invoke(new Action(() =>
                        {
                            Lbl_ScanCount.Text = "0";
                        }));
                    }
                    WriteLogHelper.WriteLogsAsync("结束", "统计数据");
                }

            });
        }


        ItemInfoEntity currentItem = new ItemInfoEntity();
        private void CbxItem_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox comboBox = (ComboBox)sender;
            if (comboBox.SelectedItem != null)
            {
                currentItem = (ItemInfoEntity)comboBox.SelectedItem;
                GetCountAsync();
            }
            else
            {
                currentItem = null;
            }
        }
        List<ItemInfoEntity> currentItemList = new List<ItemInfoEntity>();
        private void CbxItem_Leave(object sender, EventArgs e)
        {
            ComboBox comboBox = (ComboBox)sender;
            var itemText = comboBox.Text.Trim();
            if (currentItemList.FirstOrDefault(x => x.Code == itemText) == null)
            {
                comboBox.Text = "";
                if (comboBox.Items.Count > 0)
                    comboBox.SelectedIndex = -1;
            }
        }
        private async void CbxItem_TextUpdate(object sender, EventArgs e)
        {
            ComboBox comboBox = (ComboBox)sender;
            var list = await CommonRequestHelper.GetService<IQueryItemInfoService
                   >().GetListPagedAsync<ItemInfoEntity>
              (1, 20, new BaseItemInfoCondition()
              {
                  KeyWord = comboBox.Text
              });
            currentItemList = list.Data;
            if (currentItemList.Any())
            {
                comboBox.Items.Clear();
                comboBox.Items.AddRange(currentItemList.ToArray());
            }
            comboBox.SelectionStart = comboBox.Text.Length;
            comboBox.DroppedDown = true;

            Cursor = Cursors.Default;
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            LoadDataGrid();
            GetCountAsync();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            txtItemCode.Text = "";
            txtProduceNum.Text = "";
            txtOperater.Text = "";
        }
        public async Task ExportPageAsync()
        {
            var condition = new BaseCodeScanHistoryCondition()
            {
                Code = this.txtBarCode.Text.Trim(),
                ItemCode = txtItemCode.Text.Trim(),
                CreateTimeStart = dateStart.Value,
                CreateTimeEnd = dateEnd.Value,
                ProductionCode = txtProduceNum.Text.Trim(),
                CreaterName = txtOperater.Text.Trim(),

            };
            if (cbxStatus.SelectedItem != null)
            {
                condition.Status = ((StatusModel)cbxStatus.SelectedItem).Code;
            }

            var data = await CommonRequestHelper.GetService<IQueryCodeScanHistoryService>().GetListPagedAsync<CodeScanHistoryView>(
       uiPagination1.ActivePage, uiPagination1.PageSize,
       condition);
            if (!data.Data.IsListNullOrEmpty())
            {
                data.Data.ForEach(x =>
                {
                    x.StatusName = ((ScanStatusEnum)x.Status).GetDescription();
                });
                ExportToExcelModel<CodeScanHistoryView> exportToExcelModel = new ExportToExcelModel<CodeScanHistoryView>();
                exportToExcelModel.Data = data.Data;
                exportToExcelModel.FileName = DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".xls";
                exportToExcelModel.Path = @$"{AppsettingsConfig.ExportExcelPath}";
                await ExportImportExcelHelper.ExportToExcelAsync(exportToExcelModel);
                UIMessageTip.ShowOk("导出成功");
            }
            else
            {
                UIMessageTip.ShowWarning("没有数据");
            }

        }
        public async Task ExportAllAsync()
        {
            var condition = new BaseCodeScanHistoryCondition()
            {
                Code = this.txtBarCode.Text.Trim(),
                ItemCode = txtItemCode.Text.Trim(),
                CreateTimeStart = dateStart.Value,
                CreateTimeEnd = dateEnd.Value,
                ProductionCode = txtProduceNum.Text.Trim(),
                CreaterName = txtOperater.Text.Trim(),

            };
            if (cbxStatus.SelectedItem != null)
            {
                condition.Status = ((StatusModel)cbxStatus.SelectedItem).Code;
            }
            var data = await CommonRequestHelper.GetService<IQueryCodeScanHistoryService>().GetListAsync<CodeScanHistoryView>(
            condition);
            if (!data.IsListNullOrEmpty())
            {
                data.ForEach(x =>
                {
                    x.StatusName = ((ScanStatusEnum)x.Status).GetDescription();
                });
                ExportToExcelModel<CodeScanHistoryView> exportToExcelModel = new ExportToExcelModel<CodeScanHistoryView>();
                exportToExcelModel.Data = data;
                exportToExcelModel.FileName = DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".xls";
                exportToExcelModel.Path = @$"{AppsettingsConfig.ExportExcelPath}";
                await ExportImportExcelHelper.ExportToExcelAsync(exportToExcelModel);
                UIMessageTip.ShowOk("导出成功");
            }
            else
            {
                UIMessageTip.ShowWarning("没有数据");
            }
        }
        private void btnExportPage_Click(object sender, EventArgs e)
        {
            ExportPageAsync();
        }

        private void btnExportAll_Click(object sender, EventArgs e)
        {
            ExportAllAsync();
        }

        private async void btnOk_Click(object sender, EventArgs e)
        {
            var barCode = this.txtCode.Text.Trim();
            this.txtCode.Text = "";
            if (this.cbxItem.SelectedItem == null)
            {
                UIMessageBox.ShowError("请选择产品！");
                return;
            }
            currentItem = (ItemInfoEntity)cbxItem.SelectedItem;

            if (string.IsNullOrEmpty(barCode))
            {
                UIMessageBox.ShowError("请输入条码！");
                return;
            }
            if (string.IsNullOrEmpty(this.txtProcNumber.Text))
            {
                UIMessageBox.ShowError("请输入生产制令号！");
                return;
            }
            var scanEntity = new CodeScanHistoryEntity();
            scanEntity.ItemCode = currentItem.Code;
            scanEntity.ProductionCode = txtProcNumber.Text.Trim();
            scanEntity.Code = barCode;
            this.lblLastCode.Text = scanEntity.Code;
            System.Diagnostics.Stopwatch stopwatch1 = new System.Diagnostics.Stopwatch();
            stopwatch1.Start(); //  开始监视代码
            var codeList = await CommonRequestHelper.GetService<IQueryCodePrintHistoryService
                       >().GetListPagedAsync<CodePrintHistoryEntity>
                  (1, 1, new BaseCodePrintHistoryCondition()
                  {
                      Code = barCode,
                      ItemCode = currentItem.Code,
                      ProductionCode = txtProcNumber.Text.Trim()
                  });
            stopwatch1.Stop(); //  停止监视
            TimeSpan timeSpan1 = stopwatch1.Elapsed; //  获取总时间
            WriteLogHelper.WriteLogsAsync("打印查询:" + timeSpan1.TotalMilliseconds.ToString(), "SQL");
            stopwatch1.Start(); //  开始监视代码
            var scanCodeList = await CommonRequestHelper.GetService<IQueryCodeScanHistoryService
                       >().GetListPagedAsync<CodeScanHistoryEntity>
                  (1, 1, new BaseCodeScanHistoryCondition()
                  {
                      Code = barCode,
                      Status = ScanStatusEnum.Pass.ToInt()
                  });
            stopwatch1.Stop(); //  停止监视
            TimeSpan timeSpan2 = stopwatch1.Elapsed; //  获取总时间
            WriteLogHelper.WriteLogsAsync("扫码查询:" + timeSpan2.TotalMilliseconds.ToString(), "SQL");
            if (!codeList.Data.Exists(x => x.Code == scanEntity.Code))
            {
                scanEntity.Status = ScanStatusEnum.Error.ToInt();
                scanEntity.ErrorContent = "该生产制令号中不存在该条码！";
            }
            else if (scanEntity.Code.IndexOf(scanEntity.ItemCode.Replace("-", "")) < 0)
            {
                scanEntity.Status = ScanStatusEnum.Error.ToInt();
                scanEntity.ErrorContent = "该二维码内容中产品编码与当前编码不符合！";
            }
            else if (!scanCodeList.Data.Exists(x => x.Code == scanEntity.Code))
            {
                scanEntity.Status = ScanStatusEnum.Pass.ToInt();
                scanEntity.ErrorContent = "";
            }
            else
            {
                scanEntity.Status = ScanStatusEnum.Repeat.ToInt();
                scanEntity.ErrorContent = "该生产制令下条码已经扫描过！";

            }
            var result = await CommonRequestHelper.GetService<ICommandCodeScanHistoryService>().InsertAsync
                  (scanEntity);
            if (result.IsSuccess)
            {
                this.txtCode.Text = "";
                if (scanEntity.Status == ScanStatusEnum.Pass.ToInt())
                {
                    System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
                    stopwatch.Start(); //  开始监视代码
                    UIMessageTip.ShowOk("扫码通过！", 3000);
                    Mp3Player.Play("music/ok.mp3", false);
                    stopwatch.Stop(); //  停止监视
                    TimeSpan timeSpan = stopwatch.Elapsed; //  获取总时间
                    UIMessageTip.ShowOk(timeSpan.TotalMilliseconds.ToString(), 3000);
                }
                else
                {

                    // UIMessageBox.ShowError(scanEntity.ErrorContent);
                    Mp3Player.Play("music/ng.mp3", false);
                    WarningConfirmForm warningConfirmForm = new WarningConfirmForm(scanEntity.ErrorContent);
                    warningConfirmForm.ShowDialog();
                }
            }
            else
            {
                Mp3Player.Play("music/ng.mp3", false);
                UIMessageBox.ShowError("扫码入库失败" + result.ErrorMessage);

            }
            GetCountAsync();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            this.cbxItem.SelectedItem = null;
            this.txtProcNumber.Text = "";
            this.txtCode.Text = "";
        }

        private void txtCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnOk_Click(null, null);
            }
        }
        DateTime lastInputTime = DateTime.Now;
        private void txtCode_TextChanged(object sender, EventArgs e)
        {
            lastInputTime = DateTime.Now;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            //if (checkboxAutoEnter.Checked)
            //{
            //    if (!string.IsNullOrEmpty(txtCode.Text.Trim()) && (DateTime.Now - lastInputTime).TotalMilliseconds > 1000)
            //    {
            //        btnOk_Click(null, null);
            //    }
            //}
        }

        private async void BTN_CountClear_Click(object sender, EventArgs e)
        {
            var itemCode = currentItem.Code;
            if (!string.IsNullOrEmpty(itemCode))
            {
                var itemCountInfos = await CommonRequestHelper.GetService<IQueryItemScanCountService>().GetListAsync<ItemScanCountEntity>(new BaseItemScanCountCondition() { UserId = CurrentUserConfig.User.Id, ItemCode = itemCode });
                if (!itemCountInfos.IsListNullOrEmpty())
                {
                    var itemCountInfo = itemCountInfos.First();
                    itemCountInfo.Count = 0;
                    var updateRes = await CommonRequestHelper.GetService<ICommandItemScanCountService>().UpdateAsync(itemCountInfo);
                    if (updateRes.IsSuccess)
                    {
                        this.Invoke(new Action(() =>
                        {
                            Lbl_ScanCount.Text = "0";
                        }));
                        UIMessageTip.ShowOk("清零成功！", 3000);
                    }
                }
                else
                {
                    UIMessageTip.ShowWarning("没有计数数据！", 3000);
                }
            }
            else
            {
                UIMessageTip.ShowWarning("请选择物料！", 3000);
            }
        }
    }

    public class StatusModel
    {
        public int Code { set; get; }

        public string Name { set; get; }
    }
}
