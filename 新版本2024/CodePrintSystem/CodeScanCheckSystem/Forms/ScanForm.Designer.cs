﻿
namespace CodeScanCheckSystem.Forms
{
    partial class ScanForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            panel1 = new System.Windows.Forms.Panel();
            BTN_CountClear = new Sunny.UI.UIButton();
            checkboxAutoEnter = new Sunny.UI.UICheckBox();
            lblLastCode = new Sunny.UI.UILabel();
            uiLabel15 = new Sunny.UI.UILabel();
            btnClear = new Sunny.UI.UIButton();
            btnOk = new Sunny.UI.UIButton();
            txtCode = new Sunny.UI.UITextBox();
            uiLabel10 = new Sunny.UI.UILabel();
            txtProcNumber = new Sunny.UI.UITextBox();
            uiLabel9 = new Sunny.UI.UILabel();
            cbxItem = new System.Windows.Forms.ComboBox();
            uiLabel8 = new Sunny.UI.UILabel();
            uiGroupBox2 = new Sunny.UI.UIGroupBox();
            Lbl_ScanCount = new Sunny.UI.UILabel();
            uiLabel16 = new Sunny.UI.UILabel();
            lblErrorCount = new Sunny.UI.UILabel();
            lblRepeatCount = new Sunny.UI.UILabel();
            uiLabel14 = new Sunny.UI.UILabel();
            uiLabel13 = new Sunny.UI.UILabel();
            lblPass = new Sunny.UI.UILabel();
            uiLabel12 = new Sunny.UI.UILabel();
            lblTotal = new Sunny.UI.UILabel();
            uiLabel11 = new Sunny.UI.UILabel();
            uiGroupBox1 = new Sunny.UI.UIGroupBox();
            uiDataGridView1 = new Sunny.UI.UIDataGridView();
            uiPagination1 = new Sunny.UI.UIPagination();
            panel2 = new System.Windows.Forms.Panel();
            cbxStatus = new Sunny.UI.UIComboBox();
            uiLabel7 = new Sunny.UI.UILabel();
            btnExportAll = new Sunny.UI.UIButton();
            txtOperater = new Sunny.UI.UITextBox();
            btnExportPage = new Sunny.UI.UIButton();
            uiLabel5 = new Sunny.UI.UILabel();
            btnReset = new Sunny.UI.UIButton();
            txtBarCode = new Sunny.UI.UITextBox();
            uiLabel6 = new Sunny.UI.UILabel();
            btnSearch = new Sunny.UI.UIButton();
            txtProduceNum = new Sunny.UI.UITextBox();
            uiLabel4 = new Sunny.UI.UILabel();
            uiLabel3 = new Sunny.UI.UILabel();
            dateEnd = new Sunny.UI.UIDatetimePicker();
            uiLabel2 = new Sunny.UI.UILabel();
            dateStart = new Sunny.UI.UIDatetimePicker();
            txtItemCode = new Sunny.UI.UITextBox();
            uiLabel1 = new Sunny.UI.UILabel();
            timer1 = new System.Windows.Forms.Timer(components);
            PagePanel.SuspendLayout();
            panel1.SuspendLayout();
            uiGroupBox2.SuspendLayout();
            uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)uiDataGridView1).BeginInit();
            panel2.SuspendLayout();
            SuspendLayout();
            // 
            // PagePanel
            // 
            PagePanel.Controls.Add(uiGroupBox1);
            PagePanel.Controls.Add(panel1);
            PagePanel.Size = new System.Drawing.Size(1516, 789);
            // 
            // panel1
            // 
            panel1.Controls.Add(BTN_CountClear);
            panel1.Controls.Add(checkboxAutoEnter);
            panel1.Controls.Add(lblLastCode);
            panel1.Controls.Add(uiLabel15);
            panel1.Controls.Add(btnClear);
            panel1.Controls.Add(btnOk);
            panel1.Controls.Add(txtCode);
            panel1.Controls.Add(uiLabel10);
            panel1.Controls.Add(txtProcNumber);
            panel1.Controls.Add(uiLabel9);
            panel1.Controls.Add(cbxItem);
            panel1.Controls.Add(uiLabel8);
            panel1.Controls.Add(uiGroupBox2);
            panel1.Dock = System.Windows.Forms.DockStyle.Top;
            panel1.Location = new System.Drawing.Point(0, 0);
            panel1.Name = "panel1";
            panel1.Size = new System.Drawing.Size(1516, 165);
            panel1.TabIndex = 0;
            // 
            // BTN_CountClear
            // 
            BTN_CountClear.Cursor = System.Windows.Forms.Cursors.Hand;
            BTN_CountClear.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            BTN_CountClear.Location = new System.Drawing.Point(484, 122);
            BTN_CountClear.MinimumSize = new System.Drawing.Size(1, 1);
            BTN_CountClear.Name = "BTN_CountClear";
            BTN_CountClear.Size = new System.Drawing.Size(100, 35);
            BTN_CountClear.TabIndex = 18;
            BTN_CountClear.Text = "计数清零";
            BTN_CountClear.Click += BTN_CountClear_Click;
            // 
            // checkboxAutoEnter
            // 
            checkboxAutoEnter.Checked = true;
            checkboxAutoEnter.Cursor = System.Windows.Forms.Cursors.Hand;
            checkboxAutoEnter.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            checkboxAutoEnter.Location = new System.Drawing.Point(26, 135);
            checkboxAutoEnter.MinimumSize = new System.Drawing.Size(1, 1);
            checkboxAutoEnter.Name = "checkboxAutoEnter";
            checkboxAutoEnter.Padding = new System.Windows.Forms.Padding(22, 0, 0, 0);
            checkboxAutoEnter.Size = new System.Drawing.Size(150, 29);
            checkboxAutoEnter.TabIndex = 16;
            checkboxAutoEnter.Text = "自动回车";
            // 
            // lblLastCode
            // 
            lblLastCode.AutoSize = true;
            lblLastCode.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            lblLastCode.Location = new System.Drawing.Point(109, 114);
            lblLastCode.Name = "lblLastCode";
            lblLastCode.Size = new System.Drawing.Size(0, 21);
            lblLastCode.TabIndex = 15;
            lblLastCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel15
            // 
            uiLabel15.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            uiLabel15.Location = new System.Drawing.Point(26, 109);
            uiLabel15.Name = "uiLabel15";
            uiLabel15.Size = new System.Drawing.Size(77, 23);
            uiLabel15.TabIndex = 14;
            uiLabel15.Text = "条码";
            uiLabel15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnClear
            // 
            btnClear.Cursor = System.Windows.Forms.Cursors.Hand;
            btnClear.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            btnClear.Location = new System.Drawing.Point(484, 83);
            btnClear.MinimumSize = new System.Drawing.Size(1, 1);
            btnClear.Name = "btnClear";
            btnClear.Size = new System.Drawing.Size(100, 35);
            btnClear.TabIndex = 13;
            btnClear.Text = "清空";
            btnClear.Click += btnClear_Click;
            // 
            // btnOk
            // 
            btnOk.Cursor = System.Windows.Forms.Cursors.Hand;
            btnOk.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            btnOk.Location = new System.Drawing.Point(484, 22);
            btnOk.MinimumSize = new System.Drawing.Size(1, 1);
            btnOk.Name = "btnOk";
            btnOk.Size = new System.Drawing.Size(100, 54);
            btnOk.TabIndex = 12;
            btnOk.Text = "扫描";
            btnOk.Click += btnOk_Click;
            // 
            // txtCode
            // 
            txtCode.Cursor = System.Windows.Forms.Cursors.IBeam;
            txtCode.FillColor = System.Drawing.Color.White;
            txtCode.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            txtCode.Location = new System.Drawing.Point(109, 75);
            txtCode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            txtCode.Maximum = 2147483647D;
            txtCode.Minimum = -2147483648D;
            txtCode.MinimumSize = new System.Drawing.Size(1, 1);
            txtCode.Name = "txtCode";
            txtCode.Padding = new System.Windows.Forms.Padding(5);
            txtCode.Size = new System.Drawing.Size(362, 29);
            txtCode.TabIndex = 11;
            txtCode.TextChanged += txtCode_TextChanged;
            txtCode.KeyDown += txtCode_KeyDown;
            // 
            // uiLabel10
            // 
            uiLabel10.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            uiLabel10.Location = new System.Drawing.Point(26, 75);
            uiLabel10.Name = "uiLabel10";
            uiLabel10.Size = new System.Drawing.Size(81, 23);
            uiLabel10.TabIndex = 10;
            uiLabel10.Text = "条码扫描";
            uiLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtProcNumber
            // 
            txtProcNumber.Cursor = System.Windows.Forms.Cursors.IBeam;
            txtProcNumber.FillColor = System.Drawing.Color.White;
            txtProcNumber.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            txtProcNumber.Location = new System.Drawing.Point(380, 22);
            txtProcNumber.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            txtProcNumber.Maximum = 2147483647D;
            txtProcNumber.Minimum = -2147483648D;
            txtProcNumber.MinimumSize = new System.Drawing.Size(1, 1);
            txtProcNumber.Name = "txtProcNumber";
            txtProcNumber.Padding = new System.Windows.Forms.Padding(5);
            txtProcNumber.Size = new System.Drawing.Size(91, 29);
            txtProcNumber.TabIndex = 9;
            // 
            // uiLabel9
            // 
            uiLabel9.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            uiLabel9.Location = new System.Drawing.Point(278, 22);
            uiLabel9.Name = "uiLabel9";
            uiLabel9.Size = new System.Drawing.Size(100, 23);
            uiLabel9.TabIndex = 8;
            uiLabel9.Text = "生产制令号";
            uiLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbxItem
            // 
            cbxItem.FormattingEnabled = true;
            cbxItem.Location = new System.Drawing.Point(109, 22);
            cbxItem.Name = "cbxItem";
            cbxItem.Size = new System.Drawing.Size(142, 29);
            cbxItem.TabIndex = 2;
            // 
            // uiLabel8
            // 
            uiLabel8.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            uiLabel8.Location = new System.Drawing.Point(26, 22);
            uiLabel8.Name = "uiLabel8";
            uiLabel8.Size = new System.Drawing.Size(81, 23);
            uiLabel8.TabIndex = 1;
            uiLabel8.Text = "产品编码";
            uiLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiGroupBox2
            // 
            uiGroupBox2.Controls.Add(Lbl_ScanCount);
            uiGroupBox2.Controls.Add(uiLabel16);
            uiGroupBox2.Controls.Add(lblErrorCount);
            uiGroupBox2.Controls.Add(lblRepeatCount);
            uiGroupBox2.Controls.Add(uiLabel14);
            uiGroupBox2.Controls.Add(uiLabel13);
            uiGroupBox2.Controls.Add(lblPass);
            uiGroupBox2.Controls.Add(uiLabel12);
            uiGroupBox2.Controls.Add(lblTotal);
            uiGroupBox2.Controls.Add(uiLabel11);
            uiGroupBox2.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            uiGroupBox2.Location = new System.Drawing.Point(612, 5);
            uiGroupBox2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            uiGroupBox2.MinimumSize = new System.Drawing.Size(1, 1);
            uiGroupBox2.Name = "uiGroupBox2";
            uiGroupBox2.Padding = new System.Windows.Forms.Padding(0, 32, 0, 0);
            uiGroupBox2.Size = new System.Drawing.Size(452, 139);
            uiGroupBox2.TabIndex = 0;
            uiGroupBox2.Text = "今日扫描统计";
            // 
            // Lbl_ScanCount
            // 
            Lbl_ScanCount.AutoSize = true;
            Lbl_ScanCount.Font = new System.Drawing.Font("微软雅黑", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            Lbl_ScanCount.Location = new System.Drawing.Point(358, 78);
            Lbl_ScanCount.Name = "Lbl_ScanCount";
            Lbl_ScanCount.Size = new System.Drawing.Size(21, 24);
            Lbl_ScanCount.TabIndex = 11;
            Lbl_ScanCount.Text = "0";
            Lbl_ScanCount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel16
            // 
            uiLabel16.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            uiLabel16.Location = new System.Drawing.Point(336, 32);
            uiLabel16.Name = "uiLabel16";
            uiLabel16.Size = new System.Drawing.Size(87, 23);
            uiLabel16.TabIndex = 10;
            uiLabel16.Text = "当前计数";
            uiLabel16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblErrorCount
            // 
            lblErrorCount.AutoSize = true;
            lblErrorCount.Font = new System.Drawing.Font("微软雅黑", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            lblErrorCount.Location = new System.Drawing.Point(278, 78);
            lblErrorCount.Name = "lblErrorCount";
            lblErrorCount.Size = new System.Drawing.Size(21, 24);
            lblErrorCount.TabIndex = 7;
            lblErrorCount.Text = "0";
            lblErrorCount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblRepeatCount
            // 
            lblRepeatCount.AutoSize = true;
            lblRepeatCount.Font = new System.Drawing.Font("微软雅黑", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            lblRepeatCount.Location = new System.Drawing.Point(202, 78);
            lblRepeatCount.Name = "lblRepeatCount";
            lblRepeatCount.Size = new System.Drawing.Size(21, 24);
            lblRepeatCount.TabIndex = 6;
            lblRepeatCount.Text = "0";
            lblRepeatCount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel14
            // 
            uiLabel14.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            uiLabel14.Location = new System.Drawing.Point(278, 32);
            uiLabel14.Name = "uiLabel14";
            uiLabel14.Size = new System.Drawing.Size(52, 23);
            uiLabel14.TabIndex = 5;
            uiLabel14.Text = "错码";
            uiLabel14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel13
            // 
            uiLabel13.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            uiLabel13.Location = new System.Drawing.Point(202, 32);
            uiLabel13.Name = "uiLabel13";
            uiLabel13.Size = new System.Drawing.Size(53, 23);
            uiLabel13.TabIndex = 4;
            uiLabel13.Text = "重码";
            uiLabel13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblPass
            // 
            lblPass.AutoSize = true;
            lblPass.Font = new System.Drawing.Font("微软雅黑", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            lblPass.Location = new System.Drawing.Point(119, 78);
            lblPass.Name = "lblPass";
            lblPass.Size = new System.Drawing.Size(21, 24);
            lblPass.TabIndex = 3;
            lblPass.Text = "0";
            lblPass.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel12
            // 
            uiLabel12.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            uiLabel12.Location = new System.Drawing.Point(119, 32);
            uiLabel12.Name = "uiLabel12";
            uiLabel12.Size = new System.Drawing.Size(60, 23);
            uiLabel12.TabIndex = 2;
            uiLabel12.Text = "PASS";
            uiLabel12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTotal
            // 
            lblTotal.AutoSize = true;
            lblTotal.Font = new System.Drawing.Font("微软雅黑", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            lblTotal.Location = new System.Drawing.Point(24, 78);
            lblTotal.Name = "lblTotal";
            lblTotal.Size = new System.Drawing.Size(21, 24);
            lblTotal.TabIndex = 1;
            lblTotal.Text = "0";
            lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel11
            // 
            uiLabel11.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            uiLabel11.Location = new System.Drawing.Point(13, 32);
            uiLabel11.Name = "uiLabel11";
            uiLabel11.Size = new System.Drawing.Size(87, 23);
            uiLabel11.TabIndex = 0;
            uiLabel11.Text = "扫码数量";
            uiLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiGroupBox1
            // 
            uiGroupBox1.Controls.Add(uiDataGridView1);
            uiGroupBox1.Controls.Add(uiPagination1);
            uiGroupBox1.Controls.Add(panel2);
            uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            uiGroupBox1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            uiGroupBox1.Location = new System.Drawing.Point(0, 165);
            uiGroupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            uiGroupBox1.MinimumSize = new System.Drawing.Size(1, 1);
            uiGroupBox1.Name = "uiGroupBox1";
            uiGroupBox1.Padding = new System.Windows.Forms.Padding(0, 32, 0, 0);
            uiGroupBox1.Size = new System.Drawing.Size(1516, 624);
            uiGroupBox1.TabIndex = 1;
            uiGroupBox1.Text = "扫描历史";
            // 
            // uiDataGridView1
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(235, 243, 255);
            uiDataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            uiDataGridView1.BackgroundColor = System.Drawing.Color.White;
            uiDataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(80, 160, 255);
            dataGridViewCellStyle2.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(80, 160, 255);
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            uiDataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            uiDataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(48, 48, 48);
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(155, 200, 255);
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(48, 48, 48);
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            uiDataGridView1.DefaultCellStyle = dataGridViewCellStyle3;
            uiDataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            uiDataGridView1.EnableHeadersVisualStyles = false;
            uiDataGridView1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            uiDataGridView1.GridColor = System.Drawing.Color.FromArgb(80, 160, 255);
            uiDataGridView1.Location = new System.Drawing.Point(0, 127);
            uiDataGridView1.Name = "uiDataGridView1";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(235, 243, 255);
            dataGridViewCellStyle4.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(80, 160, 255);
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            uiDataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            uiDataGridView1.RowHeadersWidth = 62;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            uiDataGridView1.RowsDefaultCellStyle = dataGridViewCellStyle5;
            uiDataGridView1.RowTemplate.Height = 29;
            uiDataGridView1.SelectedIndex = -1;
            uiDataGridView1.ShowGridLine = true;
            uiDataGridView1.Size = new System.Drawing.Size(1516, 462);
            uiDataGridView1.TabIndex = 2;
            // 
            // uiPagination1
            // 
            uiPagination1.BackColor = System.Drawing.SystemColors.Control;
            uiPagination1.Dock = System.Windows.Forms.DockStyle.Bottom;
            uiPagination1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            uiPagination1.Location = new System.Drawing.Point(0, 589);
            uiPagination1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            uiPagination1.MinimumSize = new System.Drawing.Size(1, 1);
            uiPagination1.Name = "uiPagination1";
            uiPagination1.RectSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.None;
            uiPagination1.Size = new System.Drawing.Size(1516, 35);
            uiPagination1.TabIndex = 1;
            uiPagination1.Text = "uiPagination1";
            // 
            // panel2
            // 
            panel2.Controls.Add(cbxStatus);
            panel2.Controls.Add(uiLabel7);
            panel2.Controls.Add(btnExportAll);
            panel2.Controls.Add(txtOperater);
            panel2.Controls.Add(btnExportPage);
            panel2.Controls.Add(uiLabel5);
            panel2.Controls.Add(btnReset);
            panel2.Controls.Add(txtBarCode);
            panel2.Controls.Add(uiLabel6);
            panel2.Controls.Add(btnSearch);
            panel2.Controls.Add(txtProduceNum);
            panel2.Controls.Add(uiLabel4);
            panel2.Controls.Add(uiLabel3);
            panel2.Controls.Add(dateEnd);
            panel2.Controls.Add(uiLabel2);
            panel2.Controls.Add(dateStart);
            panel2.Controls.Add(txtItemCode);
            panel2.Controls.Add(uiLabel1);
            panel2.Dock = System.Windows.Forms.DockStyle.Top;
            panel2.Location = new System.Drawing.Point(0, 32);
            panel2.Name = "panel2";
            panel2.Size = new System.Drawing.Size(1516, 95);
            panel2.TabIndex = 0;
            // 
            // cbxStatus
            // 
            cbxStatus.DropDownStyle = Sunny.UI.UIDropDownStyle.DropDownList;
            cbxStatus.FillColor = System.Drawing.Color.White;
            cbxStatus.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            cbxStatus.Location = new System.Drawing.Point(406, 50);
            cbxStatus.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            cbxStatus.MinimumSize = new System.Drawing.Size(63, 0);
            cbxStatus.Name = "cbxStatus";
            cbxStatus.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            cbxStatus.Size = new System.Drawing.Size(123, 29);
            cbxStatus.TabIndex = 17;
            cbxStatus.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel7
            // 
            uiLabel7.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            uiLabel7.Location = new System.Drawing.Point(359, 50);
            uiLabel7.Name = "uiLabel7";
            uiLabel7.Size = new System.Drawing.Size(53, 23);
            uiLabel7.TabIndex = 16;
            uiLabel7.Text = "状态";
            uiLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnExportAll
            // 
            btnExportAll.Cursor = System.Windows.Forms.Cursors.Hand;
            btnExportAll.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            btnExportAll.Location = new System.Drawing.Point(873, 50);
            btnExportAll.MinimumSize = new System.Drawing.Size(1, 1);
            btnExportAll.Name = "btnExportAll";
            btnExportAll.Size = new System.Drawing.Size(69, 35);
            btnExportAll.TabIndex = 15;
            btnExportAll.Text = "导出所有";
            btnExportAll.Click += btnExportAll_Click;
            // 
            // txtOperater
            // 
            txtOperater.Cursor = System.Windows.Forms.Cursors.IBeam;
            txtOperater.FillColor = System.Drawing.Color.White;
            txtOperater.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            txtOperater.Location = new System.Drawing.Point(278, 50);
            txtOperater.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            txtOperater.Maximum = 2147483647D;
            txtOperater.Minimum = -2147483648D;
            txtOperater.MinimumSize = new System.Drawing.Size(1, 1);
            txtOperater.Name = "txtOperater";
            txtOperater.Padding = new System.Windows.Forms.Padding(5);
            txtOperater.Size = new System.Drawing.Size(74, 29);
            txtOperater.TabIndex = 9;
            // 
            // btnExportPage
            // 
            btnExportPage.Cursor = System.Windows.Forms.Cursors.Hand;
            btnExportPage.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            btnExportPage.Location = new System.Drawing.Point(873, 11);
            btnExportPage.MinimumSize = new System.Drawing.Size(1, 1);
            btnExportPage.Name = "btnExportPage";
            btnExportPage.Size = new System.Drawing.Size(69, 35);
            btnExportPage.TabIndex = 14;
            btnExportPage.Text = "导出本页";
            btnExportPage.Click += btnExportPage_Click;
            // 
            // uiLabel5
            // 
            uiLabel5.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            uiLabel5.Location = new System.Drawing.Point(211, 50);
            uiLabel5.Name = "uiLabel5";
            uiLabel5.Size = new System.Drawing.Size(60, 23);
            uiLabel5.TabIndex = 8;
            uiLabel5.Text = "操作员";
            uiLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnReset
            // 
            btnReset.Cursor = System.Windows.Forms.Cursors.Hand;
            btnReset.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            btnReset.Location = new System.Drawing.Point(798, 50);
            btnReset.MinimumSize = new System.Drawing.Size(1, 1);
            btnReset.Name = "btnReset";
            btnReset.Size = new System.Drawing.Size(69, 35);
            btnReset.TabIndex = 13;
            btnReset.Text = "重置";
            btnReset.Click += btnReset_Click;
            // 
            // txtBarCode
            // 
            txtBarCode.Cursor = System.Windows.Forms.Cursors.IBeam;
            txtBarCode.FillColor = System.Drawing.Color.White;
            txtBarCode.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            txtBarCode.Location = new System.Drawing.Point(591, 50);
            txtBarCode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            txtBarCode.Maximum = 2147483647D;
            txtBarCode.Minimum = -2147483648D;
            txtBarCode.MinimumSize = new System.Drawing.Size(1, 1);
            txtBarCode.Name = "txtBarCode";
            txtBarCode.Padding = new System.Windows.Forms.Padding(5);
            txtBarCode.Size = new System.Drawing.Size(200, 29);
            txtBarCode.TabIndex = 12;
            // 
            // uiLabel6
            // 
            uiLabel6.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            uiLabel6.Location = new System.Drawing.Point(536, 50);
            uiLabel6.Name = "uiLabel6";
            uiLabel6.Size = new System.Drawing.Size(48, 23);
            uiLabel6.TabIndex = 11;
            uiLabel6.Text = "条码";
            uiLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnSearch
            // 
            btnSearch.Cursor = System.Windows.Forms.Cursors.Hand;
            btnSearch.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            btnSearch.Location = new System.Drawing.Point(798, 11);
            btnSearch.MinimumSize = new System.Drawing.Size(1, 1);
            btnSearch.Name = "btnSearch";
            btnSearch.Size = new System.Drawing.Size(69, 35);
            btnSearch.TabIndex = 10;
            btnSearch.Text = "查询";
            btnSearch.Click += btnSearch_Click;
            // 
            // txtProduceNum
            // 
            txtProduceNum.Cursor = System.Windows.Forms.Cursors.IBeam;
            txtProduceNum.FillColor = System.Drawing.Color.White;
            txtProduceNum.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            txtProduceNum.Location = new System.Drawing.Point(109, 50);
            txtProduceNum.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            txtProduceNum.Maximum = 2147483647D;
            txtProduceNum.Minimum = -2147483648D;
            txtProduceNum.MinimumSize = new System.Drawing.Size(1, 1);
            txtProduceNum.Name = "txtProduceNum";
            txtProduceNum.Padding = new System.Windows.Forms.Padding(5);
            txtProduceNum.Size = new System.Drawing.Size(91, 29);
            txtProduceNum.TabIndex = 7;
            // 
            // uiLabel4
            // 
            uiLabel4.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            uiLabel4.Location = new System.Drawing.Point(7, 50);
            uiLabel4.Name = "uiLabel4";
            uiLabel4.Size = new System.Drawing.Size(100, 23);
            uiLabel4.TabIndex = 6;
            uiLabel4.Text = "生产制令号";
            uiLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel3
            // 
            uiLabel3.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            uiLabel3.Location = new System.Drawing.Point(565, 17);
            uiLabel3.Name = "uiLabel3";
            uiLabel3.Size = new System.Drawing.Size(19, 23);
            uiLabel3.TabIndex = 5;
            uiLabel3.Text = "-";
            uiLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dateEnd
            // 
            dateEnd.FillColor = System.Drawing.Color.White;
            dateEnd.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            dateEnd.Location = new System.Drawing.Point(591, 11);
            dateEnd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            dateEnd.MaxLength = 19;
            dateEnd.MinimumSize = new System.Drawing.Size(63, 0);
            dateEnd.Name = "dateEnd";
            dateEnd.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            dateEnd.Size = new System.Drawing.Size(200, 29);
            dateEnd.SymbolDropDown = 61555;
            dateEnd.SymbolNormal = 61555;
            dateEnd.TabIndex = 4;
            dateEnd.Text = "2021-02-03 09:27:59";
            dateEnd.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            dateEnd.Value = new System.DateTime(2021, 2, 3, 9, 27, 59, 517);
            // 
            // uiLabel2
            // 
            uiLabel2.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            uiLabel2.Location = new System.Drawing.Point(265, 11);
            uiLabel2.Name = "uiLabel2";
            uiLabel2.Size = new System.Drawing.Size(74, 23);
            uiLabel2.TabIndex = 3;
            uiLabel2.Text = "扫描时间";
            uiLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dateStart
            // 
            dateStart.FillColor = System.Drawing.Color.White;
            dateStart.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            dateStart.Location = new System.Drawing.Point(346, 11);
            dateStart.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            dateStart.MaxLength = 19;
            dateStart.MinimumSize = new System.Drawing.Size(63, 0);
            dateStart.Name = "dateStart";
            dateStart.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            dateStart.Size = new System.Drawing.Size(200, 29);
            dateStart.SymbolDropDown = 61555;
            dateStart.SymbolNormal = 61555;
            dateStart.TabIndex = 2;
            dateStart.Text = "2021-02-03 09:27:59";
            dateStart.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            dateStart.Value = new System.DateTime(2021, 2, 3, 9, 27, 59, 517);
            // 
            // txtItemCode
            // 
            txtItemCode.Cursor = System.Windows.Forms.Cursors.IBeam;
            txtItemCode.FillColor = System.Drawing.Color.White;
            txtItemCode.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            txtItemCode.Location = new System.Drawing.Point(109, 11);
            txtItemCode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            txtItemCode.Maximum = 2147483647D;
            txtItemCode.Minimum = -2147483648D;
            txtItemCode.MinimumSize = new System.Drawing.Size(1, 1);
            txtItemCode.Name = "txtItemCode";
            txtItemCode.Padding = new System.Windows.Forms.Padding(5);
            txtItemCode.Size = new System.Drawing.Size(142, 29);
            txtItemCode.TabIndex = 1;
            // 
            // uiLabel1
            // 
            uiLabel1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            uiLabel1.Location = new System.Drawing.Point(26, 11);
            uiLabel1.Name = "uiLabel1";
            uiLabel1.Size = new System.Drawing.Size(100, 23);
            uiLabel1.TabIndex = 0;
            uiLabel1.Text = "产品编码";
            uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // timer1
            // 
            timer1.Enabled = true;
            timer1.Interval = 500;
            timer1.Tick += timer1_Tick;
            // 
            // ScanForm
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(10F, 21F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(1516, 824);
            Name = "ScanForm";
            Text = "ScanForm";
            PagePanel.ResumeLayout(false);
            panel1.ResumeLayout(false);
            panel1.PerformLayout();
            uiGroupBox2.ResumeLayout(false);
            uiGroupBox2.PerformLayout();
            uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)uiDataGridView1).EndInit();
            panel2.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private Sunny.UI.UIGroupBox uiGroupBox1;
        private System.Windows.Forms.Panel panel1;
        private Sunny.UI.UIDataGridView uiDataGridView1;
        private Sunny.UI.UIPagination uiPagination1;
        private System.Windows.Forms.Panel panel2;
        private Sunny.UI.UILabel uiLabel1;
        private Sunny.UI.UITextBox txtItemCode;
        private Sunny.UI.UITextBox txtProduceNum;
        private Sunny.UI.UILabel uiLabel4;
        private Sunny.UI.UILabel uiLabel3;
        private Sunny.UI.UIDatetimePicker dateEnd;
        private Sunny.UI.UILabel uiLabel2;
        private Sunny.UI.UIDatetimePicker dateStart;
        private Sunny.UI.UITextBox txtOperater;
        private Sunny.UI.UILabel uiLabel5;
        private Sunny.UI.UIButton btnReset;
        private Sunny.UI.UITextBox txtBarCode;
        private Sunny.UI.UILabel uiLabel6;
        private Sunny.UI.UIButton btnSearch;
        private Sunny.UI.UIButton btnExportAll;
        private Sunny.UI.UIButton btnExportPage;
        private Sunny.UI.UIComboBox cbxStatus;
        private Sunny.UI.UILabel uiLabel7;
        private Sunny.UI.UIGroupBox uiGroupBox2;
        private System.Windows.Forms.ComboBox cbxItem;
        private Sunny.UI.UILabel uiLabel8;
        private Sunny.UI.UITextBox txtCode;
        private Sunny.UI.UILabel uiLabel10;
        private Sunny.UI.UITextBox txtProcNumber;
        private Sunny.UI.UILabel uiLabel9;
        private Sunny.UI.UIButton btnOk;
        private Sunny.UI.UIButton btnClear;
        private Sunny.UI.UILabel lblTotal;
        private Sunny.UI.UILabel uiLabel11;
        private Sunny.UI.UILabel lblErrorCount;
        private Sunny.UI.UILabel lblRepeatCount;
        private Sunny.UI.UILabel uiLabel14;
        private Sunny.UI.UILabel uiLabel13;
        private Sunny.UI.UILabel lblPass;
        private Sunny.UI.UILabel uiLabel12;
        private Sunny.UI.UILabel lblLastCode;
        private Sunny.UI.UILabel uiLabel15;
        private Sunny.UI.UICheckBox checkboxAutoEnter;
        private System.Windows.Forms.Timer timer1;
        private Sunny.UI.UIButton BTN_CountClear;
        private Sunny.UI.UILabel Lbl_ScanCount;
        private Sunny.UI.UILabel uiLabel16;
    }
}