﻿using CodePrintSystem.AppLayer.Basic.ViewObject;
using CodePrintSystem.Basic.Abstractions;
using CodePrintSystem.Core;
using CodePrintSystem.Utility;
using Microsoft.Extensions.DependencyInjection;
using NPOI.SS.Formula.Functions;
using Sunny.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodeScanCheckSystem.Forms
{
    public partial class PersonSet : UIPage, ISelfSingletonAutoInject
    {
        IServiceProvider serviceProvider;
        public PersonSet(IServiceProvider serviceProvider)
        {
            InitializeComponent();
            this.serviceProvider = serviceProvider;
        }


        /// <summary>
        /// 初始化基础数据
        /// </summary>
        /// <returns></returns>
        private async Task InitBaseDataAsync()
        {
            var scope = serviceProvider.CreateScope();
            var userService = scope.ServiceProvider.GetRequiredService<IQueryUserService>();
            var data = await userService.GetListAsync<UserView>();
            var noAdminUsers = data.Where(t => t.IsAdmin == false);
            this.checkedListBox1.DataSource = noAdminUsers.ToList();
            this.checkedListBox1.ValueMember = "Id";
            this.checkedListBox1.DisplayMember = "UserName";
            var selectedUserInfo = IniUtil.ReadIni("UnLockUser", "Users", "");
            if (!string.IsNullOrEmpty(selectedUserInfo))
            {
                var selectedUsers = selectedUserInfo.Split(',', options: StringSplitOptions.RemoveEmptyEntries);

                for (int i = 0; i < checkedListBox1.Items.Count; i++)
                {
                    for (int j = 0; j < selectedUsers.Length; j++)
                    {
                        var user = (UserView)checkedListBox1.Items[i];
                        if (user.UserName.Equals(selectedUsers[j]))
                        {
                            checkedListBox1.SetItemChecked(i, true);
                        }
                    }
                }



            }
        }

        private void PersonSet_Load(object sender, EventArgs e)
        {
            InitBaseDataAsync();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            List<string> selectedUsers = new List<string>();
            var checkItems = checkedListBox1.CheckedItems;
            foreach (var item in checkItems)
            {
                var user = (UserView)item;
                selectedUsers.Add(user.UserName);
            }
            IniUtil.WriteIni("UnLockUser", "Users", string.Join(",", selectedUsers));
            UIMessageTip.ShowOk("保存成功");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            InitBaseDataAsync();
        }
    }
}

