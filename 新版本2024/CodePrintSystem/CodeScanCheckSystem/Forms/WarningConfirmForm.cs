﻿using CodePrintSystem.Basic.Abstractions;
using Sunny.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MP3Sharp.Decoding;
using CodePrintSystem.Core;
using CodePrintSystem.Utility;
using Microsoft.Extensions.DependencyInjection;

namespace CodeScanCheckSystem.Forms
{
    public partial class WarningConfirmForm : Form
    {

        public WarningConfirmForm(string text)
        {
            InitializeComponent();
            this.lblText.Text = text + ",请通过指纹解锁";
            Mp3Player.Play("music/ng.mp3", false);
        }
        /// <summary>
        /// 窗体屏蔽 ALT+F4组合键强制关闭
        /// </summary>
        //protected override CreateParams CreateParams
        //{
        //    get
        //    {
        //        const int CS_NOCLOSE = 0x200;
        //        CreateParams cp = base.CreateParams;
        //        cp.ClassStyle = cp.ClassStyle | CS_NOCLOSE;
        //        return cp;
        //    }
        //}

        protected override void WndProc(ref Message m)
        {
            const int WM_SYSCOMMAND = 0x0112;
            const int SC_CLOSE = 0xF060;
            if (m.Msg == WM_SYSCOMMAND && (int)m.WParam == SC_CLOSE)
            {　　　　　　　　　　// 禁止用户通过窗口的xx按钮或通过窗口左上角下拉菜单或者按alt+f4或者任务栏鼠标关闭窗口
                // only taskmgr or msg can terminal
                return;
            }

            base.WndProc(ref m);
        }
        private async void btnConfirm_Click(object sender, EventArgs e)
        {
            if (!AppsettingsConfig.IsCanPasswordUnLock)
            {
                UIMessageTip.ShowError("只能通过指纹解锁！", 2000);
                return;
            }
            string userName = this.txtUserName.Text.Trim();
            string pwd = this.txtPwd.Text.Trim();
            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(pwd))
            {
                UIMessageTip.ShowError("用户名与密码不能为空！", 2000);
                return;
            }
            var userService = CommonRequestHelper.GetService<ICommandUserService>();
            var result = await userService.UserUnlockAsync(userName, pwd);
            if (result.IsSuccess == false)
            {
                UIMessageTip.ShowError(result.ErrorMessage, 2000);
                return;
            }
            Mp3Player.Stop();
            this.Close();
        }

        #region 指纹新的
        /// <summary>
        /// 采集之后的事
        /// </summary>
        /// <param name="result"></param>
        protected void CollectAfter(FingerprintResult result)
        {
            CheckFinger(result);
        }

        public async Task CheckFinger(FingerprintResult result)
        {
            try
            {
                // 遍历所有用户，指纹对比直接登录

                var fingePwMD5 = EncryptDecryptHelper.Md5(result.CapTmp);
                var nowUser = new UserEntity();
                nowUser = null;
                var userService = CommonRequestHelper.GetService<IQueryUserService>();
                var users = await userService.GetListAsync<UserEntity>();
                bool isSuccess = false;
                foreach (var user in users)
                {
                    if (string.IsNullOrEmpty(user.FingePrintPassword))
                    {
                        continue;
                    }
                    if (Live20RClient.DBMatch(user.FingePrintPassword, result.CapTmp))
                    {
                        if (user.IsAdmin == true)
                        {
                            nowUser = user;
                            isSuccess = true;
                        }
                        else
                        {

                            var selectedUserInfo = IniUtil.ReadIni("UnLockUser", "Users", "");
                            if (!string.IsNullOrEmpty(selectedUserInfo))
                            {
                                var selectedUsers = selectedUserInfo.Split(',', options: StringSplitOptions.RemoveEmptyEntries);
                                if (selectedUsers.Contains(user.UserName))
                                {
                                    nowUser = user;
                                    isSuccess = true;
                                }
                            }
                        }
                        break;
                    }
                }
                if (!isSuccess)
                {
                    UIMessageTip.ShowError("指纹未识别,请检查该人员是否录入指纹或有权限解锁！", 2000);
                    return;
                }
                else
                {
                    UIMessageTip.ShowOk("解锁成功");
                    Live20RClient.Close();
                    Mp3Player.Stop();
                    this.Invoke(new Action(() =>
                    {
                        this.Close();
                    }));
                }
            }
            catch (Exception ex)
            {
                WriteLogHelper.WriteLogsAsync("校验指纹异常:" + ex.ToString(), "Exception");
                UIMessageTip.ShowError("校验指纹异常!！", 2000);
                return;
            }
        }
        #endregion

        private void WarningConfirmForm_Load(object sender, EventArgs e)
        {

            if (!AppsettingsConfig.IsCanPasswordUnLock)
            {
                txtUserName.Enabled = false;
                txtPwd.Enabled = false;
                btnConfirm.Enabled = false;
            }
            else
            {
                txtUserName.Enabled = true;
                txtPwd.Enabled = true;
                btnConfirm.Enabled = true;
            }
            Task.Run(() =>
            {
                try
                {


                    Live20RClient.Open(CollectAfter);
                }
                catch (Exception ex)
                {
                    WriteLogHelper.WriteLogsAsync("初始化指纹异常:" + ex.ToString(), "Exception");
                    UIMessageTip.ShowError("初始化指纹异常!！", 2000);
                    return;
                }
            });
        }
    }
}
