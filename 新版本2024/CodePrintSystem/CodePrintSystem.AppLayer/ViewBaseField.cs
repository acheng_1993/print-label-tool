﻿using CodePrintSystem.Core;
using System;
using System.Collections.Generic;
using System.Text;
using ToolKitCore.MyNPOIExtension;

namespace CodePrintSystem.AppLayer
{
    /// <summary>
    /// View共用字段
    /// </summary>
    public class ViewBaseField
    {
        [MyResultColumn]
        public bool IsCheck { set; get; } = false;

        /// <summary>
        /// 添加人
        /// </summary>

        public long? CreaterId { get; set; }
        [MyExportDescription("创建人")]
        public string CreaterName { get; set; }
        /// <summary>
        /// 添加时间
        /// </summary>      
        /// 
        [MyExportDescription("时间")]
        public DateTime? CreateTime { get; set; }

        /// <summary>
        /// 修改人
        /// </summary>
        public long? ModifierId { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? ModifyTime { get; set; }
    }
}
