
//使用平台信息: ID:WinFormNoApi  描述:Winform无API
//代码版本信息: ID:None  描述:无 添加时间:2022/1/25 22:44:10
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Linq.Expressions;
using System;
using CodePrintSystem.Basic.Abstractions;
using CodePrintSystem.Utility;
using CodePrintSystem.Core;
using CodePrintSystem.AppLayer;
using CodePrintSystem.AppLayer.Basic.ViewObject;
using CodePrintSystem.AppLayer.Basic.DTOS;
using System.IO;
namespace CodePrintSystem.AppLayer
{
    public class ItemScanCountApp:ISelfScopedAutoInject
    {
        private readonly ICommandItemScanCountService commandItemScanCountService;
		private readonly IQueryItemScanCountService queryItemScanCountService;


         public ItemScanCountApp(ICommandItemScanCountService commandItemScanCountService,IQueryItemScanCountService queryItemScanCountService)
        {
            this.commandItemScanCountService = commandItemScanCountService;
			this.queryItemScanCountService = queryItemScanCountService;
        }

        /// <summary>
        /// 根据主键获取单个实体
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        public async Task<ItemScanCountView> GetAsync(long id)
        {
		   
           return   await queryItemScanCountService.GetAsync<ItemScanCountView>(id).ConfigureAwait(false);
		}


        /// <summary>
        /// 查询数据(分页) 返回指定实体T
        /// </summary>
        /// <param name="page">页码</param>
        /// <param name="size">每页数量</param>
        /// <param name="conditionDTO">查询条件类</param>
        /// <param name="field">返回字段</param>
        /// <param name="orderBy">排序字段</param>
        /// <returns></returns>
        public async Task<QueryPagedResponseModel<ItemScanCountView>> GetListPagedAsync(int page, int size, ItemScanCountConditionDTO conditionDTO=null, string field = null, string orderBy = null)
        {
		     var condition=conditionDTO.MapTo<BaseItemScanCountCondition>();
		     return await queryItemScanCountService.GetListPagedAsync<ItemScanCountView>(page, size, condition, field, orderBy).ConfigureAwait(false);
        }

        /// <summary>
        /// 查询数据(不分页) 返回指定实体T
        /// </summary>
        /// <param name="conditionDTO">查询条件类</param>
        /// <param name="field">返回字段</param>
        /// <param name="orderBy">排序字段</param>
        /// <returns></returns>
        public async Task<List<ItemScanCountView>> GetListAsync(ItemScanCountConditionDTO  conditionDTO, string field = null, string orderBy = null)
        {
		     var condition=conditionDTO.MapTo<BaseItemScanCountCondition>();
             return await queryItemScanCountService.GetListAsync<ItemScanCountView>(condition, field, orderBy).ConfigureAwait(false);
	    }

        /// <summary>
        /// 根据id集合获取多条数据
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<List<ItemScanCountView>> GetListByIdsAsync(List<long> ids)
        {
             return await queryItemScanCountService.GetListByIdsAsync<ItemScanCountView>(ids).ConfigureAwait(false);
	    }

	    /// <summary>
        /// 插入单个实体
        /// </summary>
        /// <param name="insertDto">对应实体数据传输对象</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<long>> InsertAsync(ItemScanCountInsertDTO insertDto)
        { 
		    var entity=insertDto.MapTo<ItemScanCountEntity>();
            var result = await commandItemScanCountService.InsertAsync(entity).ConfigureAwait(false);
            return result;
        }

		 

		   /// <summary>
        /// 批量插入实体
        /// </summary>
        /// <param name="insertDtoList">对应实体数据传输对象集合</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> InsertBatchAsync(List<ItemScanCountInsertDTO> insertDtoList)
        {
		    var entities=insertDtoList.MapToList<ItemScanCountEntity>();
            var result = await commandItemScanCountService.InsertBatchAsync(entities).ConfigureAwait(false);
            return result;
        }
       
		  
        /// <summary>
        /// 根据主键更新实体
        /// </summary>
		/// <param name="id">主键</param>
        /// <param name="updateDto">对应实体数据传输对象</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> UpdateAsync(long id,ItemScanCountUpdateDTO updateDto)
        {
		    var entity=updateDto.MapTo<ItemScanCountEntity>();
			entity.Id = id;
            var result = await commandItemScanCountService.UpdateAsync(entity).ConfigureAwait(false);  
            return result;
        }
	  
 
       /// <summary>
        /// 根据根据主键删除
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> DeleteAsync(long id)
        {
            var result = await commandItemScanCountService.DeleteAsync(id).ConfigureAwait(false);
            return result;
	   }


	     /// <summary>
        /// 批量删除 根据主键
        /// </summary>
        /// <param name="idList">主键集合</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> DeleteBatchAsync(IList<long> idList)
        {
            var result = await commandItemScanCountService.DeleteBatchAsync(idList).ConfigureAwait(false);
            return result;

        }

       

    }
}
