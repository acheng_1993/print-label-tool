

//使用平台信息: ID:WinFormNoApi  描述:Winform无API
//代码版本信息: ID:None  描述:无 添加时间:2022/1/25 22:44:10
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
namespace CodePrintSystem.AppLayer.Basic.DTOS
{
    /// <summary>
    /// 
    /// </summary>
    public class ItemScanCountInsertDTO
    {
            /// <summary>
        ///  物料编号
        /// </summary>
        [Description("物料编号")]
                 public string ItemCode {get;set;}
        
          /// <summary>
        ///  数量
        /// </summary>
        [Description("数量")]
                 public int? Count {get;set;}
        
     

    }
}
