
//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/1/30 14:04:46
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Linq.Expressions;
using System;
using CodePrintSystem.Basic.Abstractions;
using CodePrintSystem.Utility;
using CodePrintSystem.Core;
using CodePrintSystem.AppLayer;
using CodePrintSystem.AppLayer.Basic.ViewObject;
using CodePrintSystem.AppLayer.Basic.DTOS;
namespace CodePrintSystem.AppLayer
{
    public class BusinessdictionarytypeApp:ISelfScopedAutoInject
    {
        private readonly ICommandBusinessdictionarytypeService commandBusinessdictionarytypeService;
		private readonly IQueryBusinessdictionarytypeService queryBusinessdictionarytypeService;


         public BusinessdictionarytypeApp(ICommandBusinessdictionarytypeService commandBusinessdictionarytypeService,IQueryBusinessdictionarytypeService queryBusinessdictionarytypeService)
        {
            this.commandBusinessdictionarytypeService = commandBusinessdictionarytypeService;
			this.queryBusinessdictionarytypeService = queryBusinessdictionarytypeService;
        }

        /// <summary>
        /// 根据主键获取单个实体
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        public async Task<BusinessdictionarytypeView> GetAsync(long id)
        {
		   
           return   await queryBusinessdictionarytypeService.GetAsync<BusinessdictionarytypeView>(id).ConfigureAwait(false);
		}


        /// <summary>
        /// 查询数据(分页) 返回指定实体T
        /// </summary>
        /// <param name="page">页码</param>
        /// <param name="size">每页数量</param>
        /// <param name="conditionDTO">查询条件类</param>
        /// <param name="field">返回字段</param>
        /// <param name="orderBy">排序字段</param>
        /// <returns></returns>
        public async Task<QueryPagedResponseModel<BusinessdictionarytypeView>> GetListPagedAsync(int page, int size, BusinessdictionarytypeConditionDTO conditionDTO=null, string field = null, string orderBy = null)
        {
		     var condition=conditionDTO.MapTo<BaseBusinessdictionarytypeCondition>();
		     return await queryBusinessdictionarytypeService.GetListPagedAsync<BusinessdictionarytypeView>(page, size, condition, field, orderBy).ConfigureAwait(false);
        }

        /// <summary>
        /// 查询数据(不分页) 返回指定实体T
        /// </summary>
        /// <param name="conditionDTO">查询条件类</param>
        /// <param name="field">返回字段</param>
        /// <param name="orderBy">排序字段</param>
        /// <returns></returns>
        public async Task<List<BusinessdictionarytypeView>> GetListAsync(BusinessdictionarytypeConditionDTO  conditionDTO, string field = null, string orderBy = null)
        {
		     var condition=conditionDTO.MapTo<BaseBusinessdictionarytypeCondition>();
             return await queryBusinessdictionarytypeService.GetListAsync<BusinessdictionarytypeView>(condition, field, orderBy).ConfigureAwait(false);
	    }

        /// <summary>
        /// 根据id集合获取多条数据
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<List<BusinessdictionarytypeView>> GetListByIdsAsync(List<long> ids)
        {
             return await queryBusinessdictionarytypeService.GetListByIdsAsync<BusinessdictionarytypeView>(ids).ConfigureAwait(false);
	    }

	    /// <summary>
        /// 插入单个实体
        /// </summary>
        /// <param name="insertDto">对应实体数据传输对象</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<long>> InsertAsync(BusinessdictionarytypeInsertDTO insertDto)
        { 
		    var entity=insertDto.MapTo<BusinessdictionarytypeEntity>();
            var result = await commandBusinessdictionarytypeService.InsertAsync(entity).ConfigureAwait(false);
            return result;
        }

		 

		   /// <summary>
        /// 批量插入实体
        /// </summary>
        /// <param name="insertDtoList">对应实体数据传输对象集合</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> InsertBatchAsync(List<BusinessdictionarytypeInsertDTO> insertDtoList)
        {
		    var entities=insertDtoList.MapToList<BusinessdictionarytypeEntity>();
            var result = await commandBusinessdictionarytypeService.InsertBatchAsync(entities).ConfigureAwait(false);
            return result;
        }
       
		  
        /// <summary>
        /// 根据主键更新实体
        /// </summary>
		/// <param name="id">主键</param>
        /// <param name="updateDto">对应实体数据传输对象</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> UpdateAsync(long id,BusinessdictionarytypeUpdateDTO updateDto)
        {
		    var entity=updateDto.MapTo<BusinessdictionarytypeEntity>();
			entity.Id = id;
            var result = await commandBusinessdictionarytypeService.UpdateAsync(entity).ConfigureAwait(false);  
            return result;
        }
	  
 
       /// <summary>
        /// 根据根据主键删除
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> DeleteAsync(long id)
        {
            var result = await commandBusinessdictionarytypeService.DeleteAsync(id).ConfigureAwait(false);
            return result;
	   }


	     /// <summary>
        /// 批量删除 根据主键
        /// </summary>
        /// <param name="idList">主键集合</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> DeleteBatchAsync(IList<long> idList)
        {
            var result = await commandBusinessdictionarytypeService.DeleteBatchAsync(idList).ConfigureAwait(false);
            return result;

        }

       

    }
}
