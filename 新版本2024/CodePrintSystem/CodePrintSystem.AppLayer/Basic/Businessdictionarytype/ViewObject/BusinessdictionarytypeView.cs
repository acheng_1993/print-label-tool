
//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/1/30 14:04:46
using System;
using CodePrintSystem.Core;
namespace CodePrintSystem.AppLayer.Basic.ViewObject
{
    /// <summary>
    /// 
    /// </summary>
	[MyTableName("basic_businessdictionarytype")]
    [MyPrimaryKey("Id",AutoIncrement =false)]
    public class BusinessdictionarytypeView:ViewBaseField
    {
            /// <summary>
        ///  主键
        /// </summary>
        public long Id {get;set;}
        
          /// <summary>
        ///  字典类型名称
        /// </summary>
        public string TypeName {get;set;}
        
          /// <summary>
        ///  是否自定义
        /// </summary>
        public bool? IsCustom {get;set;}
        
          /// <summary>
        ///  编码 (枚举使用）
        /// </summary>
        public string Code {get;set;}
        
          /// <summary>
        ///  是否为批次属性
        /// </summary>
        public bool? IsBatchAttribute {get;set;}
        
          /// <summary>
        ///  备注
        /// </summary>
        public string Remark {get;set;}
        
          /// <summary>
        ///  是否已引用（已引用的不能修改或删除）
        /// </summary>
        public bool? IsQuote {get;set;}
        
          /// <summary>
        ///  是否启用
        /// </summary>
        public bool? IsEnabled {get;set;}
        
     

    }
}
