

//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/1/30 14:04:46
using System;
using System.Collections.Generic;
namespace CodePrintSystem.AppLayer.Basic.DTOS
{
    /// <summary>
    /// 
    /// </summary>
    public class BusinessdictionarytypeConditionDTO
    {
            /// <summary>
        ///  编码 (枚举使用）
        /// </summary>
        public string Code {get;set;}
        
           /// <summary>
        ///  是否自定义
        /// </summary>
        public bool? IsCustom {get;set;}
        
           /// <summary>
        ///  是否启用
        /// </summary>
        public bool? IsEnabled {get;set;}
        
      

    }
}
