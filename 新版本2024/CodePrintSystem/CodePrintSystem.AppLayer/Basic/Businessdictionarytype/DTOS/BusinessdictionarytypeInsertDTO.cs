

//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/1/30 14:04:46
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
namespace CodePrintSystem.AppLayer.Basic.DTOS
{
    /// <summary>
    /// 
    /// </summary>
    public class BusinessdictionarytypeInsertDTO
    {
            /// <summary>
        ///  字典类型名称
        /// </summary>
        [Description("字典类型名称")]
                  [MinLength(0)]
                   [MaxLength(50)]
                  public string TypeName {get;set;}
        
          /// <summary>
        ///  是否自定义
        /// </summary>
        [Description("是否自定义")]
                 public bool? IsCustom {get;set;}
        
          /// <summary>
        ///  编码 (枚举使用）
        /// </summary>
        [Description("编码 (枚举使用）")]
                  [MinLength(0)]
                   [MaxLength(50)]
                  public string Code {get;set;}
        
          /// <summary>
        ///  是否为批次属性
        /// </summary>
        [Description("是否为批次属性")]
                 public bool? IsBatchAttribute {get;set;}
        
          /// <summary>
        ///  备注
        /// </summary>
        [Description("备注")]
                  [MinLength(0)]
                   [MaxLength(500)]
                  public string Remark {get;set;}
        
          /// <summary>
        ///  是否已引用（已引用的不能修改或删除）
        /// </summary>
        [Description("是否已引用（已引用的不能修改或删除）")]
                 public bool? IsQuote {get;set;}
        
          /// <summary>
        ///  是否启用
        /// </summary>
        [Description("是否启用")]
                 public bool? IsEnabled {get;set;}
        
     

    }
}
