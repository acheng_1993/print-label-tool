

//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/1/15 9:54:11
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
namespace CodePrintSystem.AppLayer.SystemSet.DTOS
{
    /// <summary>
    /// 系统设置模块-唯一编码生成历史记录
    /// </summary>
    public class GenerateCodeHistoryUpdateDTO
    {
            /// <summary>
        ///  主键
        /// </summary>
        [Description("主键")]
                  [Required]
                   [Range(0, long.MaxValue)]
                  public long Id {get;set;}
        
          /// <summary>
        ///  生成的编码
        /// </summary>
        [Description("生成的编码")]
                  [MinLength(1)]
                   [MaxLength(50)]
                  public string Code {get;set;}
        
          /// <summary>
        ///  编码生成模板id
        /// </summary>
        [Description("编码生成模板id")]
                  [Range(0, long.MaxValue)]
                  public long? TemplateId {get;set;}
        
     

    }
}
