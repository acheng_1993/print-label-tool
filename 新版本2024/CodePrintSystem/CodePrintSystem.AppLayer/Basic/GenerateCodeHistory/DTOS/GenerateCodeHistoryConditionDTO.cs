

//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/1/15 9:54:11
using System;
using System.Collections.Generic;
namespace CodePrintSystem.AppLayer.SystemSet.DTOS
{
    /// <summary>
    /// 
    /// </summary>
    public class GenerateCodeHistoryConditionDTO
    {
            /// <summary>
        ///  生成的编码
        /// </summary>
        public string Code {get;set;}
        
           /// <summary>
        ///  编码生成模板id
        /// </summary>
        public long? TemplateId {get;set;}
        
           /// <summary>
        ///  创建时间起始时间
        /// </summary>
        public DateTime? CreateTimeStart {get;set;}
        
           /// <summary>
        ///  创建时间结束时间
        /// </summary>
        public DateTime? CreateTimeEnd {get;set;}
        
      

    }
}
