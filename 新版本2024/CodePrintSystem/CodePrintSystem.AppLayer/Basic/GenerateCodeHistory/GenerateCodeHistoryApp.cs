
//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/1/15 9:54:11
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Linq.Expressions;
using System;
using CodePrintSystem.Basic.Abstractions;
using CodePrintSystem.Utility;
using CodePrintSystem.Core;
using CodePrintSystem.AppLayer;
using CodePrintSystem.AppLayer.SystemSet.ViewObject;
using CodePrintSystem.AppLayer.SystemSet.DTOS;
namespace CodePrintSystem.AppLayer
{
    public class GenerateCodeHistoryApp:ISelfScopedAutoInject
    {
        private readonly ICommandGenerateCodeHistoryService commandGenerateCodeHistoryService;
		private readonly IQueryGenerateCodeHistoryService queryGenerateCodeHistoryService;


         public GenerateCodeHistoryApp(ICommandGenerateCodeHistoryService commandGenerateCodeHistoryService,IQueryGenerateCodeHistoryService queryGenerateCodeHistoryService)
        {
            this.commandGenerateCodeHistoryService = commandGenerateCodeHistoryService;
			this.queryGenerateCodeHistoryService = queryGenerateCodeHistoryService;
        }

        /// <summary>
        /// 根据主键获取单个实体
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        public async Task<GenerateCodeHistoryView> GetAsync(long id)
        {
		   
           return   await queryGenerateCodeHistoryService.GetAsync<GenerateCodeHistoryView>(id).ConfigureAwait(false);
		}


        /// <summary>
        /// 查询数据(分页) 返回指定实体T
        /// </summary>
        /// <param name="page">页码</param>
        /// <param name="size">每页数量</param>
        /// <param name="conditionDTO">查询条件类</param>
        /// <param name="field">返回字段</param>
        /// <param name="orderBy">排序字段</param>
        /// <returns></returns>
        public async Task<QueryPagedResponseModel<GenerateCodeHistoryView>> GetListPagedAsync(int page, int size, GenerateCodeHistoryConditionDTO conditionDTO=null, string field = null, string orderBy = null)
        {
		     var condition=conditionDTO.MapTo<BaseGenerateCodeHistoryCondition>();
		     return await queryGenerateCodeHistoryService.GetListPagedAsync<GenerateCodeHistoryView>(page, size, condition, field, orderBy).ConfigureAwait(false);
        }

        /// <summary>
        /// 查询数据(不分页) 返回指定实体T
        /// </summary>
        /// <param name="conditionDTO">查询条件类</param>
        /// <param name="field">返回字段</param>
        /// <param name="orderBy">排序字段</param>
        /// <returns></returns>
        public async Task<List<GenerateCodeHistoryView>> GetListAsync(GenerateCodeHistoryConditionDTO  conditionDTO, string field = null, string orderBy = null)
        {
		     var condition=conditionDTO.MapTo<BaseGenerateCodeHistoryCondition>();
             return await queryGenerateCodeHistoryService.GetListAsync<GenerateCodeHistoryView>(condition, field, orderBy).ConfigureAwait(false);
	    }

        /// <summary>
        /// 根据id集合获取多条数据
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<List<GenerateCodeHistoryView>> GetListByIdsAsync(List<long> ids)
        {
             return await queryGenerateCodeHistoryService.GetListByIdsAsync<GenerateCodeHistoryView>(ids).ConfigureAwait(false);
	    }

	    /// <summary>
        /// 插入单个实体
        /// </summary>
        /// <param name="insertDto">对应实体数据传输对象</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<long>> InsertAsync(GenerateCodeHistoryInsertDTO insertDto)
        { 
		    var entity=insertDto.MapTo<GenerateCodeHistoryEntity>();
            var result = await commandGenerateCodeHistoryService.InsertAsync(entity).ConfigureAwait(false);
            return result;
        }

		 

		   /// <summary>
        /// 批量插入实体
        /// </summary>
        /// <param name="insertDtoList">对应实体数据传输对象集合</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> InsertBatchAsync(List<GenerateCodeHistoryInsertDTO> insertDtoList)
        {
		    var entities=insertDtoList.MapToList<GenerateCodeHistoryEntity>();
            var result = await commandGenerateCodeHistoryService.InsertBatchAsync(entities).ConfigureAwait(false);
            return result;
        }
       
		  
        /// <summary>
        /// 根据主键更新实体
        /// </summary>
		/// <param name="id">主键</param>
        /// <param name="updateDto">对应实体数据传输对象</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> UpdateAsync(long id,GenerateCodeHistoryUpdateDTO updateDto)
        {
		    var entity=updateDto.MapTo<GenerateCodeHistoryEntity>();
			entity.Id = id;
            var result = await commandGenerateCodeHistoryService.UpdateAsync(entity).ConfigureAwait(false);  
            return result;
        }
	  
 
       /// <summary>
        /// 根据根据主键删除
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> DeleteAsync(long id)
        {
            var result = await commandGenerateCodeHistoryService.DeleteAsync(id).ConfigureAwait(false);
            return result;
	   }


	     /// <summary>
        /// 批量删除 根据主键
        /// </summary>
        /// <param name="idList">主键集合</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> DeleteBatchAsync(IList<long> idList)
        {
            var result = await commandGenerateCodeHistoryService.DeleteBatchAsync(idList).ConfigureAwait(false);
            return result;

        }

       

    }
}
