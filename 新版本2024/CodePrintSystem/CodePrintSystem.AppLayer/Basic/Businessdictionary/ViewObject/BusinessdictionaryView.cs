
//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/1/30 14:02:11
using System;
using CodePrintSystem.Core;
namespace CodePrintSystem.AppLayer.Basic.ViewObject
{
    /// <summary>
    /// 
    /// </summary>
	[MyTableName("basic_businessdictionary")]
    [MyPrimaryKey("Id",AutoIncrement =false)]
    public class BusinessdictionaryView:ViewBaseField
    {
            /// <summary>
        ///  主键
        /// </summary>
        public long Id {get;set;}


        /// <summary>
        ///  字典类型Id （TBasicDictionaryType的主键）
        /// </summary>
        [MyResultColumn]
        public string DictionaryTypeName { get; set; }
        /// <summary>
        ///  字典类型Id （TBasicDictionaryType的主键）
        /// </summary>
        public long? DictionaryTypeId {get;set;}
        
          /// <summary>
        ///  字典code
        /// </summary>
        public string Code {get;set;}
        
          /// <summary>
        ///  字典名称
        /// </summary>
        public string Name {get;set;}
        
          /// <summary>
        ///  简称
        /// </summary>
        public string ShortName {get;set;}
        
          /// <summary>
        ///  备注
        /// </summary>
        public string Remark {get;set;}
        
          /// <summary>
        ///  是否自定义 0否 1是
        /// </summary>
        public bool? IsCustom {get;set;}
        
          /// <summary>
        ///  是否可用 0 否 1是
        /// </summary>
        public bool? IsEnabled {get;set;}
        
          /// <summary>
        ///  是否已引用（已引用的不能修改或删除）
        /// </summary>
        public bool? IsQuote {get;set;}
        
     

    }
}
