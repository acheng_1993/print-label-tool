

//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/1/30 14:02:11
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
namespace CodePrintSystem.AppLayer.Basic.DTOS
{
    /// <summary>
    /// 
    /// </summary>
    public class BusinessdictionaryInsertDTO
    {
            /// <summary>
        ///  字典类型Id （TBasicDictionaryType的主键）
        /// </summary>
        [Description("字典类型Id （TBasicDictionaryType的主键）")]
                  [Required]
                   [Range(0, long.MaxValue)]
                  public long? DictionaryTypeId {get;set;}
        
          /// <summary>
        ///  字典code
        /// </summary>
        [Description("字典code")]
                  [Required]
                   [MinLength(0)]
                   [MaxLength(50)]
                  public string Code {get;set;}
        
          /// <summary>
        ///  字典名称
        /// </summary>
        [Description("字典名称")]
                  [Required]
                   [MinLength(0)]
                   [MaxLength(50)]
                  public string Name {get;set;}
        
          /// <summary>
        ///  简称
        /// </summary>
        [Description("简称")]
                  [Required]
                   [MinLength(0)]
                   [MaxLength(50)]
                  public string ShortName {get;set;}
        
          /// <summary>
        ///  备注
        /// </summary>
        [Description("备注")]
                  [MinLength(0)]
                   [MaxLength(500)]
                  public string Remark {get;set;}
        
          /// <summary>
        ///  是否自定义 0否 1是
        /// </summary>
        [Description("是否自定义 0否 1是")]
                 public bool? IsCustom {get;set;}
        
          /// <summary>
        ///  是否可用 0 否 1是
        /// </summary>
        [Description("是否可用 0 否 1是")]
                 public bool? IsEnabled {get;set;}
        
          /// <summary>
        ///  是否已引用（已引用的不能修改或删除）
        /// </summary>
        [Description("是否已引用（已引用的不能修改或删除）")]
                 public bool? IsQuote {get;set;}
        
     

    }
}
