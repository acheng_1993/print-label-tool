

//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/1/30 14:02:11
using System;
using System.Collections.Generic;
namespace CodePrintSystem.AppLayer.Basic.DTOS
{
    /// <summary>
    /// 
    /// </summary>
    public class BusinessdictionaryConditionDTO
    {
            /// <summary>
        ///  字典code
        /// </summary>
        public string Code {get;set;}
        
           /// <summary>
        ///  字典类型Id （TBasicDictionaryType的主键）
        /// </summary>
        public long? DictionaryTypeId {get;set;}
        
           /// <summary>
        ///  是否可用 0 否 1是
        /// </summary>
        public bool? IsEnabled {get;set;}
        
      

    }
}
