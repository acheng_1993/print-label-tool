
//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/1/30 14:09:27
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Linq.Expressions;
using System;
using CodePrintSystem.Basic.Abstractions;
using CodePrintSystem.Utility;
using CodePrintSystem.Core;
using CodePrintSystem.AppLayer;
using CodePrintSystem.AppLayer.Basic.ViewObject;
using CodePrintSystem.AppLayer.Basic.DTOS;
namespace CodePrintSystem.AppLayer
{
    public class SupplierApp:ISelfScopedAutoInject
    {
        private readonly ICommandSupplierService commandSupplierService;
		private readonly IQuerySupplierService querySupplierService;


         public SupplierApp(ICommandSupplierService commandSupplierService,IQuerySupplierService querySupplierService)
        {
            this.commandSupplierService = commandSupplierService;
			this.querySupplierService = querySupplierService;
        }

        /// <summary>
        /// 根据主键获取单个实体
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        public async Task<SupplierView> GetAsync(long id)
        {
		   
           return   await querySupplierService.GetAsync<SupplierView>(id).ConfigureAwait(false);
		}


        /// <summary>
        /// 查询数据(分页) 返回指定实体T
        /// </summary>
        /// <param name="page">页码</param>
        /// <param name="size">每页数量</param>
        /// <param name="conditionDTO">查询条件类</param>
        /// <param name="field">返回字段</param>
        /// <param name="orderBy">排序字段</param>
        /// <returns></returns>
        public async Task<QueryPagedResponseModel<SupplierView>> GetListPagedAsync(int page, int size, SupplierConditionDTO conditionDTO=null, string field = null, string orderBy = null)
        {
		     var condition=conditionDTO.MapTo<BaseSupplierCondition>();
		     return await querySupplierService.GetListPagedAsync<SupplierView>(page, size, condition, field, orderBy).ConfigureAwait(false);
        }

        /// <summary>
        /// 查询数据(不分页) 返回指定实体T
        /// </summary>
        /// <param name="conditionDTO">查询条件类</param>
        /// <param name="field">返回字段</param>
        /// <param name="orderBy">排序字段</param>
        /// <returns></returns>
        public async Task<List<SupplierView>> GetListAsync(SupplierConditionDTO  conditionDTO, string field = null, string orderBy = null)
        {
		     var condition=conditionDTO.MapTo<BaseSupplierCondition>();
             return await querySupplierService.GetListAsync<SupplierView>(condition, field, orderBy).ConfigureAwait(false);
	    }

        /// <summary>
        /// 根据id集合获取多条数据
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<List<SupplierView>> GetListByIdsAsync(List<long> ids)
        {
             return await querySupplierService.GetListByIdsAsync<SupplierView>(ids).ConfigureAwait(false);
	    }

	    /// <summary>
        /// 插入单个实体
        /// </summary>
        /// <param name="insertDto">对应实体数据传输对象</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<long>> InsertAsync(SupplierInsertDTO insertDto)
        { 
		    var entity=insertDto.MapTo<SupplierEntity>();
            var result = await commandSupplierService.InsertAsync(entity).ConfigureAwait(false);
            return result;
        }

		 

		   /// <summary>
        /// 批量插入实体
        /// </summary>
        /// <param name="insertDtoList">对应实体数据传输对象集合</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> InsertBatchAsync(List<SupplierInsertDTO> insertDtoList)
        {
		    var entities=insertDtoList.MapToList<SupplierEntity>();
            var result = await commandSupplierService.InsertBatchAsync(entities).ConfigureAwait(false);
            return result;
        }
       
		  
        /// <summary>
        /// 根据主键更新实体
        /// </summary>
		/// <param name="id">主键</param>
        /// <param name="updateDto">对应实体数据传输对象</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> UpdateAsync(long id,SupplierUpdateDTO updateDto)
        {
		    var entity=updateDto.MapTo<SupplierEntity>();
			entity.Id = id;
            var result = await commandSupplierService.UpdateAsync(entity).ConfigureAwait(false);  
            return result;
        }
	  
 
       /// <summary>
        /// 根据根据主键删除
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> DeleteAsync(long id)
        {
            var result = await commandSupplierService.DeleteAsync(id).ConfigureAwait(false);
            return result;
	   }


	     /// <summary>
        /// 批量删除 根据主键
        /// </summary>
        /// <param name="idList">主键集合</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> DeleteBatchAsync(IList<long> idList)
        {
            var result = await commandSupplierService.DeleteBatchAsync(idList).ConfigureAwait(false);
            return result;

        }

       

    }
}
