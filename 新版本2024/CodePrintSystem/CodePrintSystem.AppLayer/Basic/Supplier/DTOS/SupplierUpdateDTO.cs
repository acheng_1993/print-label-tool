

//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/1/30 14:09:27
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
namespace CodePrintSystem.AppLayer.Basic.DTOS
{
    /// <summary>
    /// 
    /// </summary>
    public class SupplierUpdateDTO
    {
            /// <summary>
        ///  主键
        /// </summary>
        [Description("主键")]
                  [Required]
                   [Range(0, long.MaxValue)]
                  public long Id {get;set;}
        
          /// <summary>
        ///  供应商代码
        /// </summary>
        [Description("供应商代码")]
                  [MinLength(0)]
                   [MaxLength(50)]
                  public string Code {get;set;}
        
          /// <summary>
        ///  名称
        /// </summary>
        [Description("名称")]
                  [MinLength(0)]
                   [MaxLength(50)]
                  public string Name {get;set;}
        
     

    }
}
