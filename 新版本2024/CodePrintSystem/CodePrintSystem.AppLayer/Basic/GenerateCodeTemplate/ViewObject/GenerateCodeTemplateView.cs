
//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/1/15 9:53:15
using System;
using CodePrintSystem.Core;
namespace CodePrintSystem.AppLayer.SystemSet.ViewObject
{
    /// <summary>
    /// 系统设置模块-唯一编码生成模板
    /// </summary>
	[MyTableName("SystemSet_GenerateCodeTemplate")]
    [MyPrimaryKey("Id", AutoIncrement = false)]
    public class GenerateCodeTemplateView : ViewBaseField
    {
        /// <summary>
        ///  主键
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        ///  编码生成模板名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        ///  编码生成模板code
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        ///  code长度
        /// </summary>
        public long? CodeLength { get; set; }

        /// <summary>
        ///  单词打印条码条数
        /// </summary>
        public int? PrintLimitOnce { get; set; }

        /// <summary>
        /// 流水号长度
        /// </summary>

        public int? SerialNumberLength { get; set; }

        /// <summary>
        ///  模板json内容
        /// </summary>
        // public string TemplateJsonText { get; set; }

        /// <summary>
        /// 模板json文件
        /// </summary>
        public string TemplateJsonFilePath { get; set; }


        /// <summary>
        /// 是否导入了模板
        /// </summary>
        /// 
        [MyResultColumn]
        public string IsImportTemplate { get { return string.IsNullOrEmpty(TemplateJsonFilePath) ? "否" : "是"; } }

        /// <summary>
        ///  是否自定义 0否 1是
        /// </summary>
        public bool? IsCustom { get; set; }

        /// <summary>
        ///  是否可用 0 否 1是
        /// </summary>
        public bool? IsEnabled { get; set; }

        /// <summary>
        ///  是否已引用（已引用的不能修改或删除）
        /// </summary>
        public bool? IsQuote { get; set; }



    }
}
