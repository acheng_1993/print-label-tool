

//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/1/15 9:53:15
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
namespace CodePrintSystem.AppLayer.SystemSet.DTOS
{
    /// <summary>
    /// 系统设置模块-唯一编码生成模板
    /// </summary>
    public class GenerateCodeTemplateUpdateDTO
    {
        /// <summary>
        ///  主键
        /// </summary>
        [Description("主键")]
        [Required]
        [Range(0, long.MaxValue)]
        public long Id { get; set; }

        /// <summary>
        ///  编码生成模板名称
        /// </summary>
        [Description("编码生成模板名称")]
        [Required]
        [MinLength(1)]
        [MaxLength(50)]
        public string Name { get; set; }

        /// <summary>
        ///  编码生成模板code
        /// </summary>
        [Description("编码生成模板code")]
        [Required]
        [MinLength(1)]
        [MaxLength(50)]
        public string Code { get; set; }

        /// <summary>
        ///  code长度
        /// </summary>
        /// 
        [Description("条码固定长度")]
        [Required]
        [Range(0,int.MaxValue)]
        public int? CodeLength { get; set; }

        /// <summary>
        ///  单词打印条码条数
        /// </summary>
        /// 
        [Description("条码固定长度")]
        [Required]
        [Range(0, int.MaxValue)]
        public int? PrintLimitOnce { get; set; }


        /// <summary>
        /// 流水号长度
        /// </summary>
        [Description("流水号长度")]
        [Required]
        [Range(0, int.MaxValue)]
        public int? SerialNumberLength { get; set; }





        /// <summary>
        ///  模板json内容
        /// </summary>
        // public string TemplateJsonText { get; set; }

        /// <summary>
        /// 模板json文件
        /// </summary>
        /// 
        [Description("模板json文件路径")]
        [Required]
        [MinLength(1)]
        [MaxLength(500)]
        public string TemplateJsonFilePath { get; set; }

        /// <summary>
        ///  是否自定义 0否 1是
        /// </summary>
        [Description("是否自定义 0否 1是")]
        public bool? IsCustom { get; set; }

        /// <summary>
        ///  是否可用 0 否 1是
        /// </summary>
        [Description("是否可用 0 否 1是")]
        public bool? IsEnabled { get; set; }

        /// <summary>
        ///  是否已引用（已引用的不能修改或删除）
        /// </summary>
        [Description("是否已引用（已引用的不能修改或删除）")]
        public bool? IsQuote { get; set; }



    }
}
