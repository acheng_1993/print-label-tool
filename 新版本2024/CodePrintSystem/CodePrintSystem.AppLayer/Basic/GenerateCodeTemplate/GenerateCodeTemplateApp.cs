
//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/1/15 9:53:15
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Linq.Expressions;
using System;
using CodePrintSystem.Basic.Abstractions;
using CodePrintSystem.Utility;
using CodePrintSystem.Core;
using CodePrintSystem.AppLayer;
using CodePrintSystem.AppLayer.SystemSet.ViewObject;
using CodePrintSystem.AppLayer.SystemSet.DTOS;
namespace CodePrintSystem.AppLayer
{
    public class GenerateCodeTemplateApp:ISelfScopedAutoInject
    {
        private readonly ICommandGenerateCodeTemplateService commandGenerateCodeTemplateService;
		private readonly IQueryGenerateCodeTemplateService queryGenerateCodeTemplateService;


         public GenerateCodeTemplateApp(ICommandGenerateCodeTemplateService commandGenerateCodeTemplateService,IQueryGenerateCodeTemplateService queryGenerateCodeTemplateService)
        {
            this.commandGenerateCodeTemplateService = commandGenerateCodeTemplateService;
			this.queryGenerateCodeTemplateService = queryGenerateCodeTemplateService;
        }

        /// <summary>
        /// 根据主键获取单个实体
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        public async Task<GenerateCodeTemplateView> GetAsync(long id)
        {
		   
           return   await queryGenerateCodeTemplateService.GetAsync<GenerateCodeTemplateView>(id).ConfigureAwait(false);
		}


        /// <summary>
        /// 查询数据(分页) 返回指定实体T
        /// </summary>
        /// <param name="page">页码</param>
        /// <param name="size">每页数量</param>
        /// <param name="conditionDTO">查询条件类</param>
        /// <param name="field">返回字段</param>
        /// <param name="orderBy">排序字段</param>
        /// <returns></returns>
        public async Task<QueryPagedResponseModel<GenerateCodeTemplateView>> GetListPagedAsync(int page, int size, GenerateCodeTemplateConditionDTO conditionDTO=null, string field = null, string orderBy = null)
        {
		     var condition=conditionDTO.MapTo<BaseGenerateCodeTemplateCondition>();
		     return await queryGenerateCodeTemplateService.GetListPagedAsync<GenerateCodeTemplateView>(page, size, condition, field, orderBy).ConfigureAwait(false);
        }

        /// <summary>
        /// 查询数据(不分页) 返回指定实体T
        /// </summary>
        /// <param name="conditionDTO">查询条件类</param>
        /// <param name="field">返回字段</param>
        /// <param name="orderBy">排序字段</param>
        /// <returns></returns>
        public async Task<List<GenerateCodeTemplateView>> GetListAsync(GenerateCodeTemplateConditionDTO  conditionDTO, string field = null, string orderBy = null)
        {
		     var condition=conditionDTO.MapTo<BaseGenerateCodeTemplateCondition>();
             return await queryGenerateCodeTemplateService.GetListAsync<GenerateCodeTemplateView>(condition, field, orderBy).ConfigureAwait(false);
	    }

        /// <summary>
        /// 根据id集合获取多条数据
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<List<GenerateCodeTemplateView>> GetListByIdsAsync(List<long> ids)
        {
             return await queryGenerateCodeTemplateService.GetListByIdsAsync<GenerateCodeTemplateView>(ids).ConfigureAwait(false);
	    }

	    /// <summary>
        /// 插入单个实体
        /// </summary>
        /// <param name="insertDto">对应实体数据传输对象</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<long>> InsertAsync(GenerateCodeTemplateInsertDTO insertDto)
        { 
		    var entity=insertDto.MapTo<GenerateCodeTemplateEntity>();
            var result = await commandGenerateCodeTemplateService.InsertAsync(entity).ConfigureAwait(false);
            return result;
        }

        public async Task<HttpResponseResultModel<bool>> UpdateEnabledBatchAsync(List<long> ids, bool isEnabled)
        {
            return await commandGenerateCodeTemplateService.UpdateEnabledBatchAsync(ids, isEnabled);
        }

        /// <summary>
        /// 批量插入实体
        /// </summary>
        /// <param name="insertDtoList">对应实体数据传输对象集合</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> InsertBatchAsync(List<GenerateCodeTemplateInsertDTO> insertDtoList)
        {
		    var entities=insertDtoList.MapToList<GenerateCodeTemplateEntity>();
            var result = await commandGenerateCodeTemplateService.InsertBatchAsync(entities).ConfigureAwait(false);
            return result;
        }
       
		  
        /// <summary>
        /// 根据主键更新实体
        /// </summary>
		/// <param name="id">主键</param>
        /// <param name="updateDto">对应实体数据传输对象</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> UpdateAsync(long id,GenerateCodeTemplateUpdateDTO updateDto)
        {
		    var entity=updateDto.MapTo<GenerateCodeTemplateEntity>();
			entity.Id = id;
            var result = await commandGenerateCodeTemplateService.UpdateAsync(entity).ConfigureAwait(false);  
            return result;
        }
	  
 
       /// <summary>
        /// 根据根据主键删除
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> DeleteAsync(long id)
        {
            var result = await commandGenerateCodeTemplateService.DeleteAsync(id).ConfigureAwait(false);
            return result;
	   }


	     /// <summary>
        /// 批量删除 根据主键
        /// </summary>
        /// <param name="idList">主键集合</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> DeleteBatchAsync(IList<long> idList)
        {
            var result = await commandGenerateCodeTemplateService.DeleteBatchAsync(idList).ConfigureAwait(false);
            return result;

        }

        /// <summary>
        /// 创建条码模板
        /// </summary>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<long>> CreateTemplateAsync(GenerateCodeTemplateCreateDTO generateCodeTemplateCreateDTO)
        {
            var entity = generateCodeTemplateCreateDTO.GenerateCodeTemplateDTO.MapTo<GenerateCodeTemplateEntity>();
            var details = generateCodeTemplateCreateDTO.GenerateCodeTemplateDetailDTOs.MapToList<GenerateCodeTemplateDetailEntity>();
            return await commandGenerateCodeTemplateService.CreateTemplateAsync(entity, details);
        }
        /// <summary>
        /// 更新条码模板
        /// </summary>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> UpdateTemplateAsync(GenerateCodeTemplateEditDTO generateCodeTemplateCreateDTO)
        {
            var entity = generateCodeTemplateCreateDTO.GenerateCodeTemplateDTO.MapTo<GenerateCodeTemplateEntity>();
            var details = generateCodeTemplateCreateDTO.GenerateCodeTemplateDetailDTOs.MapToList<GenerateCodeTemplateDetailEntity>();

            return await commandGenerateCodeTemplateService.UpdateTemplateAsync(entity, details);
        }
    }
}
