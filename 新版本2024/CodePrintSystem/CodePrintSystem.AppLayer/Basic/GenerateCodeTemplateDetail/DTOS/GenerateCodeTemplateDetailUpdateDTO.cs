

//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/1/15 9:53:44
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
namespace CodePrintSystem.AppLayer.SystemSet.DTOS
{
    /// <summary>
    /// 系统设置模块-唯一编码生成模板明细
    /// </summary>
    public class GenerateCodeTemplateDetailUpdateDTO
    {
        /// <summary>
        ///  主键
        /// </summary>
        [Description("主键")]
        [Required]
        [Range(0, long.MaxValue)]
        public long Id { get; set; }

        /// <summary>
        ///  编码生成规则模板id
        /// </summary>
        [Description("编码生成规则模板id")]
        [Range(0, long.MaxValue)]
        public long? TemplateId { get; set; }

        /// <summary>
        ///  行号（不可重复）
        /// </summary>
        [Description("序号（不可重复）")]
        [Range(0, int.MaxValue)]
        public int? RowNo { get; set; }

        /// <summary>
        ///  条码规则id
        /// </summary>
        [Description("条码规则id")]
        [Range(0, long.MaxValue)]
        public long? RuleId { get; set; }

        /// <summary>
        ///  别名
        /// </summary>
        public string AliasName { get; set; }

        /// <summary>
        ///  条码规则值
        /// </summary>
        [Description("条码规则值")]
        [MinLength(1)]
        [MaxLength(50)]
        public string RuleValue { get; set; }

        /// <summary>
        ///  字典类型id，为0 表示非字典
        /// </summary>
        /// 
        [Description("字典类型")]
        [Range(0, long.MaxValue)]
        public long? DictionaryTypeId { get; set; }


        /// <summary>
        ///  值长度
        /// </summary>
        /// 
        [Description("值长度")]
        [Range(0, long.MaxValue)]
        public long? ValueLength { get; set; }

        /// <summary>
        ///  是否自定义 0否 1是
        /// </summary>
        [Description("是否自定义 0否 1是")]
        public bool? IsCustom { get; set; }

        /// <summary>
        ///  是否可用 0 否 1是
        /// </summary>
        [Description("是否可用 0 否 1是")]
        public bool? IsEnabled { get; set; }

        /// <summary>
        ///  是否已引用（已引用的不能修改或删除）
        /// </summary>
        [Description("是否已引用（已引用的不能修改或删除）")]
        public bool? IsQuote { get; set; }



    }
}
