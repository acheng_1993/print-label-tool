
//使用平台信息: ID:WinFormNoApi  描述:Winform无API
//代码版本信息: ID:None  描述:无 添加时间:2022-10-13 21:02:23
using System;
using CodePrintSystem.Core;
using ToolKitCore.MyNPOIExtension;

namespace CodePrintSystem.AppLayer.Basic.ViewObject
{
    /// <summary>
    /// 
    /// </summary>
	[MyTableName("timecodeset")]
    [MyPrimaryKey("Id",AutoIncrement =false)]
    public class TimeCodeSetView:ViewBaseField
    {
            /// <summary>
        ///  主键
        /// </summary>
        [MyExportDescription("主键", IsIgnore = false)]
        //[ExporterHeader(DisplayName = "主键", IsIgnore = false)]
        public long Id {get;set;}
        
          /// <summary>
        ///  名称
        /// </summary>
        [MyExportDescription("名称", IsIgnore = false)]
        //[ExporterHeader(DisplayName = "名称", IsIgnore = false)]
        public string Name {get;set;}
        
          /// <summary>
        ///  编码
        /// </summary>
        [MyExportDescription("编码", IsIgnore = false)]
       // [ExporterHeader(DisplayName = "编码", IsIgnore = false)]
        public string Code {get;set;}
        
          /// <summary>
        ///  编码代号
        /// </summary>
        [MyExportDescription("编码代号", IsIgnore = false)]
        //[ExporterHeader(DisplayName = "编码代号", IsIgnore = false)]
        public string TimeCodes {get;set;}
        
          /// <summary>
        ///  类型 1：比亚迪年份，2比亚迪月份
        /// </summary>
        [MyExportDescription("类型 1：比亚迪年份，2比亚迪月份", IsIgnore = false)]
        //[ExporterHeader(DisplayName = "类型 1：比亚迪年份，2比亚迪月份", IsIgnore = false)]
        public long? Type {get;set;}
        
          /// <summary>
        ///  扩展1
        /// </summary>
        [MyExportDescription("扩展1", IsIgnore = false)]
        //[ExporterHeader(DisplayName = "扩展1", IsIgnore = false)]
        public string Attribute1 {get;set;}
        
          /// <summary>
        ///  扩展2
        /// </summary>
        [MyExportDescription("扩展2", IsIgnore = false)]
        //[ExporterHeader(DisplayName = "扩展2", IsIgnore = false)]
        public string Attribute2 {get;set;}
        
     

    }
}
