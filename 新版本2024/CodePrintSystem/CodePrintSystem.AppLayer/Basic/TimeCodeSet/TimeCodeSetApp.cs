
//使用平台信息: ID:WinFormNoApi  描述:Winform无API
//代码版本信息: ID:None  描述:无 添加时间:2022-10-13 21:02:23
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Linq.Expressions;
using System;
using CodePrintSystem.Basic.Abstractions;
using CodePrintSystem.Utility;
using CodePrintSystem.Core;
using CodePrintSystem.AppLayer;
using CodePrintSystem.AppLayer.Basic.ViewObject;
using CodePrintSystem.AppLayer.Basic.DTOS;
using System.IO;
using ToolKitCore.MyNPOIExtension;

namespace CodePrintSystem.AppLayer
{
    public class TimeCodeSetApp:ISelfScopedAutoInject
    {
        private readonly ICommandTimeCodeSetService commandTimeCodeSetService;
		private readonly IQueryTimeCodeSetService queryTimeCodeSetService;


         public TimeCodeSetApp(ICommandTimeCodeSetService commandTimeCodeSetService,IQueryTimeCodeSetService queryTimeCodeSetService)
        {
            this.commandTimeCodeSetService = commandTimeCodeSetService;
			this.queryTimeCodeSetService = queryTimeCodeSetService;
        }

        /// <summary>
        /// 根据主键获取单个实体
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        public async Task<TimeCodeSetView> GetAsync(long id)
        {
		   
           return   await queryTimeCodeSetService.GetAsync<TimeCodeSetView>(id).ConfigureAwait(false);
		}


        /// <summary>
        /// 查询数据(分页) 返回指定实体T
        /// </summary>
        /// <param name="page">页码</param>
        /// <param name="size">每页数量</param>
        /// <param name="conditionDTO">查询条件类</param>
        /// <param name="field">返回字段</param>
        /// <param name="orderBy">排序字段</param>
        /// <returns></returns>
        public async Task<QueryPagedResponseModel<TimeCodeSetView>> GetListPagedAsync(int page, int size, TimeCodeSetConditionDTO conditionDTO=null, string field = null, string orderBy = null)
        {
		     var condition=conditionDTO.MapTo<BaseTimeCodeSetCondition>();
		     return await queryTimeCodeSetService.GetListPagedAsync<TimeCodeSetView>(page, size, condition, field, orderBy).ConfigureAwait(false);
        }

        /// <summary>
        /// 查询数据(不分页) 返回指定实体T
        /// </summary>
        /// <param name="conditionDTO">查询条件类</param>
        /// <param name="field">返回字段</param>
        /// <param name="orderBy">排序字段</param>
        /// <returns></returns>
        public async Task<List<TimeCodeSetView>> GetListAsync(TimeCodeSetConditionDTO  conditionDTO, string field = null, string orderBy = null)
        {
		     var condition=conditionDTO.MapTo<BaseTimeCodeSetCondition>();
             return await queryTimeCodeSetService.GetListAsync<TimeCodeSetView>(condition, field, orderBy).ConfigureAwait(false);
	    }

        /// <summary>
        /// 根据id集合获取多条数据
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<List<TimeCodeSetView>> GetListByIdsAsync(List<long> ids)
        {
             return await queryTimeCodeSetService.GetListByIdsAsync<TimeCodeSetView>(ids).ConfigureAwait(false);
	    }

	    /// <summary>
        /// 插入单个实体
        /// </summary>
        /// <param name="insertDto">对应实体数据传输对象</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<long>> InsertAsync(TimeCodeSetInsertDTO insertDto)
        { 
		    var entity=insertDto.MapTo<TimeCodeSetEntity>();
            var result = await commandTimeCodeSetService.InsertAsync(entity).ConfigureAwait(false);
            return result;
        }

		 

		   /// <summary>
        /// 批量插入实体
        /// </summary>
        /// <param name="insertDtoList">对应实体数据传输对象集合</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> InsertBatchAsync(List<TimeCodeSetInsertDTO> insertDtoList)
        {
		    var entities=insertDtoList.MapToList<TimeCodeSetEntity>();
            var result = await commandTimeCodeSetService.InsertBatchAsync(entities).ConfigureAwait(false);
            return result;
        }
       
		  
        /// <summary>
        /// 根据主键更新实体
        /// </summary>
		/// <param name="id">主键</param>
        /// <param name="updateDto">对应实体数据传输对象</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> UpdateAsync(long id,TimeCodeSetUpdateDTO updateDto)
        {
		    var entity=updateDto.MapTo<TimeCodeSetEntity>();
			entity.Id = id;
            var result = await commandTimeCodeSetService.UpdateAsync(entity).ConfigureAwait(false);  
            return result;
        }
	  
 
       /// <summary>
        /// 根据根据主键删除
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> DeleteAsync(long id)
        {
            var result = await commandTimeCodeSetService.DeleteAsync(id).ConfigureAwait(false);
            return result;
	   }


	     /// <summary>
        /// 批量删除 根据主键
        /// </summary>
        /// <param name="idList">主键集合</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> DeleteBatchAsync(IList<long> idList)
        {
            var result = await commandTimeCodeSetService.DeleteBatchAsync(idList).ConfigureAwait(false);
            return result;

        }

         /// <summary>
        /// 导出到excel
        /// </summary>
        /// <param name="conditionDTO">查询条件类</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<string>> ExportToExcelAsync(TimeCodeSetConditionDTO  conditionDTO)
        {
            HttpResponseResultModel<string> httpResponseResultModel = new HttpResponseResultModel<string>();
          	var condition=conditionDTO.MapTo<BaseTimeCodeSetCondition>();
            var views= await queryTimeCodeSetService.GetListAsync<TimeCodeSetView>(condition).ConfigureAwait(false);

            if (views.IsListNullOrEmpty())
            {
                httpResponseResultModel.IsSuccess = false;
                httpResponseResultModel.ErrorMessage = "没有数据";
                return httpResponseResultModel;
            }
            var exportModel = new ExportToExcelModel<TimeCodeSetView>()
            {
                Data = views,
                FileName = $"ImportExcel_{DateTime.Now.ToString("yyyyMMddHHmmss")}.xls",
                Path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", $"ImportExcel/"),
                SheetName = "Sheet1",
            };
            await ExportImportExcelHelper.ExportToExcelAsync(exportModel);
            httpResponseResultModel.IsSuccess = true;
            httpResponseResultModel.BackResult = $"ImportExcel/" + exportModel.FileName;
            return httpResponseResultModel;
        }

    }
}
