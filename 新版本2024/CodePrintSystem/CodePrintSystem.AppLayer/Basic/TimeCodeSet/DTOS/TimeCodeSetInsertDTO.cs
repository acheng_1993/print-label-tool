

//使用平台信息: ID:WinFormNoApi  描述:Winform无API
//代码版本信息: ID:None  描述:无 添加时间:2022-10-13 21:02:23
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
namespace CodePrintSystem.AppLayer.Basic.DTOS
{
    /// <summary>
    /// 
    /// </summary>
    public class TimeCodeSetInsertDTO
    {
            /// <summary>
        ///  名称
        /// </summary>
        [Description("名称")]
                 public string Name {get;set;}
        
          /// <summary>
        ///  编码
        /// </summary>
        [Description("编码")]
                 public string Code {get;set;}
        
          /// <summary>
        ///  编码代号
        /// </summary>
        [Description("编码代号")]
                 public string TimeCodes {get;set;}
        
          /// <summary>
        ///  类型 1：比亚迪年份，2比亚迪月份
        /// </summary>
        [Description("类型 1：比亚迪年份，2比亚迪月份")]
                 public long? Type {get;set;}
        
          /// <summary>
        ///  扩展1
        /// </summary>
        [Description("扩展1")]
                 public string Attribute1 {get;set;}
        
          /// <summary>
        ///  扩展2
        /// </summary>
        [Description("扩展2")]
                 public string Attribute2 {get;set;}
        
     

    }
}
