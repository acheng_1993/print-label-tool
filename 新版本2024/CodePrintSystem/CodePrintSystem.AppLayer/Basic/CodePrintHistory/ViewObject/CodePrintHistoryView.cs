
//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/1/30 14:05:50
using System;
using CodePrintSystem.Core;
using ToolKitCore.MyNPOIExtension;

namespace CodePrintSystem.AppLayer.Basic.ViewObject
{
    /// <summary>
    /// 
    /// </summary>
	[MyTableName("codeprinthistory")]
    [MyPrimaryKey("Id", AutoIncrement = false)]
    public class CodePrintHistoryView : ViewBaseField
    {
        /// <summary>
        ///  主键
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        ///  完整条码
        /// </summary>
        /// 
        [MyExportDescription("条码")]
        public string Code { get; set; }

        /// <summary>
        ///  供应商代码
        /// </summary>
        public string SupplierCode { get; set; }

        /// <summary>
        ///  当前日期YYYYMMDD
        /// </summary>
        public DateTime CurrentDate { get; set; }

        /// <summary>
        ///  物料码
        /// </summary>
        /// 
        [MyExportDescription("产品编码")]
        public string ItemCode { get; set; }

        /// <summary>
        ///  版本
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        ///  工厂代码
        /// </summary>
        public string FactoryCode { get; set; }

        /// <summary>
        ///  年
        /// </summary>
        public string Year { get; set; }

        /// <summary>
        ///  周
        /// </summary>
        public string Week { get; set; }

        /// <summary>
        ///  天
        /// </summary>
        public string Day { get; set; }

        /// <summary>
        ///  流水号
        /// </summary>
        public string SerialNumber { get; set; }

        /// <summary>
        ///  0:等待打印，1：打印成功
        /// </summary>
        public int? Status { get; set; }

        /// <summary>
        /// 状态描述
        /// </summary>
        [MyResultColumn]
        public string StatusDesc
        {
            get
            {
                if (Status.HasValue)
                {
                    if(Status.Value==1)
                    {
                        return "成功";
                    }else
                    {
                        return "失败";
                    }
                }
                else
                {
                    return "失败";
                }
            }
        }



        /// <summary>
        ///  打印时间
        /// </summary>
        /// 
        [MyExportDescription("打印时间")]
        public DateTime? PrintTime { get; set; }


        /// <summary>
        /// 
        /// </summary>
        /// 
        [MyResultColumn]
        public string StrPrintTime { get { return PrintTime?.ToString("yyyy-MM-dd HH:mm:ss"); } }

        /// <summary>
        ///  版本
        /// </summary>
        public string VersionCode { get; set; }

        /// <summary>
        ///  生产制令号
        /// </summary>
        public string ProductionCode { get; set; }

        /// <summary>
        ///  操作员
        /// </summary>
        public string OperateName { get; set; }



    }
}
