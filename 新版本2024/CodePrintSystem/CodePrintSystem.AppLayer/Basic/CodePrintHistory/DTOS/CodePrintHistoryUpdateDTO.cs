

//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/1/30 14:05:50
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
namespace CodePrintSystem.AppLayer.Basic.DTOS
{
    /// <summary>
    /// 
    /// </summary>
    public class CodePrintHistoryUpdateDTO
    {
        /// <summary>
        ///  主键
        /// </summary>
        [Description("主键")]
        [Required]
        [Range(0, long.MaxValue)]
        public long Id { get; set; }

        /// <summary>
        ///  完整条码
        /// </summary>
        [Description("完整条码")]
        [MinLength(0)]
        [MaxLength(50)]
        public string Code { get; set; }

        /// <summary>
        ///  供应商代码
        /// </summary>
        [Description("供应商代码")]
        [MinLength(0)]
        [MaxLength(50)]
        public string SupplierCode { get; set; }

        /// <summary>
        ///  当前日期YYYYMMDD
        /// </summary>
        [Description("当前日期")]
        public DateTime CurrentDate { get; set; } = DateTime.Now;

        /// <summary>
        ///  产品编码
        /// </summary>
        [Description("产品编码")]
        [MinLength(0)]
        [MaxLength(50)]
        public string ItemCode { get; set; }

        /// <summary>
        ///  版本
        /// </summary>
        [Description("版本")]
        [MinLength(0)]
        [MaxLength(10)]
        public string Version { get; set; }

        /// <summary>
        ///  工厂代码
        /// </summary>
        [Description("工厂代码")]
        [MinLength(0)]
        [MaxLength(50)]
        public string FactoryCode { get; set; }

        /// <summary>
        ///  年
        /// </summary>
        [Description("年")]
        [MinLength(0)]
        [MaxLength(5)]
        public string Year { get; set; }

        /// <summary>
        ///  周
        /// </summary>
        [Description("周")]
        [MinLength(0)]
        [MaxLength(5)]
        public string Week { get; set; }

        /// <summary>
        ///  天
        /// </summary>
        [Description("天")]
        [MinLength(0)]
        [MaxLength(5)]
        public string Day { get; set; }

        /// <summary>
        ///  流水号
        /// </summary>
        [Description("流水号")]
        [MinLength(0)]
        [MaxLength(10)]
        public string SerialNumber { get; set; }

        /// <summary>
        ///  0:等待打印，1：打印成功
        /// </summary>
        [Description("0:等待打印，1：打印成功")]
        [Range(0, int.MaxValue)]
        public int? Status { get; set; }

        /// <summary>
        ///  打印时间
        /// </summary>
        [Description("打印时间")]
        public DateTime? PrintTime { get; set; }

        /// <summary>
        ///  版本
        /// </summary>
        [Description("版本")]
        [MinLength(0)]
        [MaxLength(10)]
        public string VersionCode { get; set; }

        /// <summary>
        ///  生产制令号
        /// </summary>
        [Description("生产制令号")]
        [MinLength(0)]
        [MaxLength(255)]
        public string ProductionCode { get; set; }

        /// <summary>
        ///  操作员
        /// </summary>
        [Description("操作员")]
        [MinLength(0)]
        [MaxLength(255)]
        public string OperateName { get; set; }



    }
}
