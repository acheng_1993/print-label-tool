

//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/11/8 23:23:17
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
namespace CodePrintSystem.AppLayer.DTOS
{
    /// <summary>
    /// 
    /// </summary>
    public class PrintPlanUpdateDTO
    {
            /// <summary>
        ///  主键
        /// </summary>
        [Description("主键")]
                  [Required]
                   [Range(0, int.MaxValue)]
                  public long Id {get;set;}
        
          /// <summary>
        ///  料号
        /// </summary>
        [Description("料号")]
                  [MinLength(0)]
                   [MaxLength(100)]
                  public string ItemCode {get;set;}
        
          /// <summary>
        ///  计划日期
        /// </summary>
        [Description("计划日期")]
                 public DateTime? PlanDate {get;set;}
        
          /// <summary>
        ///  计划数量
        /// </summary>
        [Description("计划数量")]
                  [Range(0, int.MaxValue)]
                  public int? PlanQty {get;set;}
        
     

    }
}
