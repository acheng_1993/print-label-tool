
//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/11/8 23:23:17
using System;
using CodePrintSystem.Core;
namespace CodePrintSystem.AppLayer.ViewObject
{
    /// <summary>
    /// 
    /// </summary>
	[MyTableName("PrintPlan")]
    [MyPrimaryKey("Id",AutoIncrement =false)]
    public class PrintPlanView:ViewBaseField
    {
            /// <summary>
        ///  主键
        /// </summary>
        public long Id {get;set;}
        
          /// <summary>
        ///  料号
        /// </summary>
        public string ItemCode {get;set;}
        
          /// <summary>
        ///  计划日期
        /// </summary>
        public DateTime? PlanDate {get;set;}
        
          /// <summary>
        ///  计划数量
        /// </summary>
        public int? PlanQty {get;set;}
        
     

    }
}
