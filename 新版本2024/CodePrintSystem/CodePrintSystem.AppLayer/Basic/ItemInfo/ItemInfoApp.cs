
//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/1/30 14:06:32
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Linq.Expressions;
using System;
using CodePrintSystem.Basic.Abstractions;
using CodePrintSystem.Utility;
using CodePrintSystem.Core;
using CodePrintSystem.AppLayer;
using CodePrintSystem.AppLayer.Basic.ViewObject;
using CodePrintSystem.AppLayer.Basic.DTOS;
using CodePrintSystem.AppLayer.SystemSet.ViewObject;

namespace CodePrintSystem.AppLayer
{
    public class ItemInfoApp : ISelfScopedAutoInject
    {
        private readonly ICommandItemInfoService commandItemInfoService;
        private readonly IQueryItemInfoService queryItemInfoService;
        private readonly IQueryGenerateCodeTemplateService queryGenerateCodeTemplateService;


        public ItemInfoApp(ICommandItemInfoService commandItemInfoService,
            IQueryItemInfoService queryItemInfoService,
            IQueryGenerateCodeTemplateService queryGenerateCodeTemplateService)
        {
            this.commandItemInfoService = commandItemInfoService;
            this.queryItemInfoService = queryItemInfoService;
            this.queryGenerateCodeTemplateService = queryGenerateCodeTemplateService;
        }

        /// <summary>
        /// 根据主键获取单个实体
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        public async Task<ItemInfoView> GetAsync(long id)
        {

            return await queryItemInfoService.GetAsync<ItemInfoView>(id).ConfigureAwait(false);
        }


        /// <summary>
        /// 查询数据(分页) 返回指定实体T
        /// </summary>
        /// <param name="page">页码</param>
        /// <param name="size">每页数量</param>
        /// <param name="conditionDTO">查询条件类</param>
        /// <param name="field">返回字段</param>
        /// <param name="orderBy">排序字段</param>
        /// <returns></returns>
        public async Task<QueryPagedResponseModel<ItemInfoView>> GetListPagedAsync(int page, int size, ItemInfoConditionDTO conditionDTO = null, string field = null, string orderBy = null)
        {
            var condition = conditionDTO.MapTo<BaseItemInfoCondition>();
            var data = await queryItemInfoService.GetListPagedAsync<ItemInfoView>(page, size, condition, field, orderBy).ConfigureAwait(false);
            if (!data.Data.IsListNullOrEmpty())
            {
                var templateIds = data.Data.Where(t => t.TemplateId > 0).Select(t => t.TemplateId).ToList();
                if (templateIds.Any())
                {
                    var templates = await queryGenerateCodeTemplateService.GetListByIdsAsync<GenerateCodeTemplateView>(templateIds);

                    foreach (var itemInfo in data.Data)
                    {
                        var template = templates.SingleOrDefault(t => t.Id == itemInfo.TemplateId);
                        itemInfo.TemplateName = template?.Name;
                        itemInfo.IsImportTemplate = template?.IsImportTemplate;
                    }
                }
            }
            return data;
        }

        /// <summary>
        /// 查询数据(不分页) 返回指定实体T
        /// </summary>
        /// <param name="conditionDTO">查询条件类</param>
        /// <param name="field">返回字段</param>
        /// <param name="orderBy">排序字段</param>
        /// <returns></returns>
        public async Task<List<ItemInfoView>> GetListAsync(ItemInfoConditionDTO conditionDTO, string field = null, string orderBy = null)
        {
            var condition = conditionDTO.MapTo<BaseItemInfoCondition>();
            var list = await queryItemInfoService.GetListAsync<ItemInfoView>(condition, field, orderBy).ConfigureAwait(false);
            return list;
        }

        /// <summary>
        /// 根据id集合获取多条数据
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<List<ItemInfoView>> GetListByIdsAsync(List<long> ids)
        {
            return await queryItemInfoService.GetListByIdsAsync<ItemInfoView>(ids).ConfigureAwait(false);
        }

        /// <summary>
        /// 插入单个实体
        /// </summary>
        /// <param name="insertDto">对应实体数据传输对象</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<long>> InsertAsync(ItemInfoInsertDTO insertDto)
        {
            var entity = insertDto.MapTo<ItemInfoEntity>();
            var result = await commandItemInfoService.InsertAsync(entity).ConfigureAwait(false);
            return result;
        }



        /// <summary>
        /// 批量插入实体
        /// </summary>
        /// <param name="insertDtoList">对应实体数据传输对象集合</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> InsertBatchAsync(List<ItemInfoInsertDTO> insertDtoList)
        {
            var entities = insertDtoList.MapToList<ItemInfoEntity>();
            var result = await commandItemInfoService.InsertBatchAsync(entities).ConfigureAwait(false);
            return result;
        }


        /// <summary>
        /// 根据主键更新实体
        /// </summary>
        /// <param name="id">主键</param>
        /// <param name="updateDto">对应实体数据传输对象</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> UpdateAsync(long id, ItemInfoUpdateDTO updateDto)
        {
            var entity = updateDto.MapTo<ItemInfoEntity>();
            entity.Id = id;
            var result = await commandItemInfoService.UpdateAsync(entity).ConfigureAwait(false);
            return result;
        }


        /// <summary>
        /// 根据根据主键删除
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> DeleteAsync(long id)
        {
            var result = await commandItemInfoService.DeleteAsync(id).ConfigureAwait(false);
            return result;
        }


        /// <summary>
        /// 批量删除 根据主键
        /// </summary>
        /// <param name="idList">主键集合</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> DeleteBatchAsync(IList<long> idList)
        {
            var result = await commandItemInfoService.DeleteBatchAsync(idList).ConfigureAwait(false);
            return result;

        }



    }
}
