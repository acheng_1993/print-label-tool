

//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/1/30 14:06:32
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
namespace CodePrintSystem.AppLayer.Basic.DTOS
{
    /// <summary>
    /// 
    /// </summary>
    public class ItemInfoUpdateDTO
    {
            /// <summary>
        ///  主键
        /// </summary>
        [Description("主键")]
                  [Required]
                   [Range(0, long.MaxValue)]
                  public long Id {get;set;}
        
          /// <summary>
        ///  名称
        /// </summary>
        [Description("名称")]
                  [MinLength(0)]
                   [MaxLength(50)]
                  public string Name {get;set;}
        
          /// <summary>
        ///  编码
        /// </summary>
        [Description("编码")]
                  [MinLength(0)]
                   [MaxLength(50)]
                  public string Code {get;set;}
        
          /// <summary>
        ///  版本
        /// </summary>
        [Description("版本")]
                  [MinLength(0)]
                   [MaxLength(50)]
                  public string VersionCode {get;set;}
        
          /// <summary>
        ///  扩展属性1
        /// </summary>
        [Description("扩展属性1")]
                  [MinLength(0)]
                   [MaxLength(50)]
                  public string Attribute1 {get;set;}
        
          /// <summary>
        ///  扩展属性2
        /// </summary>
        [Description("扩展属性2")]
                  [MinLength(0)]
                   [MaxLength(50)]
                  public string Attribute2 {get;set;}
        
          /// <summary>
        ///  扩展属性3
        /// </summary>
        [Description("扩展属性3")]
                  [MinLength(0)]
                   [MaxLength(50)]
                  public string Attribute3 {get;set;}
        
          /// <summary>
        ///  扩展属性4
        /// </summary>
        [Description("扩展属性4")]
                  [MinLength(0)]
                   [MaxLength(50)]
                  public string Attribute4 {get;set;}
        
     

    }
}
