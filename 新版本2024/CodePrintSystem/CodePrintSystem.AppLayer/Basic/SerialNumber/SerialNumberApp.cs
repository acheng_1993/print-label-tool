
//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/1/30 14:08:58
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Linq.Expressions;
using System;
using CodePrintSystem.Basic.Abstractions;
using CodePrintSystem.Utility;
using CodePrintSystem.Core;
using CodePrintSystem.AppLayer;
using CodePrintSystem.AppLayer.Basic.ViewObject;
using CodePrintSystem.AppLayer.Basic.DTOS;
namespace CodePrintSystem.AppLayer
{
    public class SerialNumberApp:ISelfScopedAutoInject
    {
        private readonly ICommandSerialNumberService commandSerialNumberService;
		private readonly IQuerySerialNumberService querySerialNumberService;


         public SerialNumberApp(ICommandSerialNumberService commandSerialNumberService,IQuerySerialNumberService querySerialNumberService)
        {
            this.commandSerialNumberService = commandSerialNumberService;
			this.querySerialNumberService = querySerialNumberService;
        }

        /// <summary>
        /// 根据主键获取单个实体
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        public async Task<SerialNumberView> GetAsync(long id)
        {
		   
           return   await querySerialNumberService.GetAsync<SerialNumberView>(id).ConfigureAwait(false);
		}


        /// <summary>
        /// 查询数据(分页) 返回指定实体T
        /// </summary>
        /// <param name="page">页码</param>
        /// <param name="size">每页数量</param>
        /// <param name="conditionDTO">查询条件类</param>
        /// <param name="field">返回字段</param>
        /// <param name="orderBy">排序字段</param>
        /// <returns></returns>
        public async Task<QueryPagedResponseModel<SerialNumberView>> GetListPagedAsync(int page, int size, SerialNumberConditionDTO conditionDTO=null, string field = null, string orderBy = null)
        {
		     var condition=conditionDTO.MapTo<BaseSerialNumberCondition>();
		     return await querySerialNumberService.GetListPagedAsync<SerialNumberView>(page, size, condition, field, orderBy).ConfigureAwait(false);
        }

        /// <summary>
        /// 查询数据(不分页) 返回指定实体T
        /// </summary>
        /// <param name="conditionDTO">查询条件类</param>
        /// <param name="field">返回字段</param>
        /// <param name="orderBy">排序字段</param>
        /// <returns></returns>
        public async Task<List<SerialNumberView>> GetListAsync(SerialNumberConditionDTO  conditionDTO, string field = null, string orderBy = null)
        {
		     var condition=conditionDTO.MapTo<BaseSerialNumberCondition>();
             return await querySerialNumberService.GetListAsync<SerialNumberView>(condition, field, orderBy).ConfigureAwait(false);
	    }

        /// <summary>
        /// 根据id集合获取多条数据
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<List<SerialNumberView>> GetListByIdsAsync(List<long> ids)
        {
             return await querySerialNumberService.GetListByIdsAsync<SerialNumberView>(ids).ConfigureAwait(false);
	    }

	    /// <summary>
        /// 插入单个实体
        /// </summary>
        /// <param name="insertDto">对应实体数据传输对象</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<long>> InsertAsync(SerialNumberInsertDTO insertDto)
        { 
		    var entity=insertDto.MapTo<SerialNumberEntity>();
            var result = await commandSerialNumberService.InsertAsync(entity).ConfigureAwait(false);
            return result;
        }

		 

		   /// <summary>
        /// 批量插入实体
        /// </summary>
        /// <param name="insertDtoList">对应实体数据传输对象集合</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> InsertBatchAsync(List<SerialNumberInsertDTO> insertDtoList)
        {
		    var entities=insertDtoList.MapToList<SerialNumberEntity>();
            var result = await commandSerialNumberService.InsertBatchAsync(entities).ConfigureAwait(false);
            return result;
        }
       
		  
        /// <summary>
        /// 根据主键更新实体
        /// </summary>
		/// <param name="id">主键</param>
        /// <param name="updateDto">对应实体数据传输对象</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> UpdateAsync(long id,SerialNumberUpdateDTO updateDto)
        {
		    var entity=updateDto.MapTo<SerialNumberEntity>();
			entity.Id = id;
            var result = await commandSerialNumberService.UpdateAsync(entity).ConfigureAwait(false);  
            return result;
        }
	  
 
       /// <summary>
        /// 根据根据主键删除
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> DeleteAsync(long id)
        {
            var result = await commandSerialNumberService.DeleteAsync(id).ConfigureAwait(false);
            return result;
	   }


	     /// <summary>
        /// 批量删除 根据主键
        /// </summary>
        /// <param name="idList">主键集合</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> DeleteBatchAsync(IList<long> idList)
        {
            var result = await commandSerialNumberService.DeleteBatchAsync(idList).ConfigureAwait(false);
            return result;

        }

       

    }
}
