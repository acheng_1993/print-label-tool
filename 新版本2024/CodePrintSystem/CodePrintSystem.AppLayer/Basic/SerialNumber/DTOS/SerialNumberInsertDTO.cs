

//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/1/30 14:08:58
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
namespace CodePrintSystem.AppLayer.Basic.DTOS
{
    /// <summary>
    /// 
    /// </summary>
    public class SerialNumberInsertDTO
    {
            /// <summary>
        ///  物料码
        /// </summary>
        [Description("物料码")]
                  [MinLength(0)]
                   [MaxLength(50)]
                  public string ItemCode {get;set;}
        
          /// <summary>
        ///  当前日期
        /// </summary>
        [Description("当前日期")]
                  [MinLength(0)]
                   [MaxLength(50)]
                  public string CurrentDate {get;set;}
        
          /// <summary>
        ///  年
        /// </summary>
        [Description("年")]
                  [MinLength(0)]
                   [MaxLength(5)]
                  public string Year {get;set;}
        
          /// <summary>
        ///  周
        /// </summary>
        [Description("周")]
                  [MinLength(0)]
                   [MaxLength(5)]
                  public string Week {get;set;}
        
          /// <summary>
        ///  天
        /// </summary>
        [Description("天")]
                  [MinLength(0)]
                   [MaxLength(5)]
                  public string Day {get;set;}
        
          /// <summary>
        ///  流水号
        /// </summary>
        [Description("流水号")]
                  [Range(0, int.MaxValue)]
                  public int? SerialNumber {get;set;}
        
     

    }
}
