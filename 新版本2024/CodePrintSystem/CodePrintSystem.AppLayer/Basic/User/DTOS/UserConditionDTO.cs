

//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/1/30 14:16:16
using System;
using System.Collections.Generic;
namespace CodePrintSystem.AppLayer.Basic.DTOS
{
    /// <summary>
    /// 
    /// </summary>
    public class UserConditionDTO
    {
            /// <summary>
        ///  是否管理员
        /// </summary>
        public bool? IsAdmin {get;set;}
        
           /// <summary>
        ///  用户名
        /// </summary>
        public string UserName {get;set;}
        
           /// <summary>
        ///  用户密码
        /// </summary>
        public string UserPassword {get;set;}
        
           /// <summary>
        ///  最近登录时间起始时间
        /// </summary>
        public DateTime? LastLoginTimeStart {get;set;}
        
           /// <summary>
        ///  最近登录时间结束时间
        /// </summary>
        public DateTime? LastLoginTimeEnd {get;set;}
        
           /// <summary>
        ///  是否启用
        /// </summary>
        public bool? IsEnabled {get;set;}
        
      

    }
}
