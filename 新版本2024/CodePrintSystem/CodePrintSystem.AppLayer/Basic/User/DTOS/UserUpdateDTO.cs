

//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/1/30 14:16:16
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
namespace CodePrintSystem.AppLayer.Basic.DTOS
{
    /// <summary>
    /// 
    /// </summary>
    public class UserUpdateDTO
    {
            /// <summary>
        ///  主键
        /// </summary>
        [Description("主键")]
                  [Required]
                   [Range(0, long.MaxValue)]
                  public long Id {get;set;}
        
          /// <summary>
        ///  是否管理员
        /// </summary>
        [Description("是否管理员")]
                 public bool? IsAdmin {get;set;}
        
          /// <summary>
        ///  用户名
        /// </summary>
        [Description("用户名")]
                  [MinLength(0)]
                   [MaxLength(50)]
                  public string UserName {get;set;}
        
          /// <summary>
        ///  用户密码
        /// </summary>
        [Description("用户密码")]
                  [MinLength(0)]
                   [MaxLength(50)]
                  public string UserPassword {get;set;}
        
          /// <summary>
        ///  最近登录时间
        /// </summary>
        [Description("最近登录时间")]
                 public DateTime? LastLoginTime {get;set;}
        
          /// <summary>
        ///  是否启用
        /// </summary>
        [Description("是否启用")]
                 public bool? IsEnabled {get;set;}
        
     

    }
}
