
//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/2/3 10:06:03
using System;
using CodePrintSystem.Core;
using ToolKitCore.MyNPOIExtension;

namespace CodePrintSystem.AppLayer.Basic.ViewObject
{
    /// <summary>
    /// 
    /// </summary>
	[MyTableName("CodeScanHistory")]
    [MyPrimaryKey("Id",AutoIncrement =false)]
    public class CodeScanHistoryView:ViewBaseField
    {
            /// <summary>
        ///  主键
        /// </summary>
        public long Id {get;set;}

        /// <summary>
        ///  编码
        /// </summary>
        [MyExportDescription("条码")]
        public string Code {get;set;}

        /// <summary>
        ///  生产制令号
        /// </summary>
        [MyExportDescription("生产制令号")]
        public string ProductionCode {get;set;}

        /// <summary>
        ///  1：正常，2：重复，3：错误
        /// </summary>
        public int? Status {get;set;}

        [MyResultColumn]
        [MyExportDescription("状态值描述")]
        public string StatusName { get; set; }

        /// <summary>
        ///  物料码
        /// </summary>
        [MyExportDescription("产品编码")]
        public string ItemCode {get;set;}

        /// <summary>
        ///  错误内容
        /// </summary>
        [MyExportDescription("错误内容")]
        public string ErrorContent {get;set;}
        
     

    }
}
