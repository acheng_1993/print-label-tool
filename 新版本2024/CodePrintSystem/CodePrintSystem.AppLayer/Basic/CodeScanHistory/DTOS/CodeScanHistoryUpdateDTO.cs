

//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/2/3 10:06:03
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
namespace CodePrintSystem.AppLayer.Basic.DTOS
{
    /// <summary>
    /// 
    /// </summary>
    public class CodeScanHistoryUpdateDTO
    {
            /// <summary>
        ///  主键
        /// </summary>
        [Description("主键")]
                  [Required]
                   [Range(0, long.MaxValue)]
                  public long Id {get;set;}
        
          /// <summary>
        ///  编码
        /// </summary>
        [Description("编码")]
                  [MinLength(0)]
                   [MaxLength(50)]
                  public string Code {get;set;}
        
          /// <summary>
        ///  生产制令号
        /// </summary>
        [Description("生产制令号")]
                  [MinLength(0)]
                   [MaxLength(50)]
                  public string ProductionCode {get;set;}
        
          /// <summary>
        ///  1：正常，2：重复，3：错误
        /// </summary>
        [Description("1：正常，2：重复，3：错误")]
                  [Range(0, int.MaxValue)]
                  public int? Status {get;set;}
        
          /// <summary>
        ///  物料码
        /// </summary>
        [Description("产品编码")]
                  [MinLength(0)]
                   [MaxLength(50)]
                  public string ItemCode {get;set;}
        
          /// <summary>
        ///  错误内容
        /// </summary>
        [Description("错误内容")]
                  [MinLength(0)]
                   [MaxLength(200)]
                  public string ErrorContent {get;set;}
        
     

    }
}
