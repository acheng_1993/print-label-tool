
//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/2/3 10:06:03
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Linq.Expressions;
using System;
using CodePrintSystem.Basic.Abstractions;
using CodePrintSystem.Utility;
using CodePrintSystem.Core;
using CodePrintSystem.AppLayer;
using CodePrintSystem.AppLayer.Basic.ViewObject;
using CodePrintSystem.AppLayer.Basic.DTOS;
namespace CodePrintSystem.AppLayer
{
    public class CodeScanHistoryApp:ISelfScopedAutoInject
    {
        private readonly ICommandCodeScanHistoryService commandCodeScanHistoryService;
		private readonly IQueryCodeScanHistoryService queryCodeScanHistoryService;


         public CodeScanHistoryApp(ICommandCodeScanHistoryService commandCodeScanHistoryService,IQueryCodeScanHistoryService queryCodeScanHistoryService)
        {
            this.commandCodeScanHistoryService = commandCodeScanHistoryService;
			this.queryCodeScanHistoryService = queryCodeScanHistoryService;
        }

        /// <summary>
        /// 根据主键获取单个实体
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        public async Task<CodeScanHistoryView> GetAsync(long id)
        {
		   
           return   await queryCodeScanHistoryService.GetAsync<CodeScanHistoryView>(id).ConfigureAwait(false);
		}


        /// <summary>
        /// 查询数据(分页) 返回指定实体T
        /// </summary>
        /// <param name="page">页码</param>
        /// <param name="size">每页数量</param>
        /// <param name="conditionDTO">查询条件类</param>
        /// <param name="field">返回字段</param>
        /// <param name="orderBy">排序字段</param>
        /// <returns></returns>
        public async Task<QueryPagedResponseModel<CodeScanHistoryView>> GetListPagedAsync(int page, int size, CodeScanHistoryConditionDTO conditionDTO=null, string field = null, string orderBy = null)
        {
		     var condition=conditionDTO.MapTo<BaseCodeScanHistoryCondition>();
		     return await queryCodeScanHistoryService.GetListPagedAsync<CodeScanHistoryView>(page, size, condition, field, orderBy).ConfigureAwait(false);
        }

        /// <summary>
        /// 查询数据(不分页) 返回指定实体T
        /// </summary>
        /// <param name="conditionDTO">查询条件类</param>
        /// <param name="field">返回字段</param>
        /// <param name="orderBy">排序字段</param>
        /// <returns></returns>
        public async Task<List<CodeScanHistoryView>> GetListAsync(CodeScanHistoryConditionDTO  conditionDTO, string field = null, string orderBy = null)
        {
		     var condition=conditionDTO.MapTo<BaseCodeScanHistoryCondition>();
             return await queryCodeScanHistoryService.GetListAsync<CodeScanHistoryView>(condition, field, orderBy).ConfigureAwait(false);
	    }

        /// <summary>
        /// 根据id集合获取多条数据
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<List<CodeScanHistoryView>> GetListByIdsAsync(List<long> ids)
        {
             return await queryCodeScanHistoryService.GetListByIdsAsync<CodeScanHistoryView>(ids).ConfigureAwait(false);
	    }

	    /// <summary>
        /// 插入单个实体
        /// </summary>
        /// <param name="insertDto">对应实体数据传输对象</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<long>> InsertAsync(CodeScanHistoryInsertDTO insertDto)
        { 
		    var entity=insertDto.MapTo<CodeScanHistoryEntity>();
            var result = await commandCodeScanHistoryService.InsertAsync(entity).ConfigureAwait(false);
            return result;
        }

		 

		   /// <summary>
        /// 批量插入实体
        /// </summary>
        /// <param name="insertDtoList">对应实体数据传输对象集合</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> InsertBatchAsync(List<CodeScanHistoryInsertDTO> insertDtoList)
        {
		    var entities=insertDtoList.MapToList<CodeScanHistoryEntity>();
            var result = await commandCodeScanHistoryService.InsertBatchAsync(entities).ConfigureAwait(false);
            return result;
        }
       
		  
        /// <summary>
        /// 根据主键更新实体
        /// </summary>
		/// <param name="id">主键</param>
        /// <param name="updateDto">对应实体数据传输对象</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> UpdateAsync(long id,CodeScanHistoryUpdateDTO updateDto)
        {
		    var entity=updateDto.MapTo<CodeScanHistoryEntity>();
			entity.Id = id;
            var result = await commandCodeScanHistoryService.UpdateAsync(entity).ConfigureAwait(false);  
            return result;
        }
	  
 
       /// <summary>
        /// 根据根据主键删除
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> DeleteAsync(long id)
        {
            var result = await commandCodeScanHistoryService.DeleteAsync(id).ConfigureAwait(false);
            return result;
	   }


	     /// <summary>
        /// 批量删除 根据主键
        /// </summary>
        /// <param name="idList">主键集合</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> DeleteBatchAsync(IList<long> idList)
        {
            var result = await commandCodeScanHistoryService.DeleteBatchAsync(idList).ConfigureAwait(false);
            return result;

        }

       

    }
}
