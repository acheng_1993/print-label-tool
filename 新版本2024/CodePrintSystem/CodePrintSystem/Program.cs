using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using CodePrintSystem.Core;
using CodePrintSystem.Forms;
using CodePrintSystem.Utility;
using Microsoft.Extensions.DependencyInjection;

namespace CodePrintSystem.AppLayer
{
    static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //UI线程异常
            Application.ThreadException += Application_ThreadException;

            //多线程异常
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;


            if (IsExistProcess())
            {
                MessageBox.Show("对不起,该系统不允许同时运行多个实例!", "系统提示", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var services = new ServiceCollection();
            //添加服务注册
            RegisterService.RegisterComponents(services);
            var serviceProvider = services.BuildServiceProvider();
            //// 插入日志例子
            //LogModel logModel = new LogModel();
            //logModel.LogMessage = "test";
            //var logger = serviceProvider.GetRequiredService<TPLLogger>();
            //logger.InsertAsync(logModel);
            string version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            CurrentUserConfig.Version = version;
            AppsettingsConfig.ServiceProvider = serviceProvider;
            AppsettingsConfig.DefaultConnectionString = ConfigurationManager.AppSettings["ConnectionString"];
            AppsettingsConfig.PrintInterval = Convert.ToInt32(ConfigurationManager.AppSettings["PrintInterval"]);
            AppsettingsConfig.PrintTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["PrintTimeout"]);
            AppsettingsConfig.PrinterName = ConfigurationManager.AppSettings["PrinterName"];
            AppsettingsConfig.ExportExcelPath = ConfigurationManager.AppSettings["ExportExcelPath"];
            AppsettingsConfig.VersionCodeLength = Convert.ToInt32(ConfigurationManager.AppSettings["VersionCodeLength"]);
            AppsettingsConfig.ItemCodeLength = Convert.ToInt32(ConfigurationManager.AppSettings["ItemCodeLength"]);
            AppsettingsConfig.SupplierCodeLength = Convert.ToInt32(ConfigurationManager.AppSettings["SupplierCodeLength"]);
            AppsettingsConfig.TaskDelayMillisecond = Convert.ToInt32(ConfigurationManager.AppSettings["TaskDelayMillisecond"]);
            AppsettingsConfig.IsFingerprint = Convert.ToBoolean(ConfigurationManager.AppSettings["IsFingerprint"]);
            AppsettingsConfig.DatabaseType = ConfigurationManager.AppSettings["DatabaseType"];
            AppsettingsConfig.BartenderSharePath = ConfigurationManager.AppSettings["BartenderSharePath"];
            int iSeedDataCenterId = 7;
            Random randomDataCenterId = new Random(iSeedDataCenterId);
            var dataCenterId = randomDataCenterId.Next(0, 31);
            AppsettingsConfig.DataCenterId = Convert.ToInt32(ConfigurationManager.AppSettings["DataCenterId"]) > 0 ? Convert.ToInt32(ConfigurationManager.AppSettings["DataCenterId"]) : dataCenterId;

            int iSeed = 6;
            Random random = new Random(iSeed);
            var machineId = random.Next(0, 31);
            AppsettingsConfig.MachineId = Convert.ToInt32(ConfigurationManager.AppSettings["MachineId"])>0? Convert.ToInt32(ConfigurationManager.AppSettings["MachineId"]) : machineId;  
            ///CreateScope 每次请求都是一个新的实例，不然一直都是同一个实例(请参考上面的例子)
            using (var scope = serviceProvider.CreateScope())
            {
                var form = scope.ServiceProvider.GetRequiredService<Login>();
                //var form = scope.ServiceProvider.GetRequiredService<PlanBoardForm>();
                Application.Run(form);
            }

        }

        static bool canCreateNew; //用来承接是否已经有一个实例在运行的布尔变量
        //限制单例运行
        static readonly Mutex m = new Mutex(
         true //如果为 true，则给予调用线程已命名的系统互斥体搜索的
              //初始所属权（如果已命名的系统互斥体是通过此调用创建的）；
              //否则为 false。
         , "CodePrintSystem1" //Mutex 的名称。如果值为 null,
                             //则 Mutex 是未命名的。
         , out canCreateNew //在此方法返回时，如果创建了局部互斥体
                            //（即，如果name为null或空字符串）或指定的命名系统互斥体，
                            //则包含布尔值 true；如果指定的命名系统互斥体已存在，
                            //则为 false。该参数未经初始化即被传递。
         );
        /// <summary>
        /// 判断当前实例是否已经运行
        /// </summary>
        /// <returns></returns>
        static bool IsExistProcess()
        {
            //if (Process.GetProcessesByName(Process.GetCurrentProcess().ProcessName).Length > 1)
            //    return true;
            //else
            //    return false;

            if (canCreateNew)
            {

                return false;
            }
            else
            {
                //
                return true;
            }

        }

        #region 捕获所有未经捕获的异常


        #region 多线程异常
        /// <summary>
        /// 多线程异常
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {

            string str = "";
            //父方法
            System.Diagnostics.StackTrace ss = new System.Diagnostics.StackTrace(true);
            System.Reflection.MethodBase mb = ss.GetFrame(1).GetMethod();

            //取得父方法命名空间    
            str += mb.DeclaringType.Namespace + ".";

            //取得父方法类名    
            str += mb.DeclaringType.Name + ".";

            //取得父方法类全名    
            str += mb.DeclaringType.FullName + ".";

            //取得父方法名    
            str += mb.Name;


            Exception ex = e.ExceptionObject as Exception;

            string EXMessage = JsonHelper.SerializeObject(ex);

            //记录异常日志
            WriteLogHelper.WriteLogsAsync("子线程异常[" + str + "]\r\n" + ex.Message + "\r\n INFO:" + EXMessage + "\r\n\r\n", "UnhandledException");
        }
        #endregion

        #region UI线程异常
        /// <summary>
        /// UI线程异常
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {

            string str = "";
            //父方法
            System.Diagnostics.StackTrace ss = new System.Diagnostics.StackTrace(true);
            System.Reflection.MethodBase mb = ss.GetFrame(1).GetMethod();

            //取得父方法命名空间    
            str += mb.DeclaringType.Namespace + ".";

            //取得父方法类名    
            str += mb.DeclaringType.Name + ".";

            //取得父方法类全名    
            str += mb.DeclaringType.FullName + ".";

            //取得父方法名    
            str += mb.Name;

            //记录异常日志
            WriteLogHelper.WriteLogsAsync("UI线程异常[" + e.Exception.TargetSite.DeclaringType.FullName + "]\r\n" + e.Exception.Message + "\r\n" + e.Exception.StackTrace + "\r\r" + e.Exception.ToString(), "UIThreadException");
        }

        #endregion
        #endregion


        /// <summary>
        /// 获取父方法名
        /// </summary>
        /// <returns></returns>
        private static string GetClassName()
        {
            string str = "";
            //父方法
            System.Diagnostics.StackTrace ss = new System.Diagnostics.StackTrace(true);
            System.Reflection.MethodBase mb = ss.GetFrame(1).GetMethod();

            //取得父方法命名空间    
            str += mb.DeclaringType.Namespace + ".";

            //取得父方法类名    
            str += mb.DeclaringType.Name + ".";

            //取得父方法类全名    
            str += mb.DeclaringType.FullName + ".";

            //取得父方法名    
            str += mb.Name;

            return str;

        }
    }
}
