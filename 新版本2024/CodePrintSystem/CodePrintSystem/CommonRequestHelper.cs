﻿using CodePrintSystem.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunny.UI;
using Microsoft.Extensions.DependencyInjection;

namespace CodePrintSystem
{
    public class CommonRequestHelper
    {
        /// <summary>
        /// 获得新的Service
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T GetService<T>()
        {
            var scope = AppsettingsConfig.ServiceProvider.CreateScope();
            var service = scope.ServiceProvider.GetRequiredService<T>();
            return service;
        }

        /// <summary>
        /// 请求提示
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="httpResponseResultModel"></param>
        /// <param name="tipMsg"></param>
        /// <returns></returns>
        public static HttpResponseResultModel<T> RequestTip<T>(HttpResponseResultModel<T> httpResponseResultModel, Action action = null, string tipMsg = "保存成功！")
        {
            if (httpResponseResultModel.IsSuccess)
            {
                UIMessageTip.ShowOk(tipMsg);
                action?.Invoke();
            }
            else
            {
                UIMessageTip.ShowError(httpResponseResultModel.ErrorMessage);
            }
            return httpResponseResultModel;
        }
    }
}
