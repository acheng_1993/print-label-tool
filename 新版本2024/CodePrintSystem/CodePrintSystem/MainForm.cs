﻿using CodePrintSystem.Basic.Abstractions;
using CodePrintSystem.Core;
using CodePrintSystem.Forms;
using CodePrintSystem.Utility;
using Microsoft.Extensions.DependencyInjection;
using Sunny.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodePrintSystem
{
    public partial class MainForm : UIHeaderMainFooterFrame, ISelfSingletonAutoInject
    {
        ItemForm itemForm;
        SupplierForm supplierForm;
        BaseDataForm baseDataForm;
        CodePrintForm printForm;
        UserForm userForm;
        TagTemplateForm tagTemplateForm;
        IServiceProvider serviceProvider;
        PrintLogForm printLogForm;
        PlanForm planForm;
        public MainForm(ItemForm itemForm, SupplierForm supplierForm, TagTemplateForm tagTemplateForm
            , BaseDataForm baseDataForm,
            UserForm userForm,
            CodePrintForm printForm,
            PrintLogForm printLogForm,
             IServiceProvider serviceProvider, PlanForm planForm)
        {
            InitializeComponent();
            this.itemForm = itemForm;
            this.supplierForm = supplierForm;
            this.tagTemplateForm = tagTemplateForm;
            this.baseDataForm = baseDataForm;
            this.userForm = userForm;
            this.printForm = printForm;
            this.printLogForm = printLogForm;
            this.serviceProvider = serviceProvider;
            this.planForm = planForm;
            LBL_Version.Text = CurrentUserConfig.Version;
            this.Text = "合晟标签打印系统";
            this.uiNavBar1.NodeMouseClick += UiNavBar1_NodeMouseClick;
            

        }
        /// <summary>
        /// 初始化主界面登录信息
        /// </summary>
        public void InitLoginInfo()
        {
            this.lblUserName.Text = CurrentUserConfig.User.UserName;
        }

        public void AddAdminMenu()
        {
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("物料维护");
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("供应商维护");
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("基础数据");
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("标签模板");
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("用户设置");
            System.Windows.Forms.TreeNode treeNode8 = new System.Windows.Forms.TreeNode("计划维护");
            System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("数据维护", new System.Windows.Forms.TreeNode[] {
                treeNode2,
                treeNode3,
                treeNode4,
                treeNode5,
                treeNode6,treeNode8});
            treeNode2.Name = "ItemSet";
            treeNode2.Text = "产品维护";
            treeNode3.Name = "SupplierSet";
            treeNode3.Text = "供应商维护";
            treeNode4.Name = "BaseDataSet";
            treeNode4.Text = "基础数据";
            treeNode5.Name = "CodeTemplate";
            treeNode5.Text = "标签模板";
            treeNode6.Name = "UserSet";
            treeNode6.Text = "用户设置";
            treeNode7.Name = "";
            treeNode7.Text = "数据维护";
            treeNode8.Name = "PlanSet";

            this.uiNavBar1.Nodes.Insert(1, treeNode7);
        }
        public void InitPage()
        {

            AddPage(printForm, (int)SystemMenuEnum.CodePrint);
            AddPage(printLogForm, (int)SystemMenuEnum.PrintLog);
            AddPage(itemForm, (int)SystemMenuEnum.ItemSet);
            if (CurrentUserConfig.User.IsAdmin.HasValue && CurrentUserConfig.User.IsAdmin.Value)
            {
                AddAdminMenu();
                AddPage(supplierForm, (int)SystemMenuEnum.SupplierSet);
                AddPage(baseDataForm, (int)SystemMenuEnum.BaseDataSet);
                AddPage(userForm, (int)SystemMenuEnum.UserSet);
                AddPage(tagTemplateForm, (int)SystemMenuEnum.CodeTemplate);
                AddPage(planForm, (int)SystemMenuEnum.PlanSet);
            }
            SelectPage((int)SystemMenuEnum.CodePrint);
        }


        private async void UiNavBar1_NodeMouseClick(TreeNode node, int menuIndex, int pageIndex)
        {
            if (node.Name == SystemMenuEnum.ItemSet.ToString())
            {
                SelectPage((int)SystemMenuEnum.ItemSet);
            }
            if (node.Name == SystemMenuEnum.SupplierSet.ToString())
            {
                SelectPage((int)SystemMenuEnum.SupplierSet);

            }
            if (node.Name == SystemMenuEnum.CodePrint.ToString())
            {
                SelectPage((int)SystemMenuEnum.CodePrint);
            }
            if (node.Name == SystemMenuEnum.BaseDataSet.ToString())
            {
                SelectPage((int)SystemMenuEnum.BaseDataSet);
            }
            if (node.Name == SystemMenuEnum.UserSet.ToString())
            {
                SelectPage((int)SystemMenuEnum.UserSet);
            }
            if (node.Name == SystemMenuEnum.CodeTemplate.ToString())
            {
                SelectPage((int)SystemMenuEnum.CodeTemplate);
            }
            if (node.Name == SystemMenuEnum.PrintLog.ToString())
            {
                SelectPage((int)SystemMenuEnum.PrintLog);
            }
            if (node.Name == SystemMenuEnum.PlanSet.ToString())
            {
                SelectPage((int)SystemMenuEnum.PlanSet);
            }
            if (node.Name == SystemMenuEnum.ExitSystem.ToString())
            {
                if (UIMessageBox.ShowAsk("确认退出？"))
                {

                    Application.Exit();
                }
            }
            if (node.Name == SystemMenuEnum.DownloadItemTemplate.ToString())
            {
                SaveFileDialog saveFile = new SaveFileDialog();  //实例化保存文件对话框对象
                saveFile.Title = "请选择保存文件路径";
                saveFile.Filter = "xlsx文件(*.xlsx)|*.xlsx|所有文件(*.*)|*.*";
                saveFile.OverwritePrompt = true;  //是否覆盖当前文件
                saveFile.RestoreDirectory = true;  //还原目录
                var filePath = @$"config/产品导入模板.xlsx";
                if (saveFile.ShowDialog() == DialogResult.OK)
                {
                    System.IO.File.Copy(filePath, saveFile.FileName, true);
                    UIMessageTip.ShowOk("下载成功");
                }
            }
            if (node.Name == SystemMenuEnum.OperateDoc.ToString())
            {
                var path = System.IO.Directory.GetCurrentDirectory() + @"/config/东莞合晟二维码打印操作说明书.docx";
                if (File.Exists(path))
                {
                    try
                    {
                        ProcessStartInfo processStartInfo = new ProcessStartInfo(path);
                        Process process = new Process();
                        process.StartInfo = processStartInfo;
                        process.StartInfo.UseShellExecute = true;
                        process.Start();
                    }
                    catch (Exception ex)
                    {
                        SaveFileDialog saveFile = new SaveFileDialog();  //实例化保存文件对话框对象
                        saveFile.Title = "请选择保存文件路径";
                        saveFile.Filter = "docx文件(*.docx)|*.docx|所有文件(*.*)|*.*";
                        saveFile.OverwritePrompt = true;  //是否覆盖当前文件
                        saveFile.RestoreDirectory = true;  //还原目录
                        var filePath = @$"config/东莞合晟二维码打印操作说明书.docx";
                        if (saveFile.ShowDialog() == DialogResult.OK)
                        {
                            System.IO.File.Copy(filePath, saveFile.FileName, true);
                            UIMessageTip.ShowOk("下载成功");
                        }
                        // MessageBox.Show(ex.Message.ToString());
                    }
                }
            }
            if (node.Name == SystemMenuEnum.PasswordChange.ToString())
            {
                UIEditOption option = new UIEditOption();
                option.AutoLabelWidth = true;
                option.Text = "密码修改";
                option.AddText("OldPwd", "原密码", "", true, true);
                option.AddText("NewPwd", "新密码", "", true, true);
                option.AddText("CheckNewPwd", "确认新密码", "", true, true);

                UIEditForm frm = new UIEditForm(option);
                frm.ShowDialog();

                if (frm.IsOK)
                {
                    if (!frm["CheckNewPwd"].ToString().Equals(frm["NewPwd"].ToString()))
                    {
                        UIMessageTip.ShowError("两次密码输入不一致！");
                        return;
                    }
                    var data = new ChangePwdModel()
                    {
                        Id = CurrentUserConfig.User.Id,
                        OldPwd = frm["OldPwd"].ToString(),
                        NewPwd = frm["NewPwd"].ToString()
                    };

                    var result = await CommonRequestHelper.GetService<ICommandUserService>().ChangePasswordAsync(
                         data);
                    CommonRequestHelper.RequestTip(result);
                }
            }
            //  BatchGenerateCodeAsync();

        }


        public async Task BatchGenerateCodeAsync()
        {
            try
            {


                using (var scope = serviceProvider.CreateScope())
                {
                    var generateCodeService = scope.ServiceProvider.GetRequiredService<IGenerateCodeService>();
                    var list = new List<GenerateCodeDetailModel>();
                    list.Add(new GenerateCodeDetailModel() { GenerateCodeTemplateDetailId = 1, InputValueOfTemplateDetail = "1" });
                    list.Add(new GenerateCodeDetailModel() { GenerateCodeTemplateDetailId = 2, InputValueOfTemplateDetail = "16" });
                    list.Add(new GenerateCodeDetailModel() { GenerateCodeTemplateDetailId = 3, InputValueOfTemplateDetail = "" });


                    GenerateCodeModel generateCodeModel = new GenerateCodeModel()
                    {
                        ItemCode = "123456-12345",
                        TemplateId = 1,
                        PrintDayTime = DateTime.Now,
                        Quantity = 100,
                        GenerateCodeDetailModel = list
                    };
                    await generateCodeService.BatchGenerateCodeAsync(generateCodeModel, true);

                }

            }
            catch (Exception ex)
            {

            }

        }


        private void MainContainer_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            var time = DateTime.Now;
            var date = time.ToString("yyyy-MM-dd HH:mm:ss");
            var dayOfWeek = CommonHelper.GetWeek(time.DayOfWeek);
            var week = CommonHelper.GetYearWeekDayByTime(time);

            this.lblTime.Text = $"{date} 第{week.weekOfYear}周  {dayOfWeek}";
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
        }

        private bool NotCanClose()
        {
            if (CurrentUserConfig.IsPrinting)
            {
                UIMessageBox.ShowWarning("关闭程序前，请先停止打印");
                return true;
            }

            return false;
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            // e.Cancel = NotCanClose();
            //return;
            switch (e.CloseReason)
            {
                //应用程序要求关闭窗口
                case CloseReason.ApplicationExitCall:
                    e.Cancel = NotCanClose(); //不拦截，响应操作
                    break;
                //自身窗口上的关闭按钮
                case CloseReason.FormOwnerClosing:
                    e.Cancel = NotCanClose();//拦截，不响应操作
                    break;
                //MDI窗体关闭事件
                case CloseReason.MdiFormClosing:
                    e.Cancel = NotCanClose();//拦截，不响应操作
                    break;
                //不明原因的关闭
                case CloseReason.None:
                    e.Cancel = NotCanClose();
                    break;
                //任务管理器关闭进程
                case CloseReason.TaskManagerClosing:
                    e.Cancel = NotCanClose();//不拦截，响应操作
                    CurrentUserConfig.IsPrinting = false;
                    break;
                //用户通过UI关闭窗口或者通过Alt+F4关闭窗口
                case CloseReason.UserClosing:
                    e.Cancel = NotCanClose();//拦截，不响应操作
                    break;
                //操作系统准备关机
                case CloseReason.WindowsShutDown:
                    e.Cancel = NotCanClose();//不拦截，响应操作
                    break;
                default:
                    e.Cancel = NotCanClose();
                    break;
            }
        }
    }
}
