﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CodePrintSystem.Basic.Abstractions;
using CodePrintSystem.Core;
using CodePrintSystem.Utility;
using Microsoft.Extensions.DependencyInjection;
using Sunny.UI;

namespace CodePrintSystem
{
    public partial class Login : UILoginForm, ISelfSingletonAutoInject
    {
        MainForm mainForm;
        IServiceProvider serviceProvider;
        public Login(ITest test, MainForm mainForm, IServiceProvider serviceProvider)
        {

            InitializeComponent();
            this.serviceProvider = serviceProvider;
            this.Text = "合晟标签打印";
            this.mainForm = mainForm;
            this.Title = "合晟标签打印";
            this.SubText = "合晟标签打印管理系统";
            this.LoginImage = UILoginImage.Login3;
            InitEvent();
        }

        private void InitEvent()
        {
            this.ButtonLoginClick += Login_ButtonLoginClick;
        }

        private async void Login_ButtonLoginClick(object sender, EventArgs e)
        {
            string userName = this.UserName.Trim();
            string pwd = this.Password;
            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(pwd))
            {
                UIMessageTip.ShowError("用户名与密码不能为空！", 2000);
                return;
            }
            var userService = serviceProvider.GetRequiredService<ICommandUserService>();
            var result = await userService.LoginAsync(new UserEntity()
            {
                UserName = UserName,
                UserPassword = pwd
            });
            if (result.IsSuccess == false)
            {
                UIMessageTip.ShowError(result.ErrorMessage, 2000);
                return;
            }

            this.Hide();
            Live20RClient.Close();
            mainForm.InitLoginInfo();
            mainForm.InitPage();
            mainForm.ShowDialog();

        }

        private void Login_Load(object sender, EventArgs e)
        {
            Task.Run(() => { Live20RClient.Open(CollectAfter); });
        }

        //  private List<UserEntity> Users { get; set; }// 所有用户
        #region 指纹新的
        /// <summary>
        /// 采集之后的事
        /// </summary>
        /// <param name="result"></param>
        protected async void CollectAfter(FingerprintResult result)
        {
            // 遍历所有用户，指纹对比直接登录

            var fingePwMD5 = EncryptDecryptHelper.Md5(result.CapTmp);

            var nowUser = new UserEntity();
            nowUser = null;
            var userService = serviceProvider.GetRequiredService<IQueryUserService>();
            var users = await userService.GetListAsync<UserEntity>();
            foreach (var user in users)
            {
                if (string.IsNullOrEmpty(user.FingePrintPassword))
                {
                    continue;
                }
                if (Live20RClient.DBMatch(user.FingePrintPassword, result.CapTmp))
                {
                    nowUser = user;
                    break;
                }
            }
            if (nowUser == null)
            {
                UIMessageTip.ShowError("指纹未识别,请检查该人员是否录入指纹！", 2000);
                return;
            }
            else
            {

                CurrentUserConfig.User = nowUser.MapTo<CurrentUser>();
                nowUser.LastLoginTime = DateTime.Now;
                Live20RClient.Close();
                this.Invoke(new Action(() =>
                {
                    this.Hide();
                    mainForm.InitLoginInfo();
                    mainForm.InitPage();
                    mainForm.ShowDialog();
                }));


            }

        }
        #endregion

        private void Login_FormClosed(object sender, FormClosedEventArgs e)
        {
            Live20RClient.Close();
        }
    }
}
