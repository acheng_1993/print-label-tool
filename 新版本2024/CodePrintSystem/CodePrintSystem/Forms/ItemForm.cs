﻿using CodePrintSystem.AppLayer;
using CodePrintSystem.AppLayer.Basic.DTOS;
using CodePrintSystem.AppLayer.Basic.ViewObject;
using CodePrintSystem.Basic.Abstractions;
using CodePrintSystem.Core;
using CodePrintSystem.EnumClass;
using CodePrintSystem.Utility;
using Sunny.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ToolKitCore.MyNPOIExtension;

namespace CodePrintSystem.Forms
{
    public partial class ItemForm : UITitlePage, ISelfSingletonAutoInject
    {


        public ItemForm()
        {
            InitializeComponent();
            this.Text = "产品维护";
            uiDataGridView1.AutoGenerateColumns = false;

        }

        private void InitDataGrid()
        {
            uiPagination1.ActivePage = 1;
            uiPagination1.PageSize = 20;
            uiDataGridView1.ReadOnly = false;
            uiDataGridView1.ColumnHeaderMouseDoubleClick += UiDataGridView1_ColumnHeaderMouseDoubleClick;
            uiPagination1.PageChanged += UiPagination1_PageChanged;
            var checkColumn = uiDataGridView1.AddCheckBoxColumn("选择", "IsCheck").SetFixedMode(100);
            checkColumn.ReadOnly = false;
            uiDataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;//自适应
            //uiDataGridView1.se;
            uiDataGridView1.AddColumn("产品编码", "Code");//.SetFixedMode(200);
            uiDataGridView1.AddColumn("版本", "VersionCode");//.SetFixedMode(100);
            uiDataGridView1.AddColumn("产品名称", "Name");//.SetFixedMode(200);
            uiDataGridView1.AddColumn("模板名称", "TemplateName");//.SetFixedMode(200);
            uiDataGridView1.AddColumn("是否导入标签模板", "IsImportTemplate");
            uiDataGridView1.AddColumn("创建时间", "CreateTime");
            LoadDataGrid();
        }

        private void UiPagination1_PageChanged(object sender, object pagingSource, int pageIndex, int count)
        {
            LoadDataGrid();
        }

        private void UiDataGridView1_ColumnHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            // 双击列头全选
            if (e.ColumnIndex == 0)
            {
                if (listData.Exists(x => x.IsCheck == false))
                {
                    listData.ForEach(x =>
                    {
                        x.IsCheck = true;
                    });
                }
                else
                {
                    listData.ForEach(x =>
                    {
                        x.IsCheck = false;
                    });
                }
                uiDataGridView1.DataSource = null;
                uiDataGridView1.DataSource = listData;
            }
        }

        private async Task LoadDataGrid()
        {
            var data = await CommonRequestHelper.GetService<ItemInfoApp>().GetListPagedAsync(uiPagination1.ActivePage, uiPagination1.PageSize,
         new ItemInfoConditionDTO()
         {
             Code = this.txtCode.Text.Trim()
         }
            );
            listData = data.Data;
            this.uiDataGridView1.DataSource = listData;
            this.uiPagination1.TotalCount = data.Total;
        }
        private bool GetSelectRows()
        {
            selectRows = listData.Where(x => x.IsCheck).ToList();
            if (!selectRows.Any())
            {
                UIMessageTip.ShowWarning("请选中一行");
                return false;
            }
            return true;
        }

        List<ItemInfoView> listData = new List<ItemInfoView>();
        List<ItemInfoView> selectRows = new List<ItemInfoView>();

        private async void btnAdd_Click(object sender, EventArgs e)
        {
            ItemEditForm editForm = new ItemEditForm(async (data) =>
            {
                var result = await CommonRequestHelper.GetService<ICommandItemInfoService>().InsertAsync(data);
                if (result.IsSuccess)
                {
                    CommonRequestHelper.RequestTip(result, () => { LoadDataGrid(); });

                }
                else
                {
                    UIMessageTip.ShowError(result.ErrorMessage);
                }
                return result.IsSuccess;
            });
            editForm.Text = "新增产品";
            editForm.ShowDialog();
            if (editForm.IsOK)
            {

            }
            editForm.Dispose();

        }

        private async void btnEdit_Click(object sender, EventArgs e)
        {
            if (GetSelectRows())
            {
                if (selectRows.Count > 1)
                {
                    UIMessageBox.Show("编辑不能选择多行");
                    return;
                }
                var data = selectRows.First().MapTo<ItemInfoEntity>();
                ItemEditForm editForm = new ItemEditForm(async (data) =>
                {
                    var service = CommonRequestHelper.GetService<ICommandItemInfoService>();
                    var result = await service.UpdateAsync(data);
                    if (result.IsSuccess)
                    {
                        CommonRequestHelper.RequestTip(result, () => { LoadDataGrid(); });
                    }
                    else
                    {
                        UIMessageTip.ShowError(result.ErrorMessage);
                    }
                    return result.IsSuccess;
                });
                editForm.Text = "更新产品";
                editForm.ItemInfo = data;
                editForm.DisabledControl();
                editForm.ShowDialog();

                if (editForm.IsOK)
                {

                }

                editForm.Dispose();
            }
        }

        private async void btnDelete_Click(object sender, EventArgs e)
        {
            if (GetSelectRows())
            {
                if (UIMessageBox.ShowAsk("删除的数据不可恢复，确认删除？"))
                {
                    var result = await CommonRequestHelper.GetService<ICommandItemInfoService>().DeleteBatchAsync(selectRows.Select(x => x.Id).ToList());
                    if (result.IsSuccess)
                    {
                        CommonRequestHelper.RequestTip<bool>(result, () => { LoadDataGrid(); }, "删除成功");
                    }
                    else
                    {
                        UIMessageBox.Show(result.ErrorMessage);

                    }
                }

            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            LoadDataGrid();
        }

        private void ItemForm_Load(object sender, EventArgs e)
        {
            InitDataGrid();
        }

        private void btn_BindTemplate_Click(object sender, EventArgs e)
        {
            BindTemplateAsync();
        }

        public async Task BindTemplateAsync()
        {
            if (GetSelectRows())
            {
                if (selectRows.Count > 1)
                {
                    UIMessageBox.Show("编辑不能选择多行");
                    return;
                }
                var data = selectRows.First().MapTo<UpdateTemplateItemInfoEntity>();
                BindTemplateForm editForm = new BindTemplateForm();
                editForm.Text = "绑定模板";
                editForm.ItemInfo = data;
                editForm.ShowDialog();
                if (editForm.IsOK)
                {
                    //var templateId = editForm.ItemInfo.TemplateId;
                    //var templateDetails = await CommonRequestHelper.GetService<IQueryGenerateCodeTemplateDetailService>().GetListAsync<GenerateCodeTemplateDetailEntity>(new BaseGenerateCodeTemplateDetailCondition() { TemplateId = templateId });
                    ////检查是否存在模板指定产品的情况
                    //var itemCodeRuleDetail = templateDetails.Where(t => t.RuleId == (long)CodeTemplateRuleEnum.ItemCode);
                    //if (itemCodeRuleDetail.Count() > 0)
                    //{
                    //    var hadTemplateItems = itemCodeRuleDetail.Where(t => t.RuleValue== data.TemplateId.ToString());
                    //    if (hadTemplateItems.Count()>0)
                    //    {
                    //        UIMessageBox.ShowError("该物料已经有模板内部指定，不能绑定另外的模板");
                    //    }
                    //}
                    var result = await CommonRequestHelper.GetService<ICommandItemInfoService>().UpdateAsync(editForm.ItemInfo);
                    if (result.IsSuccess)
                    {
                        CommonRequestHelper.RequestTip(result, () => { LoadDataGrid(); });

                    }
                    else
                    {
                        UIMessageBox.Show(result.ErrorMessage);
                    }
                }
                editForm.Dispose();
            }

        }

        private async void btnExport_Click(object sender, EventArgs e)
        {
            // 检查文件格式内容是否为空
            // 检查文件内是否有重复
            // 检查与库中是否有重复
            // 没有问题则导入
            OpenFileDialog openFileDialog = new OpenFileDialog { Filter = "XLSX文件(*.xlsx)|*.xlsx|所有文件(*.*)|*.*" };
            if (openFileDialog.ShowDialog() == DialogResult.OK && System.IO.File.Exists(openFileDialog.FileName))
            {
                string fileName = @$"files/exportItem{DateTime.Now.Year}{DateTime.Now.Month}{DateTime.Now.Day}.xlsx";
                System.IO.File.Copy(openFileDialog.FileName, fileName, true);
                string filePath = Path.Combine(Directory.GetCurrentDirectory(), fileName);
                var testData = ExportImportExcelHelper.ImportExcel<ImportItemModel>(filePath);
                if (testData == null || !testData.Any())
                {
                    UIMessageBox.Show("文件格式有误或者内容为空！");
                    return;
                }
                var count = testData.Select(x => x.Code).Distinct().Count();
                if (count != testData.Count)
                {
                    UIMessageBox.Show("文件中有产品编码重复，请检查！");
                    return;
                }
                var nullCount = testData.Where(x => string.IsNullOrEmpty(x.VersionCode)).Count();
                if (nullCount > 0)
                {
                    UIMessageBox.Show("产品版本不能为空，请检查文件！");
                    return;
                }

                bool isAllSuccess = true;
                for (var i = 0; i < testData.Count; i++)
                {
                    var ckResult = CommonHelper.CheckItemCode(testData[i].Code);
                    if (!string.IsNullOrEmpty(ckResult))
                    {
                        UIMessageBox.Show($"{ckResult}:{testData[i].Code}");
                        isAllSuccess = false;
                        return;
                    }
                    if (string.IsNullOrEmpty(testData[i].VersionCode))
                    {
                        UIMessageBox.Show($"{testData[i].Code}版本为空！请检查");
                        isAllSuccess = false;
                        return;
                    }
                    //if (testData[i].VersionCode.Length != AppsettingsConfig.VersionCodeLength)
                    //{
                    //    UIMessageBox.Show($"{testData[i].Code}版本长度不正确:{testData[i].VersionCode}长度为{testData[i].VersionCode.Length}");
                    //    isAllSuccess = false;
                    //    return;
                    //}
                }
                if (!isAllSuccess)
                {
                    return;
                }
                var allItem = (await CommonRequestHelper.GetService<IQueryItemInfoService>().GetListAsync<ItemInfoEntity>(new BaseItemInfoCondition() { }));
                var intersect = testData.Select(x => x.Code).Intersect(allItem.Select(x => x.Code)).ToList();
                if (intersect.Count > 0)
                {
                    UIMessageBox.Show("文件中有产品与库中重复，请检查！" + intersect[0]);
                    return;
                }
                this.btnExport.Enabled = false;
                Task.Run(async () =>
                {
                    try
                    {
                        var list = testData.MapToList<ItemInfoEntity>();
                        var result = await CommonRequestHelper.GetService<ICommandItemInfoService>().InsertBatchAsync(list);
                        if (result.IsSuccess)
                        {
                            UIMessageBox.Show("导入成功！");

                        }
                        else
                        {
                            UIMessageBox.Show("导入失败！");
                        }
                        this.Invoke(new Action(() =>
                        {
                            this.btnExport.Enabled = true;
                            this.btnSearch_Click(null, null);
                        }));
                    }
                    catch
                    {
                        UIMessageBox.Show("导入异常！");
                    }
                });

            }
        }
    }
}
