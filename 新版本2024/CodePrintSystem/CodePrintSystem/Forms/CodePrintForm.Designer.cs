﻿
namespace CodePrintSystem.Forms
{
    partial class CodePrintForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            uiLabel1 = new Sunny.UI.UILabel();
            cbxTemplate = new Sunny.UI.UIComboBox();
            uiGroupBox1 = new Sunny.UI.UIGroupBox();
            panel1 = new System.Windows.Forms.Panel();
            panel2 = new System.Windows.Forms.Panel();
            dateCurrent = new Sunny.UI.UIDatePicker();
            uiLabel6 = new Sunny.UI.UILabel();
            txtProduceNumber = new Sunny.UI.UITextBox();
            uiLabel3 = new Sunny.UI.UILabel();
            uiGroupBox3 = new Sunny.UI.UIGroupBox();
            lblVersion = new Sunny.UI.UILabel();
            uiLabel7 = new Sunny.UI.UILabel();
            lblCurrentItemCount = new Sunny.UI.UILabel();
            uiLabel5 = new Sunny.UI.UILabel();
            lblCurrentItem = new System.Windows.Forms.Label();
            uiLabel4 = new Sunny.UI.UILabel();
            lblCurrentCount = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            txtPrintCount = new Sunny.UI.UIIntegerUpDown();
            uiLabel2 = new Sunny.UI.UILabel();
            uiGroupBox2 = new Sunny.UI.UIGroupBox();
            pictureBox1 = new System.Windows.Forms.PictureBox();
            btnStop = new Sunny.UI.UIButton();
            btnPrint = new Sunny.UI.UIButton();
            btnRefresh = new Sunny.UI.UIButton();
            btnAdmin = new Sunny.UI.UIButton();
            timer1 = new System.Windows.Forms.Timer(components);
            uiLabel8 = new Sunny.UI.UILabel();
            cbxItem = new System.Windows.Forms.ComboBox();
            uiLabel10 = new Sunny.UI.UILabel();
            cbx_supplier = new Sunny.UI.UIComboBox();
            PagePanel.SuspendLayout();
            uiGroupBox1.SuspendLayout();
            panel2.SuspendLayout();
            uiGroupBox3.SuspendLayout();
            uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)pictureBox1).BeginInit();
            SuspendLayout();
            // 
            // PagePanel
            // 
            PagePanel.Controls.Add(cbx_supplier);
            PagePanel.Controls.Add(uiLabel10);
            PagePanel.Controls.Add(cbxItem);
            PagePanel.Controls.Add(uiLabel8);
            PagePanel.Controls.Add(btnAdmin);
            PagePanel.Controls.Add(btnRefresh);
            PagePanel.Controls.Add(panel2);
            PagePanel.Controls.Add(uiGroupBox1);
            PagePanel.Controls.Add(cbxTemplate);
            PagePanel.Controls.Add(uiLabel1);
            PagePanel.Size = new System.Drawing.Size(1399, 844);
            // 
            // uiLabel1
            // 
            uiLabel1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            uiLabel1.Location = new System.Drawing.Point(270, 19);
            uiLabel1.Name = "uiLabel1";
            uiLabel1.Size = new System.Drawing.Size(100, 23);
            uiLabel1.TabIndex = 1;
            uiLabel1.Text = "模板";
            uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbxTemplate
            // 
            cbxTemplate.DropDownStyle = Sunny.UI.UIDropDownStyle.DropDownList;
            cbxTemplate.FillColor = System.Drawing.Color.White;
            cbxTemplate.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            cbxTemplate.Location = new System.Drawing.Point(313, 14);
            cbxTemplate.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            cbxTemplate.MinimumSize = new System.Drawing.Size(63, 0);
            cbxTemplate.Name = "cbxTemplate";
            cbxTemplate.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            cbxTemplate.Size = new System.Drawing.Size(181, 29);
            cbxTemplate.TabIndex = 2;
            cbxTemplate.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiGroupBox1
            // 
            uiGroupBox1.Controls.Add(panel1);
            uiGroupBox1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            uiGroupBox1.Location = new System.Drawing.Point(24, 47);
            uiGroupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            uiGroupBox1.MinimumSize = new System.Drawing.Size(1, 1);
            uiGroupBox1.Name = "uiGroupBox1";
            uiGroupBox1.Padding = new System.Windows.Forms.Padding(15, 32, 15, 15);
            uiGroupBox1.Size = new System.Drawing.Size(965, 155);
            uiGroupBox1.TabIndex = 4;
            uiGroupBox1.Text = "模板详情";
            // 
            // panel1
            // 
            panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            panel1.Location = new System.Drawing.Point(15, 32);
            panel1.Margin = new System.Windows.Forms.Padding(15);
            panel1.Name = "panel1";
            panel1.Size = new System.Drawing.Size(935, 108);
            panel1.TabIndex = 0;
            // 
            // panel2
            // 
            panel2.Controls.Add(dateCurrent);
            panel2.Controls.Add(uiLabel6);
            panel2.Controls.Add(txtProduceNumber);
            panel2.Controls.Add(uiLabel3);
            panel2.Controls.Add(uiGroupBox3);
            panel2.Controls.Add(txtPrintCount);
            panel2.Controls.Add(uiLabel2);
            panel2.Controls.Add(uiGroupBox2);
            panel2.Controls.Add(btnStop);
            panel2.Controls.Add(btnPrint);
            panel2.Location = new System.Drawing.Point(24, 210);
            panel2.Name = "panel2";
            panel2.Size = new System.Drawing.Size(965, 288);
            panel2.TabIndex = 5;
            // 
            // dateCurrent
            // 
            dateCurrent.FillColor = System.Drawing.Color.White;
            dateCurrent.Font = new System.Drawing.Font("微软雅黑", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            dateCurrent.Location = new System.Drawing.Point(460, 10);
            dateCurrent.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            dateCurrent.MaxLength = 10;
            dateCurrent.MinimumSize = new System.Drawing.Size(63, 0);
            dateCurrent.Name = "dateCurrent";
            dateCurrent.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            dateCurrent.Size = new System.Drawing.Size(254, 32);
            dateCurrent.SymbolDropDown = 61555;
            dateCurrent.SymbolNormal = 61555;
            dateCurrent.TabIndex = 9;
            dateCurrent.Text = "2021-02-01";
            dateCurrent.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            dateCurrent.Value = new System.DateTime(2021, 2, 1, 20, 59, 1, 531);
            dateCurrent.ValueChanged += dateCurrent_ValueChanged;
            // 
            // uiLabel6
            // 
            uiLabel6.Font = new System.Drawing.Font("微软雅黑", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            uiLabel6.Location = new System.Drawing.Point(345, 10);
            uiLabel6.Name = "uiLabel6";
            uiLabel6.Size = new System.Drawing.Size(100, 23);
            uiLabel6.TabIndex = 8;
            uiLabel6.Text = "日期";
            uiLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtProduceNumber
            // 
            txtProduceNumber.Cursor = System.Windows.Forms.Cursors.IBeam;
            txtProduceNumber.FillColor = System.Drawing.Color.White;
            txtProduceNumber.Font = new System.Drawing.Font("微软雅黑", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            txtProduceNumber.Location = new System.Drawing.Point(461, 52);
            txtProduceNumber.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            txtProduceNumber.Maximum = 2147483647D;
            txtProduceNumber.Minimum = -2147483648D;
            txtProduceNumber.MinimumSize = new System.Drawing.Size(1, 1);
            txtProduceNumber.Name = "txtProduceNumber";
            txtProduceNumber.Padding = new System.Windows.Forms.Padding(5);
            txtProduceNumber.Size = new System.Drawing.Size(253, 32);
            txtProduceNumber.TabIndex = 7;
            // 
            // uiLabel3
            // 
            uiLabel3.Font = new System.Drawing.Font("微软雅黑", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            uiLabel3.Location = new System.Drawing.Point(345, 47);
            uiLabel3.Name = "uiLabel3";
            uiLabel3.Size = new System.Drawing.Size(125, 38);
            uiLabel3.TabIndex = 6;
            uiLabel3.Text = "生产制令号";
            uiLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiGroupBox3
            // 
            uiGroupBox3.Controls.Add(lblVersion);
            uiGroupBox3.Controls.Add(uiLabel7);
            uiGroupBox3.Controls.Add(lblCurrentItemCount);
            uiGroupBox3.Controls.Add(uiLabel5);
            uiGroupBox3.Controls.Add(lblCurrentItem);
            uiGroupBox3.Controls.Add(uiLabel4);
            uiGroupBox3.Controls.Add(lblCurrentCount);
            uiGroupBox3.Controls.Add(label1);
            uiGroupBox3.Font = new System.Drawing.Font("微软雅黑", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            uiGroupBox3.Location = new System.Drawing.Point(331, 132);
            uiGroupBox3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            uiGroupBox3.MinimumSize = new System.Drawing.Size(1, 1);
            uiGroupBox3.Name = "uiGroupBox3";
            uiGroupBox3.Padding = new System.Windows.Forms.Padding(0, 32, 0, 0);
            uiGroupBox3.Size = new System.Drawing.Size(383, 141);
            uiGroupBox3.TabIndex = 5;
            uiGroupBox3.Text = "当前日期统计";
            // 
            // lblVersion
            // 
            lblVersion.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            lblVersion.Location = new System.Drawing.Point(321, 106);
            lblVersion.Name = "lblVersion";
            lblVersion.Size = new System.Drawing.Size(53, 23);
            lblVersion.TabIndex = 7;
            lblVersion.Text = "无";
            lblVersion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel7
            // 
            uiLabel7.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            uiLabel7.Location = new System.Drawing.Point(268, 107);
            uiLabel7.Name = "uiLabel7";
            uiLabel7.Size = new System.Drawing.Size(46, 23);
            uiLabel7.TabIndex = 6;
            uiLabel7.Text = "版本:";
            uiLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCurrentItemCount
            // 
            lblCurrentItemCount.Font = new System.Drawing.Font("微软雅黑", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            lblCurrentItemCount.Location = new System.Drawing.Point(176, 105);
            lblCurrentItemCount.Name = "lblCurrentItemCount";
            lblCurrentItemCount.Size = new System.Drawing.Size(74, 23);
            lblCurrentItemCount.TabIndex = 5;
            lblCurrentItemCount.Text = "0";
            lblCurrentItemCount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel5
            // 
            uiLabel5.Font = new System.Drawing.Font("微软雅黑", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            uiLabel5.Location = new System.Drawing.Point(2, 106);
            uiLabel5.Name = "uiLabel5";
            uiLabel5.Size = new System.Drawing.Size(196, 23);
            uiLabel5.TabIndex = 4;
            uiLabel5.Text = "生产制令累计打印：";
            uiLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCurrentItem
            // 
            lblCurrentItem.AutoSize = true;
            lblCurrentItem.Location = new System.Drawing.Point(176, 67);
            lblCurrentItem.Name = "lblCurrentItem";
            lblCurrentItem.Size = new System.Drawing.Size(108, 25);
            lblCurrentItem.TabIndex = 3;
            lblCurrentItem.Text = "------------";
            // 
            // uiLabel4
            // 
            uiLabel4.Font = new System.Drawing.Font("微软雅黑", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            uiLabel4.Location = new System.Drawing.Point(76, 67);
            uiLabel4.Name = "uiLabel4";
            uiLabel4.Size = new System.Drawing.Size(107, 23);
            uiLabel4.TabIndex = 2;
            uiLabel4.Text = "当前产品：";
            uiLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCurrentCount
            // 
            lblCurrentCount.AutoSize = true;
            lblCurrentCount.Location = new System.Drawing.Point(176, 32);
            lblCurrentCount.Name = "lblCurrentCount";
            lblCurrentCount.Size = new System.Drawing.Size(23, 25);
            lblCurrentCount.TabIndex = 1;
            lblCurrentCount.Text = "0";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(2, 32);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(183, 25);
            label1.TabIndex = 0;
            label1.Text = "所有产品累计打印：";
            // 
            // txtPrintCount
            // 
            txtPrintCount.Font = new System.Drawing.Font("微软雅黑", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            txtPrintCount.HasMaximum = true;
            txtPrintCount.HasMinimum = true;
            txtPrintCount.Location = new System.Drawing.Point(461, 94);
            txtPrintCount.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            txtPrintCount.Maximum = 10000;
            txtPrintCount.Minimum = 1;
            txtPrintCount.MinimumSize = new System.Drawing.Size(100, 0);
            txtPrintCount.Name = "txtPrintCount";
            txtPrintCount.Size = new System.Drawing.Size(253, 33);
            txtPrintCount.TabIndex = 4;
            txtPrintCount.Text = null;
            txtPrintCount.Value = 1;
            // 
            // uiLabel2
            // 
            uiLabel2.Font = new System.Drawing.Font("微软雅黑", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            uiLabel2.Location = new System.Drawing.Point(345, 94);
            uiLabel2.Name = "uiLabel2";
            uiLabel2.Size = new System.Drawing.Size(109, 33);
            uiLabel2.TabIndex = 3;
            uiLabel2.Text = "打印数量";
            uiLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiGroupBox2
            // 
            uiGroupBox2.Controls.Add(pictureBox1);
            uiGroupBox2.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            uiGroupBox2.Location = new System.Drawing.Point(15, 19);
            uiGroupBox2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            uiGroupBox2.MinimumSize = new System.Drawing.Size(1, 1);
            uiGroupBox2.Name = "uiGroupBox2";
            uiGroupBox2.Padding = new System.Windows.Forms.Padding(0, 32, 0, 0);
            uiGroupBox2.Size = new System.Drawing.Size(240, 242);
            uiGroupBox2.TabIndex = 2;
            uiGroupBox2.Text = "二维码预览";
            // 
            // pictureBox1
            // 
            pictureBox1.Location = new System.Drawing.Point(25, 41);
            pictureBox1.Name = "pictureBox1";
            pictureBox1.Size = new System.Drawing.Size(190, 182);
            pictureBox1.TabIndex = 0;
            pictureBox1.TabStop = false;
            // 
            // btnStop
            // 
            btnStop.Cursor = System.Windows.Forms.Cursors.Hand;
            btnStop.Font = new System.Drawing.Font("微软雅黑", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            btnStop.Location = new System.Drawing.Point(753, 181);
            btnStop.MinimumSize = new System.Drawing.Size(1, 1);
            btnStop.Name = "btnStop";
            btnStop.Size = new System.Drawing.Size(140, 80);
            btnStop.TabIndex = 1;
            btnStop.Text = "停止打印";
            btnStop.Click += btnStop_Click;
            // 
            // btnPrint
            // 
            btnPrint.Cursor = System.Windows.Forms.Cursors.Hand;
            btnPrint.Font = new System.Drawing.Font("微软雅黑", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            btnPrint.Location = new System.Drawing.Point(753, 43);
            btnPrint.MinimumSize = new System.Drawing.Size(1, 1);
            btnPrint.Name = "btnPrint";
            btnPrint.Size = new System.Drawing.Size(140, 80);
            btnPrint.TabIndex = 0;
            btnPrint.Text = "打印";
            btnPrint.Click += btnPrint_Click;
            // 
            // btnRefresh
            // 
            btnRefresh.Cursor = System.Windows.Forms.Cursors.Hand;
            btnRefresh.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            btnRefresh.Location = new System.Drawing.Point(771, 14);
            btnRefresh.MinimumSize = new System.Drawing.Size(1, 1);
            btnRefresh.Name = "btnRefresh";
            btnRefresh.Size = new System.Drawing.Size(100, 35);
            btnRefresh.TabIndex = 6;
            btnRefresh.Text = "刷新";
            btnRefresh.Click += btnRefresh_Click;
            // 
            // btnAdmin
            // 
            btnAdmin.Cursor = System.Windows.Forms.Cursors.Hand;
            btnAdmin.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            btnAdmin.Location = new System.Drawing.Point(877, 14);
            btnAdmin.MinimumSize = new System.Drawing.Size(1, 1);
            btnAdmin.Name = "btnAdmin";
            btnAdmin.Size = new System.Drawing.Size(112, 35);
            btnAdmin.TabIndex = 7;
            btnAdmin.Text = "管理员授权";
            btnAdmin.Click += btnAdmin_Click;
            // 
            // timer1
            // 
            timer1.Enabled = true;
            timer1.Interval = 3000;
            timer1.Tick += timer1_Tick;
            // 
            // uiLabel8
            // 
            uiLabel8.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            uiLabel8.Location = new System.Drawing.Point(39, 19);
            uiLabel8.Name = "uiLabel8";
            uiLabel8.Size = new System.Drawing.Size(100, 23);
            uiLabel8.TabIndex = 8;
            uiLabel8.Text = "产品";
            uiLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbxItem
            // 
            cbxItem.FormattingEnabled = true;
            cbxItem.Location = new System.Drawing.Point(78, 17);
            cbxItem.Name = "cbxItem";
            cbxItem.Size = new System.Drawing.Size(176, 29);
            cbxItem.TabIndex = 9;
            // 
            // uiLabel10
            // 
            uiLabel10.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            uiLabel10.Location = new System.Drawing.Point(505, 19);
            uiLabel10.Name = "uiLabel10";
            uiLabel10.Size = new System.Drawing.Size(100, 23);
            uiLabel10.TabIndex = 11;
            uiLabel10.Text = "供应商";
            uiLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbx_supplier
            // 
            cbx_supplier.DropDownStyle = Sunny.UI.UIDropDownStyle.DropDownList;
            cbx_supplier.FillColor = System.Drawing.Color.White;
            cbx_supplier.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            cbx_supplier.Location = new System.Drawing.Point(574, 14);
            cbx_supplier.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            cbx_supplier.MinimumSize = new System.Drawing.Size(63, 0);
            cbx_supplier.Name = "cbx_supplier";
            cbx_supplier.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            cbx_supplier.Size = new System.Drawing.Size(181, 29);
            cbx_supplier.TabIndex = 3;
            cbx_supplier.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // CodePrintForm
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(10F, 21F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(1399, 879);
            Name = "CodePrintForm";
            Text = "CodePrintForm";
            Load += CodePrintForm_Load;
            PagePanel.ResumeLayout(false);
            uiGroupBox1.ResumeLayout(false);
            panel2.ResumeLayout(false);
            uiGroupBox3.ResumeLayout(false);
            uiGroupBox3.PerformLayout();
            uiGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)pictureBox1).EndInit();
            ResumeLayout(false);
        }

        #endregion
        private Sunny.UI.UIComboBox cbxTemplate;
        private Sunny.UI.UILabel uiLabel1;
        private Sunny.UI.UIGroupBox uiGroupBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private Sunny.UI.UIButton btnPrint;
        private Sunny.UI.UIButton btnStop;
        private Sunny.UI.UIIntegerUpDown txtPrintCount;
        private Sunny.UI.UILabel uiLabel2;
        private Sunny.UI.UIGroupBox uiGroupBox2;
        private Sunny.UI.UIButton btnAdmin;
        private Sunny.UI.UIButton btnRefresh;
        private Sunny.UI.UIGroupBox uiGroupBox3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Sunny.UI.UITextBox txtProduceNumber;
        private Sunny.UI.UILabel uiLabel3;
        private System.Windows.Forms.Label lblCurrentCount;
        private System.Windows.Forms.Label label1;
        private Sunny.UI.UILabel lblCurrentItemCount;
        private Sunny.UI.UILabel uiLabel5;
        private System.Windows.Forms.Label lblCurrentItem;
        private Sunny.UI.UILabel uiLabel4;
        private Sunny.UI.UIDatePicker dateCurrent;
        private Sunny.UI.UILabel uiLabel6;
        private System.Windows.Forms.Timer timer1;
        private Sunny.UI.UILabel lblVersion;
        private Sunny.UI.UILabel uiLabel7;
        private System.Windows.Forms.ComboBox cbxItem;
        private Sunny.UI.UILabel uiLabel8;
        private Sunny.UI.UIComboBox cbx_supplier;
        private Sunny.UI.UILabel uiLabel10;
    }
}