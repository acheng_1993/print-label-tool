﻿using CodePrintSystem.Basic.Abstractions;
using CodePrintSystem.Utility;
using Sunny.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodePrintSystem.Forms
{
    public partial class BindTemplateForm : UIEditForm
    {
        public BindTemplateForm()
        {
            InitializeComponent();
            this.Text = "绑定模板";
           
        }



        private UpdateTemplateItemInfoEntity itemInfo = new UpdateTemplateItemInfoEntity();
        public UpdateTemplateItemInfoEntity ItemInfo
        {
            get
            {

                itemInfo.TemplateId = cbx_user.SelectedValue.ToLong();
                return itemInfo;
            }

            set
            {
                itemInfo.Id = value.Id;
                cbx_user.SelectedValue = value.TemplateId;
            }
        }

        public async Task BindTemplateAsync()
        {
            var list = await CommonRequestHelper.GetService<IQueryGenerateCodeTemplateService>().GetListAsync<GenerateCodeTemplateEntity>();
            cbx_user.ValueMember = "Id";
            cbx_user.DisplayMember = "Name";
            list.Insert(0, new GenerateCodeTemplateEntity() { Id = 0, Name = "" });
            cbx_user.DataSource = list;

            cbx_user.SelectedIndex = 0;
        }

        private async void BindTemplate_Load(object sender, EventArgs e)
        {
            BindTemplateAsync();
        }
    }
}
