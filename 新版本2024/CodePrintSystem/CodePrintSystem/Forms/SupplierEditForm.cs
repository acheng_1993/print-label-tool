﻿using CodePrintSystem.Basic.Abstractions;
using CodePrintSystem.Core;
using Sunny.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodePrintSystem.Forms
{
    public partial class SupplierEditForm : UIEditForm
    {
        Func<SupplierEntity, Task<bool>> func;
        public SupplierEditForm(Func<SupplierEntity, Task<bool>> func)
        {
            InitializeComponent();
            this.func = func;
            if (func != null)
            {
                this.ButtonOkClick += btnOK_Click_1;
            }
        }
        public void DisabledControl()
        {
            this.txtCode.Enabled = false;
        }

        protected override bool CheckData()
        {
            //if (txtCode.Text.Trim().Length != AppsettingsConfig.SupplierCodeLength)
            //{
            //    UIMessageTip.ShowError($"供应商编码长度不为{AppsettingsConfig.SupplierCodeLength}!");
            //    return false;
            //}
            return CheckEmpty(txtCode, "请输入编码") && CheckEmpty(txtName, "请输入名称");
        }
        private SupplierEntity data = new SupplierEntity();
        public SupplierEntity Data
        {
            get
            {
                data.Code = txtCode.Text;
                data.Name = txtName.Text;
                return data;
            }

            set
            {
                data.Id = value.Id;
                txtCode.Text = value.Code;
                txtName.Text = value.Name;
            }
        }

        private async void btnOK_Click_1(object sender, EventArgs e)
        {
            var result = await func?.Invoke(Data);
            if (result == true)
            {
                DialogResult = DialogResult.OK;
                Close();
            }
        }
    }
}
