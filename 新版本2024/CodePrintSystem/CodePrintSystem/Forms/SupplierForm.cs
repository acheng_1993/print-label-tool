﻿using CodePrintSystem.AppLayer.Basic.ViewObject;
using CodePrintSystem.Basic.Abstractions;
using CodePrintSystem.Core;
using CodePrintSystem.Utility;
using Sunny.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodePrintSystem.Forms
{
    public partial class SupplierForm : UITitlePage, ISelfSingletonAutoInject
    {
        public SupplierForm()
        {
            InitializeComponent();
            this.Text = "供应商维护";
            this.uiLabel1.Text = "编号";
            uiDataGridView1.AutoGenerateColumns = false;
           
        }
        private async Task InitDataGrid()
        {
            uiPagination1.ActivePage = 1;
            uiPagination1.PageSize = 20;
            uiDataGridView1.ReadOnly = false;
            uiDataGridView1.ColumnHeaderMouseDoubleClick += UiDataGridView1_ColumnHeaderMouseDoubleClick;
            uiPagination1.PageChanged += UiPagination1_PageChanged;
            btnSearch.Click += BtnSearch_Click;
            btnAdd.Click += BtnAdd_Click;
            btnEdit.Click += BtnEdit_Click;
            btnDelete.Click += BtnDelete_Click;
            var checkColumn = uiDataGridView1.AddCheckBoxColumn("选择", "IsCheck").SetFixedMode(100);
            checkColumn.ReadOnly = false;
            uiDataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;//自适应
            //uiDataGridView1.se;
            uiDataGridView1.AddColumn("供应商编码", "Code");//.SetFixedMode(200);
            uiDataGridView1.AddColumn("供应商名称", "Name");//.SetFixedMode(200);
            await LoadDataGrid();
        }

    

        private async void UiPagination1_PageChanged(object sender, object pagingSource, int pageIndex, int count)
        {
            await LoadDataGrid();
        }

        private void UiDataGridView1_ColumnHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            // 双击列头全选
            if (e.ColumnIndex == 0)
            {
                if (listData.Exists(x => x.IsCheck == false))
                {
                    listData.ForEach(x =>
                    {
                        x.IsCheck = true;
                    });
                }
                else
                {
                    listData.ForEach(x =>
                    {
                        x.IsCheck = false;
                    });
                }
                uiDataGridView1.DataSource = null;
                uiDataGridView1.DataSource = listData;
            }
        }

        private async Task LoadDataGrid()
        {
            var data = await CommonRequestHelper.GetService<IQuerySupplierService>().GetListPagedAsync<SupplierView>(
         uiPagination1.ActivePage, uiPagination1.PageSize,
         new BaseSupplierCondition()
         {
             Code = this.txtCode.Text.Trim()
         }
            );
            listData = data.Data;
            this.uiDataGridView1.DataSource = listData;
            this.uiPagination1.TotalCount = data.Total;
        }
        private bool GetSelectRows()
        {
            selectRows = listData.Where(x => x.IsCheck).ToList();
            if (selectRows.Any() == false)
            {
                UIMessageTip.ShowWarning("请选中一行");
                return false;
            }
            return true;
        }

        List<SupplierView> listData = new List<SupplierView>();
        List<SupplierView> selectRows = new List<SupplierView>();

        private async void BtnAdd_Click(object sender, EventArgs e)
        {
            SupplierEditForm editForm = new SupplierEditForm(async (data)=> {
                var result = await CommonRequestHelper.GetService<ICommandSupplierService>().InsertAsync(data);
                if (result.IsSuccess)
                {
                    CommonRequestHelper.RequestTip(result, () => { LoadDataGrid(); });
                }
                else
                {
                    UIMessageTip.ShowError(result.ErrorMessage);

                }
                return result.IsSuccess;
            });
            editForm.Text = "新增供应商";
            editForm.ShowDialog();

            if (editForm.IsOK)
            {
                 
            }

            editForm.Dispose();
        }

        private async void BtnEdit_Click(object sender, EventArgs e)
        {
            if (GetSelectRows())
            {
                if(selectRows.Count>1)
                {
                    UIMessageBox.Show("编辑不能选择多行");
                    return;
                }
                var data = selectRows.First().MapTo<SupplierEntity>();
                SupplierEditForm editForm = new SupplierEditForm(async (data)=> {
                    var service = CommonRequestHelper.GetService<ICommandSupplierService>();
                    var result = await service.UpdateAsync(data);

                    if (result.IsSuccess)
                    {
                        CommonRequestHelper.RequestTip(result, () => { LoadDataGrid(); });
                    }
                    else
                    {
                        UIMessageTip.ShowError(result.ErrorMessage);

                    }
                    return result.IsSuccess;
                });
                editForm.Text = "更新供应商";
                editForm.Data = data;
                editForm.DisabledControl();
                editForm.ShowDialog();

                if (editForm.IsOK)
                {
                   

                }

                editForm.Dispose();
            }
        }

        private async void BtnDelete_Click(object sender, EventArgs e)
        {
            if (GetSelectRows())
            {
                if (UIMessageBox.ShowAsk("删除的数据不可恢复，确认删除？"))
                {

                    CommonRequestHelper.RequestTip<bool>(
                        await CommonRequestHelper.GetService<ICommandSupplierService>().DeleteBatchAsync(selectRows.Select(x => x.Id).ToList())
                        , () => { LoadDataGrid(); }, "删除成功");
                }
            }
        }

        private void BtnSearch_Click(object sender, EventArgs e)
        {
            LoadDataGrid();
        }

        private void SupplierForm_Load(object sender, EventArgs e)
        {
            InitDataGrid();
        }
    }
}
