﻿using CodePrintSystem.AppLayer.Basic.ViewObject;
using CodePrintSystem.Basic.Abstractions;
using CodePrintSystem.Core;
using CodePrintSystem.Utility;
using Sunny.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ToolKitCore.MyNPOIExtension;

namespace CodePrintSystem.Forms
{
    public partial class PrintLogForm : UITitlePage, ISelfSingletonAutoInject
    {
        public PrintLogForm()
        {
            InitializeComponent();
            this.Text = "打印记录";
        }



        private async Task InitDataGridAsync()
        {
            await BindUserAsync();
            uiPagination1.ActivePage = 1;
            uiPagination1.PageSize = 20;
            uiDataGridView1.ReadOnly = false;
            uiDataGridView1.ColumnHeaderMouseDoubleClick += UiDataGridView1_ColumnHeaderMouseDoubleClick;
            uiPagination1.PageChanged += UiPagination1_PageChanged;
            var checkColumn = uiDataGridView1.AddCheckBoxColumn("选择", "IsCheck").SetFixedMode(100);
            checkColumn.ReadOnly = false;
            uiDataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;//自适应
            //uiDataGridView1.se;
            uiDataGridView1.AddColumn("条码", "Code");//.SetFixedMode(200);
            uiDataGridView1.AddColumn("产品编码", "ItemCode");//.SetFixedMode(200);
            uiDataGridView1.AddColumn("生产制令", "ProductionCode");
            uiDataGridView1.AddColumn("状态", "StatusDesc");
            uiDataGridView1.AddColumn("打印人", "OperateName");
            uiDataGridView1.AddColumn("打印时间", "StrPrintTime");
            await LoadDataGridAsync();

        }
        List<CodePrintHistoryView> listData = new List<CodePrintHistoryView>();

        private void UiPagination1_PageChanged(object sender, object pagingSource, int pageIndex, int count)
        {
            LoadDataGridAsync();
        }

        private void UiDataGridView1_ColumnHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            // 双击列头全选
            if (e.ColumnIndex == 0)
            {
                if (listData.Exists(x => x.IsCheck == false))
                {
                    listData.ForEach(x =>
                    {
                        x.IsCheck = true;
                    });
                }
                else
                {
                    listData.ForEach(x =>
                    {
                        x.IsCheck = false;
                    });
                }
                uiDataGridView1.DataSource = null;
                uiDataGridView1.DataSource = listData;
            }
        }
        public async Task BindUserAsync()
        {
            var list = await CommonRequestHelper.GetService<IQueryUserService>().GetListAsync<UserEntity>();
            cbx_user.ValueMember = "UserName";
            cbx_user.DisplayMember = "UserName";
            list.Insert(0, new UserEntity() { Id = 0, UserName = "" });
            cbx_user.DataSource = list;
            cbx_user.SelectedIndex = 0;
        }

        private async Task LoadDataGridAsync()
        {
            var startTime = this.txt_startTime.Value.Date;//00:00:00
            var endTime = this.txt_endTime.Value.EndOfDay();// 23:59:59

            var data = await CommonRequestHelper.GetService<IQueryCodePrintHistoryService>().GetListPagedAsync<CodePrintHistoryView>(
         uiPagination1.ActivePage, uiPagination1.PageSize,
         new BaseCodePrintHistoryCondition()
         {
             Code = this.txtCode.Text.Trim(),
             ItemCode = txtItemCode.Text.Trim(),
             ProductionCode = this.txt_ProductionCode.Text.Trim(),
             PrintTimeStart = startTime,
             PrintTimeEnd = endTime,
             OperateName = cbx_user.SelectedValue.ToString()
         }
            );
            listData = data.Data;
            this.uiDataGridView1.DataSource = listData;
            this.uiPagination1.TotalCount = data.Total;
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            LoadDataGridAsync();
        }

        private void btn_exportPage_Click(object sender, EventArgs e)
        {
            ExportPageAsync();
        }

        public async Task ExportPageAsync()
        {
            var startTime = this.txt_startTime.Value.Date;//00:00:00
            var endTime = this.txt_endTime.Value.EndOfDay();// 23:59:59
            var data = await CommonRequestHelper.GetService<IQueryCodePrintHistoryService>().GetListPagedAsync<CodePrintHistoryView>(
       uiPagination1.ActivePage, uiPagination1.PageSize,
       new BaseCodePrintHistoryCondition()
       {
           Code = this.txtCode.Text.Trim(),
           ItemCode = txtItemCode.Text.Trim(),
           ProductionCode = this.txt_ProductionCode.Text.Trim(),
           OperateName = cbx_user.SelectedValue.ToString(),
           PrintTimeStart = startTime,
           PrintTimeEnd = endTime
       }) ;
            if (!data.Data.IsListNullOrEmpty())
            {
                ExportToExcelModel<CodePrintHistoryView> exportToExcelModel = new ExportToExcelModel<CodePrintHistoryView>();
                exportToExcelModel.Data = data.Data;
                exportToExcelModel.FileName = DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".xls";
                exportToExcelModel.Path = @$"{AppsettingsConfig.ExportExcelPath}";
                await ExportImportExcelHelper.ExportToExcelAsync(exportToExcelModel);
                UIMessageTip.ShowOk("导出成功");
            }
            else
            {
                UIMessageTip.ShowWarning("没有数据");
            }

        }
        private void btn_ExportAll_Click(object sender, EventArgs e)
        {
            ExportAllAsync();
        }

        public async Task ExportAllAsync()
        {
            var startTime = this.txt_startTime.Value.Date;//00:00:00
            var endTime = this.txt_endTime.Value.EndOfDay();// 23:59:59
            var data = await CommonRequestHelper.GetService<IQueryCodePrintHistoryService>().GetListAsync<CodePrintHistoryView>(
            new BaseCodePrintHistoryCondition()
            {
                Code = this.txtCode.Text.Trim(),
                PrintTimeStart = startTime,
                PrintTimeEnd = endTime
            }
          );

            if (!data.IsListNullOrEmpty())
            {
                ExportToExcelModel<CodePrintHistoryView> exportToExcelModel = new ExportToExcelModel<CodePrintHistoryView>();
                exportToExcelModel.Data = data;
                exportToExcelModel.FileName = DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".xls";
                exportToExcelModel.Path = @$"{AppsettingsConfig.ExportExcelPath}";
                await ExportImportExcelHelper.ExportToExcelAsync(exportToExcelModel);
                UIMessageTip.ShowOk("导出成功");
            }
            else
            {
                UIMessageTip.ShowWarning("没有数据");
            }

        }

        private void PrintLogForm_Load(object sender, EventArgs e)
        {

            uiDataGridView1.AutoGenerateColumns = false;
            this.txt_startTime.Value = DateTime.Now;
            this.txt_endTime.Value = DateTime.Now.AddDays(1);
            InitDataGridAsync();
        }
    }
}
