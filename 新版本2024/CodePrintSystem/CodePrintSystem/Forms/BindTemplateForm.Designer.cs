﻿
namespace CodePrintSystem.Forms
{
    partial class BindTemplateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbx_user = new Sunny.UI.UIComboBox();
            this.uiLabel5 = new Sunny.UI.UILabel();
            this.SuspendLayout();
            // 
            // pnlBtm
            // 
            this.pnlBtm.Location = new System.Drawing.Point(1, 211);
            this.pnlBtm.Size = new System.Drawing.Size(418, 55);
            // 
            // cbx_user
            // 
            this.cbx_user.FillColor = System.Drawing.Color.White;
            this.cbx_user.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.cbx_user.Location = new System.Drawing.Point(151, 82);
            this.cbx_user.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbx_user.MinimumSize = new System.Drawing.Size(63, 0);
            this.cbx_user.Name = "cbx_user";
            this.cbx_user.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            this.cbx_user.Size = new System.Drawing.Size(174, 29);
            this.cbx_user.TabIndex = 9;
            this.cbx_user.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel5
            // 
            this.uiLabel5.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.uiLabel5.Location = new System.Drawing.Point(65, 82);
            this.uiLabel5.Name = "uiLabel5";
            this.uiLabel5.Size = new System.Drawing.Size(79, 23);
            this.uiLabel5.TabIndex = 10;
            this.uiLabel5.Text = "模板";
            this.uiLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // BindTemplateForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(420, 269);
            this.Controls.Add(this.uiLabel5);
            this.Controls.Add(this.cbx_user);
            this.Name = "BindTemplateForm";
            this.Text = "BindTemplate";
            this.Load += new System.EventHandler(this.BindTemplate_Load);
            this.Controls.SetChildIndex(this.pnlBtm, 0);
            this.Controls.SetChildIndex(this.cbx_user, 0);
            this.Controls.SetChildIndex(this.uiLabel5, 0);
            this.ResumeLayout(false);

        }

        #endregion

        private Sunny.UI.UIComboBox cbx_user;
        private Sunny.UI.UILabel uiLabel5;
    }
}