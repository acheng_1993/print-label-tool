﻿using Sunny.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodePrintSystem.Forms
{
    public partial class PrintProcessForm : UIForm
    {
        public PrintProcessForm()
        {
            InitializeComponent();
            this.Text = "打印中";
            this.EscClose = false;
            this.ControlBox = false;
        }
        public event EventHandler ButtonStopClick;

        public void UpdateMsg(string msg) {
            this.Invoke(new Action(()=> {
                this.lblMsg.Text = msg;
            }));
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            if (ButtonStopClick != null)
            {
                ButtonStopClick.Invoke(sender, e);
            }
            this.Close();
        }
    }
}
