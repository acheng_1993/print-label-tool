﻿
namespace CodePrintSystem.Forms
{
    partial class ItemEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtCode = new Sunny.UI.UITextBox();
            this.uiLabel1 = new Sunny.UI.UILabel();
            this.txtName = new Sunny.UI.UITextBox();
            this.uiLabel2 = new Sunny.UI.UILabel();
            this.txt_Version = new Sunny.UI.UITextBox();
            this.uiLabel3 = new Sunny.UI.UILabel();
            this.SuspendLayout();
            // 
            // pnlBtm
            // 
            this.pnlBtm.Location = new System.Drawing.Point(1, 255);
            this.pnlBtm.Size = new System.Drawing.Size(453, 55);
            // 
            // txtCode
            // 
            this.txtCode.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtCode.FillColor = System.Drawing.Color.White;
            this.txtCode.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtCode.Location = new System.Drawing.Point(129, 67);
            this.txtCode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtCode.Maximum = 2147483647D;
            this.txtCode.Minimum = -2147483648D;
            this.txtCode.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtCode.Name = "txtCode";
            this.txtCode.Padding = new System.Windows.Forms.Padding(5);
            this.txtCode.Size = new System.Drawing.Size(241, 29);
            this.txtCode.TabIndex = 5;
            // 
            // uiLabel1
            // 
            this.uiLabel1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.uiLabel1.Location = new System.Drawing.Point(39, 67);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Size = new System.Drawing.Size(83, 23);
            this.uiLabel1.TabIndex = 4;
            this.uiLabel1.Text = "产品编码";
            this.uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtName
            // 
            this.txtName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtName.FillColor = System.Drawing.Color.White;
            this.txtName.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtName.Location = new System.Drawing.Point(129, 125);
            this.txtName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtName.Maximum = 2147483647D;
            this.txtName.Minimum = -2147483648D;
            this.txtName.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtName.Name = "txtName";
            this.txtName.Padding = new System.Windows.Forms.Padding(5);
            this.txtName.Size = new System.Drawing.Size(241, 29);
            this.txtName.TabIndex = 7;
            // 
            // uiLabel2
            // 
            this.uiLabel2.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.uiLabel2.Location = new System.Drawing.Point(39, 125);
            this.uiLabel2.Name = "uiLabel2";
            this.uiLabel2.Size = new System.Drawing.Size(83, 23);
            this.uiLabel2.TabIndex = 6;
            this.uiLabel2.Text = "产品名称";
            this.uiLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txt_Version
            // 
            this.txt_Version.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_Version.FillColor = System.Drawing.Color.White;
            this.txt_Version.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txt_Version.Location = new System.Drawing.Point(129, 178);
            this.txt_Version.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txt_Version.Maximum = 2147483647D;
            this.txt_Version.Minimum = -2147483648D;
            this.txt_Version.MinimumSize = new System.Drawing.Size(1, 1);
            this.txt_Version.Name = "txt_Version";
            this.txt_Version.Padding = new System.Windows.Forms.Padding(5);
            this.txt_Version.Size = new System.Drawing.Size(241, 29);
            this.txt_Version.TabIndex = 9;
            // 
            // uiLabel3
            // 
            this.uiLabel3.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.uiLabel3.Location = new System.Drawing.Point(70, 178);
            this.uiLabel3.Name = "uiLabel3";
            this.uiLabel3.Size = new System.Drawing.Size(83, 23);
            this.uiLabel3.TabIndex = 8;
            this.uiLabel3.Text = "版本";
            this.uiLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ItemEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(455, 313);
            this.Controls.Add(this.txt_Version);
            this.Controls.Add(this.uiLabel3);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.uiLabel2);
            this.Controls.Add(this.txtCode);
            this.Controls.Add(this.uiLabel1);
            this.Name = "ItemEditForm";
            this.Text = "ItemEditForm";
            this.Controls.SetChildIndex(this.pnlBtm, 0);
            this.Controls.SetChildIndex(this.uiLabel1, 0);
            this.Controls.SetChildIndex(this.txtCode, 0);
            this.Controls.SetChildIndex(this.uiLabel2, 0);
            this.Controls.SetChildIndex(this.txtName, 0);
            this.Controls.SetChildIndex(this.uiLabel3, 0);
            this.Controls.SetChildIndex(this.txt_Version, 0);
            this.ResumeLayout(false);

        }

        #endregion

        private Sunny.UI.UITextBox txtCode;
        private Sunny.UI.UILabel uiLabel1;
        private Sunny.UI.UITextBox txtName;
        private Sunny.UI.UILabel uiLabel2;
        private Sunny.UI.UITextBox txt_Version;
        private Sunny.UI.UILabel uiLabel3;
    }
}