﻿
namespace CodePrintSystem.Forms
{
    partial class TagTemplateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            uiDataGridView1 = new Sunny.UI.UIDataGridView();
            uiPanel1 = new Sunny.UI.UIPanel();
            btnDelete = new Sunny.UI.UISymbolButton();
            btnEdit = new Sunny.UI.UISymbolButton();
            btnAdd = new Sunny.UI.UISymbolButton();
            btnSearch = new Sunny.UI.UIButton();
            txtCode = new Sunny.UI.UITextBox();
            uiLabel1 = new Sunny.UI.UILabel();
            uiPagination1 = new Sunny.UI.UIPagination();
            ((System.ComponentModel.ISupportInitialize)uiDataGridView1).BeginInit();
            uiPanel1.SuspendLayout();
            SuspendLayout();
            // 
            // PagePanel
            // 
            PagePanel.Size = new System.Drawing.Size(1399, 876);
            // 
            // uiDataGridView1
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(235, 243, 255);
            uiDataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            uiDataGridView1.BackgroundColor = System.Drawing.Color.White;
            uiDataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(80, 160, 255);
            dataGridViewCellStyle2.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(80, 160, 255);
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            uiDataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            uiDataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(48, 48, 48);
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(155, 200, 255);
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(48, 48, 48);
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            uiDataGridView1.DefaultCellStyle = dataGridViewCellStyle3;
            uiDataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            uiDataGridView1.EnableHeadersVisualStyles = false;
            uiDataGridView1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            uiDataGridView1.GridColor = System.Drawing.Color.FromArgb(80, 160, 255);
            uiDataGridView1.Location = new System.Drawing.Point(0, 94);
            uiDataGridView1.Name = "uiDataGridView1";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(235, 243, 255);
            dataGridViewCellStyle4.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(80, 160, 255);
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            uiDataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            uiDataGridView1.RowsDefaultCellStyle = dataGridViewCellStyle5;
            uiDataGridView1.RowTemplate.Height = 29;
            uiDataGridView1.SelectedIndex = -1;
            uiDataGridView1.ShowGridLine = true;
            uiDataGridView1.Size = new System.Drawing.Size(1399, 782);
            uiDataGridView1.TabIndex = 15;
            // 
            // uiPanel1
            // 
            uiPanel1.Controls.Add(btnDelete);
            uiPanel1.Controls.Add(btnEdit);
            uiPanel1.Controls.Add(btnAdd);
            uiPanel1.Controls.Add(btnSearch);
            uiPanel1.Controls.Add(txtCode);
            uiPanel1.Controls.Add(uiLabel1);
            uiPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            uiPanel1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            uiPanel1.Location = new System.Drawing.Point(0, 35);
            uiPanel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            uiPanel1.MinimumSize = new System.Drawing.Size(1, 1);
            uiPanel1.Name = "uiPanel1";
            uiPanel1.Size = new System.Drawing.Size(1399, 59);
            uiPanel1.TabIndex = 14;
            uiPanel1.Text = null;
            // 
            // btnDelete
            // 
            btnDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            btnDelete.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            btnDelete.Location = new System.Drawing.Point(490, 11);
            btnDelete.Margin = new System.Windows.Forms.Padding(0);
            btnDelete.MinimumSize = new System.Drawing.Size(1, 1);
            btnDelete.Name = "btnDelete";
            btnDelete.RadiusSides = Sunny.UI.UICornerRadiusSides.RightTop | Sunny.UI.UICornerRadiusSides.RightBottom;
            btnDelete.RectSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Top | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom;
            btnDelete.Size = new System.Drawing.Size(44, 35);
            btnDelete.Symbol = 61544;
            btnDelete.TabIndex = 7;
            // 
            // btnEdit
            // 
            btnEdit.Cursor = System.Windows.Forms.Cursors.Hand;
            btnEdit.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            btnEdit.Location = new System.Drawing.Point(446, 11);
            btnEdit.Margin = new System.Windows.Forms.Padding(0);
            btnEdit.MinimumSize = new System.Drawing.Size(1, 1);
            btnEdit.Name = "btnEdit";
            btnEdit.RadiusSides = Sunny.UI.UICornerRadiusSides.None;
            btnEdit.RectSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Top | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom;
            btnEdit.Size = new System.Drawing.Size(44, 35);
            btnEdit.Symbol = 61508;
            btnEdit.TabIndex = 6;
            // 
            // btnAdd
            // 
            btnAdd.Cursor = System.Windows.Forms.Cursors.Hand;
            btnAdd.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            btnAdd.Location = new System.Drawing.Point(402, 11);
            btnAdd.Margin = new System.Windows.Forms.Padding(0);
            btnAdd.MinimumSize = new System.Drawing.Size(1, 1);
            btnAdd.Name = "btnAdd";
            btnAdd.RadiusSides = Sunny.UI.UICornerRadiusSides.LeftTop | Sunny.UI.UICornerRadiusSides.LeftBottom;
            btnAdd.RectSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom;
            btnAdd.Size = new System.Drawing.Size(44, 35);
            btnAdd.Symbol = 61543;
            btnAdd.TabIndex = 5;
            // 
            // btnSearch
            // 
            btnSearch.Cursor = System.Windows.Forms.Cursors.Hand;
            btnSearch.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            btnSearch.Location = new System.Drawing.Point(283, 11);
            btnSearch.MinimumSize = new System.Drawing.Size(1, 1);
            btnSearch.Name = "btnSearch";
            btnSearch.Size = new System.Drawing.Size(100, 35);
            btnSearch.TabIndex = 2;
            btnSearch.Text = "查询";
            // 
            // txtCode
            // 
            txtCode.Cursor = System.Windows.Forms.Cursors.IBeam;
            txtCode.FillColor = System.Drawing.Color.White;
            txtCode.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            txtCode.Location = new System.Drawing.Point(115, 17);
            txtCode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            txtCode.Maximum = 2147483647D;
            txtCode.Minimum = -2147483648D;
            txtCode.MinimumSize = new System.Drawing.Size(1, 1);
            txtCode.Name = "txtCode";
            txtCode.Padding = new System.Windows.Forms.Padding(5);
            txtCode.Size = new System.Drawing.Size(150, 29);
            txtCode.TabIndex = 1;
            // 
            // uiLabel1
            // 
            uiLabel1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            uiLabel1.Location = new System.Drawing.Point(39, 17);
            uiLabel1.Name = "uiLabel1";
            uiLabel1.Size = new System.Drawing.Size(79, 23);
            uiLabel1.TabIndex = 0;
            uiLabel1.Text = "标签模板";
            uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiPagination1
            // 
            uiPagination1.BackColor = System.Drawing.SystemColors.Control;
            uiPagination1.Dock = System.Windows.Forms.DockStyle.Bottom;
            uiPagination1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            uiPagination1.Location = new System.Drawing.Point(0, 876);
            uiPagination1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            uiPagination1.MinimumSize = new System.Drawing.Size(1, 1);
            uiPagination1.Name = "uiPagination1";
            uiPagination1.PagerCount = 5;
            uiPagination1.RectSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.None;
            uiPagination1.Size = new System.Drawing.Size(1399, 35);
            uiPagination1.TabIndex = 13;
            uiPagination1.Text = null;
            // 
            // TagTemplateForm
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(10F, 21F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(1399, 911);
            Controls.Add(uiDataGridView1);
            Controls.Add(uiPanel1);
            Controls.Add(uiPagination1);
            Name = "TagTemplateForm";
            Text = "TagTemplateForm";
            Load += TagTemplateForm_Load;
            Controls.SetChildIndex(PagePanel, 0);
            Controls.SetChildIndex(uiPagination1, 0);
            Controls.SetChildIndex(uiPanel1, 0);
            Controls.SetChildIndex(uiDataGridView1, 0);
            ((System.ComponentModel.ISupportInitialize)uiDataGridView1).EndInit();
            uiPanel1.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private Sunny.UI.UIDataGridView uiDataGridView1;
        private Sunny.UI.UIPanel uiPanel1;
        private Sunny.UI.UISymbolButton btnDelete;
        private Sunny.UI.UISymbolButton btnEdit;
        private Sunny.UI.UISymbolButton btnAdd;
        private Sunny.UI.UIButton btnSearch;
        private Sunny.UI.UITextBox txtCode;
        private Sunny.UI.UILabel uiLabel1;
        private Sunny.UI.UIPagination uiPagination1;
    }
}