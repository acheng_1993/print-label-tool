﻿using CodePrintSystem.Basic.Abstractions;
using CodePrintSystem.Utility;
using Sunny.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodePrintSystem.Forms
{
    public partial class UserEditForm : UIEditForm
    {
        Func<UserEntity, Task<bool>> func;
        public UserEditForm(Func<UserEntity, Task<bool>> func)
        {
            InitializeComponent();
            this.func = func;
            if (func != null)
            {
                this.ButtonOkClick += btnOK_Click_1;
            }
        }

        public void DisabledControl()
        {
            this.txtUserName.Enabled = false;
        }

        protected override bool CheckData()
        {
            return CheckEmpty(txtUserName, "请输入姓名");
        }
        private UserEntity user;
        public UserEntity User
        {
            get
            {
                if (user == null)
                {
                    user = new UserEntity();
                }
                user.UserName = txtUserName.Text.Trim();
                user.UserPassword = txt_pwd.Text.Trim();
                user.IsEnabled = checkIsEnabled.Checked;
                return user;
            }

            set
            {
                if (user == null)
                {
                    user = new UserEntity();
                }
                user.Id = value.Id;
                txtUserName.Text = value.UserName;
                txt_pwd.Text = "";
                checkIsEnabled.Checked = value.IsEnabled.Value;
            }
        }

        private async void btnOK_Click_1(object sender, EventArgs e)
        {
            var result = await func?.Invoke(User);
            if (result == true)
            {
                DialogResult = DialogResult.OK;
                Close();
                Live20RClient.Close();
            }
        }

        private void UserEditForm_Load(object sender, EventArgs e)
        {
            RestFingerprint();
        }


        private void UserEditForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Live20RClient.Close();
        }

        #region 指纹采集
        /// <summary>
        /// 采集之后的事情
        /// </summary>
        /// <param name="result"></param>
        public void CollectAfter(FingerprintResult result)
        {
            this.Invoke(new Action(() =>
            {
                if (result.IsSuccess == false)
                {
                    Log(result.ErrorMsg);
                    return;
                }
                this.pictureBox1.Image = result.Image;
                if (result.RegisterCount >= 3)// 采集结束
                {
                    this.User.FingePrintPassword = result.CapTmp;
                    this.User.FingePrintPasswordMD5 = EncryptDecryptHelper.Md5(result.CapTmp);
                    this.User.IsFingePrint = true;
                    Log($"指纹登记完成，请保存！");
                }
                else
                {
                    Log($"还需要采集指纹{3 - result.RegisterCount}次");
                }
            }));
        }

        /// <summary>
        /// 重置指纹采集
        /// </summary>
        private void RestFingerprint()
        {
            uiListBox1.Items.Add("*可以指纹采集了！");
           // this.textRes.Text = "*可以指纹采集了！";
            Task.Run(() => { Live20RClient.Open(CollectAfter, true); });
            Live20RClient.ResetRegister();
        }



        /// <summary>
        /// 指纹相关操作日志
        /// </summary>
        /// <param name="mes"></param>
        private void Log(string mes)
        {
            // this.textRes.Text += $"\r\n{DateTime.Now.ToString()}  {mes}";
            uiListBox1.Items.Add($"{DateTime.Now.ToString()}  {mes}");
        }
        #endregion
    }
}
