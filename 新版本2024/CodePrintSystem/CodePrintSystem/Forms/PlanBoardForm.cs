﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CodePrintSystem.Basic.Abstractions;
using CodePrintSystem.Core;
using CodePrintSystem.Utility;

namespace CodePrintSystem.Forms
{
    public partial class PlanBoardForm : Form, ISelfSingletonAutoInject
    {
        public PlanBoardForm()
        {
            InitializeComponent();
            this.Load += PlanBoardForm_Load;
        }

        private void PlanBoardForm_Load(object sender, EventArgs e)
        {
            uiDataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;//自适应
            uiDataGridView1.AutoGenerateColumns = false;
            //uiDataGridView1.se;
            uiDataGridView1.AddColumn("产品码", "ItemCode");//.SetFixedMode(200);
            uiDataGridView1.AddColumn("日期", "PlanDate");//.SetFixedMode(100);
            uiDataGridView1.AddColumn("计划数量", "PlanQty");//.SetFixedMode(100);
            uiDataGridView1.AddColumn("已打印", "ProductQty");//.SetFixedMode(100);
            uiDataGridView1.AddColumn("未打印", "NoProductQty");//.SetFixedMode(100);
            uiDataGridView1.AddColumn("百分比%", "Percent");//.SetFixedMode(100);
            uiDataGridView1.AddColumn("扫码正常", "ScanOk");//.SetFixedMode(100);
            uiDataGridView1.AddColumn("扫码错误", "ScanNg");//.SetFixedMode(100);
            uiDataGridView1.AddColumn("扫码重复", "ScanRepeat");//.SetFixedMode(100);
            uiDataGridView1.RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            uiDataGridView1.RowsDefaultCellStyle.Font = new Font("微软雅黑",25);
            uiDataGridView1.BackgroundColor = Color.Black;
            uiDataGridView1.RowsDefaultCellStyle.BackColor = Color.Black;
            GetBoardInfo();
        }

        private async void GetBoardInfo() {
            var planQuery = CommonRequestHelper.GetService<IQueryPrintPlanService>();
            var printLogQuery = CommonRequestHelper.GetService<IQueryCodePrintHistoryService>();
            var scanQuery = CommonRequestHelper.GetService<IQueryCodeScanHistoryService>();

           var plan =await  planQuery.GetBoardAsync(DateTime.Now);
            plan.ForEach(x =>
            {
                if(x.PlanQty.Value> x.ProductQty)
                x.NoProductQty = x.PlanQty.Value - x.ProductQty;

                x.Percent = x.ProductQty / x.PlanQty.Value * 100;
            });
            uiDataGridView1.DataSource = null;
            uiDataGridView1.DataSource = plan;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            GetBoardInfo();
        }
    }
}
