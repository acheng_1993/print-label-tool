﻿using CodePrintSystem.Basic.Abstractions;
using Sunny.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodePrintSystem.Forms
{
    public partial class PlanEditForm : UIEditForm
    {
        Func<PrintPlanEntity, Task<bool>> func;
        public PlanEditForm(Func<PrintPlanEntity, Task<bool>> func)
        {
            InitializeComponent();
            this.planDate.Value = DateTime.Now;
            this.func = func;
            if (func != null)
            {
                this.ButtonOkClick += btnOK_Click_1;
            }
        }

        public void DisabledControl()
        {
          
        }

        protected override bool CheckData()
        {
             return CheckEmpty(txtItemCode, "请输入产品码")&& CheckEmpty(planDate, "请输入日期");
           // return true;
        }
        private PrintPlanEntity plan;
        public PrintPlanEntity Plan
        {
            get
            {
                if (plan == null)
                {
                    plan = new PrintPlanEntity();
                    plan.PlanDate = DateTime.Today;
                }
                 plan.ItemCode = txtItemCode.Text;
                plan.PlanDate = planDate.Value ;
                plan.PlanQty = numCount.Value;
                return plan;
            }

            set
            {
                if (plan == null)
                {
                    plan = new PrintPlanEntity();
                    plan.PlanDate = DateTime.Today;
                }
                plan.Id = value.Id;
                txtItemCode.Text = value.ItemCode;
                planDate.Value = value.PlanDate.Value.Date;
                numCount.Value = value.PlanQty.Value;
            }
        }

        private async void btnOK_Click_1(object sender, EventArgs e)
        {
            var result = await func?.Invoke(Plan);
            if (result == true)
            {
                DialogResult = DialogResult.OK;
                Close();
            }
        }
    }
}
