﻿
namespace CodePrintSystem.Forms
{
    partial class BaseDataEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtName = new Sunny.UI.UITextBox();
            this.uiLabel2 = new Sunny.UI.UILabel();
            this.txtCode = new Sunny.UI.UITextBox();
            this.uiLabel1 = new Sunny.UI.UILabel();
            this.uiLabel3 = new Sunny.UI.UILabel();
            this.uiLabel4 = new Sunny.UI.UILabel();
            this.cbIsCheck = new Sunny.UI.UICheckBox();
            this.txtRemark = new Sunny.UI.UITextBox();
            this.uiLabel5 = new Sunny.UI.UILabel();
            this.cbDicType = new Sunny.UI.UIComboBox();
            this.SuspendLayout();
            // 
            // pnlBtm
            // 
            this.pnlBtm.Location = new System.Drawing.Point(1, 403);
            this.pnlBtm.Size = new System.Drawing.Size(432, 55);
            // 
            // txtName
            // 
            this.txtName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtName.FillColor = System.Drawing.Color.White;
            this.txtName.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtName.Location = new System.Drawing.Point(129, 160);
            this.txtName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtName.Maximum = 2147483647D;
            this.txtName.Minimum = -2147483648D;
            this.txtName.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtName.Name = "txtName";
            this.txtName.Padding = new System.Windows.Forms.Padding(5);
            this.txtName.Size = new System.Drawing.Size(241, 29);
            this.txtName.TabIndex = 17;
            // 
            // uiLabel2
            // 
            this.uiLabel2.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.uiLabel2.Location = new System.Drawing.Point(41, 160);
            this.uiLabel2.Name = "uiLabel2";
            this.uiLabel2.Size = new System.Drawing.Size(94, 23);
            this.uiLabel2.TabIndex = 16;
            this.uiLabel2.Text = "名称";
            this.uiLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtCode
            // 
            this.txtCode.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtCode.FillColor = System.Drawing.Color.White;
            this.txtCode.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtCode.Location = new System.Drawing.Point(129, 102);
            this.txtCode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtCode.Maximum = 2147483647D;
            this.txtCode.Minimum = -2147483648D;
            this.txtCode.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtCode.Name = "txtCode";
            this.txtCode.Padding = new System.Windows.Forms.Padding(5);
            this.txtCode.Size = new System.Drawing.Size(241, 29);
            this.txtCode.TabIndex = 15;
            // 
            // uiLabel1
            // 
            this.uiLabel1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.uiLabel1.Location = new System.Drawing.Point(41, 102);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Size = new System.Drawing.Size(94, 23);
            this.uiLabel1.TabIndex = 14;
            this.uiLabel1.Text = "编码";
            this.uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel3
            // 
            this.uiLabel3.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.uiLabel3.Location = new System.Drawing.Point(41, 55);
            this.uiLabel3.Name = "uiLabel3";
            this.uiLabel3.Size = new System.Drawing.Size(94, 23);
            this.uiLabel3.TabIndex = 18;
            this.uiLabel3.Text = "类型";
            this.uiLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel4
            // 
            this.uiLabel4.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.uiLabel4.Location = new System.Drawing.Point(41, 220);
            this.uiLabel4.Name = "uiLabel4";
            this.uiLabel4.Size = new System.Drawing.Size(94, 23);
            this.uiLabel4.TabIndex = 19;
            this.uiLabel4.Text = "是否启用";
            this.uiLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbIsCheck
            // 
            this.cbIsCheck.Checked = true;
            this.cbIsCheck.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cbIsCheck.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.cbIsCheck.Location = new System.Drawing.Point(129, 220);
            this.cbIsCheck.MinimumSize = new System.Drawing.Size(1, 1);
            this.cbIsCheck.Name = "cbIsCheck";
            this.cbIsCheck.Padding = new System.Windows.Forms.Padding(22, 0, 0, 0);
            this.cbIsCheck.Size = new System.Drawing.Size(150, 29);
            this.cbIsCheck.TabIndex = 20;
            this.cbIsCheck.Text = "启用";
            // 
            // txtRemark
            // 
            this.txtRemark.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtRemark.FillColor = System.Drawing.Color.White;
            this.txtRemark.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtRemark.Location = new System.Drawing.Point(129, 273);
            this.txtRemark.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtRemark.Maximum = 2147483647D;
            this.txtRemark.Minimum = -2147483648D;
            this.txtRemark.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtRemark.Name = "txtRemark";
            this.txtRemark.Padding = new System.Windows.Forms.Padding(5);
            this.txtRemark.Size = new System.Drawing.Size(241, 29);
            this.txtRemark.TabIndex = 22;
            // 
            // uiLabel5
            // 
            this.uiLabel5.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.uiLabel5.Location = new System.Drawing.Point(41, 273);
            this.uiLabel5.Name = "uiLabel5";
            this.uiLabel5.Size = new System.Drawing.Size(94, 23);
            this.uiLabel5.TabIndex = 21;
            this.uiLabel5.Text = "备注";
            this.uiLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbDicType
            // 
            this.cbDicType.DropDownStyle = Sunny.UI.UIDropDownStyle.DropDownList;
            this.cbDicType.FillColor = System.Drawing.Color.White;
            this.cbDicType.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.cbDicType.Location = new System.Drawing.Point(129, 55);
            this.cbDicType.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbDicType.MinimumSize = new System.Drawing.Size(63, 0);
            this.cbDicType.Name = "cbDicType";
            this.cbDicType.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            this.cbDicType.Size = new System.Drawing.Size(241, 29);
            this.cbDicType.TabIndex = 23;
            this.cbDicType.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // BaseDataEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(434, 461);
            this.Controls.Add(this.cbDicType);
            this.Controls.Add(this.txtRemark);
            this.Controls.Add(this.uiLabel5);
            this.Controls.Add(this.cbIsCheck);
            this.Controls.Add(this.uiLabel4);
            this.Controls.Add(this.uiLabel3);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.uiLabel2);
            this.Controls.Add(this.txtCode);
            this.Controls.Add(this.uiLabel1);
            this.Name = "BaseDataEditForm";
            this.Text = "BaseDataEditForm";
            this.Load += new System.EventHandler(this.BaseDataEditForm_Load);
            this.Controls.SetChildIndex(this.pnlBtm, 0);
            this.Controls.SetChildIndex(this.uiLabel1, 0);
            this.Controls.SetChildIndex(this.txtCode, 0);
            this.Controls.SetChildIndex(this.uiLabel2, 0);
            this.Controls.SetChildIndex(this.txtName, 0);
            this.Controls.SetChildIndex(this.uiLabel3, 0);
            this.Controls.SetChildIndex(this.uiLabel4, 0);
            this.Controls.SetChildIndex(this.cbIsCheck, 0);
            this.Controls.SetChildIndex(this.uiLabel5, 0);
            this.Controls.SetChildIndex(this.txtRemark, 0);
            this.Controls.SetChildIndex(this.cbDicType, 0);
            this.ResumeLayout(false);

        }

        #endregion

        private Sunny.UI.UITextBox txtName;
        private Sunny.UI.UILabel uiLabel2;
        private Sunny.UI.UITextBox txtCode;
        private Sunny.UI.UILabel uiLabel1;
        private Sunny.UI.UILabel uiLabel3;
        private Sunny.UI.UILabel uiLabel4;
        private Sunny.UI.UICheckBox cbIsCheck;
        private Sunny.UI.UITextBox txtRemark;
        private Sunny.UI.UILabel uiLabel5;
        private Sunny.UI.UIComboBox cbDicType;
    }
}