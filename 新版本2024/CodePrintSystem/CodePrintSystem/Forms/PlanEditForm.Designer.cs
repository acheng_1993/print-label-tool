﻿
namespace CodePrintSystem.Forms
{
    partial class PlanEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uiLabel1 = new Sunny.UI.UILabel();
            this.txtItemCode = new Sunny.UI.UITextBox();
            this.planDate = new Sunny.UI.UIDatePicker();
            this.uiLabel2 = new Sunny.UI.UILabel();
            this.uiLabel3 = new Sunny.UI.UILabel();
            this.numCount = new Sunny.UI.UIIntegerUpDown();
            this.SuspendLayout();
            // 
            // pnlBtm
            // 
            this.pnlBtm.Location = new System.Drawing.Point(1, 273);
            this.pnlBtm.Size = new System.Drawing.Size(535, 55);
            // 
            // uiLabel1
            // 
            this.uiLabel1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.uiLabel1.Location = new System.Drawing.Point(47, 66);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Size = new System.Drawing.Size(87, 23);
            this.uiLabel1.TabIndex = 2;
            this.uiLabel1.Text = "产品码";
            this.uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtItemCode
            // 
            this.txtItemCode.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtItemCode.FillColor = System.Drawing.Color.White;
            this.txtItemCode.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtItemCode.Location = new System.Drawing.Point(113, 66);
            this.txtItemCode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtItemCode.Maximum = 2147483647D;
            this.txtItemCode.Minimum = -2147483648D;
            this.txtItemCode.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtItemCode.Name = "txtItemCode";
            this.txtItemCode.Padding = new System.Windows.Forms.Padding(5);
            this.txtItemCode.Size = new System.Drawing.Size(233, 29);
            this.txtItemCode.TabIndex = 3;
            // 
            // planDate
            // 
            this.planDate.FillColor = System.Drawing.Color.White;
            this.planDate.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.planDate.Location = new System.Drawing.Point(113, 115);
            this.planDate.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.planDate.MaxLength = 10;
            this.planDate.MinimumSize = new System.Drawing.Size(63, 0);
            this.planDate.Name = "planDate";
            this.planDate.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            this.planDate.Size = new System.Drawing.Size(233, 29);
            this.planDate.SymbolDropDown = 61555;
            this.planDate.SymbolNormal = 61555;
            this.planDate.TabIndex = 4;
            this.planDate.Text = "2021-11-08";
            this.planDate.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.planDate.Value = new System.DateTime(2021, 11, 8, 23, 44, 9, 90);
            // 
            // uiLabel2
            // 
            this.uiLabel2.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.uiLabel2.Location = new System.Drawing.Point(47, 115);
            this.uiLabel2.Name = "uiLabel2";
            this.uiLabel2.Size = new System.Drawing.Size(47, 23);
            this.uiLabel2.TabIndex = 5;
            this.uiLabel2.Text = "日期";
            this.uiLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel3
            // 
            this.uiLabel3.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.uiLabel3.Location = new System.Drawing.Point(47, 174);
            this.uiLabel3.Name = "uiLabel3";
            this.uiLabel3.Size = new System.Drawing.Size(61, 23);
            this.uiLabel3.TabIndex = 6;
            this.uiLabel3.Text = "数量";
            this.uiLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // numCount
            // 
            this.numCount.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.numCount.HasMinimum = true;
            this.numCount.Location = new System.Drawing.Point(113, 174);
            this.numCount.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.numCount.Minimum = 1;
            this.numCount.MinimumSize = new System.Drawing.Size(100, 0);
            this.numCount.Name = "numCount";
            this.numCount.Size = new System.Drawing.Size(190, 29);
            this.numCount.TabIndex = 7;
            this.numCount.Text = null;
            this.numCount.Value = 1;
            // 
            // PlanEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(537, 331);
            this.Controls.Add(this.numCount);
            this.Controls.Add(this.uiLabel3);
            this.Controls.Add(this.uiLabel2);
            this.Controls.Add(this.planDate);
            this.Controls.Add(this.txtItemCode);
            this.Controls.Add(this.uiLabel1);
            this.Name = "PlanEditForm";
            this.Text = "PlanEditForm";
            this.Controls.SetChildIndex(this.pnlBtm, 0);
            this.Controls.SetChildIndex(this.uiLabel1, 0);
            this.Controls.SetChildIndex(this.txtItemCode, 0);
            this.Controls.SetChildIndex(this.planDate, 0);
            this.Controls.SetChildIndex(this.uiLabel2, 0);
            this.Controls.SetChildIndex(this.uiLabel3, 0);
            this.Controls.SetChildIndex(this.numCount, 0);
            this.ResumeLayout(false);

        }

        #endregion

        private Sunny.UI.UILabel uiLabel1;
        private Sunny.UI.UITextBox txtItemCode;
        private Sunny.UI.UIDatePicker planDate;
        private Sunny.UI.UILabel uiLabel2;
        private Sunny.UI.UILabel uiLabel3;
        private Sunny.UI.UIIntegerUpDown numCount;
    }
}