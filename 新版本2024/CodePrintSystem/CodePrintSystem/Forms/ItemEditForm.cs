﻿using CodePrintSystem.Basic.Abstractions;
using Sunny.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CodePrintSystem.Utility;

namespace CodePrintSystem.Forms
{
    public partial class ItemEditForm : UIEditForm
    {
        Func<ItemInfoEntity, Task<bool>> func;
        public ItemEditForm(Func<ItemInfoEntity, Task<bool>> func)
        {
            InitializeComponent();
            this.func = func;
            if (func != null)
            {
                this.ButtonOkClick += btnOK_Click_1;
            }
        }
        public void DisabledControl()
        {
            this.txtCode.Enabled = false;
        }
        protected override bool CheckData()
        {
            //
            //var checkItemResult = CommonHelper.CheckItemCode(txtCode.Text);
            //if (!string.IsNullOrEmpty(checkItemResult))
            //{

            //    UIMessageTip.ShowError(checkItemResult);
            //    return false;
            //}
            if (txt_Version.Text.Trim().Length ==0)
            {
                UIMessageTip.ShowError("请输入版本");
                return false;
            }
            return CheckEmpty(txtCode, "请输入编码") && CheckEmpty(txtName, "请输入名称");
        }
        private ItemInfoEntity itemInfo = new ItemInfoEntity();
        public ItemInfoEntity ItemInfo
        {
            get
            {
                itemInfo.Code = txtCode.Text;
                itemInfo.Name = txtName.Text;
                itemInfo.VersionCode = txt_Version.Text;
                return itemInfo;
            }

            set
            {
                itemInfo.Id = value.Id;
                txtCode.Text = value.Code;
                txtName.Text = value.Name;
                txt_Version.Text = value.VersionCode;
            }
        }

        private async void btnOK_Click_1(object sender, EventArgs e)
        {
            var result = await func?.Invoke(ItemInfo);
            if (result == true)
            {
                DialogResult = DialogResult.OK;
                Close();
            }
        }
    }
}
