﻿using CodePrintSystem.Basic.Abstractions;
using CodePrintSystem.Core;
using CodePrintSystem.EnumClass;
using CodePrintSystem.Utility;
using NPoco.Linq;
using NPOI.SS.Formula.Functions;
using Print.Helper;
using Sunny.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using VisioForge.Libs.DirectShowLib;

namespace CodePrintSystem.Forms
{
    public partial class CodePrintForm : UITitlePage, ISelfSingletonAutoInject
    {
        public CodePrintForm()
        {
            InitializeComponent();
            this.Text = "标签打印";
            this.btnPrint.Enabled = true;
            this.btnStop.Enabled = false;
            this.cbxTemplate.Enabled = false;
            this.cbx_supplier.Enabled = false;
            this.pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            this.dateCurrent.Value = DateTime.Now;
        }
        ItemInfoEntity currentItem = new ItemInfoEntity();
        SupplierEntity currentSupplier = new SupplierEntity();
        private void CbxItem_SelectedIndexChanged(object sender, EventArgs e)
        {

            ComboBox comboBox = (ComboBox)sender;
            if (comboBox.SelectedItem != null)
            {
                currentItem = (ItemInfoEntity)comboBox.SelectedItem;
                this.lblCurrentItem.Text = currentItem.Code;
                this.lblVersion.Text = currentItem.VersionCode;
                if (currentItem.TemplateId.HasValue && currentItem.TemplateId.Value > 0)
                {
                    var template = templateList.Find(x => x.Id == currentItem.TemplateId.Value);
                    if (template != null)
                    {
                        cbxTemplate.SelectedItem = template;
                        cbxTemplate.Enabled = false;
                        // selectChange();
                        // CbxTemplate_SelectedIndexChanged(null, null);
                    }
                    else
                    {
                        cbxTemplate.SelectedItem = null;
                        this.panel1.Controls.Clear();
                    }
                }
                else
                {
                    if (currentItem.Id == 0)
                    {
                        this.cbxTemplate.Enabled = false;
                    }
                    else
                    {
                        this.cbxTemplate.Enabled = true;

                    }
                    cbxTemplate.SelectedItem = null;
                    this.panel1.Controls.Clear();
                    currentTemplate = null;
                }
                cbx_supplier.Enabled = CurrentUserConfig.User.IsAdmin.Value;
                GetCountData();
            }
            else
            {
                currentItem = null;
            }
        }


        private void cbx_supplier_SelectedIndexChanged(object sender, EventArgs e)
        {
            currentSupplier = (SupplierEntity)cbx_supplier.SelectedItem;

            if (currentSupplier != null)
            {
                for (var i = 0; i < this.panel1.Controls.Count; i++)
                {
                    var control = this.panel1.Controls[i];
                    long codeId = 0;
                    string codeValue = string.Empty;
                    if (control.Name.Contains(preStr))
                    {

                        if (control is TextBox)
                        {
                            if (control.Tag != null && control.Tag.ToString() == CodeTemplateRuleEnum.SupplierCode.ToString())
                            {
                                control.Text = currentSupplier.Code;
                            }

                        }

                    }
                }
            }

        }
        List<ItemInfoEntity> currentItemList = new List<ItemInfoEntity>();
        private void CbxItem_Leave(object sender, EventArgs e)
        {
            ComboBox comboBox = (ComboBox)sender;
            var itemText = comboBox.Text.Trim();
            if (currentItemList.FirstOrDefault(x => x.Code == itemText) == null)
            {
                comboBox.Text = "";
                if (comboBox.Items.Count > 0)
                    comboBox.SelectedIndex = -1;
            }
        }
        private async void CbxItem_TextUpdate(object sender, EventArgs e)
        {
            ComboBox comboBox = (ComboBox)sender;
            var list = await CommonRequestHelper.GetService<IQueryItemInfoService
                   >().GetListPagedAsync<ItemInfoEntity>
              (1, 20, new BaseItemInfoCondition()
              {
                  KeyWord = comboBox.Text
              });
            currentItemList = list.Data;
            if (currentItemList.Any())
            {
                comboBox.Items.Clear();
                comboBox.Items.AddRange(currentItemList.ToArray());
            }
            comboBox.SelectionStart = comboBox.Text.Length;
            comboBox.DroppedDown = true;

            Cursor = Cursors.Default;
        }
        GenerateCodeTemplateEntity currentTemplate = null;
        List<GenerateCodeTemplateDetailEntity> currentTemplateList = new List<GenerateCodeTemplateDetailEntity>();
        private void CbxTemplate_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectChange();
        }



        public async Task selectChange()
        {
            var templateData = this.cbxTemplate.SelectedItem;
            // if (templateData == currentTemplate) { return; }
            if (templateData != null)
            {

                this.panel1.Controls.Clear();
                this.uiGroupBox1.Height = 125;// 分组容器默认高度
                var template = (GenerateCodeTemplateEntity)templateData;
                currentTemplate = template;
                currentTemplateList = await CommonRequestHelper.GetService<IQueryGenerateCodeTemplateDetailService>()
                    .GetListAsync<GenerateCodeTemplateDetailEntity>
                 (new BaseGenerateCodeTemplateDetailCondition()
                 {
                     TemplateId = template.Id,
                     IsEnabled = true,
                     // IsContain = true,
                 }, null, "rowNo asc");

                int index = 0;
                int lastControlX = 0;
                int lastControlWidth = 0;
                for (var i = 0; i < currentTemplateList.Count; i++)
                {
                    var detail = currentTemplateList[i];
                    var ruleType = (CodeTemplateRuleEnum)detail.RuleId.Value;
                    Control control = null;
                    Label label = new Label();
                    label.Text = detail.AliasName;
                    switch (ruleType)
                    {
                        case CodeTemplateRuleEnum.ItemCode:
                            var itemControl = new ComboBox();
                            if (currentItemList.Any())
                            {
                                itemControl.Items.Clear();
                                itemControl.Items.AddRange(currentItemList.ToArray());
                            }
                            itemControl.ValueMember = "Id";
                            itemControl.DisplayMember = "Code";
                            label.Text = "产品编码";
                            itemControl.Enabled = false;
                            if (currentItem != null)
                            {
                                itemControl.SelectedItem = currentItem;
                            }

                            itemControl.Size = new Size(150, 30);

                            control = itemControl;
                            break;
                        case CodeTemplateRuleEnum.SupplierCode:
                            var supplier = new TextBox();
                            supplier.Text = currentSupplier.Code;
                            supplier.Enabled = false;
                            supplier.Tag = CodeTemplateRuleEnum.SupplierCode.ToString();
                            control = supplier;
                            break;
                        case CodeTemplateRuleEnum.Config:
                            var config = new TextBox();
                            var appsettings = ConfigurationManager.AppSettings[detail.RuleValue];
                            config.Text = appsettings; //currentSupplier.Code;
                            config.Enabled = false;
                            config.Tag = CodeTemplateRuleEnum.Config.ToString();
                            control = config;
                            break;
                        case CodeTemplateRuleEnum.RemarkCharacter:// 合晟用于厂区区分
                            var factory = new TextBox();
                            var factoryCode = ConfigurationManager.AppSettings["FactoryCode"];
                            factory.Text = factoryCode; //currentSupplier.Code;
                            factory.Enabled = false;
                            factory.Tag = CodeTemplateRuleEnum.RemarkCharacter.ToString();
                            control = factory;
                            break;
                        case CodeTemplateRuleEnum.SequenceNumber:
                            var sequenceNumber = new TextBox();
                            sequenceNumber.Text = "自动生成";
                            sequenceNumber.Enabled = false;
                            sequenceNumber.Tag = "CanNotEdit";
                            control = sequenceNumber;
                            break;
                        case CodeTemplateRuleEnum.XiaoPengNumber:
                            var xpSequenceNumber = new TextBox();
                            xpSequenceNumber.Text = "自动生成";
                            xpSequenceNumber.Enabled = false;
                            xpSequenceNumber.Tag = "CanNotEdit";
                            control = xpSequenceNumber;
                            break;
                        case CodeTemplateRuleEnum.BYDTIME:
                            var bydtime = new TextBox();
                            bydtime.Text = "自动生成";
                            bydtime.Enabled = false;
                            bydtime.Tag = "CanNotEdit";
                            control = bydtime;
                            break;
                        case CodeTemplateRuleEnum.InputText:
                        case CodeTemplateRuleEnum.FixValue:
                            var fixValue = new TextBox();
                            fixValue.Text = detail.RuleValue;
                            fixValue.Enabled = CurrentUserConfig.User.IsAdmin.Value;
                            //label.Text = "固定值";
                            control = fixValue;
                            break;
                        case CodeTemplateRuleEnum.WeekOfYear:
                            var weekOfYear = new TextBox();
                            weekOfYear.Text = CommonHelper.GetYearWeekDayByTime(this.dateCurrent.Value).weekOfYear.ToString().PadLeft(2, '0');
                            weekOfYear.Enabled = false;
                            weekOfYear.Tag = "CanNotEdit";
                            // label.Text = "周次";
                            control = weekOfYear;
                            break;
                        case CodeTemplateRuleEnum.DayOfWeek:
                            var dayOfWeek = new TextBox();
                            dayOfWeek.Text = CommonHelper.GetYearWeekDayByTime(this.dateCurrent.Value).dayOfWeek.ToString();
                            dayOfWeek.Enabled = false;
                            dayOfWeek.Tag = "CanNotEdit";
                            //label.Text = "星期日期";
                            control = dayOfWeek;
                            break;

                        case CodeTemplateRuleEnum.Version:
                            var version = new TextBox();
                            version.Text = currentItem.VersionCode;
                            version.Enabled = false;
                            version.Tag = "CanNotEdit";
                            control = version;
                            break;
                        case CodeTemplateRuleEnum.Time:
                        case CodeTemplateRuleEnum.ClassShift:
                        case CodeTemplateRuleEnum.CustomType:
                        case CodeTemplateRuleEnum.Number:
                            //case CodeTemplateRuleEnum.RemarkCharacter:
                            // case CodeTemplateRuleEnum.Version:
                            try
                            {
                                var cbxDicValue = new ComboBox();
                                cbxDicValue.DropDownStyle = ComboBoxStyle.DropDownList;
                                var condition = new BaseBusinessdictionaryCondition()
                                {
                                    IsEnabled = true,
                                    DictionaryTypeId = CommonHelper.GetTypeId(detail.RuleId.Value)
                                };
                                var dicList = await CommonRequestHelper.GetService<IQueryBusinessdictionaryService>().GetListAsync<BusinessdictionaryEntity>(condition);
                                cbxDicValue.Tag = detail.RuleId.Value;
                                cbxDicValue.ValueMember = "Id";
                                cbxDicValue.DisplayMember = "Code";
                                cbxDicValue.Items.Clear();
                                cbxDicValue.Items.AddRange(dicList.ToArray());
                                // label.Text = ruleType.GetDescription();
                                if (!string.IsNullOrEmpty(detail.RuleValue))
                                {
                                    var defaultItem = dicList.Find(x => x.Code == detail.RuleValue);
                                    if (defaultItem != null)
                                    {
                                        cbxDicValue.SelectedItem = defaultItem;
                                        cbxDicValue.Enabled = CurrentUserConfig.User.IsAdmin.Value;
                                        if (ruleType == CodeTemplateRuleEnum.Time)
                                        {
                                            label.Text = ((CodeTimeRuleEnum)defaultItem.Id).GetDescription();

                                        }
                                    }

                                }
                                control = cbxDicValue;
                            }
                            catch (Exception ex)
                            {

                            }
                            break;


                    }
                    if (control == null)
                    {
                        var ruleDicList = await CommonRequestHelper.GetService<IQueryBusinessdictionaryService>().GetListAsync<BusinessdictionaryEntity>(new BaseBusinessdictionaryCondition()
                        {
                            IsDictionaryRule = true,
                            IsEnabled = true
                        });
                        var ruleDic = ruleDicList.Find(x => x.Id == detail.RuleId);
                        if (ruleDic != null && ruleDic.RuleDictionaryTypeId.Value > 0)
                        {
                            var cbxDicValue = new ComboBox();
                            cbxDicValue.DropDownStyle = ComboBoxStyle.DropDownList;
                            var dicList = await CommonRequestHelper.GetService<IQueryBusinessdictionaryService
                      >().GetListAsync<BusinessdictionaryEntity>
                         (new BaseBusinessdictionaryCondition()
                         {
                             IsEnabled = true,
                             DictionaryTypeId = ruleDic.RuleDictionaryTypeId
                         });
                            cbxDicValue.Tag = detail.RuleId.Value;
                            cbxDicValue.ValueMember = "Id";
                            cbxDicValue.DisplayMember = "Code";
                            cbxDicValue.Items.Clear();
                            cbxDicValue.Items.AddRange(dicList.ToArray());
                            // label.Text = ruleDic.Name;
                            if (!string.IsNullOrEmpty(detail.RuleValue))
                            {
                                var defaultItem = dicList.Find(x => x.Code == detail.RuleValue);
                                if (defaultItem != null)
                                {
                                    cbxDicValue.SelectedItem = defaultItem;
                                    cbxDicValue.Enabled = CurrentUserConfig.User.IsAdmin.Value;
                                }

                            }
                            control = cbxDicValue;
                        }
                    }
                    if (control != null)
                    {

                        if (index % 8 == 0)
                        {
                            lastControlX = 0;
                            lastControlWidth = 0;
                        }

                        int x = lastControlX + lastControlWidth + 10;
                        int y = 10;
                        if (index > 7)
                        {
                            y += 80 * (index / 8);
                        }
                        if (control.Width != 150)
                        {
                            control.Size = new Size(100, 30);
                        }
                        lastControlX = x;
                        lastControlWidth = control.Width;
                        label.Location = new Point(x, y);
                        label.Text = (index + 1) + "-" + label.Text;
                        if (detail.IsContain == false)
                        {
                            label.ForeColor = Color.Red;
                        }
                        control.Location = new Point(x, y + 30);
                        control.Name = preStr + detail.Id;
                        panel1.Controls.Add(control);
                        panel1.Controls.Add(label);
                        index++;
                    }
                }
                if ((index % 8) == 0)
                {
                    this.uiGroupBox1.Height += (index / 8) * 60;
                }
                else
                {
                    this.uiGroupBox1.Height += ((index / 8) + 1) * 60;
                }
                this.panel2.Location = new Point(uiGroupBox1.Location.X, uiGroupBox1.Location.Y + uiGroupBox1.Height + 20);
            }
            this.dateCurrent.Enabled = CurrentUserConfig.User.IsAdmin.Value;
        }
        private void SupplierControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox comboBox = (ComboBox)sender;
            if (comboBox.SelectedItem != null)
            {
                currentSupplier = (SupplierEntity)comboBox.SelectedItem;
            }
        }
        List<GenerateCodeTemplateEntity> templateList = new List<GenerateCodeTemplateEntity>();
        List<BusinessdictionaryEntity> ruleTypeList = new List<BusinessdictionaryEntity>();
        private async Task InitTemplateData()
        {


            cbxItem.ValueMember = "Id";
            cbxItem.DisplayMember = "Code";
            var itemList = await CommonRequestHelper.GetService<IQueryItemInfoService
                                 >().GetListPagedAsync<ItemInfoEntity>
                            (1, 20, new BaseItemInfoCondition()
                            {
                                KeyWord = cbxItem.Text
                            });
            currentItemList = itemList.Data;
            if (currentItemList.Any())
            {
                cbxItem.Items.Clear();
                cbxItem.Items.AddRange(currentItemList.ToArray());
                cbxItem.Items.Insert(0, new ItemInfoEntity() { Id = 0, Name = "" });
            }
            var supplierList = await CommonRequestHelper.GetService<IQuerySupplierService>().GetListAsync<SupplierEntity>();
            cbx_supplier.ValueMember = "Id";
            cbx_supplier.DisplayMember = "Name";
            cbx_supplier.DataSource = supplierList;
            cbx_supplier.SelectedItem = supplierList.OrderBy(t => t.Id).First();
            currentSupplier = supplierList.OrderBy(t => t.Id).First();

            templateList = await CommonRequestHelper.GetService<IQueryGenerateCodeTemplateService>().GetListAsync<GenerateCodeTemplateEntity>
              (new BaseGenerateCodeTemplateCondition()
              {
                  IsEnabled = true
              });
            cbxTemplate.ValueMember = "Id";
            cbxTemplate.DisplayMember = "Name";
            cbxTemplate.DataSource = templateList;


            ruleTypeList = await CommonRequestHelper.GetService<IQueryBusinessdictionaryService>().GetListAsync<BusinessdictionaryEntity>
           (new BaseBusinessdictionaryCondition()
           {
               DictionaryTypeId = CommonStaticValue.CodeTemplateRuleTypeId.ToLong()
           });
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            this.panel1.Controls.Clear();

            this.cbxItem.SelectedIndex = 0;
            this.cbxItem.SelectedText = "";
            cbxTemplate.Enabled = false;
            this.InitTemplateData();
            GetCountData();
            this.currentItem = null;
            this.cbxItem.SelectedItem = null;
            this.cbxTemplate.SelectedItem = null;
            this.currentTemplate = null;
            this.cbxTemplate.Text = "";

        }
        private async Task GetCountData()
        {
            return;
            var todayCount = await CommonRequestHelper.GetService<IQueryCodePrintHistoryService>().GetCountListAsync(
                new BaseCodePrintHistoryCondition()
                {
                    CurrentDate = this.dateCurrent.Value.Date
                });
            this.Invoke((EventHandler)delegate
            {
                this.lblCurrentCount.Text = todayCount.ToString();
            });

            if (currentItem != null && !string.IsNullOrEmpty(currentItem.Code) && !string.IsNullOrEmpty(txtProduceNumber.Text))
            {
                var itemTodayCount = await CommonRequestHelper.GetService<IQueryCodePrintHistoryService>().GetCountListAsync(
                    new BaseCodePrintHistoryCondition()
                    {
                        ProductionCode = txtProduceNumber.Text.Trim(),
                        CurrentDate = this.dateCurrent.Value.Date
                    });
                this.Invoke((EventHandler)delegate
                {
                    this.lblCurrentItem.Text = currentItem.Code;
                    this.lblCurrentItemCount.Text = itemTodayCount.ToString();
                });
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            //  GetCountData();
        }
        /// <summary>
        /// 管理员授权
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btnAdmin_Click(object sender, EventArgs e)
        {
            UIEditOption option = new UIEditOption();
            option.AutoLabelWidth = true;
            option.Text = "管理员授权";
            option.AddText("UserName", "用户", "", true, false);
            option.AddText("UserPassword", "密码", "", true, true);

            UIEditForm frm = new UIEditForm(option);
            frm.ShowDialog();

            if (frm.IsOK)
            {
                var data = new UserEntity()
                {
                    UserName = frm["UserName"].ToString(),
                    UserPassword = frm["UserPassword"].ToString()
                };
                if (data.UserName.ToUpper().Equals("ADMIN"))
                {
                    var result = await CommonRequestHelper.GetService<ICommandUserService>().LoginAsync(
                         data);
                    if (result.IsSuccess)
                    {
                        UIMessageTip.ShowOk("授权成功！");
                        CurrentUserConfig.User.IsAdmin = true;
                        EnabledControls();
                    }
                    else
                    {
                        UIMessageTip.ShowError("授权失败！");
                    }
                }
                else
                {
                    UIMessageTip.ShowError("授权失败！");
                }

            }


        }
        private void EnabledControls()
        {

            for (var i = 0; i < this.panel1.Controls.Count; i++)
            {
                var control = this.panel1.Controls[i];
                if (control.Tag != null && control.Tag.ToString() == CodeTemplateRuleEnum.SupplierCode.ToString())
                {
                    control.Enabled = false;
                }
                else if (control.Tag == null || control.Tag.ToString() != "CanNotEdit")
                {

                    control.Enabled = true;
                }
            }
            this.dateCurrent.Enabled = CurrentUserConfig.User.IsAdmin.Value;
            this.cbx_supplier.Enabled = CurrentUserConfig.User.IsAdmin.Value;
        }
        string preStr = "templateDataValue_";
        List<GenerateCodeDetailModel> generateCodeDetailModelList = new List<GenerateCodeDetailModel>();
        Dictionary<string, string> currentOtherPrintData = new Dictionary<string, string>();
        /// <summary>
        /// 获取模板数据
        /// </summary>
        /// <returns></returns>
        private bool GetTemplateDataAndCheck()
        {
            currentOtherPrintData = new Dictionary<string, string>();
            generateCodeDetailModelList = new List<GenerateCodeDetailModel>();
            for (var i = 0; i < this.panel1.Controls.Count; i++)
            {
                var control = this.panel1.Controls[i];
                long codeId = 0;
                string codeValue = string.Empty;
                if (control.Name.Contains(preStr))
                {
                    var templateDetail = currentTemplateList.Find(x => x.Id == control.Name.Replace(preStr, "").ToLong());
                    var inputValue = string.Empty;
                    if (control is ComboBox)
                    {
                        var combobox = (ComboBox)control;
                        if (combobox.SelectedItem == null)
                        {
                            UIMessageTip.ShowError($"{templateDetail.AliasName}为空值，请检查！");
                            return false;
                        }
                        var tempData = combobox.SelectedItem.MapTo<TemplateValueModel>();
                        codeValue = tempData.Code;
                        inputValue = tempData.Id.ToString();
                    }
                    if (control is TextBox)
                    {
                        if (string.IsNullOrEmpty(control.Text.Trim()))
                        {
                            UIMessageTip.ShowError($"{templateDetail.AliasName}为空值，请检查！");
                            return false;
                        }
                        inputValue = control.Text.Trim();

                    }
                    // 补充数据
                    generateCodeDetailModelList.Add(new GenerateCodeDetailModel()
                    {
                        GenerateCodeTemplateDetailId = control.Name.Replace(preStr, "").ToLong(),
                        InputValueOfTemplateDetail = inputValue
                    });
                    // var templateDetail = currentTemplateList.Find(x => x.Id == control.Name.Replace(preStr, "").ToLong());
                    if (templateDetail != null)
                    {
                        var ruleType = ruleTypeList.Find(x => x.Id == templateDetail.RuleId);
                        if (ruleType != null && ruleType.IsDictionaryRule.Value)
                        {
                            if (!currentOtherPrintData.ContainsKey(ruleType.Code))
                            {
                                currentOtherPrintData.Add(ruleType.Code, codeValue);
                            }
                        }
                    }
                }
            }


            return true;
        }



        private void btnPrint_Click(object sender, EventArgs e)
        {
            // 校验输入是否都已经正确
            if (this.currentTemplate == null)
            {
                UIMessageTip.ShowError("模板必须选择！");
                return;

            }
            if (this.currentItem == null || currentItem.Code == "")
            {
                UIMessageTip.ShowError("产品必须选择！");
                return;

            }
            if (string.IsNullOrEmpty(this.txtProduceNumber.Text.Trim()))
            {
                UIMessageTip.ShowError("生产制令号必须输入！");
                return;

            }
            //if (this.txtProduceNumber.Text.Trim().Length != 10)
            //{
            //    UIMessageTip.ShowError("生产制令号长度为10！");
            //    return;

            //}
            //string path = AppsettingsConfig.BartenderSharePath + currentTemplate.TemplateJsonFilePath;
            string fileName = currentTemplate.TemplateJsonFilePath;
            if (string.IsNullOrEmpty(fileName) && string.IsNullOrEmpty(fileName))
            {
                UIMessageTip.ShowError("模板设计文件缺失！");
                return;
            }
            if (currentTemplate.TemplateJsonFilePath.ToLower().EndsWith(".json"))
            {
                if (!File.Exists(currentTemplate.TemplateJsonFilePath))
                {
                    var stream = File.Create(currentTemplate.TemplateJsonFilePath);
                    byte[] byteArray = System.Text.Encoding.Default.GetBytes(currentTemplate.TemplateJsonText);
                    stream.Write(byteArray, 0, byteArray.Length);
                    stream.Close();
                    stream.Dispose();

                }
            }
            else
            {
                string path = Path.GetDirectoryName(fileName);
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                if (!File.Exists(fileName))
                {
                    //判断文件是否存在
                    if (!File.Exists(currentTemplate.TemplateJsonFilePath))
                    {
                        File.Create(currentTemplate.TemplateJsonFilePath).Close();
                    }
                    File.WriteAllBytes(currentTemplate.TemplateJsonFilePath, currentTemplate.BTWFile);
                   
                }
                else
                {
                    byte[] fileData = File.ReadAllBytes(currentTemplate.TemplateJsonFilePath);

                    string hash = HashHelper.CalculateSHA1(fileData);
                    if (hash!= currentTemplate.FileHash)
                    {
                        File.WriteAllBytes(currentTemplate.TemplateJsonFilePath, currentTemplate.BTWFile);
                    }
                }
            }

            var loopCount = Convert.ToInt32(this.txtPrintCount.Value);
            // 获取模板最小打印数
            if (loopCount < currentTemplate.PrintLimitOnce) { loopCount = currentTemplate.PrintLimitOnce.Value; }
            int count = loopCount / currentTemplate.PrintLimitOnce.Value * currentTemplate.PrintLimitOnce.Value;
            if (GetTemplateDataAndCheck())
            {
                this.btnPrint.Enabled = false;
                this.btnStop.Enabled = true;
                CurrentUserConfig.IsPrinting = true;
                DisabledPrintControls();
                Task.Run(async () => { await DoPrintAsync(count); await GetCountData(); });


            }
        }



        bool isLoop = true;
        PrintProcessForm printProcessForm = null;
        int tempCurrentPrintCount = 0, totalPrintCount = 0;
        /// <summary>
        /// 打印
        /// </summary>
        /// <param name="itemEntity"></param>
        /// <param name="supplierEntity"></param>
        /// <param name="loopCount"></param>
        /// <returns></returns>
        private async Task DoPrintAsync(int loopCount)
        {
            var logList = new List<CodePrintHistoryEntity>();
            Dictionary<PrintSetting, PrinterTagConfig> printDicList = new Dictionary<PrintSetting, PrinterTagConfig>();

            try
            {
                var generateCodeService = CommonRequestHelper.GetService<IGenerateCodeService>();


                GenerateCodeModel generateCodeModel = new GenerateCodeModel()
                {
                    ItemCode = currentItem.Code,
                    TemplateId = currentTemplate.Id,
                    PrintDayTime = this.dateCurrent.Value,
                    Quantity = loopCount,
                    GenerateCodeDetailModel = generateCodeDetailModelList
                };
                var serialResult = await generateCodeService.BatchGenerateCodeAsync(generateCodeModel, true);

                isLoop = true;
                int tempPrintCount = 0;
                if (currentItem == null)
                {
                    currentItem = new ItemInfoEntity();
                }
                if (currentSupplier == null)
                {
                    currentSupplier = new SupplierEntity();
                }
                if (serialResult.IsSuccess)
                {
                    if (!serialResult.BackResult.IsListNullOrEmpty())
                    {
                        if (serialResult.BackResult.Count != serialResult.BackResult.Distinct().Count())
                        {
                            WriteLogHelper.WriteLogsAsync($"{string.Join("\n", serialResult.BackResult)}", $"{generateCodeModel.ItemCode}重复");
                            return;
                        }
                    }
                    CurrentUserConfig.IsPrinting = true;
                    tempCurrentPrintCount = 0;
                    totalPrintCount = serialResult.BackResult.Count / currentTemplate.PrintLimitOnce.Value;

                    // 分解动作开两个任务进行打印工作，加快打印速率
                    for (var i = 0; i < serialResult.BackResult.Count; i++)
                    {
                        var code = serialResult.BackResult[i].BarCode;
                        var getDayOfYear = CommonHelper.GetYearWeekDayByTime(this.dateCurrent.Value);
                        logList.Add(new CodePrintHistoryEntity()
                        {
                            SupplierCode = currentSupplier.Code,
                            Code = code,
                            OperateName = CurrentUserConfig.User.UserName,
                            ProductionCode = this.txtProduceNumber.Text.Trim(),
                            Day = getDayOfYear.dayOfWeek.ToString(),
                            Week = getDayOfYear.weekOfYear.ToString().PadLeft(2, '0'),
                            Year = getDayOfYear.year.ToString(),
                            CurrentDate = this.dateCurrent.Value,//ToString("yyyyMMdd"),
                            CreateTime = DateTime.Now,
                            PrintTime = DateTime.Now,
                            ItemCode = currentItem.Code,
                            SerialNumber = code,
                            IsDeleted = false,
                            Status = 1
                        });

                    }
                    if (logList.Any())
                    {
                        var result = await CommonRequestHelper.GetService<ICommandCodePrintHistoryService>().InsertBatchAsync(logList);
                        if (result.IsSuccess == false)
                        {
                            UIMessageTip.ShowError("打印日志入库失败，请稍后重试！" + result.ErrorMessage, 5000);
                            return;
                        }
                        //  UIMessageBox.Show($"打印任务创建成功！本次计划打印 {logList.Count} 次");
                        UIMessageTip.ShowOk($"打印任务创建成功！本次计划打印 {logList.Count} 次", 3000);
                        var codes = logList.Select(t => t.Code);
                        WriteLogHelper.WriteLogsAsync($"{string.Join("\n", codes)}", $"{generateCodeModel.ItemCode}");
                    }

                    else
                    {
                        UIMessageTip.ShowError("打印日志不存在，请稍后重试！", 5000);
                        return;
                    }

                    if (currentTemplate.TemplateJsonFilePath.ToLower().EndsWith(".json"))
                    {

                        for (var i = 0; i < serialResult.BackResult.Count; i = i + currentTemplate.PrintLimitOnce.Value)
                        {
                            if (CurrentUserConfig.IsPrinting == false) { break; }
                            //WriteLogHelper.WriteLogsAsync("1.开始打印，生成预览二维码", "PrintStepLog");
                            this.pictureBox1.Image = QRCodeHelper.CreateQRimg(serialResult.BackResult[i].BarCode);
                            Dictionary<string, string> printData = new Dictionary<string, string>();
                            List<string> printCodeLogs = new List<string>();
                            for (var j = 0; j < currentTemplate.PrintLimitOnce.Value; j++)
                            {
                                tempPrintCount++;
                                var code = serialResult.BackResult[i + j];
                                printData.Add($"itemCode{j + 1}", $"{currentItem.Code}");
                                printData.Add($"versionCode{j + 1}", currentItem.VersionCode);
                                printData.Add($"supplierCode{j + 1}", currentSupplier.Code);
                                printData.Add($"itemName{j + 1}", currentItem.Name);
                                printData.Add($"barCode{j + 1}", code.BarCode);
                                printData.Add($"sn{j + 1}", code.SerialNumber);
                                printData.Add($"timeCode{j + 1}", dateCurrent.Value.ToString("yyyy-MM-dd"));
                                // 加载其它字典数据到模板
                                foreach (var item in currentOtherPrintData)
                                {
                                    printData.Add($"{item.Key.ToLower()}{j + 1}", item.Value);
                                }
                                if (code.BarCode.Length > 6)
                                {
                                    printData.Add($"xiaoPengBatchNo{j + 1}", code.BarCode.Right(6));
                                }
                                printCodeLogs.Add(code.BarCode);
                            }
                            var printConfig = PrintHelper.GetPrinterTagConfig(@$"{currentTemplate.TemplateJsonFilePath}", printData);
                            if (AppsettingsConfig.TaskDelayMillisecond > 0)
                            {
                                await Task.Delay(AppsettingsConfig.TaskDelayMillisecond);
                            }
                            WriteLogHelper.WriteLogsAsync($"{string.Join("\n", printCodeLogs)}", $"{generateCodeModel.ItemCode}_Print");
                            SendPrintNewAsync(printConfig, new PrintSetting() { Count = 1, DocumentName = serialResult.BackResult[i].BarCode, PrinterName = AppsettingsConfig.PrinterName });
                        }
                    }
                    else
                    {
                        // 组装打印内容
                        List<string> printData = new List<string>();
                        string title = "\"产品码\",\"产品名称\",\"版本\",\"日期\",\"唯一码\",\"供应商代码\",\"流水号\"";
                        int dataI = 0;
                        List<int> dataIList = new List<int>();
                        for (int i = 0; i < serialResult.BackResult.Count; i++)
                        {
                            var code = serialResult.BackResult[i];
                            var details = code.DataDetails;
                            var singleData = $"\"{currentItem.Code}\",\"{currentItem.Name}\",\"{currentItem.VersionCode}\",\"{dateCurrent.Value.ToString("yyyy年MM月dd日")}\",\"{code.BarCode}\",\"{currentSupplier.Code}\",\"{code.SerialNumber}\"";
                            int tempI = 0;
                            generateCodeDetailModelList.ForEach(x =>
                            {
                                var item = currentTemplateList.Find(y => y.Id == x.GenerateCodeTemplateDetailId);
                                if (item != null && title.Contains($"\"{item.AliasName}\"") == false)
                                {
                                    title += $",\"{item.AliasName}\"";
                                    dataIList.Add(dataI);
                                }
                                dataI++;
                                if (dataIList.Contains(tempI))
                                {
                                    //singleData += $",\"{x.InputValueOfTemplateDetail}\"";
                                    singleData += $",\"{details[item.Id].ShowValue}\"";
                                }
                                tempI++;
                            });
                            if (i == 0)
                            {
                                // 第一条需加载标题内容
                                printData.Add(title);
                            }
                            printData.Add(singleData);

                        }

                        // 调用打印
                        BartenderHelper helper = new BartenderHelper();
                        // 生成打印文本
                        var ppath = await WriteLogHelper.WriteLogsAsync(printData, $"{currentItem.Code}_Print", Guid.NewGuid().ToString());
                        var pPrint = helper.BTPrintByDataBase(Environment.CurrentDirectory + "//"+currentTemplate.TemplateJsonFilePath, ppath);

                    }
                }
                else
                {
                    CurrentUserConfig.IsPrinting = false;
                    UIMessageBox.ShowError(serialResult.ErrorMessage);
                }

                if (tempCurrentPrintCount != totalPrintCount)
                {
                    // await Task.Delay(3000);
                }

                if (tempPrintCount == loopCount)
                {
                    CurrentUserConfig.IsPrinting = false;
                }


            }
            catch (Exception ex)
            {
                WriteLogHelper.WriteLogsAsync(ex.ToString(), "Exception");
                UIMessageBox.ShowError("打印异常！" + ex.ToString());
                this.Invoke(new Action(() =>
                {
                    this.btnStop.Enabled = false;
                    this.btnPrint.Enabled = true;
                    CurrentUserConfig.IsPrinting = false;
                    EnabledPrintControls();
                }));
            }
            if (logList.Any())
            {
                logList.Clear();
                this.Invoke(new Action(() =>
                {
                    this.btnStop.Enabled = false;
                    this.btnPrint.Enabled = true;
                    CurrentUserConfig.IsPrinting = false;
                    EnabledPrintControls();
                }));
            }
        }
        Random random = new Random();
        private async Task SendPrintNewAsync(PrinterTagConfig printConfig, PrintSetting printSetting)
        {
            Task.Run(() =>
           {
               PrintHelperV2 printHelperV2 = new PrintHelperV2();
               printHelperV2.Print(printConfig, printSetting);
           });

        }



        private void btnStop_Click(object sender, EventArgs e)
        {
            isLoop = false;
            this.btnStop.Enabled = false;
            this.btnPrint.Enabled = true;
            EnabledPrintControls();
            CurrentUserConfig.IsPrinting = false;
        }

        private async void CodePrintForm_Load(object sender, EventArgs e)
        {

            cbxItem.TextUpdate += CbxItem_TextUpdate;
            cbxItem.Leave += CbxItem_Leave;
            cbxItem.SelectedIndexChanged += CbxItem_SelectedIndexChanged;
            cbx_supplier.SelectedIndexChanged += cbx_supplier_SelectedIndexChanged;
            cbxTemplate.SelectedIndexChanged += CbxTemplate_SelectedIndexChanged;



            await InitTemplateData();
            GetCountData();
        }


        private void DisabledPrintControls()
        {
            this.btnStop.Enabled = true;
            this.btnPrint.Enabled = false;
            this.btnRefresh.Enabled = false;
            this.cbxItem.Enabled = false;
            this.cbxTemplate.Enabled = false;
            this.cbx_supplier.Enabled = false;
            this.uiGroupBox1.Enabled = false;
            this.dateCurrent.Enabled = false;
            this.txtProduceNumber.Enabled = false;
            this.txtPrintCount.Enabled = false;
        }

        private void EnabledPrintControls()
        {
            this.btnStop.Enabled = false;
            this.btnPrint.Enabled = true;
            this.btnRefresh.Enabled = true;
            this.cbxItem.Enabled = true;
            this.cbxTemplate.Enabled = currentItem.TemplateId.HasValue ? (currentItem.TemplateId > 0 ? false : true) : true;//CurrentUserConfig.User.IsAdmin.Value;
            this.uiGroupBox1.Enabled = true;
            this.dateCurrent.Enabled = CurrentUserConfig.User.IsAdmin.Value;
            this.cbx_supplier.Enabled = CurrentUserConfig.User.IsAdmin.Value;
            this.txtProduceNumber.Enabled = true;
            this.txtPrintCount.Enabled = true;
        }

        private void dateCurrent_ValueChanged(object sender, DateTime value)
        {
            GetCountData();
        }
    }



    public class TemplateValueModel
    {
        public long Id { set; get; }
        public string Code { set; get; }
    }

    public class DesignTemplateKeyValueModel
    {
        public string ItemCode { set; get; }
        public string ItemName { set; get; }
        public string SupplierCode { set; get; }

        public string Version { set; get; }
        public string BarCode { set; get; }
    }
}
