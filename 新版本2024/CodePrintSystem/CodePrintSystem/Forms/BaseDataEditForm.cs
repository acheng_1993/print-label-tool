﻿using CodePrintSystem.Basic.Abstractions;
using CodePrintSystem.Utility;
using Sunny.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodePrintSystem.Forms
{
    public partial class BaseDataEditForm : UIEditForm
    {
        Func<BusinessdictionaryEntity, Task<bool>> func;
        public BaseDataEditForm(Func<BusinessdictionaryEntity, Task<bool>> func)
        {
            InitializeComponent();
            this.func = func;
            if (func != null)
            {
                this.ButtonOkClick += btnOK_Click_1;
            }

        }

        List<BusinessdictionarytypeEntity> typeList = new List<BusinessdictionarytypeEntity>();
        public async Task InitType()
        {
            typeList = await CommonRequestHelper.GetService<IQueryBusinessdictionarytypeService>().GetListAsync<BusinessdictionarytypeEntity>
               (new BaseBusinessdictionarytypeCondition() { IsCustom = true });
            cbDicType.ValueMember = "Id";
            cbDicType.DisplayMember = "TypeName";
            cbDicType.DataSource = typeList;

        }

        public void DisabledControl()
        {
            this.txtCode.Enabled = false;
            this.cbDicType.Enabled = false;
        }

        protected override bool CheckData()
        {

            return CheckEmpty(txtCode, "请输入编码") && CheckEmpty(txtName, "请输入名称")
                && CheckEmpty(cbDicType, "请选择类型");
        }
        private BusinessdictionaryEntity data = new BusinessdictionaryEntity();
        public BusinessdictionaryEntity Data
        {
            get
            {
                data.Code = txtCode.Text;
                data.Name = txtName.Text;
                data.IsEnabled = cbIsCheck.Checked;
                data.ShortName = txtName.Text;
                data.DictionaryTypeId = cbDicType.SelectedValue.ToLong();
                data.Remark = txtRemark.Text;
                return data;
            }

            set
            {
                data.Id = value.Id;
                txtCode.Text = value.Code;
                txtName.Text = value.Name;
                cbIsCheck.Checked = value.IsEnabled.Value;
                txtRemark.Text = value.Remark;
                var type  = typeList.Find(x => x.Id == value.DictionaryTypeId);
                if (type != null)
                    cbDicType.SelectedItem = type;
            }
        }

        private async void BaseDataEditForm_Load(object sender, EventArgs e)
        {
           await  InitType();
        }

        private async void btnOK_Click_1(object sender, EventArgs e)
        {
            var result = await func?.Invoke(Data);
            if (result == true)
            {
                DialogResult = DialogResult.OK;
                Close();
            }
        }
    }
}
