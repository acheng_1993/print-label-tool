﻿
namespace CodePrintSystem.Forms
{
    partial class TagTemplateEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            cbIsCheck = new Sunny.UI.UICheckBox();
            uiLabel4 = new Sunny.UI.UILabel();
            txtName = new Sunny.UI.UITextBox();
            uiLabel2 = new Sunny.UI.UILabel();
            txtCode = new Sunny.UI.UITextBox();
            uiLabel1 = new Sunny.UI.UILabel();
            uiDataGridView1 = new Sunny.UI.UIDataGridView();
            btnDelete = new Sunny.UI.UISymbolButton();
            btnEdit = new Sunny.UI.UISymbolButton();
            btnAdd = new Sunny.UI.UISymbolButton();
            btnImport = new Sunny.UI.UIButton();
            btnExport = new Sunny.UI.UIButton();
            uiLabel3 = new Sunny.UI.UILabel();
            txtCodeLength = new Sunny.UI.UIIntegerUpDown();
            uiLabel5 = new Sunny.UI.UILabel();
            printLimit = new Sunny.UI.UIIntegerUpDown();
            uiLabel6 = new Sunny.UI.UILabel();
            txtSequenceNumber = new Sunny.UI.UIIntegerUpDown();
            ((System.ComponentModel.ISupportInitialize)uiDataGridView1).BeginInit();
            SuspendLayout();
            // 
            // pnlBtm
            // 
            pnlBtm.Location = new System.Drawing.Point(1, 574);
            pnlBtm.Size = new System.Drawing.Size(865, 55);
            // 
            // cbIsCheck
            // 
            cbIsCheck.Checked = true;
            cbIsCheck.Cursor = System.Windows.Forms.Cursors.Hand;
            cbIsCheck.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            cbIsCheck.Location = new System.Drawing.Point(480, 59);
            cbIsCheck.MinimumSize = new System.Drawing.Size(1, 1);
            cbIsCheck.Name = "cbIsCheck";
            cbIsCheck.Padding = new System.Windows.Forms.Padding(22, 0, 0, 0);
            cbIsCheck.Size = new System.Drawing.Size(150, 29);
            cbIsCheck.TabIndex = 28;
            cbIsCheck.Text = "启用";
            // 
            // uiLabel4
            // 
            uiLabel4.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            uiLabel4.Location = new System.Drawing.Point(392, 59);
            uiLabel4.Name = "uiLabel4";
            uiLabel4.Size = new System.Drawing.Size(94, 23);
            uiLabel4.TabIndex = 27;
            uiLabel4.Text = "是否启用";
            uiLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtName
            // 
            txtName.Cursor = System.Windows.Forms.Cursors.IBeam;
            txtName.FillColor = System.Drawing.Color.White;
            txtName.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            txtName.Location = new System.Drawing.Point(130, 117);
            txtName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            txtName.Maximum = 2147483647D;
            txtName.Minimum = -2147483648D;
            txtName.MinimumSize = new System.Drawing.Size(1, 1);
            txtName.Name = "txtName";
            txtName.Padding = new System.Windows.Forms.Padding(5);
            txtName.Size = new System.Drawing.Size(241, 29);
            txtName.TabIndex = 26;
            // 
            // uiLabel2
            // 
            uiLabel2.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            uiLabel2.Location = new System.Drawing.Point(42, 117);
            uiLabel2.Name = "uiLabel2";
            uiLabel2.Size = new System.Drawing.Size(94, 23);
            uiLabel2.TabIndex = 25;
            uiLabel2.Text = "模板名称";
            uiLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtCode
            // 
            txtCode.Cursor = System.Windows.Forms.Cursors.IBeam;
            txtCode.FillColor = System.Drawing.Color.White;
            txtCode.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            txtCode.Location = new System.Drawing.Point(130, 59);
            txtCode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            txtCode.Maximum = 2147483647D;
            txtCode.Minimum = -2147483648D;
            txtCode.MinimumSize = new System.Drawing.Size(1, 1);
            txtCode.Name = "txtCode";
            txtCode.Padding = new System.Windows.Forms.Padding(5);
            txtCode.Size = new System.Drawing.Size(241, 29);
            txtCode.TabIndex = 24;
            // 
            // uiLabel1
            // 
            uiLabel1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            uiLabel1.Location = new System.Drawing.Point(42, 59);
            uiLabel1.Name = "uiLabel1";
            uiLabel1.Size = new System.Drawing.Size(94, 23);
            uiLabel1.TabIndex = 23;
            uiLabel1.Text = "模板编码";
            uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiDataGridView1
            // 
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(235, 243, 255);
            uiDataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle6;
            uiDataGridView1.BackgroundColor = System.Drawing.Color.White;
            uiDataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(80, 160, 255);
            dataGridViewCellStyle7.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.FromArgb(80, 160, 255);
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            uiDataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            uiDataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.FromArgb(48, 48, 48);
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.FromArgb(155, 200, 255);
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.FromArgb(48, 48, 48);
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            uiDataGridView1.DefaultCellStyle = dataGridViewCellStyle8;
            uiDataGridView1.EnableHeadersVisualStyles = false;
            uiDataGridView1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            uiDataGridView1.GridColor = System.Drawing.Color.FromArgb(80, 160, 255);
            uiDataGridView1.Location = new System.Drawing.Point(42, 253);
            uiDataGridView1.Name = "uiDataGridView1";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(235, 243, 255);
            dataGridViewCellStyle9.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.FromArgb(80, 160, 255);
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            uiDataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.White;
            uiDataGridView1.RowsDefaultCellStyle = dataGridViewCellStyle10;
            uiDataGridView1.RowTemplate.Height = 29;
            uiDataGridView1.SelectedIndex = -1;
            uiDataGridView1.ShowGridLine = true;
            uiDataGridView1.Size = new System.Drawing.Size(817, 308);
            uiDataGridView1.TabIndex = 29;
            // 
            // btnDelete
            // 
            btnDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            btnDelete.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            btnDelete.Location = new System.Drawing.Point(130, 206);
            btnDelete.Margin = new System.Windows.Forms.Padding(0);
            btnDelete.MinimumSize = new System.Drawing.Size(1, 1);
            btnDelete.Name = "btnDelete";
            btnDelete.RadiusSides = Sunny.UI.UICornerRadiusSides.RightTop | Sunny.UI.UICornerRadiusSides.RightBottom;
            btnDelete.RectSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Top | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom;
            btnDelete.Size = new System.Drawing.Size(44, 35);
            btnDelete.Symbol = 61544;
            btnDelete.TabIndex = 32;
            // 
            // btnEdit
            // 
            btnEdit.Cursor = System.Windows.Forms.Cursors.Hand;
            btnEdit.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            btnEdit.Location = new System.Drawing.Point(86, 206);
            btnEdit.Margin = new System.Windows.Forms.Padding(0);
            btnEdit.MinimumSize = new System.Drawing.Size(1, 1);
            btnEdit.Name = "btnEdit";
            btnEdit.RadiusSides = Sunny.UI.UICornerRadiusSides.None;
            btnEdit.RectSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Top | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom;
            btnEdit.Size = new System.Drawing.Size(44, 35);
            btnEdit.Symbol = 61508;
            btnEdit.TabIndex = 31;
            // 
            // btnAdd
            // 
            btnAdd.Cursor = System.Windows.Forms.Cursors.Hand;
            btnAdd.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            btnAdd.Location = new System.Drawing.Point(42, 206);
            btnAdd.Margin = new System.Windows.Forms.Padding(0);
            btnAdd.MinimumSize = new System.Drawing.Size(1, 1);
            btnAdd.Name = "btnAdd";
            btnAdd.RadiusSides = Sunny.UI.UICornerRadiusSides.LeftTop | Sunny.UI.UICornerRadiusSides.LeftBottom;
            btnAdd.RectSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom;
            btnAdd.Size = new System.Drawing.Size(44, 35);
            btnAdd.Symbol = 61543;
            btnAdd.TabIndex = 30;
            // 
            // btnImport
            // 
            btnImport.Cursor = System.Windows.Forms.Cursors.Hand;
            btnImport.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            btnImport.Location = new System.Drawing.Point(467, 111);
            btnImport.MinimumSize = new System.Drawing.Size(1, 1);
            btnImport.Name = "btnImport";
            btnImport.Size = new System.Drawing.Size(100, 35);
            btnImport.TabIndex = 33;
            btnImport.Text = "导入模板";
            btnImport.Click += btnImport_Click;
            // 
            // btnExport
            // 
            btnExport.Cursor = System.Windows.Forms.Cursors.Hand;
            btnExport.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            btnExport.Location = new System.Drawing.Point(605, 111);
            btnExport.MinimumSize = new System.Drawing.Size(1, 1);
            btnExport.Name = "btnExport";
            btnExport.Size = new System.Drawing.Size(100, 35);
            btnExport.TabIndex = 34;
            btnExport.Text = "导出模板";
            btnExport.Click += btnExport_Click;
            // 
            // uiLabel3
            // 
            uiLabel3.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            uiLabel3.Location = new System.Drawing.Point(43, 165);
            uiLabel3.Name = "uiLabel3";
            uiLabel3.Size = new System.Drawing.Size(94, 23);
            uiLabel3.TabIndex = 35;
            uiLabel3.Text = "条码长度";
            uiLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtCodeLength
            // 
            txtCodeLength.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            txtCodeLength.HasMaximum = true;
            txtCodeLength.HasMinimum = true;
            txtCodeLength.Location = new System.Drawing.Point(131, 159);
            txtCodeLength.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            txtCodeLength.Minimum = 0;
            txtCodeLength.MinimumSize = new System.Drawing.Size(100, 0);
            txtCodeLength.Name = "txtCodeLength";
            txtCodeLength.Size = new System.Drawing.Size(116, 29);
            txtCodeLength.TabIndex = 36;
            txtCodeLength.Text = null;
            // 
            // uiLabel5
            // 
            uiLabel5.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            uiLabel5.Location = new System.Drawing.Point(254, 165);
            uiLabel5.Name = "uiLabel5";
            uiLabel5.Size = new System.Drawing.Size(133, 23);
            uiLabel5.TabIndex = 37;
            uiLabel5.Text = "每次打印条码数";
            uiLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // printLimit
            // 
            printLimit.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            printLimit.HasMaximum = true;
            printLimit.HasMinimum = true;
            printLimit.Location = new System.Drawing.Point(381, 159);
            printLimit.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            printLimit.Minimum = 1;
            printLimit.MinimumSize = new System.Drawing.Size(100, 0);
            printLimit.Name = "printLimit";
            printLimit.Size = new System.Drawing.Size(116, 29);
            printLimit.TabIndex = 37;
            printLimit.Text = null;
            printLimit.Value = 1;
            // 
            // uiLabel6
            // 
            uiLabel6.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            uiLabel6.Location = new System.Drawing.Point(509, 165);
            uiLabel6.Name = "uiLabel6";
            uiLabel6.Size = new System.Drawing.Size(94, 23);
            uiLabel6.TabIndex = 38;
            uiLabel6.Text = "流水号长度";
            uiLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSequenceNumber
            // 
            txtSequenceNumber.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            txtSequenceNumber.HasMaximum = true;
            txtSequenceNumber.HasMinimum = true;
            txtSequenceNumber.Location = new System.Drawing.Point(605, 159);
            txtSequenceNumber.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            txtSequenceNumber.Minimum = 1;
            txtSequenceNumber.MinimumSize = new System.Drawing.Size(100, 0);
            txtSequenceNumber.Name = "txtSequenceNumber";
            txtSequenceNumber.Size = new System.Drawing.Size(116, 29);
            txtSequenceNumber.TabIndex = 37;
            txtSequenceNumber.Text = null;
            txtSequenceNumber.Value = 5;
            // 
            // TagTemplateEditForm
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(10F, 21F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(867, 632);
            Controls.Add(txtSequenceNumber);
            Controls.Add(uiLabel6);
            Controls.Add(printLimit);
            Controls.Add(uiLabel5);
            Controls.Add(txtCodeLength);
            Controls.Add(uiLabel3);
            Controls.Add(btnExport);
            Controls.Add(btnImport);
            Controls.Add(btnDelete);
            Controls.Add(btnEdit);
            Controls.Add(btnAdd);
            Controls.Add(uiDataGridView1);
            Controls.Add(cbIsCheck);
            Controls.Add(uiLabel4);
            Controls.Add(txtName);
            Controls.Add(uiLabel2);
            Controls.Add(txtCode);
            Controls.Add(uiLabel1);
            Name = "TagTemplateEditForm";
            Text = "TagTemplateEditForm";
            Load += TagTemplateEditForm_Load;
            Controls.SetChildIndex(pnlBtm, 0);
            Controls.SetChildIndex(uiLabel1, 0);
            Controls.SetChildIndex(txtCode, 0);
            Controls.SetChildIndex(uiLabel2, 0);
            Controls.SetChildIndex(txtName, 0);
            Controls.SetChildIndex(uiLabel4, 0);
            Controls.SetChildIndex(cbIsCheck, 0);
            Controls.SetChildIndex(uiDataGridView1, 0);
            Controls.SetChildIndex(btnAdd, 0);
            Controls.SetChildIndex(btnEdit, 0);
            Controls.SetChildIndex(btnDelete, 0);
            Controls.SetChildIndex(btnImport, 0);
            Controls.SetChildIndex(btnExport, 0);
            Controls.SetChildIndex(uiLabel3, 0);
            Controls.SetChildIndex(txtCodeLength, 0);
            Controls.SetChildIndex(uiLabel5, 0);
            Controls.SetChildIndex(printLimit, 0);
            Controls.SetChildIndex(uiLabel6, 0);
            Controls.SetChildIndex(txtSequenceNumber, 0);
            ((System.ComponentModel.ISupportInitialize)uiDataGridView1).EndInit();
            ResumeLayout(false);
        }

        #endregion

        private Sunny.UI.UICheckBox cbIsCheck;
        private Sunny.UI.UILabel uiLabel4;
        private Sunny.UI.UITextBox txtName;
        private Sunny.UI.UILabel uiLabel2;
        private Sunny.UI.UITextBox txtCode;
        private Sunny.UI.UILabel uiLabel1;
        private Sunny.UI.UIDataGridView uiDataGridView1;
        private Sunny.UI.UISymbolButton btnDelete;
        private Sunny.UI.UISymbolButton btnEdit;
        private Sunny.UI.UISymbolButton btnAdd;
        private Sunny.UI.UIButton btnImport;
        private Sunny.UI.UIButton btnExport;
        private Sunny.UI.UILabel uiLabel3;
        private Sunny.UI.UIIntegerUpDown txtCodeLength;
        private Sunny.UI.UILabel uiLabel5;
        private Sunny.UI.UIIntegerUpDown printLimit;
        private Sunny.UI.UILabel uiLabel6;
        private Sunny.UI.UIIntegerUpDown txtSequenceNumber;
    }
}