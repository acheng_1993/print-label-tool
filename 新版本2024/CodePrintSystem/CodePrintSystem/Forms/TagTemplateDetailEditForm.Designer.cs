﻿
namespace CodePrintSystem.Forms
{
    partial class TagTemplateDetailEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            uiLabel1 = new Sunny.UI.UILabel();
            txtRowNo = new Sunny.UI.UIIntegerUpDown();
            uiLabel2 = new Sunny.UI.UILabel();
            cbxRuleType = new Sunny.UI.UIComboBox();
            uiLabel3 = new Sunny.UI.UILabel();
            cbxItem = new System.Windows.Forms.ComboBox();
            txtInputValue = new Sunny.UI.UITextBox();
            txtRuleValueDisabled = new Sunny.UI.UITextBox();
            cbxDicValue = new System.Windows.Forms.ComboBox();
            cbxSupplier = new System.Windows.Forms.ComboBox();
            txtValueLength = new Sunny.UI.UIIntegerUpDown();
            uiLabel4 = new Sunny.UI.UILabel();
            cbxIsAdminEdit = new Sunny.UI.UICheckBox();
            uiLabel5 = new Sunny.UI.UILabel();
            btnClear = new Sunny.UI.UIButton();
            txt_Alias = new Sunny.UI.UITextBox();
            CH_IsContain = new Sunny.UI.UICheckBox();
            SuspendLayout();
            // 
            // pnlBtm
            // 
            pnlBtm.Location = new System.Drawing.Point(1, 404);
            pnlBtm.Size = new System.Drawing.Size(535, 55);
            // 
            // uiLabel1
            // 
            uiLabel1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            uiLabel1.Location = new System.Drawing.Point(55, 79);
            uiLabel1.Name = "uiLabel1";
            uiLabel1.Size = new System.Drawing.Size(100, 23);
            uiLabel1.TabIndex = 2;
            uiLabel1.Text = "序号";
            uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtRowNo
            // 
            txtRowNo.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            txtRowNo.HasMaximum = true;
            txtRowNo.HasMinimum = true;
            txtRowNo.Location = new System.Drawing.Point(149, 79);
            txtRowNo.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            txtRowNo.Maximum = 99999;
            txtRowNo.Minimum = 1;
            txtRowNo.MinimumSize = new System.Drawing.Size(100, 0);
            txtRowNo.Name = "txtRowNo";
            txtRowNo.Size = new System.Drawing.Size(174, 29);
            txtRowNo.TabIndex = 3;
            txtRowNo.Text = "uiIntegerUpDown1";
            txtRowNo.Value = 1;
            // 
            // uiLabel2
            // 
            uiLabel2.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            uiLabel2.Location = new System.Drawing.Point(55, 127);
            uiLabel2.Name = "uiLabel2";
            uiLabel2.Size = new System.Drawing.Size(100, 23);
            uiLabel2.TabIndex = 4;
            uiLabel2.Text = "规则类型";
            uiLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbxRuleType
            // 
            cbxRuleType.FillColor = System.Drawing.Color.White;
            cbxRuleType.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            cbxRuleType.Location = new System.Drawing.Point(149, 127);
            cbxRuleType.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            cbxRuleType.MinimumSize = new System.Drawing.Size(63, 0);
            cbxRuleType.Name = "cbxRuleType";
            cbxRuleType.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            cbxRuleType.Size = new System.Drawing.Size(174, 29);
            cbxRuleType.TabIndex = 5;
            cbxRuleType.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel3
            // 
            uiLabel3.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            uiLabel3.Location = new System.Drawing.Point(55, 177);
            uiLabel3.Name = "uiLabel3";
            uiLabel3.Size = new System.Drawing.Size(100, 23);
            uiLabel3.TabIndex = 6;
            uiLabel3.Text = "规则默认值";
            uiLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbxItem
            // 
            cbxItem.FormattingEnabled = true;
            cbxItem.Location = new System.Drawing.Point(149, 177);
            cbxItem.Name = "cbxItem";
            cbxItem.Size = new System.Drawing.Size(174, 29);
            cbxItem.TabIndex = 8;
            // 
            // txtInputValue
            // 
            txtInputValue.Cursor = System.Windows.Forms.Cursors.IBeam;
            txtInputValue.FillColor = System.Drawing.Color.White;
            txtInputValue.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            txtInputValue.Location = new System.Drawing.Point(359, 342);
            txtInputValue.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            txtInputValue.Maximum = 2147483647D;
            txtInputValue.Minimum = -2147483648D;
            txtInputValue.MinimumSize = new System.Drawing.Size(1, 1);
            txtInputValue.Name = "txtInputValue";
            txtInputValue.Padding = new System.Windows.Forms.Padding(5);
            txtInputValue.Size = new System.Drawing.Size(174, 29);
            txtInputValue.TabIndex = 9;
            // 
            // txtRuleValueDisabled
            // 
            txtRuleValueDisabled.Cursor = System.Windows.Forms.Cursors.IBeam;
            txtRuleValueDisabled.Enabled = false;
            txtRuleValueDisabled.FillColor = System.Drawing.Color.White;
            txtRuleValueDisabled.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            txtRuleValueDisabled.Location = new System.Drawing.Point(359, 268);
            txtRuleValueDisabled.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            txtRuleValueDisabled.Maximum = 2147483647D;
            txtRuleValueDisabled.Minimum = -2147483648D;
            txtRuleValueDisabled.MinimumSize = new System.Drawing.Size(1, 1);
            txtRuleValueDisabled.Name = "txtRuleValueDisabled";
            txtRuleValueDisabled.Padding = new System.Windows.Forms.Padding(5);
            txtRuleValueDisabled.Size = new System.Drawing.Size(174, 29);
            txtRuleValueDisabled.TabIndex = 10;
            txtRuleValueDisabled.Text = "uiTextBox1";
            // 
            // cbxDicValue
            // 
            cbxDicValue.FormattingEnabled = true;
            cbxDicValue.Location = new System.Drawing.Point(359, 305);
            cbxDicValue.Name = "cbxDicValue";
            cbxDicValue.Size = new System.Drawing.Size(174, 29);
            cbxDicValue.TabIndex = 11;
            // 
            // cbxSupplier
            // 
            cbxSupplier.FormattingEnabled = true;
            cbxSupplier.Location = new System.Drawing.Point(359, 231);
            cbxSupplier.Name = "cbxSupplier";
            cbxSupplier.Size = new System.Drawing.Size(174, 29);
            cbxSupplier.TabIndex = 10;
            // 
            // txtValueLength
            // 
            txtValueLength.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            txtValueLength.HasMaximum = true;
            txtValueLength.HasMinimum = true;
            txtValueLength.Location = new System.Drawing.Point(149, 231);
            txtValueLength.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            txtValueLength.Maximum = 99999;
            txtValueLength.Minimum = 0;
            txtValueLength.MinimumSize = new System.Drawing.Size(100, 0);
            txtValueLength.Name = "txtValueLength";
            txtValueLength.Size = new System.Drawing.Size(174, 29);
            txtValueLength.TabIndex = 11;
            txtValueLength.Text = "uiIntegerUpDown1";
            // 
            // uiLabel4
            // 
            uiLabel4.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            uiLabel4.Location = new System.Drawing.Point(55, 231);
            uiLabel4.Name = "uiLabel4";
            uiLabel4.Size = new System.Drawing.Size(100, 23);
            uiLabel4.TabIndex = 10;
            uiLabel4.Text = "数据长度";
            uiLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbxIsAdminEdit
            // 
            cbxIsAdminEdit.Cursor = System.Windows.Forms.Cursors.Hand;
            cbxIsAdminEdit.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            cbxIsAdminEdit.Location = new System.Drawing.Point(149, 304);
            cbxIsAdminEdit.MinimumSize = new System.Drawing.Size(1, 1);
            cbxIsAdminEdit.Name = "cbxIsAdminEdit";
            cbxIsAdminEdit.Padding = new System.Windows.Forms.Padding(22, 0, 0, 0);
            cbxIsAdminEdit.Size = new System.Drawing.Size(150, 29);
            cbxIsAdminEdit.TabIndex = 13;
            cbxIsAdminEdit.Text = "管理员可编辑";
            cbxIsAdminEdit.Visible = false;
            // 
            // uiLabel5
            // 
            uiLabel5.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            uiLabel5.Location = new System.Drawing.Point(149, 265);
            uiLabel5.Name = "uiLabel5";
            uiLabel5.Size = new System.Drawing.Size(100, 23);
            uiLabel5.TabIndex = 14;
            uiLabel5.Text = "默认0不限制";
            uiLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnClear
            // 
            btnClear.Cursor = System.Windows.Forms.Cursors.Hand;
            btnClear.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            btnClear.Location = new System.Drawing.Point(329, 177);
            btnClear.MinimumSize = new System.Drawing.Size(1, 1);
            btnClear.Name = "btnClear";
            btnClear.Size = new System.Drawing.Size(75, 29);
            btnClear.TabIndex = 15;
            btnClear.Text = "清空";
            btnClear.Click += btnClear_Click;
            // 
            // txt_Alias
            // 
            txt_Alias.Cursor = System.Windows.Forms.Cursors.IBeam;
            txt_Alias.FillColor = System.Drawing.Color.White;
            txt_Alias.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            txt_Alias.Location = new System.Drawing.Point(331, 127);
            txt_Alias.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            txt_Alias.Maximum = 2147483647D;
            txt_Alias.Minimum = -2147483648D;
            txt_Alias.MinimumSize = new System.Drawing.Size(1, 1);
            txt_Alias.Name = "txt_Alias";
            txt_Alias.Padding = new System.Windows.Forms.Padding(5);
            txt_Alias.Size = new System.Drawing.Size(174, 29);
            txt_Alias.TabIndex = 11;
            txt_Alias.Watermark = "别名";
            // 
            // CH_IsContain
            // 
            CH_IsContain.Checked = true;
            CH_IsContain.Cursor = System.Windows.Forms.Cursors.Hand;
            CH_IsContain.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            CH_IsContain.Location = new System.Drawing.Point(340, 79);
            CH_IsContain.MinimumSize = new System.Drawing.Size(1, 1);
            CH_IsContain.Name = "CH_IsContain";
            CH_IsContain.Padding = new System.Windows.Forms.Padding(22, 0, 0, 0);
            CH_IsContain.Size = new System.Drawing.Size(150, 29);
            CH_IsContain.TabIndex = 16;
            CH_IsContain.Text = "是否包含在条码中";
            // 
            // TagTemplateDetailEditForm
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(10F, 21F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(537, 462);
            Controls.Add(CH_IsContain);
            Controls.Add(txt_Alias);
            Controls.Add(btnClear);
            Controls.Add(uiLabel5);
            Controls.Add(cbxIsAdminEdit);
            Controls.Add(cbxSupplier);
            Controls.Add(cbxDicValue);
            Controls.Add(txtRuleValueDisabled);
            Controls.Add(txtValueLength);
            Controls.Add(uiLabel4);
            Controls.Add(txtInputValue);
            Controls.Add(cbxItem);
            Controls.Add(uiLabel3);
            Controls.Add(cbxRuleType);
            Controls.Add(uiLabel2);
            Controls.Add(txtRowNo);
            Controls.Add(uiLabel1);
            Name = "TagTemplateDetailEditForm";
            Text = "TagTemplateDetailEditForm";
            Load += TagTemplateDetailEditForm_Load;
            Controls.SetChildIndex(pnlBtm, 0);
            Controls.SetChildIndex(uiLabel1, 0);
            Controls.SetChildIndex(txtRowNo, 0);
            Controls.SetChildIndex(uiLabel2, 0);
            Controls.SetChildIndex(cbxRuleType, 0);
            Controls.SetChildIndex(uiLabel3, 0);
            Controls.SetChildIndex(cbxItem, 0);
            Controls.SetChildIndex(txtInputValue, 0);
            Controls.SetChildIndex(uiLabel4, 0);
            Controls.SetChildIndex(txtValueLength, 0);
            Controls.SetChildIndex(txtRuleValueDisabled, 0);
            Controls.SetChildIndex(cbxDicValue, 0);
            Controls.SetChildIndex(cbxSupplier, 0);
            Controls.SetChildIndex(cbxIsAdminEdit, 0);
            Controls.SetChildIndex(uiLabel5, 0);
            Controls.SetChildIndex(btnClear, 0);
            Controls.SetChildIndex(txt_Alias, 0);
            Controls.SetChildIndex(CH_IsContain, 0);
            ResumeLayout(false);
        }

        #endregion

        private Sunny.UI.UILabel uiLabel1;
        private Sunny.UI.UIIntegerUpDown txtRowNo;
        private Sunny.UI.UILabel uiLabel2;
        private Sunny.UI.UIComboBox cbxRuleType;
        private Sunny.UI.UILabel uiLabel3;
        private System.Windows.Forms.ComboBox cbxItem;
        private Sunny.UI.UITextBox txtInputValue;
        private System.Windows.Forms.ComboBox cbxSupplier;
        private System.Windows.Forms.ComboBox cbxDicValue;
        private Sunny.UI.UITextBox txtRuleValueDisabled;
        private Sunny.UI.UIIntegerUpDown txtValueLength;
        private Sunny.UI.UILabel uiLabel4;
        private Sunny.UI.UICheckBox cbxIsAdminEdit;
        private Sunny.UI.UILabel uiLabel5;
        private Sunny.UI.UIButton btnClear;
        private Sunny.UI.UITextBox txt_Alias;
        private Sunny.UI.UICheckBox uiCheckBox1;
        private Sunny.UI.UICheckBox CH_IsContain;
    }
}