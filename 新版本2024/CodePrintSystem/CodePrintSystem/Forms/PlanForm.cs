﻿using CodePrintSystem.Basic.Abstractions;
using CodePrintSystem.Core;
using CodePrintSystem.Utility;
using Microsoft.Extensions.DependencyInjection;
using Sunny.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodePrintSystem.Forms
{
    public partial class PlanForm : UITitlePage, ISelfSingletonAutoInject
    {
        IServiceProvider serviceProvider;
        public PlanForm(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;

            InitializeComponent();
            this.Text = "计划维护";
            uiDataGridView1.AutoGenerateColumns = false;
            this.Load += PlanForm_Load;

        }

        private void PlanForm_Load(object sender, EventArgs e)
        {
            InitDataGrid();
            this.btnAdd.Click += btnAdd_Click;
            this.uiPagination1.PageChanged += uiPagination1_PageChanged;
            this.btnSearch.Click += btnSearch_Click;
            dateStart.Value = DateTime.Today;
            dateEnd.Value = DateTime.Today.AddDays(1).AddSeconds(-1);
        }

        public IQueryPrintPlanService GetQueryService()
        {
            var scope = serviceProvider.CreateScope();
            var userService = scope.ServiceProvider.GetRequiredService<IQueryPrintPlanService>();
            return userService;
        }
        public ICommandPrintPlanService GetCommandService()
        {
            var scope = serviceProvider.CreateScope();
            var userService = scope.ServiceProvider.GetRequiredService<ICommandPrintPlanService>();
            return userService;
        }

        private async Task InitDataGrid()
        {
            uiPagination1.ActivePage = 1;
            uiPagination1.PageSize = 20;
            uiDataGridView1.ReadOnly = false;
            uiDataGridView1.ColumnHeaderMouseDoubleClick += UiDataGridView1_ColumnHeaderMouseDoubleClick;
            uiDataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;//自适应
            var checkColumn = uiDataGridView1.AddCheckBoxColumn("选择", "IsCheck").SetFixedMode(100);
            checkColumn.ReadOnly = false;
            //uiDataGridView1.se;
            uiDataGridView1.AddColumn("产品码", "ItemCode");//.SetFixedMode(200);
            uiDataGridView1.AddColumn("日期", "PlanDate");//.SetFixedMode(100);
            uiDataGridView1.AddColumn("计划数量", "PlanQty");//.SetFixedMode(100);
            await LoadDataGrid();
        }

        private void UiDataGridView1_ColumnHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            // 双击列头全选
            if (e.ColumnIndex == 0)
            {
                if (userViews.Exists(x => x.IsCheck == false))
                {
                    userViews.ForEach(x =>
                    {
                        x.IsCheck = true;
                    });
                }
                else
                {
                    userViews.ForEach(x =>
                    {
                        x.IsCheck = false;
                    });
                }
                uiDataGridView1.DataSource = null;
                uiDataGridView1.DataSource = userViews;
            }
        }

        List<PrintPlanEntity> userViews = new List<PrintPlanEntity>();
        List<PrintPlanEntity> selectRows = new List<PrintPlanEntity>();
        private async Task LoadDataGrid()
        {
            var data = await GetQueryService().GetListPagedAsync<PrintPlanEntity>(
         uiPagination1.ActivePage, uiPagination1.PageSize,
         new BasePrintPlanCondition()
         {
             ItemCode = this.txtUserName.Text.Trim(),
             PlanDateStart = dateStart.Value.Date,
             PlanDateEnd = dateEnd.Value.Date.AddDays(1).AddMilliseconds(-1)
         }
            );
            userViews = data.Data;
            userViews.ForEach(x =>
            {
                x.PlanDate = x.PlanDate.Value.Date;
            });
            this.uiDataGridView1.DataSource = userViews;
            this.uiPagination1.TotalCount = data.Total;
        }


        private void btnSearch_Click(object sender, EventArgs e)
        {
            LoadDataGrid();
        }

        private async void uiPagination1_PageChanged(object sender, object pagingSource, int pageIndex, int count)
        {
            await LoadDataGrid();
        }

    
        /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void uiSymbolButton1_Click(object sender, EventArgs e)
        {
            var selectRows = userViews.Where(x => x.IsCheck).ToList();
            if (!selectRows.Any())
            {
                UIMessageTip.ShowWarning("请选中一行");
                return;
            }
            if (selectRows.Count > 1)
            {
                UIMessageBox.Show("编辑不能选择多行");
                return;
            }
            var row = selectRows.FirstOrDefault();
            var data = row.MapTo<PrintPlanEntity>();
            PlanEditForm userEditForm = new PlanEditForm(async (data) => {

                var current = Convert.ToDateTime(data.PlanDate.Value.ToString("yyyy-MM-dd"));

                var old = await GetQueryService().GetListAsync<PrintPlanEntity>(new BasePrintPlanCondition()
                {
                    ItemCode = data.ItemCode,
                    PlanDateStart = current,
                    PlanDateEnd = current.AddDays(1).AddMinutes(-1)
                });
                if (old.Exists(x => x.ItemCode == data.ItemCode&&x.Id!=data.Id))
                {

                    UIMessageTip.ShowError("产品码计划重复！");
                    return false;
                }

                var result = await GetCommandService().UpdateAsync(data);
                if (result.IsSuccess)
                {
                    CommonRequestHelper.RequestTip(result, () => { LoadDataGrid(); });
                }
                else
                {
                    UIMessageTip.ShowError(result.ErrorMessage);

                }
                return result.IsSuccess;

            });
            userEditForm.Text = "更新计划";
            userEditForm.Plan = data;
            userEditForm.DisabledControl();
            userEditForm.ShowDialog();

            if (userEditForm.IsOK)
            {

            }

            userEditForm.Dispose();
            //UIMessageBox.Show("选中了"+string.Join(",",userViews.Where(x=>x.IsCheck).Select(x=>x.UserName).ToList()));
        }
        /// <summary>
        /// 增加
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btnAdd_Click(object sender, EventArgs e)
        {

            PlanEditForm userEditForm = new PlanEditForm(async (data) => {
                data.Id = GeneratePrimaryKeyIdHelper.GetPrimaryKeyId();

                var current = Convert.ToDateTime(data.PlanDate.Value.ToString("yyyy-MM-dd"));

                var old = await GetQueryService().GetListAsync<PrintPlanEntity>(new BasePrintPlanCondition() { 
                 ItemCode = data.ItemCode,
                  PlanDateStart = current,
                    PlanDateEnd = current.AddDays(1).AddMinutes(-1)
                });
                if (old.Exists(x => x.ItemCode == data.ItemCode)) {

                    UIMessageTip.ShowError("产品码计划重复！");
                    return false;
                }
                var result = await GetCommandService().InsertAsync(data);
                if (result.IsSuccess)
                {
                    CommonRequestHelper.RequestTip(result, () => { LoadDataGrid(); });
                }
                else
                {
                    UIMessageTip.ShowError(result.ErrorMessage);

                }
                return result.IsSuccess;
            });
            userEditForm.Text = "新增";
            userEditForm.ShowDialog();

            if (userEditForm.IsOK)
            {

            }

            userEditForm.Dispose();
        }
        private bool GetSelectRows()
        {
            selectRows = userViews.Where(x => x.IsCheck).ToList();
            if (!selectRows.Any())
            {
                UIMessageTip.ShowWarning("请选中一行");
                return false;
            }
            return true;
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void uiSymbolButton2_Click(object sender, EventArgs e)
        {
            if (GetSelectRows())
            {
                if (UIMessageBox.ShowAsk("删除的数据不可恢复，确认删除？"))
                {
                    var result = await GetCommandService().DeleteBatchAsync(selectRows.Select(x => x.Id).ToList());
                    CommonRequestHelper.RequestTip<bool>(result, () => { LoadDataGrid(); }, "删除成功");

                }
            }
        }


    

  

     
    }
}
