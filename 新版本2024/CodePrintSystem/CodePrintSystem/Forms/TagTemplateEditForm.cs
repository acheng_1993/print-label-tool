﻿using CodePrintSystem.Basic.Abstractions;
using Sunny.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CodePrintSystem.Utility;
using CodePrintSystem.AppLayer.SystemSet.ViewObject;
using System.IO;
using Print.Helper;
using CodePrintSystem.Core;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json;
using MongoDB.Bson;
using SharpCompress.Common;
using NPoco.fastJSON;
using VisioForge.MediaFramework.ONVIF;
using System.Runtime.Intrinsics.Arm;
using System.Security.Cryptography;

namespace CodePrintSystem.Forms
{
    public partial class TagTemplateEditForm : UIEditForm
    {
        Func<GenerateCodeTemplateEntity, List<GenerateCodeTemplateDetailEntity>, Task<bool>> func;
        public TagTemplateEditForm(Func<GenerateCodeTemplateEntity, List<GenerateCodeTemplateDetailEntity>, Task<bool>> func)
        {
            InitializeComponent();
            this.func = func;
            if (func != null)
            {
                this.ButtonOkClick += TagTemplateEditForm_ButtonOkClick;
            }
            uiDataGridView1.AutoGenerateColumns = false;
            uiDataGridView1.AllowUserToAddRows = false;
            InitDataGrid();

        }

        private async void TagTemplateEditForm_ButtonOkClick(object sender, EventArgs e)
        {
            var result = await func?.Invoke(Data, Details);
            if (result == true)
            {
                DialogResult = DialogResult.OK;
                Close();
            }

        }

        private void UiDataGridView1_ColumnHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            // 双击列头全选
            if (e.ColumnIndex == 0)
            {
                if (listData.Exists(x => x.IsCheck == false))
                {
                    listData.ForEach(x =>
                    {
                        x.IsCheck = true;
                    });
                }
                else
                {
                    listData.ForEach(x =>
                    {
                        x.IsCheck = false;
                    });
                }
                uiDataGridView1.DataSource = null;
                uiDataGridView1.DataSource = listData;
            }
        }
        private async Task InitDataGrid()
        {
            uiDataGridView1.ReadOnly = false;
            uiDataGridView1.ColumnHeaderMouseDoubleClick += UiDataGridView1_ColumnHeaderMouseDoubleClick;

            btnAdd.Click += BtnAdd_Click;
            btnEdit.Click += BtnEdit_Click;
            btnDelete.Click += BtnDelete_Click;
            var checkColumn = uiDataGridView1.AddCheckBoxColumn("选择", "IsCheck").SetFixedMode(100);
            checkColumn.ReadOnly = false;
            //uiDataGridView1.se;

            uiDataGridView1.AddColumn("序号", "RowNo").SetFixedMode(50);
            uiDataGridView1.AddColumn("规则类型", "RuleName").SetFixedMode(100);
            uiDataGridView1.AddColumn("别名", "AliasName").SetFixedMode(100);
            uiDataGridView1.AddColumn("规则默认值", "RuleValue").SetFixedMode(100);
            uiDataGridView1.AddColumn("限制长度", "ValueLength").SetFixedMode(100);
            uiDataGridView1.AddColumn("是否包含", "StrIsContain").SetFixedMode(150);


        }

        private bool GetSelectRows()
        {
            selectRows = listData.Where(x => x.IsCheck).ToList();
            if (selectRows.Any() == false)
            {
                UIMessageTip.ShowWarning("请选中一行");
                return false;
            }
            return true;
        }
        List<BusinessdictionaryEntity> ruleTypes = new List<BusinessdictionaryEntity>();
        List<GenerateCodeTemplateDetailView> listData = new List<GenerateCodeTemplateDetailView>();
        List<GenerateCodeTemplateDetailView> selectRows = new List<GenerateCodeTemplateDetailView>();
        private void BtnDelete_Click(object sender, EventArgs e)
        {
            if (GetSelectRows())
            {
                listData = listData.Except(selectRows).ToList();
                uiDataGridView1.DataSource = null;
                uiDataGridView1.DataSource = listData;
            }
        }

        public void CreateForm()
        {
            BindRuleTypeAsync();
            this.uiDataGridView1.DataSource = null;
            this.listData = new List<GenerateCodeTemplateDetailView>();
            this.selectRows = new List<GenerateCodeTemplateDetailView>();
            this.Details = new List<GenerateCodeTemplateDetailEntity>();

        }
        public void UpdateForm()
        {
            ShowUpdateDataAsync();
        }



        public async Task ShowUpdateDataAsync()
        {
            await BindRuleTypeAsync();
            this.uiDataGridView1.DataSource = null;
            this.listData = Details.MapToList<GenerateCodeTemplateDetailView>();
            listData.ForEach(row =>
            {
                row.RuleName = ruleTypes.Find(x => x.Id == row.RuleId.Value)?.Name;
            });
            this.uiDataGridView1.DataSource = listData;
            this.selectRows = new List<GenerateCodeTemplateDetailView>();
        }

        private async void BtnEdit_Click(object sender, EventArgs e)
        {
            if (GetSelectRows())
            {
                if (selectRows.Count > 1)
                {
                    UIMessageBox.Show("编辑不能选择多行");
                    return;
                }
                TagTemplateDetailEditForm editForm = new TagTemplateDetailEditForm();
                editForm.Text = "更新模板规则";
                var data = selectRows.First().MapTo<GenerateCodeTemplateDetailEntity>();
                editForm.Data = data;
                editForm.ShowDialog();

                if (editForm.IsOK)
                {
                    var editRow = this.listData.FindIndex(x => x.Id == editForm.Data.Id);
                    if (editRow >= 0)
                    {
                        var row = editForm.Data.MapTo<GenerateCodeTemplateDetailView>();
                        row.RuleName = ruleTypes.Find(x => x.Id == row.RuleId.Value)?.Name;
                        this.listData[editRow] = row;
                        this.uiDataGridView1.DataSource = null;
                        this.uiDataGridView1.DataSource = listData;
                    }
                }

                editForm.Dispose();
            }
        }

        private async void BtnAdd_Click(object sender, EventArgs e)
        {
            TagTemplateDetailEditForm editForm = new TagTemplateDetailEditForm();
            editForm.Text = "新增模板规则";
            editForm.AllRows = listData;
            editForm.CreateForm();
            editForm.ShowDialog();

            if (editForm.IsOK)
            {
                var row = editForm.Data.MapTo<GenerateCodeTemplateDetailView>();
                row.RuleName = ruleTypes.Find(x => x.Id == row.RuleId.Value)?.Name;
                this.listData.Add(row);
                this.uiDataGridView1.DataSource = null;
                this.uiDataGridView1.DataSource = listData;
            }

            editForm.Dispose();
        }

        public void DisabledControl()
        {
            this.txtCode.Enabled = false;
        }

        protected override bool CheckData()
        {
            this.Details = listData.MapToList<GenerateCodeTemplateDetailEntity>();

            return (CheckEmpty(txtCode, "请输入编码") && CheckEmpty(txtName, "请输入名称"));
        }
        private GenerateCodeTemplateEntity data = new GenerateCodeTemplateEntity();
        public List<GenerateCodeTemplateDetailEntity> Details = new List<GenerateCodeTemplateDetailEntity>();
        public GenerateCodeTemplateEntity Data
        {
            get
            {
                data.Code = txtCode.Text;
                data.Name = txtName.Text;
                data.IsEnabled = cbIsCheck.Checked;
                data.CodeLength = txtCodeLength.Value;
                data.PrintLimitOnce = printLimit.Value;
                data.SerialNumberLength = txtSequenceNumber.Value;
                return data;
            }

            set
            {
                data.Id = value.Id;
                txtCode.Text = value.Code;
                txtName.Text = value.Name;
                data.TemplateJsonFilePath = value.TemplateJsonFilePath;
                data.TemplateJsonText = value.TemplateJsonText;
                data.SerialNumberLength = value.SerialNumberLength;
                txtSequenceNumber.Value = value.SerialNumberLength.Value;
                txtCodeLength.Value = value.CodeLength.Value.ToInt();
                data.PrintLimitOnce = value.PrintLimitOnce;
                printLimit.Value = value.PrintLimitOnce.Value;
                cbIsCheck.Checked = value.IsEnabled.Value;
                data.PrintLimitOnce = value.PrintLimitOnce;
            }
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog { Filter = "JSON文件(*.json)|*.json|btw文件(*.btw)|*.btw|所有文件(*.*)|*.*" };//btw文件(*.btw)|*.btw|
            if (openFileDialog.ShowDialog() == DialogResult.OK && System.IO.File.Exists(openFileDialog.FileName))
            {
                //string fileName1= 
                // string FileName = $"Files\\{Data.Code}\\{Path.GetFileName(openFileDialog.FileName)}";
                // string path = AppsettingsConfig.BartenderSharePath + $"Files\\{Data.Code}";

                string FileName = $"Files\\{Data.Code}\\{Path.GetFileName(openFileDialog.FileName)}";
                string path = $"Files\\{Data.Code}";
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                FileInfo fi1 = new FileInfo(openFileDialog.FileName);

                fi1.CopyTo(FileName, true);
                if (openFileDialog.FileName.EndsWith(".json"))
                {
                    string json = string.Empty;
                    using (FileStream fs = new FileStream(FileName, FileMode.Open, System.IO.FileAccess.Read, FileShare.ReadWrite))
                    {
                        using (StreamReader sr = new StreamReader(fs))
                        {
                            json = sr.ReadToEnd().ToString();
                        }
                    }
                    if (!string.IsNullOrEmpty(json))
                    {
                        try
                        {
                            var config = JsonHelper.DeserializeObject<PrinterTagConfig>(json);

                            data.TemplateJsonFilePath = FileName;
                            data.TemplateJsonText = json;
                            byte[] fileData = File.ReadAllBytes(FileName);
                            data.FileHash = HashHelper.CalculateSHA1(fileData);
                            UIMessageBox.ShowSuccess("导入成功！");
                        }
                        catch
                        {
                            UIMessageBox.ShowError("导入失败！请检查模板文件");
                        }
                    }
                    else
                    {
                        UIMessageBox.ShowError("导入失败！请检查模板文件");
                    }

                }
                else
                {
                    data.TemplateJsonFilePath = FileName;
                    byte[] fileData = File.ReadAllBytes(FileName);
                    data.BTWFile = fileData;
                    data.FileHash = HashHelper.CalculateSHA1(fileData);
                    UIMessageBox.ShowSuccess("导入成功！");
                }
            }
        }

         

        private async void btnExport_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(data.TemplateJsonFilePath))
            {
                UIMessageBox.ShowError("没有模板");
                return;
            }
            SaveFileDialog saveFile = new SaveFileDialog();  //实例化保存文件对话框对象
            saveFile.Title = "请选择保存文件路径";
            if (data.TemplateJsonFilePath.Contains(".json"))
            {
                saveFile.Filter = "JSON文件(*.json)|*.json|所有文件(*.*)|*.*";//btw文件(*.btw)|*.btw|
            }
            else
            {
                saveFile.Filter = "btw文件(*.btw)|*.btw|所有文件(*.*)|*.*";//btw文件(*.btw)|*.btw|
            }
            //saveFile.Filter = "JSON文件(*.json)|*.json|btw文件(*.btw)|*.btw|所有文件(*.*)|*.*";//btw文件(*.btw)|*.btw|
            saveFile.OverwritePrompt = true;  //是否覆盖当前文件
            saveFile.RestoreDirectory = true;  //还原目录
            if (saveFile.ShowDialog() == DialogResult.OK)
            {
                var jsonData = await CommonRequestHelper.GetService<IQueryGenerateCodeTemplateService>().GetAsync<GenerateCodeTemplateEntity>(Data.Id);
                string FileName = data.TemplateJsonFilePath;
                string path = Path.GetDirectoryName(FileName);
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                try
                {
                    if (data.TemplateJsonFilePath.Contains(".json"))
                    {
                        // 写入文件
                        using (StreamWriter file = File.CreateText(FileName))
                        {
                            file.Write(jsonData.TemplateJsonText);
                        }
                    }
                    else
                    {
                        //判断文件是否存在
                        if (!File.Exists(FileName))
                        {
                            File.Create(FileName).Close();
                        }

                        File.WriteAllBytes(FileName, jsonData.BTWFile);

                    }
                    System.IO.File.Copy(FileName, saveFile.FileName, true);
                }
                catch (Exception ex)
                {
                    UIMessageBox.ShowSuccess($"导出失败！{ex.Message.ToString()}");
                    return;
                }
                UIMessageBox.ShowSuccess("导出成功！");
            }
        }

        private void btnOK_Click_1(object sender, EventArgs e)
        {

        }

        private void TagTemplateEditForm_Load(object sender, EventArgs e)
        {

        }

        public async Task BindRuleTypeAsync()
        {
            uiDataGridView1.DataSource = null;
            ruleTypes = await CommonRequestHelper.GetService<IQueryBusinessdictionaryService>()
                .GetListAsync<BusinessdictionaryEntity>(
         new BaseBusinessdictionaryCondition()
         {
             DictionaryTypeId = CommonStaticValue.CodeTemplateRuleTypeId.ToLong()
         }
            );
        }
    }
}
