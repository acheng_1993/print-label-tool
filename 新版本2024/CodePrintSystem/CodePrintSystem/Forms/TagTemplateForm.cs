﻿using CodePrintSystem.AppLayer.Basic.ViewObject;
using CodePrintSystem.AppLayer.SystemSet.ViewObject;
using CodePrintSystem.Basic.Abstractions;
using CodePrintSystem.Core;
using CodePrintSystem.Utility;
using Sunny.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodePrintSystem.Forms
{
    public partial class TagTemplateForm : UITitlePage, ISelfSingletonAutoInject
    {
        public TagTemplateForm()
        {
            InitializeComponent();
            this.Text = "标签模板";
            this.uiLabel1.Text = "模板名称";
            uiDataGridView1.AutoGenerateColumns = false;

        }

        private async Task InitDataGrid()
        {
            uiPagination1.ActivePage = 1;
            uiPagination1.PageSize = 20;
            uiDataGridView1.ReadOnly = false;
            uiDataGridView1.ColumnHeaderMouseDoubleClick += UiDataGridView1_ColumnHeaderMouseDoubleClick;
            uiPagination1.PageChanged += UiPagination1_PageChanged;
            btnSearch.Click += BtnSearch_Click;
            btnAdd.Click += BtnAdd_Click;
            btnEdit.Click += BtnEdit_Click;
            btnDelete.Click += BtnDelete_Click;
            var checkColumn = uiDataGridView1.AddCheckBoxColumn("选择", "IsCheck").SetFixedMode(100);
            uiDataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;//自适应
            checkColumn.ReadOnly = false;
            //uiDataGridView1.se;

            uiDataGridView1.AddColumn("编码", "Code");//.SetFixedMode(200);
            uiDataGridView1.AddColumn("名称", "Name");//.SetFixedMode(200);
            //uiDataGridView1.AddColumn("条码总长度", "CodeLength");
            uiDataGridView1.AddColumn("是否导入标签模板", "IsImportTemplate");
            //uiDataGridView1.AddColumn("流水号长度", "SerialNumberLength");
            uiDataGridView1.AddCheckBoxColumn("是否自定义", "IsCustom");
            uiDataGridView1.AddCheckBoxColumn("启用", "IsEnabled");//.SetFixedMode(200);
            await LoadDataGrid();
        }



        private async void UiPagination1_PageChanged(object sender, object pagingSource, int pageIndex, int count)
        {
            await LoadDataGrid();
        }

        private void UiDataGridView1_ColumnHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            // 双击列头全选
            if (e.ColumnIndex == 0)
            {
                if (listData.Exists(x => x.IsCheck == false))
                {
                    listData.ForEach(x =>
                    {
                        x.IsCheck = true;
                    });
                }
                else
                {
                    listData.ForEach(x =>
                    {
                        x.IsCheck = false;
                    });
                }
                uiDataGridView1.DataSource = null;
                uiDataGridView1.DataSource = listData;
            }
        }

        private async Task LoadDataGrid()
        {
            var data = await CommonRequestHelper.GetService<IQueryGenerateCodeTemplateService>()
                .GetListPagedAsync<GenerateCodeTemplateView>(
         uiPagination1.ActivePage, uiPagination1.PageSize,
         new BaseGenerateCodeTemplateCondition()
         {
             Keyword = this.txtCode.Text.Trim()
         }
            );

            listData = data.Data;

            this.uiDataGridView1.DataSource = listData;
            this.uiPagination1.TotalCount = data.Total;
        }
        private bool GetSelectRows()
        {
            selectRows = listData.Where(x => x.IsCheck).ToList();
            if (selectRows.Any() == false)
            {
                UIMessageTip.ShowWarning("请选中一行");
                return false;
            }
            return true;
        }

        List<GenerateCodeTemplateView> listData = new List<GenerateCodeTemplateView>();
        List<GenerateCodeTemplateView> selectRows = new List<GenerateCodeTemplateView>();



        private async void BtnAdd_Click(object sender, EventArgs e)
        {
            TagTemplateEditForm editForm = new TagTemplateEditForm(async (data, details) =>
            {
                var result = await CommonRequestHelper.GetService<ICommandGenerateCodeTemplateService>().CreateTemplateAsync(

            data, details);
                if (result.IsSuccess)
                {
                    CommonRequestHelper.RequestTip(result, () => { LoadDataGrid(); });
                }
                else
                {
                    UIMessageTip.ShowError(result.ErrorMessage);

                }
                return result.IsSuccess;
            });
            editForm.Text = "新增模板";
            editForm.ShowDialog();

            if (editForm.IsOK)
            {

            }

            editForm.Dispose();
        }


        private async void BtnEdit_Click(object sender, EventArgs e)
        {
            if (GetSelectRows())
            {
                if (selectRows.Count > 1)
                {
                    UIMessageBox.Show("编辑不能选择多行");
                    return;
                }
                var data = selectRows.First().MapTo<GenerateCodeTemplateEntity>();
                // var id = selectRows.First().Id;
                // var data = await CommonRequestHelper.GetService<IQueryGenerateCodeTemplateService>().GetAsync<GenerateCodeTemplateEntity>(id);
                TagTemplateEditForm editForm = new TagTemplateEditForm(async (data, details) =>
                {
                    var service = CommonRequestHelper.GetService<ICommandGenerateCodeTemplateService>();
                    var result = await service.UpdateTemplateAsync(data, details);
                    if (result.IsSuccess)
                    {
                        CommonRequestHelper.RequestTip(result, () => { LoadDataGrid(); });
                    }
                    else
                    {
                        UIMessageTip.ShowError(result.ErrorMessage);

                    }
                    return result.IsSuccess;
                });
                editForm.Text = "更新模板";
                editForm.Data = data;
                editForm.DisabledControl();
                editForm.Details = await CommonRequestHelper.GetService<IQueryGenerateCodeTemplateDetailService>()
                .GetListAsync<GenerateCodeTemplateDetailEntity>(
         new BaseGenerateCodeTemplateDetailCondition()
         {
             TemplateId = data.Id
         }
            );
                editForm.UpdateForm();
                editForm.ShowDialog();

                if (editForm.IsOK)
                {

                }

                editForm.Dispose();
            }
        }

        private async void BtnDelete_Click(object sender, EventArgs e)
        {
            if (GetSelectRows())
            {
                if (UIMessageBox.ShowAsk("此操作将删除该数据,确认删除？"))
                {
                    var result = await CommonRequestHelper.GetService<ICommandGenerateCodeTemplateService>().DeleteBatchAsync(selectRows.Select(x => x.Id).ToList());

                    CommonRequestHelper.RequestTip<bool>(result, () => { LoadDataGrid(); }, "删除成功");

                }
            }
        }

        private void BtnSearch_Click(object sender, EventArgs e)
        {
            LoadDataGrid();
        }

        private void TagTemplateForm_Load(object sender, EventArgs e)
        {
            InitDataGrid();
        }
    }
}
