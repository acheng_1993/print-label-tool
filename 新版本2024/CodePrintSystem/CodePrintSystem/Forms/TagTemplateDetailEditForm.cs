﻿using CodePrintSystem.AppLayer.SystemSet.ViewObject;
using CodePrintSystem.Basic.Abstractions;
using CodePrintSystem.EnumClass;
using CodePrintSystem.Utility;
using Sunny.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodePrintSystem.Forms
{
    public partial class TagTemplateDetailEditForm : UIEditForm
    {
        public TagTemplateDetailEditForm()
        {
            InitializeComponent();
            cbxRuleType.DropDownStyle = UIDropDownStyle.DropDownList;
            cbxRuleType.SelectedIndexChanged += CbxRuleType_SelectedIndexChanged;
            cbxItem.TextUpdate += CbxItem_TextUpdate;
            cbxItem.Leave += CbxItem_Leave;
            cbxItem.SelectedIndexChanged += CbxItem_SelectedIndexChanged;
            cbxSupplier.DropDownStyle = ComboBoxStyle.DropDownList;
            cbxDicValue.DropDownStyle = ComboBoxStyle.DropDownList;
            cbxSupplier.SelectedIndexChanged += CbxSupplier_SelectedIndexChanged;
            cbxDicValue.SelectedIndexChanged += CbxDicValue_SelectedIndexChanged;
            txtInputValue.TextChanged += TxtInputValue_TextChanged;
            //cbxSupplier.TextUpdate += CbxSupplier_TextUpdate;
            // cbxSupplier.Leave += CbxSupplier_Leave;
           
        }

        private void TxtInputValue_TextChanged(object sender, EventArgs e)
        {
            data.RuleValue = txtInputValue.Text;
        }

        private void CbxDicValue_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbxDicValue.SelectedItem != null)
                data.RuleValue = ((BusinessdictionaryEntity)cbxDicValue.SelectedItem).Code.ToString();
        }

        private void CbxSupplier_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbxSupplier.SelectedItem != null)
                data.RuleValue = ((SupplierEntity)cbxSupplier.SelectedItem).Code.ToString();
        }

        private void CbxItem_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cbxItem.SelectedItem!=null)
            data.RuleValue = ((ItemInfoEntity)cbxItem.SelectedItem).Code.ToString();
        }

        public List<GenerateCodeTemplateDetailView> AllRows = new List<GenerateCodeTemplateDetailView>();
        public void CreateForm() {
            if (AllRows.Any()) {
                this.txtRowNo.Value = AllRows.Max(x => x.RowNo.Value) + 1;
                    }
        }


        List<SupplierEntity> currentSupplierList = new List<SupplierEntity>();
        private async void CbxSupplier_Leave(object sender, EventArgs e)
        {
            var itemText = this.cbxSupplier.Text.Trim();
            if (currentSupplierList.FirstOrDefault(x => x.Code == itemText) == null)
            {
                this.cbxItem.Text = "";
                if (this.cbxSupplier.Items.Count > 0)
                    this.cbxSupplier.SelectedIndex = -1;
            }
        }

        private async void CbxSupplier_TextUpdate(object sender, EventArgs e)
        {
            var list = await CommonRequestHelper.GetService<IQuerySupplierService
                  >().GetListAsync<SupplierEntity>
             (new BaseSupplierCondition()
             {
                 KeyWord = ""
             });
            currentSupplierList = list;
            if (currentSupplierList.Any())
            {
                this.cbxSupplier.Items.Clear();
                this.cbxSupplier.Items.AddRange(currentSupplierList.ToArray());
            }
            this.cbxSupplier.SelectionStart = this.cbxSupplier.Text.Length;
            this.cbxSupplier.DroppedDown = true;

            Cursor = Cursors.Default;
        }

        private void CbxItem_Leave(object sender, EventArgs e)
        {
            var itemText = this.cbxItem.Text.Trim();
            if (currentItemList.FirstOrDefault(x => x.Code == itemText) == null)
            {
                this.cbxItem.Text = "";
                if (this.cbxItem.Items.Count > 0)
                    this.cbxItem.SelectedIndex = -1;
            }
        }

        List<ItemInfoEntity> currentItemList = new List<ItemInfoEntity>();
        private async void CbxItem_TextUpdate(object sender, EventArgs e)
        {
            //var list = await CommonRequestHelper.GetService<IQueryItemInfoService
            //       >().GetListPagedAsync<ItemInfoEntity>
            //  (1, 20, new BaseItemInfoCondition()
            //  {
            //      KeyWord = cbxItem.Text
            //  });
           // currentItemList =  list.Data;
            if (currentItemList.Any())
            {
                this.cbxItem.Items.Clear();
                this.cbxItem.Items.AddRange(currentItemList.ToArray());
            }
            this.cbxItem.SelectionStart = this.cbxItem.Text.Length;
            this.cbxItem.DroppedDown = true;

            Cursor = Cursors.Default;
        }

        List<ItemInfoEntity> itemList = new List<ItemInfoEntity>();
        List<SupplierEntity> supplierList = new List<SupplierEntity>();
        List<BusinessdictionaryEntity> dicList = new List<BusinessdictionaryEntity>();
        BusinessdictionaryEntity currentRuleType = null;
        private async void CbxRuleType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbxRuleType.SelectedItem == currentRuleType) {
                return;
            }
            currentRuleType = (BusinessdictionaryEntity)cbxRuleType.SelectedItem;
            var selectValue = cbxRuleType.SelectedValue.ToLong();
            if(string.IsNullOrEmpty(this.txt_Alias.Text))
            this.txt_Alias.Text = cbxRuleType.SelectedText;
            if (selectValue == CodeTemplateRuleEnum.ItemCode.ToLong())
            {
                HiddenAll();
                cbxItem.Visible = true;
               // var list = await CommonRequestHelper.GetService<IQueryItemInfoService
               //     >().GetListPagedAsync<ItemInfoEntity>
               //(1,50,new BaseItemInfoCondition()
               //{
               //    Code = ""
               //});
                cbxItem.ValueMember = "Code";
                cbxItem.DisplayMember = "Code";
                cbxItem.Items.Clear();
                //itemList =   list.Data;
                cbxItem.Items.AddRange(itemList.ToArray());
            }
            //if (selectValue == CodeTemplateRuleEnum.SupplierCode.ToLong())
            //{
            //    HiddenAll();
            //    cbxSupplier.Visible = true;
            //    cbxSupplier.Location = cbxItem.Location;
            //    cbxSupplier.BringToFront();
            //    //
            //    //UIMessageTip.Show(cbxSupplier.Location.X+","+ cbxSupplier.Location.Y);
            //    supplierList = await CommonRequestHelper.GetService<IQuerySupplierService
            //        >().GetListAsync<SupplierEntity>
            //   (new BaseSupplierCondition()
            //   {
            //       KeyWord = ""
            //   });
            //    cbxSupplier.ValueMember = "Code";
            //    cbxSupplier.DisplayMember = "Code";
            //    cbxSupplier.Items.Clear();
            //    cbxSupplier.Items.AddRange(supplierList.ToArray());
            //}
            if (selectValue == CodeTemplateRuleEnum.ClassShift.ToLong()
                || selectValue == CodeTemplateRuleEnum.RemarkCharacter.ToLong()
                || selectValue == CodeTemplateRuleEnum.CustomType.ToLong()
                 || selectValue == CodeTemplateRuleEnum.Number.ToLong()
                 || selectValue == CodeTemplateRuleEnum.Time.ToLong()
                // || selectValue == CodeTemplateRuleEnum.Version.ToLong()
                 || currentRuleType.IsDictionaryRule.Value ==true
                 || selectValue == CodeTemplateRuleEnum.Config.ToLong()
                 )
            {
                HiddenAll();
                cbxDicValue.Visible = true;
                cbxDicValue.Location = cbxItem.Location;
                cbxDicValue.BringToFront();
                var dicTypeId = CommonHelper.GetTypeId(selectValue);
                if (dicTypeId == 0) {
                    dicTypeId = currentRuleType.RuleDictionaryTypeId.Value;
                }
                dicList = await CommonRequestHelper.GetService<IQueryBusinessdictionaryService
                    >().GetListAsync<BusinessdictionaryEntity>
               (new BaseBusinessdictionaryCondition()
               {
                   IsEnabled =true,
                   DictionaryTypeId = dicTypeId
               }) ;
                data.DictionaryTypeId = dicTypeId;
                cbxDicValue.ValueMember = "Code";
                cbxDicValue.DisplayMember = "Name";
                cbxDicValue.Items.Clear();
                cbxDicValue.Items.AddRange(dicList.ToArray());
            }
            if (selectValue == CodeTemplateRuleEnum.SequenceNumber.ToLong()
              || selectValue == CodeTemplateRuleEnum.DayOfWeek.ToLong()
              || selectValue == CodeTemplateRuleEnum.WeekOfYear.ToLong())
            {
                HiddenAll();
                txtRuleValueDisabled.Visible = true;
                txtRuleValueDisabled.Location = cbxItem.Location;
                txtRuleValueDisabled.BringToFront();
                txtRuleValueDisabled.Text = "系统生成无需输入";
            }
            if (selectValue == CodeTemplateRuleEnum.InputText.ToLong() ||
                selectValue == CodeTemplateRuleEnum.FixValue.ToLong()) {
                HiddenAll();
                txtInputValue.Visible = true;
                txtInputValue.Location = cbxItem.Location;
                txtInputValue.BringToFront();
            }
            if (selectValue == CodeTemplateRuleEnum.Version.ToLong())
            {
                HiddenAll();
                txtRuleValueDisabled.Visible = true;
                txtRuleValueDisabled.Location = cbxItem.Location;
                txtRuleValueDisabled.BringToFront();
                txtRuleValueDisabled.Text = "产品带入无需输入";
            }
            if (selectValue == CodeTemplateRuleEnum.SupplierCode.ToLong())
            {
                HiddenAll();
                txtRuleValueDisabled.Visible = true;
                txtRuleValueDisabled.Location = cbxItem.Location;
                txtRuleValueDisabled.BringToFront();
                txtRuleValueDisabled.Text = "打印时选择";
            }
            

        }

        private void HiddenAll()
        {
            txtInputValue.Visible = false;
            cbxItem.Visible = false;
            cbxSupplier.Visible = false;
            cbxDicValue.Visible = false;
            txtRuleValueDisabled.Visible = false;
        }

        public void DisabledControl()
        {

        }
        List<BusinessdictionaryEntity> ruleTypeList = new List<BusinessdictionaryEntity>();
        /// <summary>
        /// 初始化数据
        /// </summary>
        /// <returns></returns>
        private async Task InitData()
        {
            ruleTypeList = await CommonRequestHelper.GetService<IQueryBusinessdictionaryService>().GetListAsync<BusinessdictionaryEntity>
          (new BaseBusinessdictionaryCondition()
          {
              DictionaryTypeId = CommonStaticValue.CodeTemplateRuleTypeId.ToLong()
          });
            cbxRuleType.ValueMember = "Id";
            cbxRuleType.DisplayMember = "Name";
            cbxRuleType.DataSource = ruleTypeList;
            if (data.RuleId.HasValue)
            {
                var currentType = ruleTypeList.Find(x => x.Id == data.RuleId.Value);
                cbxRuleType.SelectedItem = currentType;
                CbxRuleType_SelectedIndexChanged(null, null);
                var index = dicList.FindIndex(x => x.Code == data.RuleValue);
                cbxDicValue.SelectedIndex = index;
                index = itemList.FindIndex(x => x.Code == data.RuleValue);
                cbxItem.SelectedIndex = index;
                index = supplierList.FindIndex(x => x.Code == data.RuleValue);
                cbxSupplier.SelectedIndex = index;
            }
        }

        protected override bool CheckData()
        {
            // 行号重复问题
            if (AllRows.Where(x => x.Id != data.Id).ToList().Exists(x => x.RowNo == txtRowNo.Value)) {
                UIMessageTip.ShowError("序号不能重复");
                return false;
            }
            return true;
        }

     

        private GenerateCodeTemplateDetailEntity data = new GenerateCodeTemplateDetailEntity();
        public  GenerateCodeTemplateDetailEntity Data
        {
            get
            {
                data.RowNo = txtRowNo.Value;
                data.RuleId = cbxRuleType.SelectedValue.ToLong();
                data.IsAdminEdit = cbxIsAdminEdit.Checked;
                data.ValueLength = txtValueLength.Value;
                data.AliasName = string.IsNullOrEmpty(txt_Alias.Text.Trim())? cbxRuleType.SelectedText: txt_Alias.Text.Trim();
                //data.RuleValue = GetRuleValue();
                data.IsContain=CH_IsContain.Checked;
                return data;
            }

            set
            {
                data.Id = value.Id;
                txtRowNo.Value = value.RowNo.Value;
                data.RuleId = value.RuleId;
                data.RuleValue = value.RuleValue;
                //var currentType =  ruleTypeList.Find(x => x.Id == value.RuleId.Value);
                //cbxRuleType.SelectedValue = currentType;
                cbxIsAdminEdit.Checked = value.IsAdminEdit;
                txtValueLength.Value = value.ValueLength.Value.ToInt();
                txtInputValue.Text = value.RuleValue;
                txt_Alias.Text = value.AliasName;
                CH_IsContain.Checked = value.IsContain.Value;
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            this.cbxDicValue.SelectedIndex = -1;
            this.cbxItem.SelectedIndex = -1;
            this.cbxSupplier.SelectedIndex = -1;
            this.txtInputValue.Text = "";
        }

        private void TagTemplateDetailEditForm_Load(object sender, EventArgs e)
        {

            HiddenAll();
            InitData();
        }
    }
}
