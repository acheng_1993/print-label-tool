﻿using CodePrintSystem.Core;
using Sunny.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CodePrintSystem.Basic.Abstractions;
using Microsoft.Extensions.DependencyInjection;
using CodePrintSystem.Utility;
using CodePrintSystem.AppLayer.Basic.ViewObject;

namespace CodePrintSystem.Forms
{
    public partial class UserForm : UITitlePage, ISelfSingletonAutoInject
    {
        IServiceProvider serviceProvider;
        public UserForm(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
            
            InitializeComponent();
            this.Text = "用户维护";
            uiDataGridView1.AutoGenerateColumns = false;
        }

        public IQueryUserService GetQueryService()
        {
            var scope = serviceProvider.CreateScope();
            var userService = scope.ServiceProvider.GetRequiredService<IQueryUserService>();
            return userService;
        }
        public ICommandUserService GetCommandService()
        {
            var scope = serviceProvider.CreateScope();
            var userService = scope.ServiceProvider.GetRequiredService<ICommandUserService>();
            return userService;
        }

        private async Task InitDataGrid()
        {
            uiPagination1.ActivePage = 1;
            uiPagination1.PageSize = 20;
            uiDataGridView1.ReadOnly = false;
            uiDataGridView1.ColumnHeaderMouseDoubleClick += UiDataGridView1_ColumnHeaderMouseDoubleClick;
            uiDataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;//自适应
            var checkColumn = uiDataGridView1.AddCheckBoxColumn("选择", "IsCheck").SetFixedMode(100);
            checkColumn.ReadOnly = false;
            //uiDataGridView1.se;
            uiDataGridView1.AddColumn("用户名", "UserName");//.SetFixedMode(200);
            uiDataGridView1.AddCheckBoxColumn("是否启用", "IsEnabled");//.SetFixedMode(100);
            uiDataGridView1.AddCheckBoxColumn("是否管理员", "IsAdmin");//.SetFixedMode(100);
            uiDataGridView1.AddCheckBoxColumn("指纹状态", "IsFingePrint");//.SetFixedMode(100);
            uiDataGridView1.AddColumn("最后登录时间", "LastLoginTime");//.SetFixedMode(400);
            await LoadDataGrid();
        }

        private void UiDataGridView1_ColumnHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            // 双击列头全选
            if (e.ColumnIndex == 0)
            {
                if (userViews.Exists(x => x.IsCheck == false))
                {
                    userViews.ForEach(x =>
                    {
                        x.IsCheck = true;
                    });
                }
                else
                {
                    userViews.ForEach(x =>
                    {
                        x.IsCheck = false;
                    });
                }
                uiDataGridView1.DataSource = null;
                uiDataGridView1.DataSource = userViews;
            }
        }

        List<UserView> userViews = new List<UserView>();
        List<UserView> selectRows = new List<UserView>();
        private async Task LoadDataGrid()
        {
            var data = await GetQueryService().GetListPagedAsync<UserView>(
         uiPagination1.ActivePage, uiPagination1.PageSize,
         new BaseUserCondition()
         {
             UserName = this.txtUserName.Text.Trim()
         }
            );
            userViews = data.Data;
            this.uiDataGridView1.DataSource = userViews;
            this.uiPagination1.TotalCount = data.Total;
        }


        private void btnSearch_Click(object sender, EventArgs e)
        {
            LoadDataGrid();
        }

        private async void uiPagination1_PageChanged(object sender, object pagingSource, int pageIndex, int count)
        {
            await LoadDataGrid();
        }

        private void PagePanel_Load(object sender, EventArgs e)
        {
            InitDataGrid();
        }
        /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void uiSymbolButton1_Click(object sender, EventArgs e)
        {
            var selectRows = userViews.Where(x => x.IsCheck).ToList();
            if (!selectRows.Any())
            {
                UIMessageTip.ShowWarning("请选中一行");
                return;
            }
            if (selectRows.Count > 1)
            {
                UIMessageBox.Show("编辑不能选择多行");
                return;
            }
            var row = selectRows.FirstOrDefault();
            var data = row.MapTo<UserEntity>();
            UserEditForm userEditForm = new UserEditForm(async (data) => {

                var result = await GetCommandService().UpdateAsync(data);
                if (result.IsSuccess)
                {
                    CommonRequestHelper.RequestTip(result, () => { LoadDataGrid(); });
                }
                else
                {
                    UIMessageTip.ShowError(result.ErrorMessage);

                }
                return result.IsSuccess;

            });
            userEditForm.Text = "更新用户";
            userEditForm.User = data;
            userEditForm.DisabledControl();
            userEditForm.ShowDialog();

            if (userEditForm.IsOK)
            {
                
            }

            userEditForm.Dispose();
            //UIMessageBox.Show("选中了"+string.Join(",",userViews.Where(x=>x.IsCheck).Select(x=>x.UserName).ToList()));
        }
        /// <summary>
        /// 增加
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btnAdd_Click(object sender, EventArgs e)
        {

            UserEditForm userEditForm = new UserEditForm(async  (data)=> {
                var result = await GetCommandService().InsertAsync(data);
                if (result.IsSuccess)
                {
                    CommonRequestHelper.RequestTip(result, () => { LoadDataGrid(); });
                }
                else
                {
                    UIMessageTip.ShowError(result.ErrorMessage);

                }
                return result.IsSuccess;
            });
            userEditForm.Text = "新增用户";
            userEditForm.ShowDialog();

            if (userEditForm.IsOK)
            {
                
            }

            userEditForm.Dispose();
        }
        private bool GetSelectRows()
        {
            selectRows = userViews.Where(x => x.IsCheck).ToList();
            if (!selectRows.Any())
            {
                UIMessageTip.ShowWarning("请选中一行");
                return false;
            }
            return true;
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void uiSymbolButton2_Click(object sender, EventArgs e)
        {
            if (GetSelectRows())
            {
                if (UIMessageBox.ShowAsk("删除的数据不可恢复，确认删除？"))
                {
                    var result = await GetCommandService().DeleteBatchAsync(selectRows.Select(x => x.Id).ToList());
                    CommonRequestHelper.RequestTip<bool>(result, () => { LoadDataGrid(); }, "删除成功");

                }
            }
        }


        /// <summary>
        /// 重置密码
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btnResetPwd_Click(object sender, EventArgs e)
        {
            if (GetSelectRows())
            {
                CommonRequestHelper.RequestTip(await GetCommandService().BatchResetPwdAsync(selectRows.Select(x => x.Id).ToList())
                    , () => { LoadDataGrid(); }, "重置成功");
            }


        }

        private void UserForm_Load(object sender, EventArgs e)
        {

        }
    }
}
