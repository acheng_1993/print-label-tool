﻿
namespace CodePrintSystem.Forms
{
    partial class PrintLogForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.uiDataGridView1 = new Sunny.UI.UIDataGridView();
            this.uiPanel1 = new Sunny.UI.UIPanel();
            this.txtItemCode = new Sunny.UI.UITextBox();
            this.uiLabel6 = new Sunny.UI.UILabel();
            this.cbx_user = new Sunny.UI.UIComboBox();
            this.uiLabel5 = new Sunny.UI.UILabel();
            this.txt_ProductionCode = new Sunny.UI.UITextBox();
            this.uiLabel4 = new Sunny.UI.UILabel();
            this.txt_endTime = new Sunny.UI.UIDatetimePicker();
            this.uiLabel3 = new Sunny.UI.UILabel();
            this.txt_startTime = new Sunny.UI.UIDatetimePicker();
            this.uiLabel2 = new Sunny.UI.UILabel();
            this.btnSearch = new Sunny.UI.UIButton();
            this.txtCode = new Sunny.UI.UITextBox();
            this.uiLabel1 = new Sunny.UI.UILabel();
            this.uiPagination1 = new Sunny.UI.UIPagination();
            this.btn_ExportAll = new Sunny.UI.UIButton();
            this.btn_exportPage = new Sunny.UI.UIButton();
            this.PagePanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiDataGridView1)).BeginInit();
            this.uiPanel1.SuspendLayout();
            this.uiPagination1.SuspendLayout();
            this.SuspendLayout();
            // 
            // PagePanel
            // 
            this.PagePanel.Controls.Add(this.uiDataGridView1);
            this.PagePanel.Controls.Add(this.uiPanel1);
            this.PagePanel.Controls.Add(this.uiPagination1);
            this.PagePanel.Size = new System.Drawing.Size(1500, 878);
            // 
            // uiDataGridView1
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(243)))), ((int)(((byte)(255)))));
            this.uiDataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.uiDataGridView1.BackgroundColor = System.Drawing.Color.White;
            this.uiDataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.uiDataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.uiDataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(200)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.uiDataGridView1.DefaultCellStyle = dataGridViewCellStyle3;
            this.uiDataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiDataGridView1.EnableHeadersVisualStyles = false;
            this.uiDataGridView1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.uiDataGridView1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            this.uiDataGridView1.Location = new System.Drawing.Point(0, 94);
            this.uiDataGridView1.Name = "uiDataGridView1";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(243)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.uiDataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            this.uiDataGridView1.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.uiDataGridView1.RowTemplate.Height = 29;
            this.uiDataGridView1.SelectedIndex = -1;
            this.uiDataGridView1.ShowGridLine = true;
            this.uiDataGridView1.Size = new System.Drawing.Size(1500, 749);
            this.uiDataGridView1.TabIndex = 9;
            // 
            // uiPanel1
            // 
            this.uiPanel1.Controls.Add(this.txtItemCode);
            this.uiPanel1.Controls.Add(this.uiLabel6);
            this.uiPanel1.Controls.Add(this.cbx_user);
            this.uiPanel1.Controls.Add(this.uiLabel5);
            this.uiPanel1.Controls.Add(this.txt_ProductionCode);
            this.uiPanel1.Controls.Add(this.uiLabel4);
            this.uiPanel1.Controls.Add(this.txt_endTime);
            this.uiPanel1.Controls.Add(this.uiLabel3);
            this.uiPanel1.Controls.Add(this.txt_startTime);
            this.uiPanel1.Controls.Add(this.uiLabel2);
            this.uiPanel1.Controls.Add(this.btnSearch);
            this.uiPanel1.Controls.Add(this.txtCode);
            this.uiPanel1.Controls.Add(this.uiLabel1);
            this.uiPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiPanel1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.uiPanel1.Location = new System.Drawing.Point(0, 0);
            this.uiPanel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiPanel1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiPanel1.Name = "uiPanel1";
            this.uiPanel1.Size = new System.Drawing.Size(1500, 94);
            this.uiPanel1.TabIndex = 8;
            this.uiPanel1.Text = null;
            // 
            // txtItemCode
            // 
            this.txtItemCode.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtItemCode.FillColor = System.Drawing.Color.White;
            this.txtItemCode.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtItemCode.Location = new System.Drawing.Point(98, 55);
            this.txtItemCode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtItemCode.Maximum = 2147483647D;
            this.txtItemCode.Minimum = -2147483648D;
            this.txtItemCode.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtItemCode.Name = "txtItemCode";
            this.txtItemCode.Padding = new System.Windows.Forms.Padding(5);
            this.txtItemCode.Size = new System.Drawing.Size(167, 29);
            this.txtItemCode.TabIndex = 10;
            // 
            // uiLabel6
            // 
            this.uiLabel6.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.uiLabel6.Location = new System.Drawing.Point(12, 55);
            this.uiLabel6.Name = "uiLabel6";
            this.uiLabel6.Size = new System.Drawing.Size(79, 23);
            this.uiLabel6.TabIndex = 9;
            this.uiLabel6.Text = "产品编码";
            this.uiLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbx_user
            // 
            this.cbx_user.FillColor = System.Drawing.Color.White;
            this.cbx_user.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.cbx_user.Location = new System.Drawing.Point(617, 14);
            this.cbx_user.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbx_user.MinimumSize = new System.Drawing.Size(63, 0);
            this.cbx_user.Name = "cbx_user";
            this.cbx_user.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            this.cbx_user.Size = new System.Drawing.Size(174, 29);
            this.cbx_user.TabIndex = 8;
            this.cbx_user.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel5
            // 
            this.uiLabel5.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.uiLabel5.Location = new System.Drawing.Point(545, 20);
            this.uiLabel5.Name = "uiLabel5";
            this.uiLabel5.Size = new System.Drawing.Size(79, 23);
            this.uiLabel5.TabIndex = 7;
            this.uiLabel5.Text = "打印人";
            this.uiLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txt_ProductionCode
            // 
            this.txt_ProductionCode.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_ProductionCode.FillColor = System.Drawing.Color.White;
            this.txt_ProductionCode.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txt_ProductionCode.Location = new System.Drawing.Point(374, 14);
            this.txt_ProductionCode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txt_ProductionCode.Maximum = 2147483647D;
            this.txt_ProductionCode.Minimum = -2147483648D;
            this.txt_ProductionCode.MinimumSize = new System.Drawing.Size(1, 1);
            this.txt_ProductionCode.Name = "txt_ProductionCode";
            this.txt_ProductionCode.Padding = new System.Windows.Forms.Padding(5);
            this.txt_ProductionCode.Size = new System.Drawing.Size(150, 29);
            this.txt_ProductionCode.TabIndex = 2;
            // 
            // uiLabel4
            // 
            this.uiLabel4.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.uiLabel4.Location = new System.Drawing.Point(288, 17);
            this.uiLabel4.Name = "uiLabel4";
            this.uiLabel4.Size = new System.Drawing.Size(79, 23);
            this.uiLabel4.TabIndex = 6;
            this.uiLabel4.Text = "生产制令";
            this.uiLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txt_endTime
            // 
            this.txt_endTime.DateFormat = "yyyy-MM-dd";
            this.txt_endTime.FillColor = System.Drawing.Color.White;
            this.txt_endTime.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txt_endTime.Location = new System.Drawing.Point(617, 51);
            this.txt_endTime.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txt_endTime.MaxLength = 10;
            this.txt_endTime.MinimumSize = new System.Drawing.Size(63, 0);
            this.txt_endTime.Name = "txt_endTime";
            this.txt_endTime.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            this.txt_endTime.Size = new System.Drawing.Size(150, 29);
            this.txt_endTime.SymbolDropDown = 61555;
            this.txt_endTime.SymbolNormal = 61555;
            this.txt_endTime.TabIndex = 5;
            this.txt_endTime.Text = "2021-02-01";
            this.txt_endTime.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.txt_endTime.Value = new System.DateTime(2021, 2, 1, 0, 0, 0, 0);
            // 
            // uiLabel3
            // 
            this.uiLabel3.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.uiLabel3.Location = new System.Drawing.Point(531, 55);
            this.uiLabel3.Name = "uiLabel3";
            this.uiLabel3.Size = new System.Drawing.Size(79, 23);
            this.uiLabel3.TabIndex = 5;
            this.uiLabel3.Text = "结束时间";
            this.uiLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txt_startTime
            // 
            this.txt_startTime.DateFormat = "yyyy-MM-dd";
            this.txt_startTime.FillColor = System.Drawing.Color.White;
            this.txt_startTime.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txt_startTime.Location = new System.Drawing.Point(374, 51);
            this.txt_startTime.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txt_startTime.MaxLength = 10;
            this.txt_startTime.MinimumSize = new System.Drawing.Size(63, 0);
            this.txt_startTime.Name = "txt_startTime";
            this.txt_startTime.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            this.txt_startTime.Size = new System.Drawing.Size(150, 29);
            this.txt_startTime.SymbolDropDown = 61555;
            this.txt_startTime.SymbolNormal = 61555;
            this.txt_startTime.TabIndex = 4;
            this.txt_startTime.Text = "2021-02-01";
            this.txt_startTime.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.txt_startTime.Value = new System.DateTime(2021, 2, 1, 0, 0, 0, 0);
            // 
            // uiLabel2
            // 
            this.uiLabel2.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.uiLabel2.Location = new System.Drawing.Point(282, 55);
            this.uiLabel2.Name = "uiLabel2";
            this.uiLabel2.Size = new System.Drawing.Size(79, 23);
            this.uiLabel2.TabIndex = 3;
            this.uiLabel2.Text = "开始时间";
            this.uiLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnSearch
            // 
            this.btnSearch.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSearch.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnSearch.Location = new System.Drawing.Point(849, 17);
            this.btnSearch.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(102, 63);
            this.btnSearch.TabIndex = 2;
            this.btnSearch.Text = "查询";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtCode
            // 
            this.txtCode.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtCode.FillColor = System.Drawing.Color.White;
            this.txtCode.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtCode.Location = new System.Drawing.Point(98, 17);
            this.txtCode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtCode.Maximum = 2147483647D;
            this.txtCode.Minimum = -2147483648D;
            this.txtCode.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtCode.Name = "txtCode";
            this.txtCode.Padding = new System.Windows.Forms.Padding(5);
            this.txtCode.Size = new System.Drawing.Size(167, 29);
            this.txtCode.TabIndex = 1;
            // 
            // uiLabel1
            // 
            this.uiLabel1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.uiLabel1.Location = new System.Drawing.Point(39, 17);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Size = new System.Drawing.Size(79, 23);
            this.uiLabel1.TabIndex = 0;
            this.uiLabel1.Text = "条码";
            this.uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiPagination1
            // 
            this.uiPagination1.BackColor = System.Drawing.SystemColors.Control;
            this.uiPagination1.Controls.Add(this.btn_ExportAll);
            this.uiPagination1.Controls.Add(this.btn_exportPage);
            this.uiPagination1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiPagination1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.uiPagination1.Location = new System.Drawing.Point(0, 843);
            this.uiPagination1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiPagination1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiPagination1.Name = "uiPagination1";
            this.uiPagination1.PagerCount = 5;
            this.uiPagination1.RectSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.None;
            this.uiPagination1.Size = new System.Drawing.Size(1500, 35);
            this.uiPagination1.TabIndex = 7;
            this.uiPagination1.Text = null;
            // 
            // btn_ExportAll
            // 
            this.btn_ExportAll.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_ExportAll.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btn_ExportAll.Location = new System.Drawing.Point(864, 3);
            this.btn_ExportAll.MinimumSize = new System.Drawing.Size(1, 1);
            this.btn_ExportAll.Name = "btn_ExportAll";
            this.btn_ExportAll.Size = new System.Drawing.Size(101, 29);
            this.btn_ExportAll.TabIndex = 20;
            this.btn_ExportAll.Text = "导出所有";
            this.btn_ExportAll.Click += new System.EventHandler(this.btn_ExportAll_Click);
            // 
            // btn_exportPage
            // 
            this.btn_exportPage.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_exportPage.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btn_exportPage.Location = new System.Drawing.Point(757, 3);
            this.btn_exportPage.MinimumSize = new System.Drawing.Size(1, 1);
            this.btn_exportPage.Name = "btn_exportPage";
            this.btn_exportPage.Size = new System.Drawing.Size(101, 29);
            this.btn_exportPage.TabIndex = 6;
            this.btn_exportPage.Text = "导出当前页";
            this.btn_exportPage.Click += new System.EventHandler(this.btn_exportPage_Click);
            // 
            // PrintLogForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1500, 913);
            this.Name = "PrintLogForm";
            this.Text = "PrintLog";
            this.Load += new System.EventHandler(this.PrintLogForm_Load);
            this.PagePanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiDataGridView1)).EndInit();
            this.uiPanel1.ResumeLayout(false);
            this.uiPagination1.ResumeLayout(false);
            this.uiPagination1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Sunny.UI.UIDataGridView uiDataGridView1;
        private Sunny.UI.UIPanel uiPanel1;
        private Sunny.UI.UIButton btnSearch;
        private Sunny.UI.UITextBox txtCode;
        private Sunny.UI.UILabel uiLabel1;
        private Sunny.UI.UIPagination uiPagination1;
        private Sunny.UI.UIDatetimePicker txt_endTime;
        private Sunny.UI.UILabel uiLabel3;
        private Sunny.UI.UIDatetimePicker txt_startTime;
        private Sunny.UI.UILabel uiLabel2;
        private Sunny.UI.UIButton btn_exportPage;
        private Sunny.UI.UIButton btn_ExportAll;
        private Sunny.UI.UILabel uiLabel5;
        private Sunny.UI.UITextBox txt_ProductionCode;
        private Sunny.UI.UILabel uiLabel4;
        private Sunny.UI.UIComboBox cbx_user;
        private Sunny.UI.UITextBox txtItemCode;
        private Sunny.UI.UILabel uiLabel6;
    }
}