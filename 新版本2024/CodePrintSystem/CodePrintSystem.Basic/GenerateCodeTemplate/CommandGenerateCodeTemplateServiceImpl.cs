
//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/1/15 9:53:15
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Linq.Expressions;
using System;
using CodePrintSystem.Core;
using CodePrintSystem.Basic.Abstractions;
using CodePrintSystem.Utility;
using CodePrintSystem.Utility.EnumClass;
using CodePrintSystem.EnumClass;

namespace CodePrintSystem.Basic
{
    internal class CommandGenerateCodeTemplateServiceImpl : ICommandGenerateCodeTemplateService, IAutoInject
    {
        private readonly IDefaultUnitOfWorkV2<long, IEntity<long>, ICommandGenerateCodeTemplateRepository<long>> unitOfWork;
        private readonly IQueryGenerateCodeTemplateService queryGenerateCodeTemplateService;
        private readonly IQueryGenerateCodeTemplateDetailService queryGenerateCodeTemplateDetailService;
        private readonly ICommandGenerateCodeTemplateDetailService commandGenerateCodeTemplateDetailService;

        public CommandGenerateCodeTemplateServiceImpl(IDefaultUnitOfWorkV2<long, IEntity<long>,
            ICommandGenerateCodeTemplateRepository<long>> unitOfWork,
            IQueryGenerateCodeTemplateService queryGenerateCodeTemplateService,
            ICommandGenerateCodeTemplateDetailService commandGenerateCodeTemplateDetailService,
            IQueryGenerateCodeTemplateDetailService queryGenerateCodeTemplateDetailService)
        {
            this.unitOfWork = unitOfWork;
            this.queryGenerateCodeTemplateService = queryGenerateCodeTemplateService;
            this.commandGenerateCodeTemplateDetailService = commandGenerateCodeTemplateDetailService;
            this.queryGenerateCodeTemplateDetailService = queryGenerateCodeTemplateDetailService;
        }

        #region 插入

        /// <summary>
        /// 插入单个实体
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<long>> InsertAsync(GenerateCodeTemplateEntity entity, bool isCommit = true)
        {
            HttpResponseResultModel<long> httpResponseResultModel = new HttpResponseResultModel<long> { IsSuccess = false };
            unitOfWork.RegisterInsert(entity);
            if (isCommit)
            {
                await CommitAsync();
            }
            httpResponseResultModel.BackResult = entity.Id;
            httpResponseResultModel.IsSuccess = true;
            return httpResponseResultModel;
        }

        /// <summary>
        /// 批量插入实体
        /// </summary>
        /// <param name="entityList"></param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> InsertBatchAsync(List<GenerateCodeTemplateEntity> entityList, bool isCommit = true)
        {
            HttpResponseResultModel<bool> httpResponseResultModel = new HttpResponseResultModel<bool> { IsSuccess = false };
            foreach (var entity in entityList)
            {
                unitOfWork.RegisterInsert(entity);
            }
            if (isCommit)
            {
                await CommitAsync();
            }
            httpResponseResultModel.BackResult = true;
            httpResponseResultModel.IsSuccess = true;
            return httpResponseResultModel;
        }


        #endregion

        #region 更新
        /// <summary>
        /// 根据主键更新实体
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> UpdateAsync(GenerateCodeTemplateEntity entity, bool isCommit = true)
        {
            List<long> idList = new List<long>()
            {entity.Id };
            HttpResponseResultModel<bool> httpResponseResultModel = new HttpResponseResultModel<bool> { IsSuccess = false };
            var isCustom = await queryGenerateCodeTemplateService.IsTemplateIsCustomAsync(idList).ConfigureAwait(false);
            if (isCustom)
            {
                httpResponseResultModel.IsSuccess = false;
                httpResponseResultModel.ErrorMessage = "系统内置模板不可修改";
                return httpResponseResultModel;
            }
            unitOfWork.RegisterUpdate(entity);
            if (isCommit)
            {
                await CommitAsync();
            }
            httpResponseResultModel.IsSuccess = true;
            httpResponseResultModel.BackResult = true;
            return httpResponseResultModel;
        }


        /// <summary>
        /// 批量更新实体
        /// </summary>
        /// <param name="entityList"></param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> UpdateBatchAsync(List<GenerateCodeTemplateEntity> entityList, bool isCommit = true)
        {
            HttpResponseResultModel<bool> httpResponseResultModel = new HttpResponseResultModel<bool> { IsSuccess = false };
            foreach (var entity in entityList)
            {
                unitOfWork.RegisterUpdate(entity);
            }
            if (isCommit)
            {
                await CommitAsync();
            }
            httpResponseResultModel.IsSuccess = true;
            httpResponseResultModel.BackResult = true;
            return httpResponseResultModel;
        }








        #endregion

        #region 删除

        /// <summary>
        /// 根据根据主键删除
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> DeleteAsync(long id, bool isCommit = true)
        {
            HttpResponseResultModel<bool> httpResponseResultModel = new HttpResponseResultModel<bool> { IsSuccess = false };
            var model = await queryGenerateCodeTemplateService.GetAsync<GenerateCodeTemplateEntity>(id);
            if (model == null || model.Id == 0)
            {
                httpResponseResultModel.ErrorMessage = GenerateCodeTemplateErrorEnum.TemplateIsNotExist.GetDescription();
                return httpResponseResultModel;
            }
            if (model.IsQuote.Value)
            {
                httpResponseResultModel.ErrorMessage = "";//CommonErrorEnum.IsQuoteCanNotEditError.GetLocalizer();
                return httpResponseResultModel;
            }
            unitOfWork.RegisterDelete(id);
            if (isCommit)
            {
                await CommitAsync();
            }
            httpResponseResultModel.IsSuccess = true;
            httpResponseResultModel.BackResult = true;
            return httpResponseResultModel;
        }

        /// <summary>
        /// 批量删除 根据主键
        /// </summary>
        /// <param name="idList">主键集合</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> DeleteBatchAsync(IList<long> idList, bool isCommit = true)
        {
            HttpResponseResultModel<bool> httpResponseResultModel = new HttpResponseResultModel<bool> { IsSuccess = false };
            var isCustom = await queryGenerateCodeTemplateService.IsTemplateIsCustomAsync(idList.ToList()).ConfigureAwait(false);
            if (isCustom)
            {
                httpResponseResultModel.IsSuccess = false;
                httpResponseResultModel.ErrorMessage = "系统内置模板不可修改";
                return httpResponseResultModel;
            }
            var models = await queryGenerateCodeTemplateService.GetListByIdsAsync<GenerateCodeTemplateEntity>(idList.ToList());
            if (models.IsListNullOrEmpty())
            {
                httpResponseResultModel.ErrorMessage = GenerateCodeTemplateErrorEnum.TemplateIsNotExist.GetDescription();
                return httpResponseResultModel;
            }
            if (models.Exists(x => x.IsQuote.Value))
            {
                httpResponseResultModel.ErrorMessage = "";//CommonErrorEnum.IsQuoteCanNotEditError.GetDescription();
                return httpResponseResultModel;
            }
            unitOfWork.RegisterDeleteBatch(idList);
            if (isCommit)
            {
                await CommitAsync();
            }
            httpResponseResultModel.IsSuccess = true;
            httpResponseResultModel.BackResult = true;
            return httpResponseResultModel;

        }


        #endregion


        #region 保存 有则更新 无则插入


        /// <summary>
        /// 保存实体，有则更新，无则新增
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> SaveAsync(GenerateCodeTemplateEntity entity, bool isCommit = true)
        {
            HttpResponseResultModel<bool> httpResponseResultModel = new HttpResponseResultModel<bool> { IsSuccess = false };
            var isExist = await queryGenerateCodeTemplateService.ExistsAsync(entity.Id).ConfigureAwait(false);
            if (isExist)
            {
                unitOfWork.RegisterUpdate(entity);
            }
            else
            {
                unitOfWork.RegisterInsert(entity);
            }
            if (isCommit)
            {
                await CommitAsync();
            }
            httpResponseResultModel.IsSuccess = true;
            return httpResponseResultModel;
        }




        /// <summary>
        ///  有则更新（增加），无则删除
        /// 1.entities中有， oldIdList没有的数据插入
        /// 2.oldIdList 和entities中有 都有的数据更新
        /// 3.oldIdList中有，entities中没有的数据删除
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entities">新数据</param>
        /// <param name="oldIdList">旧数据实体id</param>
        /// <returns></returns>
        public virtual async Task<HttpResponseResultModel<bool>> UpsertDeleteAsync(List<GenerateCodeTemplateEntity> entities, List<long> oldIdList, bool isCommit = true)
        {
            HttpResponseResultModel<bool> httpResponseResultModel = new HttpResponseResultModel<bool> { IsSuccess = false };
            var newEntityList = new List<IEntity<long>>();
            foreach (var entity in entities)
            {
                newEntityList.Add(entity);
            }
            unitOfWork.RegisterUpsertDelete(newEntityList, oldIdList);
            if (isCommit)
            {
                await CommitAsync();
            }
            httpResponseResultModel.BackResult = true;
            httpResponseResultModel.IsSuccess = true;
            return httpResponseResultModel;
        }
        #endregion

        #region 事务

        /// <summary>
        /// 事务
        /// </summary>
        /// <returns></returns>
        public async Task CommitAsync()
        {
            await unitOfWork.CommitAsync().ConfigureAwait(false);
        }
        #endregion


        /// <summary>
        /// 创建条码模板
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="detailList"></param>
        /// <param name="isCommit"></param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<long>> CreateTemplateAsync(GenerateCodeTemplateEntity entity, List<GenerateCodeTemplateDetailEntity> detailList, bool isCommit = true)
        {
            HttpResponseResultModel<long> httpResponseResultModel = new HttpResponseResultModel<long>() { IsSuccess = false };

            // 明细不能为空并且必须包含流水号配置项
            // Code 不能重复而且不能更新
            // 行号不能重复
            if (string.IsNullOrEmpty(entity.Code) || string.IsNullOrEmpty(entity.Name))
            {
                httpResponseResultModel.ErrorMessage = GenerateCodeTemplateErrorEnum.NameOrCodeRequire.GetDescription();
                return httpResponseResultModel;
            }
            if (detailList.Count != detailList.Select(x => new { RuleId = x.RuleId.Value, RuleValue = x.RuleValue }).Distinct().Count())
            {
                httpResponseResultModel.ErrorMessage = "模板明细不可重复";
                return httpResponseResultModel;
            }
            var oldTemplates = await queryGenerateCodeTemplateService.GetListAsync<GenerateCodeTemplateEntity>(new BaseGenerateCodeTemplateCondition()
            {
                Code = entity.Code
            });
            if (oldTemplates.Any())
            {
                httpResponseResultModel.ErrorMessage = GenerateCodeTemplateErrorEnum.CodeCanNotRepeat.GetDescription();
                return httpResponseResultModel;
            }
            if (detailList.IsListNullOrEmpty() || detailList.Exists(x => 
                        (x.RuleId.Value == CodeTemplateRuleEnum.SequenceNumber.ToLong()
                            || x.RuleId.Value== CodeTemplateRuleEnum.XiaoPengNumber.ToLong()
                        )) == false)
            {
                httpResponseResultModel.ErrorMessage = GenerateCodeTemplateErrorEnum.DetailMustHaveSerialNumber.GetDescription();
                return httpResponseResultModel;
            }
            if (detailList.Where(x => x.RuleId.Value == CodeTemplateRuleEnum.SequenceNumber.ToLong()
              || x.RuleId.Value == CodeTemplateRuleEnum.XiaoPengNumber.ToLong()).Count() > 1)
            {
                httpResponseResultModel.ErrorMessage = GenerateCodeTemplateErrorEnum.SerialNumberOnlyOne.GetDescription();
                return httpResponseResultModel;
            }
            if (detailList.Count != detailList.Select(x => x.RowNo.Value).Distinct().Count())
            {
                httpResponseResultModel.ErrorMessage = GenerateCodeTemplateErrorEnum.RowNoCanNotRepeat.GetDescription();
                return httpResponseResultModel;
            }
            if (detailList.Exists(x => x.RuleId.Value == CodeTemplateRuleEnum.FixValue.ToLong() && string.IsNullOrEmpty(x.RuleValue)))
            {
                httpResponseResultModel.ErrorMessage = "固定值不能为空";
                return httpResponseResultModel;
            }
            detailList.ForEach(x =>
            {
                x.TemplateId = entity.Id;

                // unitOfWork.RegisterInsert(x);
            });
            await commandGenerateCodeTemplateDetailService.InsertBatchAsync(detailList, false);

            unitOfWork.RegisterInsert(entity);
            if (isCommit)
            {
                await CommitAsync();
            }

            httpResponseResultModel.IsSuccess = true;
            httpResponseResultModel.BackResult = entity.Id;

            return httpResponseResultModel;
        }

        /// <summary>
        /// 更新条码模板
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="detailList"></param>
        /// <param name="isCommit"></param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> UpdateTemplateAsync(GenerateCodeTemplateEntity entity, List<GenerateCodeTemplateDetailEntity> detailList, bool isCommit = true)
        {
            // 明细不能为空并且必须包含流水号配置项
            // Code 不能重复而且不能更新
            // 行号不能重复
            // 如果已被使用则不能被更新
            HttpResponseResultModel<bool> httpResponseResultModel = new HttpResponseResultModel<bool>() { IsSuccess = false };
            List<long> idList = new List<long>()
            {entity.Id };
            if (detailList.Count != detailList.Select(x => new { RuleId = x.RuleId.Value, RuleValue = x.RuleValue }).Distinct().Count())
            {
                httpResponseResultModel.ErrorMessage = "模板明细不可重复";
                return httpResponseResultModel;
            }
            var isCustom = await queryGenerateCodeTemplateService.IsTemplateIsCustomAsync(idList).ConfigureAwait(false);
            if (isCustom)
            {
                httpResponseResultModel.IsSuccess = false;
                httpResponseResultModel.ErrorMessage = "系统内置模板不可修改";
                return httpResponseResultModel;
            }
            if (string.IsNullOrEmpty(entity.Code) || string.IsNullOrEmpty(entity.Name))
            {
                httpResponseResultModel.ErrorMessage = GenerateCodeTemplateErrorEnum.NameOrCodeRequire.GetDescription();
                return httpResponseResultModel;
            }
            var oldTemplate = await queryGenerateCodeTemplateService.GetAsync<GenerateCodeTemplateNoBTWEntity>(entity.Id);
            if (oldTemplate == null || oldTemplate.Id == 0)
            {
                httpResponseResultModel.ErrorMessage = GenerateCodeTemplateErrorEnum.TemplateIsNotExist.GetDescription();
                return httpResponseResultModel;
            }
            if (oldTemplate.IsQuote.Value)
            {
                httpResponseResultModel.ErrorMessage = "";//CommonErrorEnum.IsQuoteCanNotEditError.GetLocalizer();
                return httpResponseResultModel;
            }

            if (detailList.IsListNullOrEmpty() || detailList.Exists(x =>
                       x.RuleId.Value == CodeTemplateRuleEnum.SequenceNumber.ToLong()
                           || x.RuleId.Value == CodeTemplateRuleEnum.XiaoPengNumber.ToLong()
                       ) == false)
            {
                httpResponseResultModel.ErrorMessage = GenerateCodeTemplateErrorEnum.DetailMustHaveSerialNumber.GetDescription();
                return httpResponseResultModel;
            }
            if (detailList.Where(x => x.RuleId.Value == CodeTemplateRuleEnum.SequenceNumber.ToLong()
                           || x.RuleId.Value == CodeTemplateRuleEnum.XiaoPengNumber.ToLong()
            ).Count() > 1)
            {
                httpResponseResultModel.ErrorMessage = GenerateCodeTemplateErrorEnum.SerialNumberOnlyOne.GetDescription();
                return httpResponseResultModel;
            }
            if (detailList.Count != detailList.Select(x => x.RowNo.Value).Distinct().Count())
            {
                httpResponseResultModel.ErrorMessage = GenerateCodeTemplateErrorEnum.RowNoCanNotRepeat.GetDescription();
                return httpResponseResultModel;
            }
            if (detailList.Exists(x => x.RuleId.Value == CodeTemplateRuleEnum.FixValue.ToLong() && string.IsNullOrEmpty(x.RuleValue)))
            {
                httpResponseResultModel.ErrorMessage = "固定值不能为空";
                return httpResponseResultModel;
            }
            detailList.ForEach(x =>
            {
                x.TemplateId = entity.Id;
                if (x.Id == 0)
                {
                    x.Id = GeneratePrimaryKeyIdHelper.GetPrimaryKeyId();
                }
            });
            var oldDetail = await queryGenerateCodeTemplateDetailService.GetListAsync<GenerateCodeTemplateDetailEntity>
                (new BaseGenerateCodeTemplateDetailCondition()
                {
                    TemplateId = entity.Id
                });
            httpResponseResultModel = await commandGenerateCodeTemplateDetailService.UpsertDeleteAsync(detailList, oldDetail.Select(x => x.Id).ToList(), false);
            if (httpResponseResultModel.IsSuccess == false)
            {
                return httpResponseResultModel;
            }
            // BTW 不知道为啥 null 也会更新成为null
            if(entity.BTWFile==null)
            {
                var notBTWEntity = entity.MapTo<GenerateCodeTemplateNoBTWEntity>();
                unitOfWork.RegisterUpdate(notBTWEntity);
            }
            else
            {
                unitOfWork.RegisterUpdate(entity);
            }
           
            if (isCommit)
            {
                await CommitAsync();
            }
            return httpResponseResultModel;
        }

        /// <summary>
        /// 批量启用禁用
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="isEnabled"></param>
        /// <param name="isCommit"></param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> UpdateEnabledBatchAsync(List<long> ids, bool isEnabled, bool isCommit = true)
        {
            HttpResponseResultModel<bool> httpResponseResultModel = new HttpResponseResultModel<bool> { IsSuccess = false };
            foreach (var id in ids)
            {
                GenerateCodeTemplateEnableEntity updateEntity = new GenerateCodeTemplateEnableEntity();
                updateEntity.Id = id;
                updateEntity.IsEnabled = isEnabled;
                unitOfWork.RegisterUpdate(updateEntity);
            }
            if (isCommit)
            {
                await CommitAsync();
            }
            httpResponseResultModel.IsSuccess = true;
            httpResponseResultModel.BackResult = true;
            return httpResponseResultModel;
        }
    }
}
