﻿using CodePrintSystem.Core;
using CodePrintSystem.Basic.Abstractions;
using CodePrintSystem.Utility;
using CodePrintSystem.Utility.EnumClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodePrintSystem.EnumClass;
using CodePrintSystem.Basic.Abstractions.SerialNumber.Model;
using AutoMapper.Internal;

namespace CodePrintSystem.Basic
{
    internal class GenreateCodeServiceImpl : IGenerateCodeService, IAutoInject
    {
        private readonly IQueryGenerateCodeTemplateDetailService queryGenerateCodeTemplateDetailService;

        private readonly ICommandGenerateCodeHistoryService commandGenerateCodeHistoryService;

        private readonly IQueryGenerateCodeTemplateService queryGenerateCodeTemplateService;
        private readonly ICommandSerialNumberService commandSerialNumberService;
        private readonly IQuerySerialNumberService querySerialNumberService;
        private readonly CommonService commonService;
        public GenreateCodeServiceImpl(IQueryGenerateCodeTemplateDetailService queryGenerateCodeTemplateDetailService,
            ICommandGenerateCodeHistoryService commandGenerateCodeHistoryService,
            IQueryGenerateCodeTemplateService queryGenerateCodeTemplateService,
            ICommandSerialNumberService commandSerialNumberService,
            IQuerySerialNumberService querySerialNumberService,
            CommonService commonService)
        {
            this.queryGenerateCodeTemplateDetailService = queryGenerateCodeTemplateDetailService;
            this.commandGenerateCodeHistoryService = commandGenerateCodeHistoryService;
            this.queryGenerateCodeTemplateService = queryGenerateCodeTemplateService;
            this.commandSerialNumberService = commandSerialNumberService;
            this.querySerialNumberService = querySerialNumberService;
            this.commonService = commonService;
        }
        /// <summary>
        /// 根据模板批量生成条码
        /// </summary>
        /// <param name="BatchGenerateCodeAsync"></param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<List<BarCodeResultModel>>> BatchGenerateCodeAsync(GenerateCodeModel generateCodeModel, bool isCommit)
        {
            // 校验模板与填充值是否合法
            // 根据模板明细规则进行排序操作
            // 填充各规则对应的内容
            // 批量获取流水号
            HttpResponseResultModel<List<BarCodeResultModel>> httpResponseResultModel = new HttpResponseResultModel<List<BarCodeResultModel>>();
            httpResponseResultModel.BackResult = new List<BarCodeResultModel>();
            var template = await queryGenerateCodeTemplateService.GetAsync<GenerateCodeTemplateEntity>(generateCodeModel.TemplateId);
            var templateDetails = await queryGenerateCodeTemplateDetailService.GetListAsync<GenerateCodeTemplateDetailEntity>
                (new BaseGenerateCodeTemplateDetailCondition()
                {
                    TemplateId = generateCodeModel.TemplateId,
                    IsEnabled = true
                }, null, " rowNo asc ");
            if (generateCodeModel.Quantity <= 0)
            {
                httpResponseResultModel.ErrorMessage = GenerateCodeTemplateErrorEnum.QuantityIsError.GetDescription();
                return httpResponseResultModel;
            }
            if (templateDetails.IsListNullOrEmpty())
            {
                httpResponseResultModel.ErrorMessage = GenerateCodeTemplateErrorEnum.TemplateIsNotExist.GetDescription();
                return httpResponseResultModel;
            }
            if (generateCodeModel.GenerateCodeDetailModel.IsListNullOrEmpty())
            {
                httpResponseResultModel.ErrorMessage = GenerateCodeTemplateErrorEnum.DetailsIsNull.GetDescription();
                return httpResponseResultModel;
            }
            // 序列号占位符
            var serialNumberPlaceholder = "|" + Guid.NewGuid().ToString() + "|";
            string finalPrintCode = string.Empty;
            Dictionary<long, DetailInfo> dic = new Dictionary<long, DetailInfo>();
            for (var i = 0; i < templateDetails.Count; i++)
            {
                var detail = templateDetails[i];
                var codeRuleEnum = (CodeTemplateRuleEnum)detail.RuleId.Value;
                var dateTime = generateCodeModel.PrintDayTime;
                var inputValueModel = generateCodeModel.GenerateCodeDetailModel.Find(x => x.GenerateCodeTemplateDetailId == detail.Id);
                if (inputValueModel == null)
                {
                    continue;
                }
                //正常情况下，一个条码模板的同一个内容不会重复，所以可以循环内查询
                var partRuleCodeValue = await commonService.GetValueByTemplateRuleAsync(codeRuleEnum, dateTime, inputValueModel.InputValueOfTemplateDetail, detail.DictionaryTypeId.Value, serialNumberPlaceholder);
                if (string.IsNullOrEmpty(partRuleCodeValue))
                {
                    httpResponseResultModel.ErrorMessage = GenerateCodeTemplateErrorEnum.CodeValueIsEmpty.GetDescription();
                    return httpResponseResultModel;
                }
                if (detail.ValueLength > 0)
                {
                    //流水号长度插入了临时字符串肯定不相等
                    if (codeRuleEnum != CodeTemplateRuleEnum.SequenceNumber)
                    {
                        if (partRuleCodeValue.Length != detail.ValueLength)
                        {
                            httpResponseResultModel.ErrorMessage = $"{detail.AliasName}的设定长度为{detail.ValueLength},实际值为{partRuleCodeValue}，长度为{partRuleCodeValue.Length}";
                            return httpResponseResultModel;
                        }
                    }

                }
                if (detail.IsContain == true)
                {
                    finalPrintCode += partRuleCodeValue;
                }
                dic.Add(detail.Id, new DetailInfo { Name = detail.AliasName, ShowValue = partRuleCodeValue });
            }
            if (!finalPrintCode.Contains(serialNumberPlaceholder))
            {
                httpResponseResultModel.ErrorMessage = GenerateCodeTemplateErrorEnum.DetailMustHaveSerialNumber.GetDescription();
                return httpResponseResultModel;
            }
            var serialNumberGenerateModel = new SerialNumberGenerateModel
            {
                ItemCode = generateCodeModel.ItemCode,
                PrintCount = generateCodeModel.Quantity,
                PrintDay = generateCodeModel.PrintDayTime,
                SerialNumberLength = template.SerialNumberLength.HasValue ? template.SerialNumberLength.Value : 0
                ,
                SerialNumberType = GetSerialNumberType(templateDetails)
            };

            //计算实际的条码长度
            int printCodeLength = finalPrintCode.Length - serialNumberPlaceholder.Length + serialNumberGenerateModel.SerialNumberLength;
            if (template.CodeLength.Value > 0)
            {
                bool isCodeVaild = (printCodeLength == template.CodeLength.Value);
                if (!isCodeVaild)
                {
                    httpResponseResultModel.ErrorMessage = "打印的条码总长度和实际设置的条码模板长度不一致";
                    return httpResponseResultModel;
                }
            }
            var newSerialNumberInfo = await commandSerialNumberService.GetSerialNumberByItemAsync(serialNumberGenerateModel);
            if (!newSerialNumberInfo.IsSuccess)
            {
                httpResponseResultModel.ErrorMessage = newSerialNumberInfo.ErrorMessage;
                return httpResponseResultModel;
            }
            var newSerialNumberList = newSerialNumberInfo.BackResult.NewSerialNumbers;

            var generateCodeHistoryList = new List<GenerateCodeHistoryEntity>();


            for (var i = 0; i < generateCodeModel.Quantity; i++)
            {
                var code = finalPrintCode.Replace(serialNumberPlaceholder, newSerialNumberList[i]);

                dic = dic.ToDictionary(k => k.Key, k => new DetailInfo
                {
                    Name = k.Value.Name,
                    ShowValue = k.Value.ShowValue.Contains(serialNumberPlaceholder) ? k.Value.ShowValue.Replace(serialNumberPlaceholder, newSerialNumberList[i]) : k.Value.ShowValue
                });

                httpResponseResultModel.BackResult.Add(new BarCodeResultModel()
                {
                    BarCode = code,
                    SerialNumber = newSerialNumberList[i],
                    DataDetails = dic
                });
                generateCodeHistoryList.Add(new GenerateCodeHistoryEntity()
                {
                    Code = code,
                    TemplateId = generateCodeModel.TemplateId
                });
            }
            // 保存生成历史
            if (generateCodeHistoryList.Any())
            {
                await commandGenerateCodeHistoryService.InsertBatchAsync(generateCodeHistoryList, false);
            }
            if (isCommit)
            {
                await commandSerialNumberService.CommitAsync();
            }
            httpResponseResultModel.IsSuccess = true;
            return httpResponseResultModel;
        }


        private long GetSerialNumberType(List<GenerateCodeTemplateDetailEntity> details)
        {

            if (details.Exists(x => x.RuleId.Value == CodeTemplateRuleEnum.XiaoPengNumber.ToLong()))
            {
                return CodeTemplateRuleEnum.XiaoPengNumber.ToLong();
            }
            else
            {
                return CodeTemplateRuleEnum.SequenceNumber.ToLong();
            }


        }
    }
}
