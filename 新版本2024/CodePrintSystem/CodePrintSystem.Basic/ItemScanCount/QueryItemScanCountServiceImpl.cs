
//使用平台信息: ID:WinFormNoApi  描述:Winform无API
//代码版本信息: ID:None  描述:无 添加时间:2022/1/25 22:44:10
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Linq.Expressions;
using System;

using CodePrintSystem.Core;
using CodePrintSystem.Basic.Abstractions;
using CodePrintSystem.Utility;
namespace CodePrintSystem.Basic
{
    internal class QueryItemScanCountServiceImpl : IQueryItemScanCountService, IAutoInject
    {
        private readonly IQueryItemScanCountRepository queryRepository;

        public QueryItemScanCountServiceImpl(IQueryItemScanCountRepository queryRepository)
        {
            this.queryRepository = queryRepository;
        }

 

        /// <summary>
        /// 查询数据(分页) 返回指定实体T
        /// </summary>
        ///<typeparam name="T">返回实体类型</typeparam>
        /// <param name="page">页码</param>
        /// <param name="size">每页数量</param>
        /// <param name="condition">查询条件类</param>
        /// <param name="field">返回字段</param>
        /// <param name="orderBy">排序字段</param>
        /// <returns></returns>
        public async Task<QueryPagedResponseModel<T>> GetListPagedAsync<T>(int page, int size, BaseItemScanCountCondition condition=null, string field = null, string orderBy = null)
        {
            return await queryRepository.GetListPagedAsync<T>(page, size, condition, field, orderBy).ConfigureAwait(false);
        }


        


        /// <summary>
        /// 查询数据(不分页) 返回指定实体T
        /// </summary>
        ///<typeparam name="T">返回实体类型</typeparam>
        /// <param name="page">页码</param>
        /// <param name="size">每页数量</param>
        /// <param name="condition">查询条件类</param>
        /// <param name="field">返回字段</param>
        /// <param name="orderBy">排序字段</param>
        /// <returns></returns>
        public async Task<List<T>> GetListAsync<T>(BaseItemScanCountCondition  condition=null, string field = null, string orderBy = null)
        {
            return await queryRepository.GetListAsync<T>(condition, field, orderBy).ConfigureAwait(false);
        }

        /// <summary>
        /// 根据主键获取单个实体
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        public async Task<T> GetAsync<T>(long id)
        {
            return await queryRepository.GetAsync<T,long>(id).ConfigureAwait(false);
        }


		 /// <summary>
        /// 根据主键集合获取多个实体
        /// </summary>
        /// <param name="ids">主键集合</param>
        /// <returns></returns>
        public async Task<List<T>> GetListByIdsAsync<T>(List<long> ids)
        {
            return await queryRepository.GetListByIdsAsync<T,long>(ids).ConfigureAwait(false);
        }

		  /// <summary>
        /// 根据主键判断数据是否存在
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        public async Task<bool> ExistsAsync(long id)
        {
            return await queryRepository.ExistsAsync(id).ConfigureAwait(false);
        }


        /// <summary>
        /// 检查字段是否唯一
        /// </summary>
        ///<typeparam name="TColunmValue">数据库对应字段值类型long，int等</typeparam>
        /// <param name="column"></param>
        /// <returns></returns>
        private async Task<bool> IsUniqueAsync<TColunmValue>(KeyValuePair<string, TColunmValue> column)
        {
            return await queryRepository.IsUniqueAsync(column).ConfigureAwait(false);
        }


		
     
	  
    

                /// <summary>
        /// 检查物料编号是否唯一
        /// </summary>
        /// <param name="itemCode"></param>
        /// <returns></returns>
        public async Task<bool> IsItemCodeUniqueAsync(string itemCode)
        {
             bool isUnique = await  queryRepository.IsItemCodeUniqueAsync( itemCode);
            return isUnique;
        }
         
                  /// <summary>
        /// 根据物料编号获取单个实体
        /// </summary>
        /// <param name="itemCode">物料编号</param>
        /// <returns></returns>
        public async Task<T> GetSingleByItemCodeAsync<T>(string  itemCode)
        {
             return await queryRepository.GetSingleByItemCodeAsync<T>(itemCode).ConfigureAwait(false);
        }
        
                  

    }
}
