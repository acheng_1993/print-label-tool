
//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/1/30 14:08:58
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Linq.Expressions;
using System;
using CodePrintSystem.Core;
using CodePrintSystem.Basic.Abstractions;
using CodePrintSystem.Utility;
using CodePrintSystem.Basic.Abstractions.SerialNumber.Model;
using CodePrintSystem.EnumClass;

namespace CodePrintSystem.Basic
{
    internal class CommandSerialNumberServiceImpl : ICommandSerialNumberService, IAutoInject
    {
        private readonly IDefaultUnitOfWorkV2<long, IEntity<long>, ICommandSerialNumberRepository<long>> unitOfWork;
        private readonly IQuerySerialNumberService querySerialNumberService;
        private readonly IQueryGenerateCodeTemplateService queryGenerateCodeTemplateService;
        readonly ICommandSerialNumberRepository<long> commandSerialNumberRepository;
        private readonly static object obj = new object();

        public CommandSerialNumberServiceImpl(IDefaultUnitOfWorkV2<long, IEntity<long>, ICommandSerialNumberRepository<long>> unitOfWork,
            IQuerySerialNumberService querySerialNumberService,
             ICommandSerialNumberRepository<long> commandSerialNumberRepository,
            IQueryGenerateCodeTemplateService queryGenerateCodeTemplateService)
        {
            this.unitOfWork = unitOfWork;
            this.querySerialNumberService = querySerialNumberService;
            this.queryGenerateCodeTemplateService = queryGenerateCodeTemplateService;
            this.commandSerialNumberRepository = commandSerialNumberRepository;
        }

        #region 插入

        /// <summary>
        /// 插入单个实体
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<long>> InsertAsync(SerialNumberEntity entity, bool isCommit = true)
        {
            HttpResponseResultModel<long> httpResponseResultModel = new HttpResponseResultModel<long> { IsSuccess = false };
            unitOfWork.RegisterInsert(entity);
            if (isCommit)
            {
                await CommitAsync();
            }
            httpResponseResultModel.BackResult = entity.Id;
            httpResponseResultModel.IsSuccess = true;
            return httpResponseResultModel;
        }

        /// <summary>
        /// 批量插入实体
        /// </summary>
        /// <param name="entityList"></param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> InsertBatchAsync(List<SerialNumberEntity> entityList, bool isCommit = true)
        {
            HttpResponseResultModel<bool> httpResponseResultModel = new HttpResponseResultModel<bool> { IsSuccess = false };
            foreach (var entity in entityList)
            {
                unitOfWork.RegisterInsert(entity);
            }
            if (isCommit)
            {
                await CommitAsync();
            }
            httpResponseResultModel.BackResult = true;
            httpResponseResultModel.IsSuccess = true;
            return httpResponseResultModel;
        }


        #endregion

        #region 更新
        /// <summary>
        /// 根据主键更新实体
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> UpdateAsync(SerialNumberEntity entity, bool isCommit = true)
        {
            HttpResponseResultModel<bool> httpResponseResultModel = new HttpResponseResultModel<bool> { IsSuccess = false };
            unitOfWork.RegisterUpdate(entity);
            if (isCommit)
            {
                await CommitAsync();
            }
            httpResponseResultModel.IsSuccess = true;
            httpResponseResultModel.BackResult = true;
            return httpResponseResultModel;
        }

        public async Task<HttpResponseResultModel<bool>> UpdateAsync(UpdatePartSerialNumberEntity entity, bool isCommit = true)
        {
            HttpResponseResultModel<bool> httpResponseResultModel = new HttpResponseResultModel<bool> { IsSuccess = false };
            unitOfWork.RegisterUpdate(entity);
            if (isCommit)
            {
                await CommitAsync();
            }
            httpResponseResultModel.IsSuccess = true;
            httpResponseResultModel.BackResult = true;
            return httpResponseResultModel;
        }


        /// <summary>
        /// 批量更新实体
        /// </summary>
        /// <param name="entityList"></param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> UpdateBatchAsync(List<SerialNumberEntity> entityList, bool isCommit = true)
        {
            HttpResponseResultModel<bool> httpResponseResultModel = new HttpResponseResultModel<bool> { IsSuccess = false };
            foreach (var entity in entityList)
            {
                unitOfWork.RegisterUpdate(entity);
            }
            if (isCommit)
            {
                await CommitAsync();
            }
            httpResponseResultModel.IsSuccess = true;
            httpResponseResultModel.BackResult = true;
            return httpResponseResultModel;
        }








        #endregion

        #region 删除

        /// <summary>
        /// 根据根据主键删除
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> DeleteAsync(long id, bool isCommit = true)
        {
            HttpResponseResultModel<bool> httpResponseResultModel = new HttpResponseResultModel<bool> { IsSuccess = false };
            unitOfWork.RegisterDelete(id);
            if (isCommit)
            {
                await CommitAsync();
            }
            httpResponseResultModel.IsSuccess = true;
            httpResponseResultModel.BackResult = true;
            return httpResponseResultModel;
        }

        /// <summary>
        /// 批量删除 根据主键
        /// </summary>
        /// <param name="idList">主键集合</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> DeleteBatchAsync(IList<long> idList, bool isCommit = true)
        {
            HttpResponseResultModel<bool> httpResponseResultModel = new HttpResponseResultModel<bool> { IsSuccess = false };
            unitOfWork.RegisterDeleteBatch(idList);
            if (isCommit)
            {
                await CommitAsync();
            }
            httpResponseResultModel.IsSuccess = true;
            httpResponseResultModel.BackResult = true;
            return httpResponseResultModel;

        }


        #endregion


        #region 保存 有则更新 无则插入


        /// <summary>
        /// 保存实体，有则更新，无则新增
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> SaveAsync(SerialNumberEntity entity, bool isCommit = true)
        {
            HttpResponseResultModel<bool> httpResponseResultModel = new HttpResponseResultModel<bool> { IsSuccess = false };
            var isExist = await querySerialNumberService.ExistsAsync(entity.Id).ConfigureAwait(false);
            if (isExist)
            {
                unitOfWork.RegisterUpdate(entity);
            }
            else
            {
                unitOfWork.RegisterInsert(entity);
            }
            if (isCommit)
            {
                await CommitAsync();
            }
            httpResponseResultModel.IsSuccess = true;
            return httpResponseResultModel;
        }




        /// <summary>
        ///  有则更新（增加），无则删除
        /// 1.entities中有， oldIdList没有的数据插入
        /// 2.oldIdList 和entities中有 都有的数据更新
        /// 3.oldIdList中有，entities中没有的数据删除
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entities">新数据</param>
        /// <param name="oldIdList">旧数据实体id</param>
        /// <returns></returns>
        public virtual async Task<HttpResponseResultModel<bool>> UpsertDeleteAsync(List<SerialNumberEntity> entities, List<long> oldIdList, bool isCommit = true)
        {
            HttpResponseResultModel<bool> httpResponseResultModel = new HttpResponseResultModel<bool> { IsSuccess = false };
            var newEntityList = new List<IEntity<long>>();
            foreach (var entity in entities)
            {
                newEntityList.Add(entity);
            }
            unitOfWork.RegisterUpsertDelete(newEntityList, oldIdList);
            if (isCommit)
            {
                await CommitAsync();
            }
            httpResponseResultModel.BackResult = true;
            httpResponseResultModel.IsSuccess = true;
            return httpResponseResultModel;
        }
        #endregion

        #region 事务

        /// <summary>
        /// 事务
        /// </summary>
        /// <returns></returns>
        public async Task CommitAsync()
        {
            await unitOfWork.CommitAsync().ConfigureAwait(false);
        }
        #endregion


        /// <summary>
        /// 获取流水号
        /// </summary>
        /// <param name="serialNumberGenerateModel"></param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<GetSerialNumberResponseModel>> GetSerialNumberByItemAsync(SerialNumberGenerateModel serialNumberGenerateModel)
        {
            HttpResponseResultModel<GetSerialNumberResponseModel> result = new HttpResponseResultModel<GetSerialNumberResponseModel>() { IsSuccess = false };
            GetSerialNumberResponseModel responseModel = new GetSerialNumberResponseModel();
            responseModel.NewSerialNumbers = new List<string>();
            // 查询当前物料当天的流水号
            // 没有的话则从1开始并记录库中
            char postfixCompletion = '0';
            var currentSerialNumber = 0;
            var newSerialNumber = 0;
            var serialNumberLength = serialNumberGenerateModel.SerialNumberLength;
            if (serialNumberGenerateModel.PrintCount <= 0)
            {
                result.ErrorMessage = "打印次数不能小于0,生成流水失败，请重试！";
                return result;
            }
            if (string.IsNullOrEmpty(serialNumberGenerateModel.ItemCode))
            {
                result.ErrorMessage = "产品编码为空,生成流水失败，请重试！";
                return result;
            }
            if (serialNumberGenerateModel.SerialNumberLength <= 0)
            {
                result.ErrorMessage = "流水长度不能小于0,生成流水失败，请重试！";
                return result;
            }

            // 进来先锁表，防止重复打印
            var scope = commandSerialNumberRepository.BeginDBTransaction();
            using (scope)
            {
                await commandSerialNumberRepository.LockSerialNumberAsync(serialNumberGenerateModel.ItemCode, serialNumberGenerateModel.PrintDay);

                var oldSerialNumber = await querySerialNumberService.GetByItemCodeOfDayAsync<SerialNumberEntity>(serialNumberGenerateModel.ItemCode, serialNumberGenerateModel.PrintDay);
                if (oldSerialNumber == null || oldSerialNumber.Id == 0)
                {
                    var yearWeekDay = CommonHelper.GetYearWeekDayByTime(serialNumberGenerateModel.PrintDay);
                    oldSerialNumber = new SerialNumberEntity();
                    oldSerialNumber.ItemCode = serialNumberGenerateModel.ItemCode;
                    oldSerialNumber.Year = yearWeekDay.year.ToString();
                    oldSerialNumber.Week = yearWeekDay.weekOfYear.ToString();
                    oldSerialNumber.Day = yearWeekDay.dayOfWeek.ToString();
                    oldSerialNumber.CurrentDate = serialNumberGenerateModel.PrintDay.ToString("yyyy-MM-dd");
                    oldSerialNumber.SerialNumber = serialNumberGenerateModel.PrintCount;
                    var insertResult = await InsertAsync(oldSerialNumber, true);
                    if (!insertResult.IsSuccess)
                    {
                        result.ErrorMessage = "生成流水失败，请重试！";
                        return result;
                    }
                    newSerialNumber = serialNumberGenerateModel.PrintCount;
                }
                else
                {
                    currentSerialNumber = oldSerialNumber.SerialNumber.Value;
                    newSerialNumber = oldSerialNumber.SerialNumber.Value + serialNumberGenerateModel.PrintCount;
                    oldSerialNumber.SerialNumber = newSerialNumber;
                    UpdatePartSerialNumberEntity updatePartSerialNumberEntity = new UpdatePartSerialNumberEntity();
                    updatePartSerialNumberEntity.Id = oldSerialNumber.Id;
                    updatePartSerialNumberEntity.SerialNumber = newSerialNumber;
                    if (newSerialNumber.ToString().Length > serialNumberLength)
                    {
                        result.ErrorMessage = $"当前流水号 {newSerialNumber.ToString()},长度为 {newSerialNumber.ToString().Length},已经超出模板设置的长度 {serialNumberLength},生成流水失败,请重试!";//$"流水号长度小于本次生成的流水号，当前流水号{newSerialNumber.ToString()}，生成流水失败,请重试!";
                        return result;
                    }
                    // 定制流水号替换
                    // 小鹏流水号 4HK001  年月日流水号
                    if (serialNumberGenerateModel.SerialNumberType == CodeTemplateRuleEnum.XiaoPengNumber.ToLong())
                    {
                        // 超出当日最大流水
                        if (newSerialNumber > 5553)
                        {
                            result.ErrorMessage = $"当前流水号 {newSerialNumber.ToString()},大于小鹏流水号当日限制,生成流水失败,请重试!";//$"流水号长度小于本次生成的流水号，当前流水号{newSerialNumber.ToString()}，生成流水失败,请重试!";
                            return result;
                        }
                    }
                    // 继续计数
                    var updateResult = await UpdateAsync(updatePartSerialNumberEntity, true);
                    if (!updateResult.IsSuccess)
                    {
                        result.ErrorMessage = "生成流水失败，请重试！";
                        return result;
                    }
                }
                scope.Complete();
            }
           
            for (var i = currentSerialNumber; i < newSerialNumber; i++)
            {
                if (serialNumberGenerateModel.SerialNumberType == CodeTemplateRuleEnum.XiaoPengNumber.ToLong())
                {
                    responseModel.NewSerialNumbers.Add(GetXiaoPengSerialNumber(serialNumberGenerateModel.PrintDay,i+1));
                }
                else
                {
                    responseModel.NewSerialNumbers.Add((i + 1).ToString().PadLeft(serialNumberLength, postfixCompletion));
                }
            }
            result.BackResult = responseModel;
            result.IsSuccess = true;
            return result;

        }

        /// <summary>
        /// 获取小鹏批次流水号
        /// </summary>
        /// <param name="date"></param>
        /// <param name="number"></param>
        /// <returns></returns>
        private string GetXiaoPengSerialNumber(DateTime date,int number) {
            var batch = GetXiaoPengYear(date) + GetXiaoPengMonth(date) + GetXiaoPengDay(date);
            var numberCode = "";
            // 流水码要求：从001~999，A01~A99, B01~B99，C01~C99……，01A~99A, 01B~99B……等类推编流水码，
            // 同零件号及供应商不可有重复的批次代码，其中字母“I”、“O”、“Q”不得使用
            string[] codes = {"A","B","C","D","E","F","G","H"  ,"J","K","L","M","N","P","R","S","T","U","V","W","X","Y", "Z" };
            if (number <= 999)
            {
                numberCode = number.ToString().PadLeft(3, '0');
            }
            else if (number <= 3276)
            {
                int tempIndex = (number - 999) / 99;
                int tempNumber = (number - 999) % 99;
                int index = tempNumber == 0 ? tempIndex - 1 : tempIndex;
                var partNumber = (tempNumber==0?99:tempNumber).ToString().PadLeft(2,'0');
                numberCode = codes[index] + partNumber;
            }
            else if (number <= 5553)
            {
                int tempIndex = (number - 3276) / 99;
                int tempNumber = (number - 3276) % 99;
                int index = tempNumber == 0 ? tempIndex - 1 : tempIndex;
                var partNumber = (tempNumber == 0 ? 99 : tempNumber).ToString().PadLeft(2, '0');
                numberCode =   partNumber+ codes[index];
            }
            else {
                return "";
            }

            return batch + numberCode;
        }
        /// <summary>
        /// 获取小鹏年
        /// </summary>
        /// <returns></returns>
        private string GetXiaoPengYear(DateTime date) {
            string[] yearCode = {"1","2","3","4","5","6","7" ,"8","9"
                ,"A","B","C","D","E","F","G","H"  ,"J","K","L","M","N","P","R","S","T","V","W","X","Y"};
            var year = yearCode[date.Year - 2018];
            return year;

        }
        /// <summary>
        /// 获取小鹏月
        /// </summary>
        /// <returns></returns>
        private string GetXiaoPengMonth(DateTime date)
        {
            string[] monthCode = { "A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M" };
            var month = monthCode[date.Month-1];
            return month;
        }
        /// <summary>
        /// 获取小鹏日
        /// </summary>
        /// <returns></returns>
        private string GetXiaoPengDay(DateTime date)
        {
            string[] dayCode = {"1","2","3","4","5","6","7" ,"8","9"
                ,"A","B","C","D","E","F","G","H"  ,"J","K","L","M","N","P","R","S","T","U","V","W","X","Y"};
            var day = dayCode[date.Day - 1];
            return day;
        }
    }
}
