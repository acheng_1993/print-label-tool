
//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/1/30 14:16:16
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Linq.Expressions;
using System;
using CodePrintSystem.Core;
using CodePrintSystem.Basic.Abstractions;
using CodePrintSystem.Utility;
namespace CodePrintSystem.Basic
{
    internal class CommandUserServiceImpl : ICommandUserService, IAutoInject
    {
        private readonly IDefaultUnitOfWorkV2<long, IEntity<long>, ICommandUserRepository<long>> unitOfWork;
        private readonly IQueryUserService queryUserService;

        public CommandUserServiceImpl(IDefaultUnitOfWorkV2<long, IEntity<long>, ICommandUserRepository<long>> unitOfWork, IQueryUserService queryUserService)
        {
            this.unitOfWork = unitOfWork;
            this.queryUserService = queryUserService;
        }

        #region 插入

        /// <summary>
        /// 插入单个实体
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<long>> InsertAsync(UserEntity entity, bool isCommit = true)
        {
            HttpResponseResultModel<long> httpResponseResultModel = new HttpResponseResultModel<long> { IsSuccess = false };
            var isUserNameUnique = await queryUserService.IsUserNameUniqueAsync(entity.UserName).ConfigureAwait(false);
            if (!isUserNameUnique)
            {
                httpResponseResultModel.IsSuccess = false;
                httpResponseResultModel.ErrorMessage = "用户名不能重复";
                return httpResponseResultModel;
            }
            entity.IsAdmin = false;
            entity.UserPassword = EncryptDecryptHelper.Md5(entity.UserPassword);
            unitOfWork.RegisterInsert(entity);
            if (isCommit)
            {
                await CommitAsync();
            }
            httpResponseResultModel.BackResult = entity.Id;
            httpResponseResultModel.IsSuccess = true;
            return httpResponseResultModel;
        }

        /// <summary>
        /// 批量插入实体
        /// </summary>
        /// <param name="entityList"></param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> InsertBatchAsync(List<UserEntity> entityList, bool isCommit = true)
        {
            HttpResponseResultModel<bool> httpResponseResultModel = new HttpResponseResultModel<bool> { IsSuccess = false };
            foreach (var entity in entityList)
            {
                unitOfWork.RegisterInsert(entity);
            }
            if (isCommit)
            {
                await CommitAsync();
            }
            httpResponseResultModel.BackResult = true;
            httpResponseResultModel.IsSuccess = true;
            return httpResponseResultModel;
        }


        #endregion

        #region 更新
        /// <summary>
        /// 根据主键更新实体
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> UpdateAsync(UserEntity entity, bool isCommit = true)
        {
            HttpResponseResultModel<bool> httpResponseResultModel = new HttpResponseResultModel<bool> { IsSuccess = false };
            var oldEntity = await queryUserService.GetAsync<UserEntity>(entity.Id);
            if (oldEntity.IsAdmin == true && entity.IsEnabled == false)
            {
                httpResponseResultModel.ErrorMessage = "管理员账户不能禁用";
                return httpResponseResultModel;
            }
            if (string.IsNullOrEmpty(entity.UserPassword))
            {
                entity.UserPassword = oldEntity.UserPassword;
            }
            unitOfWork.RegisterUpdate(entity);
            if (isCommit)
            {
                await CommitAsync();
            }
            httpResponseResultModel.IsSuccess = true;
            httpResponseResultModel.BackResult = true;
            return httpResponseResultModel;
        }


        /// <summary>
        /// 批量更新实体
        /// </summary>
        /// <param name="entityList"></param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> UpdateBatchAsync(List<UserEntity> entityList, bool isCommit = true)
        {
            HttpResponseResultModel<bool> httpResponseResultModel = new HttpResponseResultModel<bool> { IsSuccess = false };
            foreach (var entity in entityList)
            {
                unitOfWork.RegisterUpdate(entity);
            }
            if (isCommit)
            {
                await CommitAsync();
            }
            httpResponseResultModel.IsSuccess = true;
            httpResponseResultModel.BackResult = true;
            return httpResponseResultModel;
        }








        #endregion

        #region 删除

        /// <summary>
        /// 根据根据主键删除
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> DeleteAsync(long id, bool isCommit = true)
        {
            HttpResponseResultModel<bool> httpResponseResultModel = new HttpResponseResultModel<bool> { IsSuccess = false };
            unitOfWork.RegisterDelete(id);
            if (isCommit)
            {
                await CommitAsync();
            }
            httpResponseResultModel.IsSuccess = true;
            httpResponseResultModel.BackResult = true;
            return httpResponseResultModel;
        }

        /// <summary>
        /// 批量删除 根据主键
        /// </summary>
        /// <param name="idList">主键集合</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> DeleteBatchAsync(IList<long> idList, bool isCommit = true)
        {
            HttpResponseResultModel<bool> httpResponseResultModel = new HttpResponseResultModel<bool> { IsSuccess = false };
            var users = await queryUserService.GetListByIdsAsync<UserEntity>(idList.ToList());
            if (users.Exists(x => x.IsAdmin == true))
            {
                httpResponseResultModel.ErrorMessage = "管理员账户不能删除";
                return httpResponseResultModel;
            }
            unitOfWork.RegisterDeleteBatch(idList);
            if (isCommit)
            {
                await CommitAsync();
            }
            httpResponseResultModel.IsSuccess = true;
            httpResponseResultModel.BackResult = true;
            return httpResponseResultModel;

        }


        #endregion


        #region 保存 有则更新 无则插入


        /// <summary>
        /// 保存实体，有则更新，无则新增
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> SaveAsync(UserEntity entity, bool isCommit = true)
        {
            HttpResponseResultModel<bool> httpResponseResultModel = new HttpResponseResultModel<bool> { IsSuccess = false };
            var isExist = await queryUserService.ExistsAsync(entity.Id).ConfigureAwait(false);
            if (isExist)
            {
                unitOfWork.RegisterUpdate(entity);
            }
            else
            {
                unitOfWork.RegisterInsert(entity);
            }
            if (isCommit)
            {
                await CommitAsync();
            }
            httpResponseResultModel.IsSuccess = true;
            return httpResponseResultModel;
        }




        /// <summary>
        ///  有则更新（增加），无则删除
        /// 1.entities中有， oldIdList没有的数据插入
        /// 2.oldIdList 和entities中有 都有的数据更新
        /// 3.oldIdList中有，entities中没有的数据删除
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entities">新数据</param>
        /// <param name="oldIdList">旧数据实体id</param>
        /// <returns></returns>
        public virtual async Task<HttpResponseResultModel<bool>> UpsertDeleteAsync(List<UserEntity> entities, List<long> oldIdList, bool isCommit = true)
        {
            HttpResponseResultModel<bool> httpResponseResultModel = new HttpResponseResultModel<bool> { IsSuccess = false };
            var newEntityList = new List<IEntity<long>>();
            foreach (var entity in entities)
            {
                newEntityList.Add(entity);
            }
            unitOfWork.RegisterUpsertDelete(newEntityList, oldIdList);
            if (isCommit)
            {
                await CommitAsync();
            }
            httpResponseResultModel.BackResult = true;
            httpResponseResultModel.IsSuccess = true;
            return httpResponseResultModel;
        }
        #endregion

        #region 事务

        /// <summary>
        /// 事务
        /// </summary>
        /// <returns></returns>
        public async Task CommitAsync()
        {
            await unitOfWork.CommitAsync().ConfigureAwait(false);
        }
        #endregion



        /// <summary>
        /// 用户授权解锁（需要管理员权限）
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="userPassword"></param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> UserUnlockAsync(string userName, string userPassword)
        {
            HttpResponseResultModel<bool> httpResponseResultModel = new HttpResponseResultModel<bool>() { IsSuccess = false };
            var users = await queryUserService.GetListAsync<UserEntity>(new BaseUserCondition()
            {
                UserName = userName,
                UserPassword = EncryptDecryptHelper.Md5(userPassword)
            });
            if (users.Any() == false)
            {
                httpResponseResultModel.ErrorMessage = "用户名或密码错误！";
                return httpResponseResultModel;
            }
            UserEntity user = users.FirstOrDefault();

            if (user.IsEnabled.Value == false)
            {
                httpResponseResultModel.ErrorMessage = "用户已禁用！";
                return httpResponseResultModel;
            }
            if (user.IsAdmin == false)
            {
                httpResponseResultModel.ErrorMessage = "用户非管理员不能解锁！";
                return httpResponseResultModel;
            }
            httpResponseResultModel.IsSuccess = true;
            httpResponseResultModel.BackResult = true;
            return httpResponseResultModel;
        }

        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="userEntity"></param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> LoginAsync(UserEntity userEntity)
        {
            HttpResponseResultModel<bool> httpResponseResultModel = new HttpResponseResultModel<bool>() { IsSuccess = false };
            var users = await queryUserService.GetListAsync<UserEntity>(new BaseUserCondition()
            {
                UserName = userEntity.UserName,
                UserPassword = EncryptDecryptHelper.Md5(userEntity.UserPassword)
            });
            if (users.Any() == false)
            {
                httpResponseResultModel.ErrorMessage = "用户名或密码错误！";
                return httpResponseResultModel;
            }
            UserEntity user = users.FirstOrDefault();
            // UserEntity user =null;

            if (user.IsEnabled.Value == false)
            {
                httpResponseResultModel.ErrorMessage = "用户已禁用！";
                return httpResponseResultModel;
            }
            CurrentUserConfig.User = user.MapTo<CurrentUser>();
            user.LastLoginTime = DateTime.Now;
            unitOfWork.RegisterUpdate(user);
            await CommitAsync();

            httpResponseResultModel.IsSuccess = true;
            httpResponseResultModel.BackResult = true;
            return httpResponseResultModel;
        }

        public async Task<HttpResponseResultModel<bool>> ChangePasswordAsync(ChangePwdModel changePwdModel)
        {
            HttpResponseResultModel<bool> httpResponseResultModel = new HttpResponseResultModel<bool>() { IsSuccess = false };
            var oldPwd = EncryptDecryptHelper.Md5(changePwdModel.OldPwd);
            var newPwd = EncryptDecryptHelper.Md5(changePwdModel.NewPwd);
            UserEntity user = await queryUserService.GetAsync<UserEntity>(CurrentUserConfig.User.Id);
            if (user == null || user.Id == 0)
            {
                httpResponseResultModel.ErrorMessage = "用户信息不存在！";
                return httpResponseResultModel;
            }
            if (user.UserPassword.Equals(oldPwd))
            {
                user.UserPassword = newPwd;

                unitOfWork.RegisterUpdate(user);
                await CommitAsync();
            }
            else
            {
                httpResponseResultModel.ErrorMessage = "原密码输入有误！";
                return httpResponseResultModel;
            }

            httpResponseResultModel.IsSuccess = true;
            httpResponseResultModel.BackResult = true;
            return httpResponseResultModel;
        }
        /// <summary>
        /// 重置密码
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="isCommit"></param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> BatchResetPwdAsync(List<long> ids, bool isCommit = true)
        {
            HttpResponseResultModel<bool> httpResponseResultModel = new HttpResponseResultModel<bool>();

            ids.ForEach(x =>
            {
                var user = new UserEntity()
                {
                    Id = x,
                    UserPassword = EncryptDecryptHelper.Md5("123456")
                };
                unitOfWork.RegisterUpdate(user);
            });

            if (isCommit)
            {
                await CommitAsync();
            }
            httpResponseResultModel.IsSuccess = true;
            httpResponseResultModel.BackResult = true;
            return httpResponseResultModel;
        }
    }
}
