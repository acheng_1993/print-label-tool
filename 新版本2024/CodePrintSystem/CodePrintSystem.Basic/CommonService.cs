﻿using CodePrintSystem.Basic.Abstractions;
using CodePrintSystem.Core;
using CodePrintSystem.EnumClass;
using CodePrintSystem.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodePrintSystem.Basic
{
    internal class CommonService: ISelfScopedAutoInject
    {

        private readonly IQuerySupplierService querySupplierService;
        private readonly IQueryItemInfoService queryItemInfoService;
        private readonly IQueryBusinessdictionaryService queryBusinessdictionaryService;
        readonly IQueryTimeCodeSetService timeCodeSetService;
        public CommonService(
            IQuerySupplierService querySupplierService,
            IQueryBusinessdictionaryService queryBusinessdictionaryService,
             IQueryItemInfoService queryItemInfoService,
             IQueryTimeCodeSetService timeCodeSetService)
        {
            this.querySupplierService = querySupplierService;
            this.queryItemInfoService = queryItemInfoService;
            this.queryBusinessdictionaryService = queryBusinessdictionaryService;
            this.timeCodeSetService = timeCodeSetService;
        }
        public async Task<string> GetValueByTemplateRuleAsync(CodeTemplateRuleEnum codeTemplateRule, DateTime dateTime, string inputValue, long dictionaryTypeId, string serialNumberPlaceholder)
        {
            // 序列号占位符
            var returnValue = "";
            switch (codeTemplateRule)
            {
                case CodeTemplateRuleEnum.SequenceNumber:
                    returnValue = serialNumberPlaceholder;
                    break;
                case CodeTemplateRuleEnum.XiaoPengNumber:
                    returnValue = serialNumberPlaceholder;
                    break;
                //case CodeTemplateRuleEnum.BusinessDictionaryCode:
                //    returnValue = await BusinessDictionaryHandleAsync(inputValue.ToLong(), dictionaryTypeId);
                //    break;
                case CodeTemplateRuleEnum.FixValue:
                case CodeTemplateRuleEnum.InputText:
                case CodeTemplateRuleEnum.Version:
                case CodeTemplateRuleEnum.SupplierCode:
                case CodeTemplateRuleEnum.RemarkCharacter:// 合晟的改成从配置文件读取
                case CodeTemplateRuleEnum.Config:
                    returnValue = inputValue;
                    break;
                case CodeTemplateRuleEnum.ItemCode:
                    returnValue = await ProductHandleAsync(inputValue.ToLong());
                    returnValue = returnValue.Replace("-", "");
                    break;
                case CodeTemplateRuleEnum.Number:
                case CodeTemplateRuleEnum.CustomType:
                case CodeTemplateRuleEnum.ClassShift:
                //case CodeTemplateRuleEnum.RemarkCharacter:
                    returnValue = await BusinessDictionaryHandleAsync(inputValue.ToLong());
                    break;
                case CodeTemplateRuleEnum.BYDTIME:
                    var byd_year = await timeCodeSetService.GetCodeByTimeAndType(dateTime, TimeCodeEnum.BYD_Year);
                    var byd_m = await timeCodeSetService.GetCodeByTimeAndType(dateTime, TimeCodeEnum.BYD_Month);
                    var byd_d = dateTime.Day.ToString().PadLeft(2, '0');
                    returnValue = $"{byd_year}{byd_m}{byd_d}";
                    break;
                case CodeTemplateRuleEnum.DayOfWeek:
                    if (string.IsNullOrWhiteSpace(inputValue))
                    {
                        returnValue = inputValue;
                    }
                    else
                    {
                        returnValue = CommonHelper.GetYearWeekDayByTime(dateTime).dayOfWeek.ToString();
                    }
                    break;
                case CodeTemplateRuleEnum.WeekOfYear:
                    if (string.IsNullOrWhiteSpace(inputValue))
                    {
                        returnValue = inputValue;
                    }
                    else
                    {
                        returnValue = CommonHelper.GetYearWeekDayByTime(dateTime).weekOfYear.ToString().PadLeft(2, '0');
                    }
                    break;

                case CodeTemplateRuleEnum.Time:
                    if (string.IsNullOrWhiteSpace(inputValue))
                    {
                        returnValue = inputValue;

                    }
                    else
                    {
                        var codeTimeRule = (CodeTimeRuleEnum)inputValue.ToLong();
                        returnValue = TimeHandle(dateTime, codeTimeRule);
                    }
                    break;
                default:
                    returnValue = await BusinessDictionaryHandleAsync(inputValue.ToLong());
                    break;
            }
            return returnValue;
        }


        /// <summary>
        /// 获取字典内容Code
        /// 返回空则字典内容有问题
        /// </summary>
        /// <param name="value"></param>
        /// <param name="templateValue"></param>
        /// <returns></returns>
        private async Task<string> BusinessDictionaryHandleAsync(long dictionaryId)
        {
            var dictionary = await queryBusinessdictionaryService.GetAsync<BusinessdictionaryEntity>(dictionaryId);
            return dictionary?.Code;
        }
        /// <summary>
        /// 获取产品编码，为空则产品编码有问题
        /// </summary>
        /// <returns></returns>
        private async Task<string> ProductHandleAsync(long value)
        {
            var product = await queryItemInfoService.GetAsync<ItemInfoEntity>(value);
            return product?.Code;
        }

        /// <summary>
        /// 获取供应商编码，为空则编码有问题
        /// </summary>
        /// <returns></returns>
        private async Task<string> SupplierHandleAsync(long supplierId)
        {
            var supplier = await querySupplierService.GetAsync<SupplierEntity>(supplierId);
            return supplier?.Code;
        }
        /// <summary>
        /// 获取时间处理
        /// </summary>
        /// <returns></returns>
        private string TimeHandle(DateTime dateTime, CodeTimeRuleEnum codeTimeRule)
        {
            switch (codeTimeRule)
            {
                case CodeTimeRuleEnum.Year_YYYY:
                    return CommonHelper.GetYearWeekDayByTime(dateTime).year.ToString();
                case CodeTimeRuleEnum.Year_YY:
                    return CommonHelper.GetYearWeekDayByTime(dateTime).year.ToString().Substring(2, 2);
                case CodeTimeRuleEnum.Month:
                    return dateTime.Month.ToString().PadLeft(2, '0');
                case CodeTimeRuleEnum.Day:
                    return dateTime.Day.ToString().PadLeft(2, '0');
                case CodeTimeRuleEnum.Hour:
                    return dateTime.Hour.ToString().PadLeft(2, '0')+ dateTime.Minute.ToString().PadLeft(2, '0')+ dateTime.Second.ToString().PadLeft(2, '0');
                case CodeTimeRuleEnum.Minute:
                    return dateTime.Minute.ToString().PadLeft(2, '0');
                case CodeTimeRuleEnum.Seconds:
                    return dateTime.Second.ToString().PadLeft(2, '0');
            }
            return "";
        }
    }


}

