
//使用平台信息: ID:WinFormNoApi  描述:Winform无API
//代码版本信息: ID:None  描述:无 添加时间:2022-10-13 21:02:23
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Linq.Expressions;
using System;

using CodePrintSystem.Core;
using CodePrintSystem.Basic.Abstractions;
using CodePrintSystem.Utility;
namespace CodePrintSystem.Basic
{
    internal class QueryTimeCodeSetServiceImpl : IQueryTimeCodeSetService, IAutoInject
    {
        private readonly IQueryTimeCodeSetRepository queryRepository;

        public QueryTimeCodeSetServiceImpl(IQueryTimeCodeSetRepository queryRepository)
        {
            this.queryRepository = queryRepository;
        }

 

        /// <summary>
        /// 查询数据(分页) 返回指定实体T
        /// </summary>
        ///<typeparam name="T">返回实体类型</typeparam>
        /// <param name="page">页码</param>
        /// <param name="size">每页数量</param>
        /// <param name="condition">查询条件类</param>
        /// <param name="field">返回字段</param>
        /// <param name="orderBy">排序字段</param>
        /// <returns></returns>
        public async Task<QueryPagedResponseModel<T>> GetListPagedAsync<T>(int page, int size, BaseTimeCodeSetCondition condition=null, string field = null, string orderBy = null)
        {
            return await queryRepository.GetListPagedAsync<T>(page, size, condition, field, orderBy).ConfigureAwait(false);
        }


        


        /// <summary>
        /// 查询数据(不分页) 返回指定实体T
        /// </summary>
        ///<typeparam name="T">返回实体类型</typeparam>
        /// <param name="page">页码</param>
        /// <param name="size">每页数量</param>
        /// <param name="condition">查询条件类</param>
        /// <param name="field">返回字段</param>
        /// <param name="orderBy">排序字段</param>
        /// <returns></returns>
        public async Task<List<T>> GetListAsync<T>(BaseTimeCodeSetCondition  condition=null, string field = null, string orderBy = null)
        {
            return await queryRepository.GetListAsync<T>(condition, field, orderBy).ConfigureAwait(false);
        }

        /// <summary>
        /// 根据主键获取单个实体
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        public async Task<T> GetAsync<T>(long id)
        {
            return await queryRepository.GetAsync<T,long>(id).ConfigureAwait(false);
        }


		 /// <summary>
        /// 根据主键集合获取多个实体
        /// </summary>
        /// <param name="ids">主键集合</param>
        /// <returns></returns>
        public async Task<List<T>> GetListByIdsAsync<T>(List<long> ids)
        {
            return await queryRepository.GetListByIdsAsync<T,long>(ids).ConfigureAwait(false);
        }

		  /// <summary>
        /// 根据主键判断数据是否存在
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        public async Task<bool> ExistsAsync(long id)
        {
            return await queryRepository.ExistsAsync(id).ConfigureAwait(false);
        }


        /// <summary>
        /// 检查字段是否唯一
        /// </summary>
        ///<typeparam name="TColunmValue">数据库对应字段值类型long，int等</typeparam>
        /// <param name="column"></param>
        /// <returns></returns>
        private async Task<bool> IsUniqueAsync<TColunmValue>(KeyValuePair<string, TColunmValue> column)
        {
            return await queryRepository.IsUniqueAsync(column).ConfigureAwait(false);
        }


        /// <summary>
        /// 获取时间代码
        /// </summary>
        /// <param name="time"></param>
        /// <param name="timeCodeEnum"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<string> GetCodeByTimeAndType(DateTime time, TimeCodeEnum timeCodeEnum)
        {
          var timeCodeData =await  GetAsync<TimeCodeSetEntity>(timeCodeEnum.ToInt());
            if(timeCodeData==null|| timeCodeData.Id == 0)
            {
                return "";
            }
            if (string.IsNullOrEmpty(timeCodeData.TimeCodes)) {
                return "";
            }
            var timeCodeList = timeCodeData.TimeCodes.Split(",").ToList();

            switch (timeCodeEnum)
            {
                case TimeCodeEnum.BYD_Year:
                    var i= (time.Year - timeCodeData.StartYear) % timeCodeList.Count;
                    if (i >= 0 && i < timeCodeList.Count) {
                        return timeCodeList[i];
                    }
                    break;
                case TimeCodeEnum.BYD_Month:
                    if (time.Month <= timeCodeList.Count)
                    {
                        return timeCodeList[time.Month-1];
                    }
                    break;
            }
                    return "";
        }
    }
}
