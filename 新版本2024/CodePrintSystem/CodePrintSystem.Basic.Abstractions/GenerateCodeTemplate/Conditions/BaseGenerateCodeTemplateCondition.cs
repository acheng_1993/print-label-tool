

//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/1/15 9:53:15
using System;
using System.Collections.Generic;
namespace CodePrintSystem.Basic.Abstractions
{
    /// <summary>
    /// 
    /// </summary>
    public class BaseGenerateCodeTemplateCondition
    {
            /// <summary>
        ///  编码生成模板名称
        /// </summary>
        public string Name {get;set;}
        
           /// <summary>
        ///  编码生成模板code
        /// </summary>
        public string Code {get;set;}
        
           /// <summary>
        ///  是否自定义 0否 1是
        /// </summary>
        public bool? IsCustom {get;set;}
        
           /// <summary>
        ///  是否可用 0 否 1是
        /// </summary>
        public bool? IsEnabled {get;set;}
        
           /// <summary>
        ///  是否已引用（已引用的不能修改或删除）
        /// </summary>
        public bool? IsQuote {get;set;}
        
      
        public string Keyword { get; set; }
    }
}
