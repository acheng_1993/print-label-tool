
//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/2/3 10:06:03
using System;
using CodePrintSystem.Core;
namespace CodePrintSystem.Basic.Abstractions
{
    /// <summary>
    /// 
    /// </summary>
    [MyTableName("CodeScanHistory")]
    [MyPrimaryKey("Id",AutoIncrement =false)]
    public class CodeScanHistoryEntity: BaseField,IEntity<long>
    {

       public  CodeScanHistoryEntity()
       {
                    Id = GeneratePrimaryKeyIdHelper.GetPrimaryKeyId();
                         }
       public long Id{get;set;}
            /// <summary>
        ///  编码
        /// </summary>
        public string Code {get;set;}
        
          /// <summary>
        ///  生产制令号
        /// </summary>
        public string ProductionCode {get;set;}
        
          /// <summary>
        ///  1：正常，2：重复，3：错误
        /// </summary>
        public int? Status {get;set;}
        
          /// <summary>
        ///  物料码
        /// </summary>
        public string ItemCode {get;set;}
        
          /// <summary>
        ///  错误内容
        /// </summary>
        public string ErrorContent {get;set;}
        
     

    }

   
}
