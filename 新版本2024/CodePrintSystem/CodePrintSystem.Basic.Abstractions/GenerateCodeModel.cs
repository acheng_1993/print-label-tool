﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodePrintSystem.Basic.Abstractions
{
    public class BarCodeResultModel { 
        public string BarCode { get; set; } 
        public string SerialNumber { set; get; }

        public Dictionary<long, DetailInfo> DataDetails { get; set; } = new Dictionary<long, DetailInfo>();
    
    }


    public class DetailInfo
    {
        public string Name { get; set; }

        public string ShowValue { get; set; }
    }
    public class GenerateCodeModel
    {
         
        /// <summary>
        /// 物料条码
        /// </summary>
        public string ItemCode { get; set; }


        /// <summary>
        /// 打印的哪一天
        /// </summary>
        public DateTime PrintDayTime { get; set; }

        /// <summary>
        /// 条码模板ID
        /// </summary>
        public long TemplateId { set; get; }

        /// <summary>
        /// 需要生成的条码数量
        /// </summary>
        public int Quantity { set; get; }



        /// <summary>
        /// 条码模板明细填充值
        /// </summary>
        public List<GenerateCodeDetailModel> GenerateCodeDetailModel { set; get; }
    }

    /// <summary>
    /// 生成条码明细值
    /// </summary>
    public class GenerateCodeDetailModel
    {
        /// <summary>
        /// 明细模板ID
        /// </summary>
        public long GenerateCodeTemplateDetailId { set; get; }
        /// <summary>
        ///  模板明细的输入的值（如下拉的id）
        /// </summary>
        public string InputValueOfTemplateDetail { set; get; }
         

    }
}
