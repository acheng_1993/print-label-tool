﻿using CodePrintSystem.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodePrintSystem.Basic.Abstractions
{
    public interface IGenerateCodeService
    {

        /// <summary>
        /// 根据模板与填充值批量生成条码
        /// </summary>
        /// <param name="generateCodeModel"></param>
        /// <param name="isCommit"></param>
        /// <returns></returns>
        Task<HttpResponseResultModel<List<BarCodeResultModel>>> BatchGenerateCodeAsync(GenerateCodeModel generateCodeModel,bool isCommit);

    }
}
