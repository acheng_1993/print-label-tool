

//使用平台信息: ID:WinFormNoApi  描述:Winform无API
//代码版本信息: ID:None  描述:无 添加时间:2022/1/25 22:44:10
using System;
using System.Collections.Generic;
namespace CodePrintSystem.Basic.Abstractions
{
    /// <summary>
    /// 
    /// </summary>
    public class BaseItemScanCountCondition
    {
        /// <summary>
        ///  物料编号
        /// </summary>
        public string ItemCode { get; set; }

        public long? UserId { get; set; }

    }
}
