

//使用平台信息: ID:WinFormNoApi  描述:Winform无API
//代码版本信息: ID:None  描述:无 添加时间:2022/1/25 22:44:10
using System.Collections.Generic;
using System.Threading.Tasks;
using CodePrintSystem.Core;
using System.Runtime.CompilerServices;
///限定只能由固定的程序集访问
/// 限定仓储接口只能由对应的服务程序集使用
[assembly: InternalsVisibleTo(assemblyName: "CodePrintSystem.Basic")]
[assembly: InternalsVisibleTo(assemblyName: "CodePrintSystem.Repository")]
namespace CodePrintSystem.Basic.Abstractions
{
    internal interface IQueryItemScanCountRepository : IQueryBaseRepository
    {
       
            /// <summary>
        /// 查询分页数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <param name="condition"></param>
        /// <param name="field"></param>
        /// <param name="orderBy"></param>
        /// <returns></returns>
        Task<QueryPagedResponseModel<T>> GetListPagedAsync<T>(int page, int size, BaseItemScanCountCondition condition = null, string field = null, string orderBy = null);


        /// <summary>
        /// 不分页数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="condition"></param>
        /// <param name="field"></param>
        /// <param name="orderBy"></param>
        /// <returns></returns>
        Task<List<T>> GetListAsync<T>(BaseItemScanCountCondition condition, string field = null, string orderBy = null);


            

                /// <summary>
        /// 检查物料编号是否唯一
        /// </summary>
        /// <param name="itemCode"></param>
        /// <returns></returns>
            Task<bool> IsItemCodeUniqueAsync(string itemCode);
         
         
                  /// <summary>
        /// 根据物料编号获取单个实体
        /// </summary>
        /// <param name="itemCode">物料编号</param>
        /// <returns></returns>
            Task<T> GetSingleByItemCodeAsync<T>(string  itemCode);
       
        
                }
}
