

//使用平台信息: ID:WinFormNoApi  描述:Winform无API
//代码版本信息: ID:None  描述:无 添加时间:2022/1/25 22:44:10
using System.Collections.Generic;
using System.Threading.Tasks;
using CodePrintSystem.Core;
using System.Runtime.CompilerServices;
///限定只能由固定的程序集访问
/// 限定仓储接口只能由对应的服务程序集使用
[assembly: InternalsVisibleTo(assemblyName: "CodePrintSystem.Basic")]
[assembly: InternalsVisibleTo(assemblyName: "CodePrintSystem.Repository")]
namespace CodePrintSystem.Basic.Abstractions
{
    internal interface ICommandItemScanCountRepository<TPrimaryKeyType> : ICommandBaseRepository<TPrimaryKeyType>
    {
       
          
    }
}
