
//使用平台信息: ID:WinFormNoApi  描述:Winform无API
//代码版本信息: ID:None  描述:无 添加时间:2022/1/25 22:44:10
using System;
using CodePrintSystem.Core;
namespace CodePrintSystem.Basic.Abstractions
{
    /// <summary>
    /// 
    /// </summary>
    [MyTableName("ItemScanCount")]
    [MyPrimaryKey("Id", AutoIncrement = false)]
    public class ItemScanCountEntity : BaseField, IEntity<long>
    {

        public ItemScanCountEntity()
        {
            Id = GeneratePrimaryKeyIdHelper.GetPrimaryKeyId();
        }
        public long Id { get; set; }
        /// <summary>
        ///  物料编号
        /// </summary>
        public string ItemCode { get; set; }

        /// <summary>
        ///  数量
        /// </summary>
        public int? Count { get; set; }

        /// <summary>
        /// 机器码
        /// </summary>
        public int? MachineId { get; set; }

        public long? UserId { get; set; }

    }


}
