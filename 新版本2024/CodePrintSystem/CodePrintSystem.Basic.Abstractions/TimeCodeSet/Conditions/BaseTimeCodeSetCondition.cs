

//使用平台信息: ID:WinFormNoApi  描述:Winform无API
//代码版本信息: ID:None  描述:无 添加时间:2022-10-13 21:02:23
using System;
using System.Collections.Generic;
namespace CodePrintSystem.Basic.Abstractions
{
    /// <summary>
    /// 
    /// </summary>
    public class BaseTimeCodeSetCondition
    {
            /// <summary>
        ///  
        /// </summary>
        public string Name {get;set;}
        
           /// <summary>
        ///  
        /// </summary>
        public string Code {get;set;}
        
           /// <summary>
        ///  
        /// </summary>
        public long? Type {get;set;}
        
           /// <summary>
        ///  
        /// </summary>
        public string TimeCodes {get;set;}
        
           /// <summary>
        ///  
        /// </summary>
        public string Attribute1 {get;set;}
        
           /// <summary>
        ///  
        /// </summary>
        public string Attribute2 {get;set;}
        
      

    }
}
