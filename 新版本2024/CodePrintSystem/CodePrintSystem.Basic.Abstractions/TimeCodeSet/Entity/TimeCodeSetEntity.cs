
//使用平台信息: ID:WinFormNoApi  描述:Winform无API
//代码版本信息: ID:None  描述:无 添加时间:2022-10-13 21:02:23
using System;
using CodePrintSystem.Core;
namespace CodePrintSystem.Basic.Abstractions
{
    /// <summary>
    /// 
    /// </summary>
    [MyTableName("timecodeset")]
    [MyPrimaryKey("Id", AutoIncrement = false)]
    public class TimeCodeSetEntity : BaseField, IEntity<long>
    {

        public TimeCodeSetEntity()
        {
            Id = GeneratePrimaryKeyIdHelper.GetPrimaryKeyId();
        }
        public long Id { get; set; }
        /// <summary>
        ///  名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        ///  编码
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        ///  编码代号
        /// </summary>
        public string TimeCodes { get; set; }

        /// <summary>
        ///  类型 1：比亚迪年份，2比亚迪月份
        /// </summary>
        public long? Type { get; set; }

        /// <summary>
        ///  扩展1
        /// </summary>
        public string Attribute1 { get; set; }

        /// <summary>
        ///  扩展2
        /// </summary>
        public string Attribute2 { get; set; }

        /// <summary>
        /// 开始年份
        /// </summary>
        public int StartYear { set; get; }


    }


}
