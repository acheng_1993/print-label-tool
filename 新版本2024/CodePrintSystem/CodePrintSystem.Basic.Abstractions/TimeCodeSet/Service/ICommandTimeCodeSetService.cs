
 
 //使用平台信息: ID:WinFormNoApi  描述:Winform无API
//代码版本信息: ID:None  描述:无 添加时间:2022-10-13 21:02:23
using System.Collections.Generic;
using System.Threading.Tasks;
using CodePrintSystem.Core;
using System;
namespace CodePrintSystem.Basic.Abstractions
{
    public interface ICommandTimeCodeSetService
    {
	  /// <summary>
        /// 插入单个实体
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<HttpResponseResultModel<long>> InsertAsync(TimeCodeSetEntity entity,bool isCommit = true);


        /// <summary>
        /// 批量插入实体
        /// </summary>
        /// <param name="entityList"></param>
        /// <returns></returns>
        Task<HttpResponseResultModel<bool>> InsertBatchAsync(List<TimeCodeSetEntity> entityList,bool isCommit = true);


        /// <summary>
        /// 根据主键更新实体
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<HttpResponseResultModel<bool>> UpdateAsync(TimeCodeSetEntity entity,bool isCommit = true);

        
        
        

        /// <summary>
        /// 根据主键批量更新实体
        /// </summary>
        /// <param name="entityList">实体集合</param>
        /// <returns></returns>
        Task<HttpResponseResultModel<bool>> UpdateBatchAsync(List<TimeCodeSetEntity> entityList,bool isCommit = true);

        /// <summary>
        /// 保存实体，有则更新，无则新增
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<HttpResponseResultModel<bool>> SaveAsync(TimeCodeSetEntity entity,bool isCommit = true);


      
        /// <summary>
        ///  有则更新（增加），无则删除
        /// 1.entities中有， oldIdList没有的数据插入
        /// 2.oldIdList 和entities中有 都有的数据更新
        /// 3.oldIdList中有，entities中没有的数据删除
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entities">新数据</param>
        /// <param name="oldIdList">旧数据实体id</param>
        /// <returns></returns>
        Task<HttpResponseResultModel<bool>> UpsertDeleteAsync(List<TimeCodeSetEntity> entities, List<long> oldIdList, bool isCommit = true);



        /// <summary>
        /// 根据根据主键删除
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        Task<HttpResponseResultModel<bool>> DeleteAsync(long id,bool isCommit = true);

        /// <summary>
        /// 批量删除 根据主键
        /// </summary>
        /// <param name="idList">主键集合</param>
        /// <returns></returns>
        Task<HttpResponseResultModel<bool>> DeleteBatchAsync(IList<long> idList,bool isCommit = true);
   
   }
}
