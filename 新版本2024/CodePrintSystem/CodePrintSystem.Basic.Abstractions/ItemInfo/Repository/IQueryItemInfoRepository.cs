

//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/1/30 14:06:32
using System.Collections.Generic;
using System.Threading.Tasks;
using CodePrintSystem.Core;

namespace CodePrintSystem.Basic.Abstractions
{
    public interface IQueryItemInfoRepository : IQueryBaseRepository
    {
       
            /// <summary>
        /// 查询分页数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <param name="condition"></param>
        /// <param name="field"></param>
        /// <param name="orderBy"></param>
        /// <returns></returns>
        Task<QueryPagedResponseModel<T>> GetListPagedAsync<T>(int page, int size, BaseItemInfoCondition condition = null, string field = null, string orderBy = null);


        /// <summary>
        /// 不分页数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="condition"></param>
        /// <param name="field"></param>
        /// <param name="orderBy"></param>
        /// <returns></returns>
        Task<List<T>> GetListAsync<T>(BaseItemInfoCondition condition, string field = null, string orderBy = null);


            

                /// <summary>
        /// 检查编码是否唯一
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
            Task<bool> IsCodeUniqueAsync(string code);
         
         
                  /// <summary>
        /// 根据编码获取单个实体
        /// </summary>
        /// <param name="code">编码</param>
        /// <returns></returns>
            Task<T> GetSingleByCodeAsync<T>(string  code);
       
        
            

        
            

        
                }
}
