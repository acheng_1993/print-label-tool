
//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/1/30 14:06:32
using System;
using CodePrintSystem.Core;
namespace CodePrintSystem.Basic.Abstractions
{
    /// <summary>
    /// 
    /// </summary>
    [MyTableName("iteminfo")]
    [MyPrimaryKey("Id", AutoIncrement = false)]
    public class UpdateTemplateItemInfoEntity : BaseField, IEntity<long>
    {
        public long Id { get; set; }
        public long? TemplateId { set; get; }
    }

        /// <summary>
        /// 
        /// </summary>
        [MyTableName("iteminfo")]
    [MyPrimaryKey("Id",AutoIncrement =false)]
    public class ItemInfoEntity: BaseField,IEntity<long>
    {

       public  ItemInfoEntity()
       {
                    Id = GeneratePrimaryKeyIdHelper.GetPrimaryKeyId();
                         }
       public long Id{get;set;}
            /// <summary>
        ///  名称
        /// </summary>
        public string Name {get;set;}

        /// <summary>
        ///  编码
        /// </summary>
        /// 
        [MyComputedColumn(ComputedColumnType = MyComputedColumnTypeEnum.ComputedOnUpdate)]
        public string Code {get;set;}
        
          /// <summary>
        ///  版本
        /// </summary>
        public string VersionCode {get;set;}
        
          /// <summary>
        ///  扩展属性1
        /// </summary>
        public string Attribute1 {get;set;}
        
          /// <summary>
        ///  扩展属性2
        /// </summary>
        public string Attribute2 {get;set;}
        
          /// <summary>
        ///  扩展属性3
        /// </summary>
        public string Attribute3 {get;set;}
        
          /// <summary>
        ///  扩展属性4
        /// </summary>
        public string Attribute4 {get;set;}

        public long? TemplateId { set; get; }
     

    }

   
}
