﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodePrintSystem.Basic.Abstractions.SerialNumber.Model
{
   public class SerialNumberGenerateModel
    {
        /// <summary>
        /// 物料条码
        /// </summary>
        public string ItemCode { set; get; }

        /// <summary>
        ///  打印时间
        /// </summary>
        public DateTime PrintDay { set; get; }
        /// <summary>
        /// 流水数量
        /// </summary>
        public int PrintCount { set; get; }

        /// <summary>
        /// 流水号长度
        /// </summary>
        public int SerialNumberLength { set; get; }

        /// <summary>
        /// 流水号类型
        /// </summary>
        public long SerialNumberType { set; get; }
    }
}
