﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodePrintSystem.Basic.Abstractions.SerialNumber.Model
{
   public class GetSerialNumberResponseModel
    {
        /// <summary>
        ///  新增的流水号
        /// </summary>
        public List<string> NewSerialNumbers { get; set; }

        /// <summary>
        ///  最新流水信息
        /// </summary>
     //   public SerialNumberEntity NewSerialNumberInfo { get; set; }
    }
}
