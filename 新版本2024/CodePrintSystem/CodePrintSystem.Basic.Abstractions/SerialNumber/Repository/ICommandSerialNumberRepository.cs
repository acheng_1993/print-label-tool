

//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/1/30 14:08:58
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CodePrintSystem.Core;


namespace CodePrintSystem.Basic.Abstractions
{
    public interface ICommandSerialNumberRepository<TPrimaryKeyType> : ICommandBaseRepository<TPrimaryKeyType>
    {
         Task<bool> LockSerialNumberAsync(string itemCode, DateTime printDayTime);
          
    }

  
}
