

//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/1/30 14:08:58
using System;
using System.Collections.Generic;
namespace CodePrintSystem.Basic.Abstractions
{
    /// <summary>
    /// 
    /// </summary>
    public class BaseSerialNumberCondition
    {
            /// <summary>
        ///  物料码
        /// </summary>
        public string ItemCode {get;set;}
        
           /// <summary>
        ///  当前日期
        /// </summary>
        public string CurrentDate {get;set;}
        
           /// <summary>
        ///  年
        /// </summary>
        public string Year {get;set;}
        
           /// <summary>
        ///  周
        /// </summary>
        public string Week {get;set;}
        
           /// <summary>
        ///  天
        /// </summary>
        public string Day {get;set;}
        
           /// <summary>
        ///  流水号
        /// </summary>
        public int? SerialNumber {get;set;}
        
      

    }
}
