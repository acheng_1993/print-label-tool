
//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/1/30 14:09:27
using System;
using CodePrintSystem.Core;
namespace CodePrintSystem.Basic.Abstractions
{
    /// <summary>
    /// 
    /// </summary>
    [MyTableName("supplier")]
    [MyPrimaryKey("Id", AutoIncrement = false)]
    public class SupplierEntity : BaseField, IEntity<long>
    {

        public SupplierEntity()
        {
            Id = GeneratePrimaryKeyIdHelper.GetPrimaryKeyId();
        }
        public long Id { get; set; }
        /// <summary>
        ///  供应商代码
        /// </summary>
        /// 
        [MyComputedColumn(ComputedColumnType = MyComputedColumnTypeEnum.ComputedOnUpdate)]
        public string Code { get; set; }

        /// <summary>
        ///  名称
        /// </summary>
        public string Name { get; set; }



    }


}
