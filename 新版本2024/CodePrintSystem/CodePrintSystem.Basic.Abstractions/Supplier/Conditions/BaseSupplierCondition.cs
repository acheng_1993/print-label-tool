

//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/1/30 14:09:27
using System;
using System.Collections.Generic;
namespace CodePrintSystem.Basic.Abstractions
{
    /// <summary>
    /// 
    /// </summary>
    public class BaseSupplierCondition
    {
        public string KeyWord { set; get; }
            /// <summary>
        ///  供应商代码
        /// </summary>
        public string Code {get;set;}
        
           /// <summary>
        ///  名称
        /// </summary>
        public string Name {get;set;}
        
      

    }
}
