

//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/11/8 23:23:17
using System.Collections.Generic;
using System.Threading.Tasks;
using CodePrintSystem.Core;
using System.Runtime.CompilerServices;
using System;
///限定只能由固定的程序集访问
/// 限定仓储接口只能由对应的服务程序集使用
[assembly: InternalsVisibleTo(assemblyName: "CodePrintSystem.Basic")]
[assembly: InternalsVisibleTo(assemblyName: "CodePrintSystem.Repository")]
namespace CodePrintSystem.Basic.Abstractions
{
    internal interface IQueryPrintPlanRepository : IQueryBaseRepository
    {

        Task<List<PrintPlanBoard>> GetBoardAsync(DateTime dateTime);

       
            /// <summary>
        /// 查询分页数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <param name="condition"></param>
        /// <param name="field"></param>
        /// <param name="orderBy"></param>
        /// <returns></returns>
        Task<QueryPagedResponseModel<T>> GetListPagedAsync<T>(int page, int size, BasePrintPlanCondition condition = null, string field = null, string orderBy = null);


        /// <summary>
        /// 不分页数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="condition"></param>
        /// <param name="field"></param>
        /// <param name="orderBy"></param>
        /// <returns></returns>
        Task<List<T>> GetListAsync<T>(BasePrintPlanCondition condition, string field = null, string orderBy = null);


            

        
            

        
                }
}
