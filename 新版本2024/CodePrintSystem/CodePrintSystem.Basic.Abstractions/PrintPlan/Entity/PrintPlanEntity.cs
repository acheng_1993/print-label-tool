
//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/11/8 23:23:17
using System;
using CodePrintSystem.Core;
namespace CodePrintSystem.Basic.Abstractions
{
    public class PrintBoardCodeModel { 
    public string ItemCode { set; get; }
        public int PlanQty { set; get; }
    }
    public class PrintBoardModel
    {
        public string ItemCode { set; get; }
        public int Status { set; get; }

        public int Qty { set; get; }
    }

    public class PrintPlanBoard : BaseField, IEntity<int>
    {

        public PrintPlanBoard()
        {
        }

       
        public int Id { get; set; }
        /// <summary>
        ///  料号
        /// </summary>
        public string ItemCode { get; set; }

        /// <summary>
        ///  计划日期
        /// </summary>
        public DateTime? PlanDate { get; set; }

        /// <summary>
        ///  计划数量
        /// </summary>
        public int? PlanQty { get; set; }


        public int ProductQty { set; get; }
        public int NoProductQty { set; get; }

        public int Percent { set; get; }

        public int ScanOk { set; get; }

        public int ScanNg { set; get; }

        public int ScanRepeat { set; get; }

    }

    /// <summary>
    /// 
    /// </summary>
    [MyTableName("PrintPlan")]
    [MyPrimaryKey("Id",AutoIncrement =false)]
    public class PrintPlanEntity: BaseField,IEntity<long>
    {

       public  PrintPlanEntity()
       {
                         }

        [MyResultColumn]
        public bool IsCheck { set; get; } = false;
        public long Id{get;set;}
            /// <summary>
        ///  料号
        /// </summary>
                  public string ItemCode {get;set;}
        
          /// <summary>
        ///  计划日期
        /// </summary>
                  public DateTime? PlanDate {get;set;}
        
          /// <summary>
        ///  计划数量
        /// </summary>
                  public int? PlanQty {get;set;}
        
     

    }

   
}
