
//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/1/30 14:16:16
using System;
using CodePrintSystem.Core;
namespace CodePrintSystem.Basic.Abstractions
{

    /// <summary>
    /// 修改密码model
    /// </summary>
    public class ChangePwdModel
    {
        public long Id { get; set; }
        public string OldPwd { set; get; }

        public string NewPwd { set; get; }
    }


    /// <summary>
    /// 
    /// </summary>
    [MyTableName("[user]")]
    [MyPrimaryKey("Id", AutoIncrement = false)]
    public class UserEntity : BaseField, IEntity<long>
    {

        public UserEntity()
        {
            Id = GeneratePrimaryKeyIdHelper.GetPrimaryKeyId();
        }
        public long Id { get; set; }
        /// <summary>
        ///  是否管理员
        /// </summary>
        public bool? IsAdmin { get; set; }

        /// <summary>
        ///  用户名
        /// </summary>
        /// 
        [MyComputedColumn(ComputedColumnType = MyComputedColumnTypeEnum.ComputedOnUpdate)]
        public string UserName { get; set; }

        /// <summary>
        ///  用户密码
        /// </summary>
        public string UserPassword { get; set; }

        /// <summary>
        ///  最近登录时间
        /// </summary>
        public DateTime? LastLoginTime { get; set; }

        /// <summary>
        ///  是否启用
        /// </summary>
        public bool? IsEnabled { get; set; }

        /// <summary>
        /// 指纹密码
        /// </summary>
        public string FingePrintPassword { get; set; }

        /// <summary>
        /// 指纹密码md5加密
        /// </summary>
        public string FingePrintPasswordMD5 { get; set; }
        /// <summary>
        /// 指纹密码状态
        /// </summary>
        public bool IsFingePrint { get; set; }
    }


}
