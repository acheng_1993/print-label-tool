

//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/1/15 9:53:44
using System;
using System.Collections.Generic;
namespace CodePrintSystem.Basic.Abstractions
{
    /// <summary>
    /// 
    /// </summary>
    public class BaseGenerateCodeTemplateDetailCondition
    {
        /// <summary>
        ///  编码生成规则模板ids
        /// </summary>
        public List<long> TemplateIds { get; set; }
        /// <summary>
        ///  编码生成规则模板id
        /// </summary>
        public long? TemplateId {get;set;}
        
           /// <summary>
        ///  行号（不可重复）
        /// </summary>
        public int? RowNo {get;set;}
        
           /// <summary>
        ///  条码规则id
        /// </summary>
        public long? RuleId {get;set;}
        
           /// <summary>
        ///  条码规则值
        /// </summary>
        public string RuleValue {get;set;}
        
           /// <summary>
        ///  是否自定义 0否 1是
        /// </summary>
        public bool? IsCustom {get;set;}
        
           /// <summary>
        ///  是否可用 0 否 1是
        /// </summary>
        public bool? IsEnabled {get;set;}
        
           /// <summary>
        ///  是否已引用（已引用的不能修改或删除）
        /// </summary>
        public bool? IsQuote {get;set;}

        /// <summary>
        ///   是否包含在条码中
        /// </summary>
        public bool? IsContain { get; set; }

    }
}
