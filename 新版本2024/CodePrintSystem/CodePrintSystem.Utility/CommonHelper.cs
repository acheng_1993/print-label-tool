﻿using CodePrintSystem.Core;
using CodePrintSystem.EnumClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodePrintSystem.Utility
{
    public class CommonHelper
    {
        public static long GetTypeId(long ruleTypeId) {
            if (ruleTypeId == CodeTemplateRuleEnum.ClassShift.ToLong()) {
                return 4;
            }
            if (ruleTypeId == CodeTemplateRuleEnum.Number.ToLong())
            {
                return 1;
            }
            if (ruleTypeId == CodeTemplateRuleEnum.CustomType.ToLong())
            {
                return 2;
            }
            if (ruleTypeId == CodeTemplateRuleEnum.RemarkCharacter.ToLong())
            {
                return 3;
            }
            if (ruleTypeId == CodeTemplateRuleEnum.Time.ToLong())
            {
                return 6;
            }
            if (ruleTypeId == CodeTemplateRuleEnum.Version.ToLong())
            {
                return 7;
            }
            if (ruleTypeId == CodeTemplateRuleEnum.Config.ToLong())
            {
                return 11;
            }
            return 0;
        }
        public static string GetWeek(DayOfWeek dayOfWeek)
        {
            switch (dayOfWeek)
            {
                case DayOfWeek.Monday:
                    return "星期一";
                case DayOfWeek.Tuesday:
                    return "星期二";
                case DayOfWeek.Wednesday:
                    return "星期三";

                case DayOfWeek.Thursday:
                    return "星期四";
                case DayOfWeek.Friday:
                    return "星期五";
                case DayOfWeek.Saturday:
                    return "星期六";
                default:
                    return "星期日";
            }
        }
        /// <summary>
        /// 检查itemCode
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public static string CheckItemCode(string code)
        {
            if (code.Length != AppsettingsConfig.ItemCodeLength)
            {
                return $"产品编码长度必须为{AppsettingsConfig.ItemCodeLength}！";
            }
            if (code.IndexOf('-') != 6 || code.LastIndexOf('-') != 6)
            {
                return "产品编码包含的 - 位置必须是第七位！";
            }

            return "";
        }
        /// <summary>
        /// 获取周次 结果 年、周次、周天,年为4位
        /// </summary>
        /// <param name="curDay"></param>
        /// <returns></returns>
        public static (int  year,int weekOfYear,int dayOfWeek) GetYearWeekDayByTime(DateTime curDay)
        {

            int firstdayofweek = Convert.ToInt32(Convert.ToDateTime(curDay.Year.ToString() + "- " + "1-1 ").DayOfWeek);

            int days = curDay.DayOfYear;
            int daysOutOneWeek = days - (7 - firstdayofweek);

            if (daysOutOneWeek <= 0)
            {
                return (curDay.Year,1,(int)curDay.DayOfWeek);
            }
            else
            {
                int weeks = daysOutOneWeek / 7;
                if (daysOutOneWeek % 7 != 0)
                    weeks++;
                int week = weeks + 1;
                //最后一周算下一年
                //if (week > 53)
                //{
                //    return (curDay.Year+1, 1, (int)curDay.DayOfWeek);
                //}
                return (curDay.Year , week, (int)curDay.DayOfWeek);
            }
        }
    }
}
