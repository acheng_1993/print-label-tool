﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodePrintSystem.Utility
{
    /// <summary>
    /// 菜单按钮枚举
    /// </summary>
    public enum SystemMenuEnum
    {
        CodePrint = 0,
        ItemSet = 1,
        SupplierSet = 2,
        BaseDataSet = 3,
        PasswordChange = 4,
        OperateDoc = 5,
        ItemTemplateDoc = 6,
        ExitSystem = 7,
        CodeTemplate = 8,
        UserSet = 9,
        PrintLog = 10,
        /// <summary>
        /// 扫描
        /// </summary>
        CodeScan = 11,
        DownloadItemTemplate = 12,
        /// <summary>
        /// 计划维护
        /// </summary>
        PlanSet=13,
        PersonSet=14,
    }
}
