﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodePrintSystem.Utility
{
    /// <summary>
    /// 时间代码类型枚举
    /// </summary>
    public enum TimeCodeEnum
    {
        /// <summary>
        /// 比亚迪年份
        /// </summary>
        BYD_Year=1,
        /// <summary>
        /// 比亚迪月份
        /// </summary>
        BYD_Month=2,
    }
}
