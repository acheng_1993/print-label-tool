﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodePrintSystem.EnumClass
{
    /// <summary>
    /// 条码模板规则枚举
    /// </summary>
    public enum CodeTemplateRuleEnum
    {

        /// <summary>
        /// 产品编码
        /// </summary>
        ItemCode = 1,
        /// <summary>
        /// 供应商编码
        /// </summary>
        SupplierCode = 2,
        ///// <summary>
        ///// 字典编码
        ///// </summary>
        //BusinessDictionaryCode = 3,
        /// <summary>
        /// 流水号
        /// </summary>
        SequenceNumber = 4,
        /// <summary>
        /// 小鹏流水号
        /// </summary>
        XiaoPengNumber = 28,
        /// <summary>
        /// 固定值
        /// </summary>
        FixValue = 5,
        /// <summary>
        /// 输入值
        /// </summary>
        InputText = 6,
        /// <summary>
        /// 时间
        /// </summary>
        Time = 7,
        /// <summary>
        /// 星期日期 一周的第几天（0-6） 0 代表星期天
        /// </summary>
        DayOfWeek = 14,
        /// <summary>
        ///   周次 一年中的第几周，最大52周，如果53周，计入下一年
        /// </summary>
        WeekOfYear = 15,
        /// <summary>
        /// 数字
        /// </summary>
        [Description("数字")]
        Number = 21,
        /// <summary>
        /// 类型
        /// </summary>
        [Description("类型")]
        CustomType = 22,
        /// <summary>
        /// 备用字符
        /// </summary>
        [Description("备用字符")]
        RemarkCharacter = 23,
        /// <summary>
        /// 班次
        /// </summary>
        [Description("班次")]
        ClassShift = 24,
        /// <summary>
        /// 版本
        /// </summary>
        [Description("版本")]
        Version=25,
        [Description("配置")]
        Config = 35,
        [Description("比亚迪年月")]
        BYDTIME = 40,

    }
    /// <summary>
    /// 条码时间枚举
    /// </summary>
    public enum CodeTimeRuleEnum
    {
        /// <summary>
        /// 时间 YYYY
        /// </summary>
        [Description("年份YYYY")]
        Year_YYYY = 8,
        /// <summary>
        /// 年份
        /// </summary>
        [Description("年份YY")]
        Year_YY = 20,
        /// <summary>
        /// 月 MM 
        /// </summary>
        [Description("月份")]
        Month = 9,
        /// <summary>
        /// DD 天
        /// </summary>
        [Description("日(DD)")]
        Day = 10,
        /// <summary>
        /// HH 小时
        /// </summary>
        [Description("小时(HH)")]
        Hour = 11,
        /// <summary>
        /// mm 分钟
        /// </summary>
        [Description("分钟(mm)")]
        Minute = 12,
        /// <summary>
        /// ss 秒
        /// </summary>
        [Description("秒(ss)")]
        Seconds = 13
    }
}
