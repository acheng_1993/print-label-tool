﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodePrintSystem.Utility
{
    /// <summary>
    /// 扫码状态枚举
    /// </summary>
    public enum ScanStatusEnum
    {
        /// <summary>
        /// 通过
        /// </summary>
        [Description("扫码通过")]
        Pass=1,
        /// <summary>
        /// 扫码重复
        /// </summary>
        [Description("扫码重复")]
        Repeat =2,
        /// <summary>
        /// 错误
        /// </summary>
        [Description("条码错误")]
        Error =3,
    }
}
