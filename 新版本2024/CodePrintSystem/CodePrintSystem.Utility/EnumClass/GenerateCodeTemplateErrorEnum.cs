﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodePrintSystem.Utility
{
    /// <summary>
    /// 生成条码模板错误枚举
    /// </summary>
    public enum GenerateCodeTemplateErrorEnum
    {
        /// <summary>
        /// 模板不存在
        /// </summary>
        [Description("模板不存在")]
        TemplateIsNotExist,
        /// <summary>
        /// 名称和编码不能为空
        /// </summary>
        [Description("名称和编码不能为空")]
        NameOrCodeRequire,
        /// <summary>
        /// 明细必须包含流水号
        /// </summary>
        [Description("明细必须包含流水号")]
        DetailMustHaveSerialNumber,
        /// <summary>
        /// 流水号只能存在一个
        /// </summary>
        [Description("流水号只能存在一个")]
        SerialNumberOnlyOne,
        /// <summary>
        /// 模板明细为空，请检查
        /// </summary>
        [Description("模板明细为空")]
        DetailsIsNull,

        /// <summary>
        /// 编码不能重复
        /// </summary>
        [Description("编码不能重复")]
        CodeCanNotRepeat,
        /// <summary>
        /// 行号不能重复
        /// </summary>
        [Description("序号不能重复")]
        RowNoCanNotRepeat,
        /// <summary>
        /// 固定值不能为空
        /// </summary>
        [Description("固定值不能为空")]
        FixValueCanNotNull,
        /// <summary>
        /// 条码组合值存在空选项，请检查
        /// </summary>
        [Description("条码组合值存在空选项")]
        CodeValueIsEmpty,
        /// <summary>
        /// 条码数量异常
        /// </summary>
        [Description("条码数量异常")]
        QuantityIsError,
    }
}
