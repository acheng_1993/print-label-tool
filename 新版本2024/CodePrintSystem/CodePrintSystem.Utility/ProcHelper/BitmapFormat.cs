﻿using System;
using System.Runtime.InteropServices;
using System.IO;

namespace CodePrintSystem.Utility
{
    public class BitmapFormat
    {
        public struct BITMAPFILEHEADERStruct
        {
            public ushort bfType;
            public int bfSize;
            public ushort bfReserved1;
            public ushort bfReserved2;
            public int bfOffBits;
        }

        public struct MASKStruct
        {
            public byte redmask;
            public byte greenmask;
            public byte bluemask;
            public byte rgbReserved;
        }

        public struct BITMAPINFOHEADERStruct
        {
            public int biSize;
            public int biWidth;
            public int biHeight;
            public ushort biPlanes;
            public ushort biBitCount;
            public int biCompression;
            public int biSizeImage;
            public int biXPelsPerMeter;
            public int biYPelsPerMeter;
            public int biClrUsed;
            public int biClrImportant;
        }

        /*******************************************
        * 函数名称：RotatePic       
        * 函数功能：旋转图片，目的是保存和显示的图片与按的指纹方向不同     
        * 函数入参：BmpBuf---旋转前的指纹字符串
        * 函数出参：ResBuf---旋转后的指纹字符串
        * 函数返回：无
        *********************************************/
        public static void RotatePic(byte[] bmpBuf, int width, int height, ref byte[] resBuf)
        {
            int rowLoop = 0;
            int colLoop = 0;
            int bmpBuflen = width * height;

            try
            {
                for (rowLoop = 0; rowLoop < bmpBuflen;)
                {
                    for (colLoop = 0; colLoop < width; colLoop++)
                    {
                        resBuf[rowLoop + colLoop] = bmpBuf[bmpBuflen - rowLoop - width + colLoop];
                    }

                    rowLoop = rowLoop + width;
                }
            }
            catch (Exception ex)
            {
                //ZKCE.SysException.ZKCELogger logger = new ZKCE.SysException.ZKCELogger(ex);
                //logger.Append();
            }
        }

        /*******************************************
        * 函数名称：StructToBytes       
        * 函数功能：将结构体转化成无符号字符串数组     
        * 函数入参：StructObj---被转化的结构体
        *           Size---被转化的结构体的大小
        * 函数出参：无
        * 函数返回：结构体转化后的数组
        *********************************************/
        public static byte[] StructToBytes(object structObj, int size)
        {
            int structSize = Marshal.SizeOf(structObj);
            byte[] getBytes = new byte[structSize];

            try
            {
                IntPtr structPtr = Marshal.AllocHGlobal(structSize);
                Marshal.StructureToPtr(structObj, structPtr, false);
                Marshal.Copy(structPtr, getBytes, 0, structSize);
                Marshal.FreeHGlobal(structPtr);

                if (size == 14)
                {
                    byte[] newBytes = new byte[size];
                    int count = 0;
                    int loop = 0;

                    for (loop = 0; loop < structSize; loop++)
                    {
                        if (loop != 2 && loop != 3)
                        {
                            newBytes[count] = getBytes[loop];
                            count++;
                        }
                    }

                    return newBytes;
                }
                else
                {
                    return getBytes;
                }
            }
            catch (Exception ex)
            {
                //ZKCE.SysException.ZKCELogger logger = new ZKCE.SysException.ZKCELogger(ex);
                //logger.Append();

                return getBytes;
            }
        }

        /*******************************************
        * 函数名称：GetBitmap       
        * 函数功能：将传进来的数据保存为图片     
        * 函数入参：buffer---图片数据
        *           nWidth---图片的宽度
        *           nHeight---图片的高度
        * 函数出参：无
        * 函数返回：无
        *********************************************/
        public static void GetBitmap(byte[] buffer, int nWidth, int nHeight, ref MemoryStream ms)
        {
            int colorIndex = 0;
            ushort nBitCount = 8;
            int nColorTableEntries = 256;
            byte[] resBuf = new byte[nWidth * nHeight * 2];

            try
            {
                BITMAPFILEHEADERStruct bmpHeader = new BITMAPFILEHEADERStruct();
                BITMAPINFOHEADERStruct bmpInfoHeader = new BITMAPINFOHEADERStruct();
                MASKStruct[] colorMask = new MASKStruct[nColorTableEntries];

                int w = (((nWidth + 3) / 4) * 4);

                //图片头信息
                bmpInfoHeader.biSize = Marshal.SizeOf(bmpInfoHeader);
                bmpInfoHeader.biWidth = nWidth;
                bmpInfoHeader.biHeight = nHeight;
                bmpInfoHeader.biPlanes = 1;
                bmpInfoHeader.biBitCount = nBitCount;
                bmpInfoHeader.biCompression = 0;
                bmpInfoHeader.biSizeImage = 0;
                bmpInfoHeader.biXPelsPerMeter = 0;
                bmpInfoHeader.biYPelsPerMeter = 0;
                bmpInfoHeader.biClrUsed = nColorTableEntries;
                bmpInfoHeader.biClrImportant = nColorTableEntries;

                //文件头信息
                bmpHeader.bfType = 0x4D42;
                bmpHeader.bfOffBits = 14 + Marshal.SizeOf(bmpInfoHeader) + bmpInfoHeader.biClrUsed * 4;
                bmpHeader.bfSize = bmpHeader.bfOffBits + ((((w * bmpInfoHeader.biBitCount + 31) / 32) * 4) * bmpInfoHeader.biHeight);
                bmpHeader.bfReserved1 = 0;
                bmpHeader.bfReserved2 = 0;

                ms.Write(StructToBytes(bmpHeader, 14), 0, 14);
                ms.Write(StructToBytes(bmpInfoHeader, Marshal.SizeOf(bmpInfoHeader)), 0, Marshal.SizeOf(bmpInfoHeader));

                //调试板信息
                for (colorIndex = 0; colorIndex < nColorTableEntries; colorIndex++)
                {
                    colorMask[colorIndex].redmask = (byte)colorIndex;
                    colorMask[colorIndex].greenmask = (byte)colorIndex;
                    colorMask[colorIndex].bluemask = (byte)colorIndex;
                    colorMask[colorIndex].rgbReserved = 0;

                    ms.Write(StructToBytes(colorMask[colorIndex], Marshal.SizeOf(colorMask[colorIndex])), 0, Marshal.SizeOf(colorMask[colorIndex]));
                }

                //图片旋转，解决指纹图片倒立的问题
                RotatePic(buffer, nWidth, nHeight, ref resBuf);

                byte[] filter = null;
                if (w - nWidth > 0)
                {
                    filter = new byte[w - nWidth];
                }
                for (int i = 0; i < nHeight; i++)
                {
                    ms.Write(resBuf, i * nWidth, nWidth);
                    if (w - nWidth > 0)
                    {
                        ms.Write(resBuf, 0, w - nWidth);
                    }
                }
            }
            catch (Exception ex)
            {
                // ZKCE.SysException.ZKCELogger logger = new ZKCE.SysException.ZKCELogger(ex);
                // logger.Append();
            }
        }

        /*******************************************
        * 函数名称：WriteBitmap       
        * 函数功能：将传进来的数据保存为图片     
        * 函数入参：buffer---图片数据
        *           nWidth---图片的宽度
        *           nHeight---图片的高度
        * 函数出参：无
        * 函数返回：无
        *********************************************/
        public static void WriteBitmap(byte[] buffer, int nWidth, int nHeight)
        {
            int colorIndex = 0;
            ushort nBitCount = 8;
            int nColorTableEntries = 256;
            byte[] resBuf = new byte[nWidth * nHeight];

            try
            {

                BITMAPFILEHEADERStruct bmpHeader = new BITMAPFILEHEADERStruct();
                BITMAPINFOHEADERStruct bmpInfoHeader = new BITMAPINFOHEADERStruct();
                MASKStruct[] colorMask = new MASKStruct[nColorTableEntries];
                int w = (((nWidth + 3) / 4) * 4);
                //图片头信息
                bmpInfoHeader.biSize = Marshal.SizeOf(bmpInfoHeader);
                bmpInfoHeader.biWidth = nWidth;
                bmpInfoHeader.biHeight = nHeight;
                bmpInfoHeader.biPlanes = 1;
                bmpInfoHeader.biBitCount = nBitCount;
                bmpInfoHeader.biCompression = 0;
                bmpInfoHeader.biSizeImage = 0;
                bmpInfoHeader.biXPelsPerMeter = 0;
                bmpInfoHeader.biYPelsPerMeter = 0;
                bmpInfoHeader.biClrUsed = nColorTableEntries;
                bmpInfoHeader.biClrImportant = nColorTableEntries;

                //文件头信息
                bmpHeader.bfType = 0x4D42;
                bmpHeader.bfOffBits = 14 + Marshal.SizeOf(bmpInfoHeader) + bmpInfoHeader.biClrUsed * 4;
                bmpHeader.bfSize = bmpHeader.bfOffBits + ((((w * bmpInfoHeader.biBitCount + 31) / 32) * 4) * bmpInfoHeader.biHeight);
                bmpHeader.bfReserved1 = 0;
                bmpHeader.bfReserved2 = 0;

                Stream fileStream = File.Open("finger.bmp", FileMode.Create, FileAccess.Write);
                BinaryWriter tmpBinaryWriter = new BinaryWriter(fileStream);

                tmpBinaryWriter.Write(StructToBytes(bmpHeader, 14));
                tmpBinaryWriter.Write(StructToBytes(bmpInfoHeader, Marshal.SizeOf(bmpInfoHeader)));

                //调试板信息
                for (colorIndex = 0; colorIndex < nColorTableEntries; colorIndex++)
                {
                    colorMask[colorIndex].redmask = (byte)colorIndex;
                    colorMask[colorIndex].greenmask = (byte)colorIndex;
                    colorMask[colorIndex].bluemask = (byte)colorIndex;
                    colorMask[colorIndex].rgbReserved = 0;

                    tmpBinaryWriter.Write(StructToBytes(colorMask[colorIndex], Marshal.SizeOf(colorMask[colorIndex])));
                }

                //图片旋转，解决指纹图片倒立的问题
                RotatePic(buffer, nWidth, nHeight, ref resBuf);

                //写图片
                //TmpBinaryWriter.Write(ResBuf);
                byte[] filter = null;
                if (w - nWidth > 0)
                {
                    filter = new byte[w - nWidth];
                }
                for (int i = 0; i < nHeight; i++)
                {
                    tmpBinaryWriter.Write(resBuf, i * nWidth, nWidth);
                    if (w - nWidth > 0)
                    {
                        tmpBinaryWriter.Write(resBuf, 0, w - nWidth);
                    }
                }

                fileStream.Close();
                tmpBinaryWriter.Close();
            }
            catch (Exception ex)
            {
                //ZKCE.SysException.ZKCELogger logger = new ZKCE.SysException.ZKCELogger(ex);
                //logger.Append();
            }
        }
    }
}
