﻿using System.Drawing;

namespace CodePrintSystem.Utility
{
    /// <summary>
    /// 获取指纹
    /// </summary>
    public class FingerprintResult
    {
        /// <summary>
        /// 指纹模板
        /// </summary>
        public string CapTmp { get; set; }

        /// <summary>
        /// 指纹图像
        /// </summary>
        public Image Image { get; set; }

        /// <summary>
        /// 指纹图像-字节
        /// </summary>
        public byte[] FPBuffer { get; set; }

        /// <summary>
        /// 是否成功
        /// </summary>
        public bool IsSuccess { get; set; }

        /// <summary>
        /// 错误信息
        /// </summary>
        public string ErrorMsg { get; set; }

        /// <summary>
        /// 登记次数
        /// </summary>
        public int RegisterCount { get; set; }
    }

}
