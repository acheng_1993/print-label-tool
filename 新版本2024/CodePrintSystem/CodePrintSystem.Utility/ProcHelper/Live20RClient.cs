﻿using libzkfpcsharp;
using System;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using CodePrintSystem.Core;

namespace CodePrintSystem.Utility
{
    /// <summary>
    /// 客户端
    /// </summary>
    public class Live20RClient
    {
        /// <summary>
        /// 构造
        /// </summary>
        static Live20RClient()
        {
            if (AppsettingsConfig.IsFingerprint)
            {
                // 初始化&打开
                var initRes = zkfp2.Init();
                if (initRes != zkfperrdef.ZKFP_ERR_OK || initRes != zkfperrdef.ZKFP_ERR_ALREADY_INIT)
                {
                    SetState(initRes);
                }
                DeviceCount = zkfp2.GetDeviceCount();
            }
        }

        /// <summary>
        /// 打开设备
        /// </summary>
        /// <param name="collectAfter">采集之后的操作</param>
        /// <param name="isRegister">true 指纹登记3次， false单次操作</param>
        /// <param name="index">设置索引</param>
        /// <returns></returns>
        public static bool Open(Action<FingerprintResult> collectAfter, bool isRegister = false, int index = 0)
        {
            if (!AppsettingsConfig.IsFingerprint) {
                return true;
            }
            if (IsOpen)
            {
                return true;
            }
            Live20RClient.isRegister = isRegister;
            Live20RClient.collectAfter = collectAfter;
            return Open(index);
        }

        /// <summary>
        /// 打开设置
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        private static bool Open(int index)
        {
            if (!AppsettingsConfig.IsFingerprint)
            {
                IsOpen = false;
                return true;
            }
                if (IsOpen)
            {
                return IsOpen;
            }
            for (int i = 0; i < 3; i++)
            {
                regTmps[i] = new byte[2048];
            }
            if (IntPtr.Zero == (devHandle = zkfp2.OpenDevice(index)))// 连接设备
            {
                SetState(zkfperrdef.ZKFP_ERR_OPEN);
                return false;
            }
            if (IntPtr.Zero == (dBHandle = zkfp2.DBInit()))// 初始化算法库
            {
                SetState(zkfperrdef.ZKFP_ERR_INITLIB);
                zkfp2.CloseDevice(devHandle);
                devHandle = IntPtr.Zero;
                return false;
            }

            // 获取图像宽高 字节数组
            byte[] paramValue = new byte[4];
            int size = 4;
            zkfp2.GetParameters(devHandle, 1, paramValue, ref size);
            zkfp2.ByteArray2Int(paramValue, ref mfpWidth);
            size = 4;
            zkfp2.GetParameters(devHandle, 2, paramValue, ref size);
            zkfp2.ByteArray2Int(paramValue, ref mfpHeight);
            fPBuffer = new byte[mfpWidth * mfpHeight];

            // 开启线程监听
            IsOpen = true;
            runTask = Task.Run(DoCapture);
            return true;
        }

        private static Task runTask;

        /// <summary>
        /// 关闭
        /// </summary>
        public static void Close()
        {
            try
            {
                if (IsOpen == false)
                {
                    return;
                }
                IsOpen = false;

                zkfp2.CloseDevice(devHandle);// 关闭设备
                zkfp2.DBFree(dBHandle);// 释放算法库
                devHandle = IntPtr.Zero;
                dBHandle = IntPtr.Zero;

                isRegister = false;
                registerCount = 0;
                regTmps = new byte[3][];
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// 指纹对比
        /// </summary>
        /// <param name="temp1"></param>
        /// <param name="temp2"></param>
        /// <returns></returns>
        public static bool DBMatch(string temp1, string temp2)
        {
            if (!AppsettingsConfig.IsFingerprint)
            {
                IsOpen = false;
                return false;
            }
            return zkfp2.DBMatch(dBHandle, zkfp2.Base64ToBlob(temp1), zkfp2.Base64ToBlob(temp2)) > 0;
        }

        #region 公共参数
        /// <summary>
        /// 是否打开
        /// </summary>
        public static bool IsOpen { get; set; } = false;

        /// <summary>
        /// 最新错误码
        /// </summary>
        public static int ErrorCode { get; set; } = 0;

        /// <summary>
        /// 连接设备数
        /// </summary>
        public static int DeviceCount { get; set; } = 0;
        #endregion

        #region 私有参数
        private static bool isRegister = true;// 是否为登记
        private static Action<FingerprintResult> collectAfter;// 采集之后的操作

        private static IntPtr devHandle = IntPtr.Zero;// 设备句柄
        private static IntPtr dBHandle = IntPtr.Zero;// 算法操作句柄
        private static byte[] capTmp = new byte[2048];// 指纹模板(建议预分配 2048Bytes)
        private static int cbCapTmp = 2048;// 指纹模板 数组长度

        private static int mfpWidth;
        private static int mfpHeight;
        private static byte[] fPBuffer = new byte[300 * 400]; // 返回图像（数组大小为 imageWidth*imageHeight）

        private static int registerCount = 0;// 登记次数
        private static byte[][] regTmps = new byte[3][];// 登记的指纹
        #endregion

        #region 采集方法
        /// <summary>
        /// 循环获取指纹
        /// </summary>
        private static void DoCapture()
        {
            while (IsOpen)
            {
                try
                {
                    int ret = zkfp2.AcquireFingerprint(devHandle, fPBuffer, capTmp, ref cbCapTmp);// 采集指纹
                    if (ret == zkfp.ZKFP_ERR_OK)// 采集成功
                    {
                        GetFingerprintResult();
                    }
                }
                catch (Exception ex)
                {
                }
                Thread.Sleep(200);
            };
        }

        /// <summary>
        /// 指纹处理
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        private static void GetFingerprintResult()
        {
            var result = new FingerprintResult()
            {
                IsSuccess = true,
                FPBuffer = fPBuffer,
                Image = GetBitmap(fPBuffer),
                CapTmp = zkfp2.BlobToBase64(capTmp, cbCapTmp),
            };
            if (isRegister)
            {
                result = DoCaptureRegister(result);
            }
            collectAfter?.Invoke(result);
        }

        /// <summary>
        /// 采集的操作
        /// </summary>
        private static FingerprintResult DoCaptureRegister(FingerprintResult result)
        {
            try
            {
                // 此次跟上次做对比
                if (registerCount > 0 && zkfp2.DBMatch(dBHandle, capTmp, regTmps[registerCount - 1]) <= 0)
                {
                    result.IsSuccess = false;
                    result.ErrorMsg = "请用同一个手指按3次登记";
                    return result;
                }
                Array.Copy(capTmp, regTmps[registerCount], cbCapTmp);// 加入缓存
                registerCount++;
                result.RegisterCount = registerCount;
                if (registerCount >= 3)
                {
                    var regTmp = new byte[2048];
                    var cbRegTmp = 0;
                    if (zkfp.ZKFP_ERR_OK == zkfp2.DBMerge(dBHandle, regTmps[0], regTmps[1], regTmps[2], regTmp, ref cbRegTmp))
                    {
                        result.CapTmp = zkfp2.BlobToBase64(regTmp, cbRegTmp);
                    }
                    else
                    {
                        result.IsSuccess = false;
                        result.ErrorMsg = $"合并指纹模板有误";
                    }
                    // 指纹采集初始化
                    ResetRegister();
                    //Close();
                }
            }
            catch (Exception ex)
            {
                result.ErrorMsg = ex.Message;
                result.IsSuccess = false;
            }
            return result;
        }

        /// <summary>
        /// 指纹登记采集初始化
        /// </summary>
        public static void ResetRegister()
        {
            registerCount = 0;
            regTmps = new byte[3][];// 登记的指纹
            for (int i = 0; i < 3; i++)
            {
                regTmps[i] = new byte[2048];
            }
        }

        /// <summary>
        /// 获取图片
        /// </summary>
        /// <returns></returns>
        private static Bitmap GetBitmap(byte[] buffer)
        {
            MemoryStream ms = new MemoryStream();
            BitmapFormat.GetBitmap(buffer, mfpWidth, mfpHeight, ref ms);
            return new Bitmap(ms);
        }
        #endregion

        /// <summary>
        /// 设置状态
        /// </summary>
        /// <param name="isOpen"></param>
        /// <param name="errorCode"></param>
        private static void SetState(int errorCode, bool isOpen = false)
        {
            IsOpen = IsOpen;
            ErrorCode = errorCode;
        }

    }
}
