﻿using libzkfpcsharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodePrintSystem.Utility
{
    /// <summary>
    /// 指纹采集器
    /// </summary>
    public class Live20Helper
    {
        /// <summary>
        /// 初始化库
        /// </summary>
        /// <returns></returns>
        public static int Init()
        {
            return zkfp2.Init();
        }
    }
}
