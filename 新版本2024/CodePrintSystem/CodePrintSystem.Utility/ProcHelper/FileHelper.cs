﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodePrintSystem.Utility.ProcHelper
{
    /// <summary>
    /// 文件帮助类
    /// </summary>
    public class FileHelper
    {
        /// <summary>
        /// 打开资源管理器
        /// </summary>
        /// <param name="path"></param>
        public static void ProcessStart(string path)
        {
            Process p = new Process();
            p.StartInfo.FileName = "explorer.exe";
            p.StartInfo.Arguments = $@" /select, {path}";
            p.Start();
        }
    }
}
