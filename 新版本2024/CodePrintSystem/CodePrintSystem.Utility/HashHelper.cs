﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CodePrintSystem.Utility
{
    public static class HashHelper
    {
        public static string CalculateSHA1(byte[] fileData)
        {
            using (var sha1 = SHA1.Create())
            {
                byte[] hash = sha1.ComputeHash(fileData);
                return BitConverter.ToString(hash).Replace("-", "").ToLower();
            }
        }
    }
}
