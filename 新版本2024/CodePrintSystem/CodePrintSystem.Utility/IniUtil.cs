﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace CodePrintSystem.Utility
{
    /// <summary>
    /// 配置文件操作类
    /// </summary>
    public class IniUtil
    {
        private static readonly object _lock = new object();
        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);
        /// <summary>
        /// 写ini
        /// </summary>
        /// <param name="section"></param>
        /// <param name="key"></param>
        /// <param name="val"></param>
        /// <param name="iniName">配置文件名称，无需带.ini</param>
        /// <returns></returns>
        public static long WriteIni(string section, string key, string val, string iniName)
        {
            lock (_lock)
            {
                string filePath = Environment.CurrentDirectory + "\\" + iniName + ".ini";
                return WritePrivateProfileString(section, key, val, filePath);
            }
        }
        /// <summary>
        /// 写INI
        /// </summary>
        /// <param name="section">节点</param>
        /// <param name="key">Key</param>
        /// <param name="val">结果</param>
        /// <returns></returns>
        public static long WriteIni(string section, string key, string val)
        {
            lock (_lock)
            {
                string filePath = Environment.CurrentDirectory + "\\config.ini";
                return WritePrivateProfileString(section, key, val, filePath);
            }
        }
        /// <summary>
        /// 读取ini配置
        /// </summary>
        /// <param name="section">节点</param>
        /// <param name="key">key</param>
        /// <param name="def">读取失败的默认值</param>
        /// <param name="iniName">配置文件名</param>
        /// <returns></returns>
        public static string ReadIni(string section, string key, string def, string iniName)
        {
            lock (_lock)
            {
                string filePath = Environment.CurrentDirectory + "\\" + iniName + ".ini";
                StringBuilder retVal = new StringBuilder();
                GetPrivateProfileString(section, key, def, retVal, 2147483647, filePath);
                return retVal.ToString();
            }
        }
        /// <summary>
        /// 读取ini配置
        /// </summary>
        /// <param name="section">节点</param>
        /// <param name="key">key</param>
        /// <param name="def">读取失败后的默认值</param>
        /// <returns></returns>
        public static string ReadIni(string section, string key, string def)
        {
            lock (_lock)
            {
                string filePath = Environment.CurrentDirectory + "\\config.ini";
                StringBuilder retVal = new StringBuilder();
                GetPrivateProfileString(section, key, def, retVal, 2147483647, filePath);
                return retVal.ToString();
            }
        }
    }
}
