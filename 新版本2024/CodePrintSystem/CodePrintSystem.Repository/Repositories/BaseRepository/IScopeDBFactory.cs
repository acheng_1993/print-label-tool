﻿using CodePrintSystem.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace CodePrintSystem.Repository
{
    internal interface IScopeDBFactory
    {
        CustomDatabase GetScopeDb();
    }
}
