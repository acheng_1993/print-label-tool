

//使用平台信息: ID:WinFormNoApi  描述:Winform无API
//代码版本信息: ID:None  描述:无 添加时间:2022-10-13 21:02:23
using System;
using System.Collections.Generic;
using NPoco;
using System.Data.SqlClient;
using System.Threading.Tasks;

using CodePrintSystem.Core;
using CodePrintSystem.Basic.Abstractions;
using CodePrintSystem.Utility;


namespace CodePrintSystem.Repository
{
    internal class TimeCodeSetRepositoryImpl : AbstractRepository<TimeCodeSetEntity,BaseTimeCodeSetCondition,long>,IQueryTimeCodeSetRepository,ICommandTimeCodeSetRepository<long>, IAutoInject
    {

	     public TimeCodeSetRepositoryImpl (IScopeDBFactory scopeDBFactory) : base(scopeDBFactory)
        {

        }

         

        
            

        
            

        
            

        
            

        
            

        
            
        public override Sql TrunConditionToSql(Sql sql,BaseTimeCodeSetCondition condition)
        {
            if (condition != null)
            {
                        if (!string.IsNullOrEmpty(condition.Name))
        {
            sql.Append(" And Name=@Name", new { Name=condition.Name });
        }
            if (!string.IsNullOrEmpty(condition.Code))
            {
                sql.Append(" And Code=@Code", new { Code=condition.Code });
            }
                if(condition.Type.HasValue)
                {
                     if (condition.Type > 0)
                    {
                        sql.Append(" And Type=@Type", new { Type=condition.Type.Value });
                    }
                }
                    if (!string.IsNullOrEmpty(condition.TimeCodes))
                    {
                        sql.Append(" And TimeCodes LIKE @TimeCodes ", new { TimeCodes= $"%{condition.TimeCodes}%"});
                    }
                        if (!string.IsNullOrEmpty(condition.Attribute1))
                        {
                            sql.Append(" And Attribute1=@Attribute1", new { Attribute1=condition.Attribute1 });
                        }
                            if (!string.IsNullOrEmpty(condition.Attribute2))
                            {
                                sql.Append(" And Attribute2=@Attribute2", new { Attribute2=condition.Attribute2 });
                            }

            }
            return sql;
        }
    }
}

