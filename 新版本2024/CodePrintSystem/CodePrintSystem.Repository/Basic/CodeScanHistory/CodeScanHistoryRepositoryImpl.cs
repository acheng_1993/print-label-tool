

//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/2/3 10:06:03
using System;
using System.Collections.Generic;
using NPoco;
using System.Data.SqlClient;
using System.Threading.Tasks;

using CodePrintSystem.Core;
using CodePrintSystem.Basic.Abstractions;
using CodePrintSystem.Utility;


namespace CodePrintSystem.Repository
{
    internal class CodeScanHistoryRepositoryImpl : AbstractRepository<CodeScanHistoryEntity, BaseCodeScanHistoryCondition, long>, IQueryCodeScanHistoryRepository, ICommandCodeScanHistoryRepository<long>, IAutoInject
    {

        public CodeScanHistoryRepositoryImpl(IScopeDBFactory scopeDBFactory) : base(scopeDBFactory)
        {

        }



        public async Task<List<T>> GetStatusCountGroupByItemCodeAsync<T>(BaseCodeScanHistoryCondition condition)
        {
            var sql = new Sql();
            sql.Append($"SELECT  Status,COUNT(0) as Count FROM {PocoData.TableInfo.TableName}   WHERE  @CreateTimeStart<=CreateTime And CreateTime<=@CreateTimeEnd and  ItemCode=@ItemCode  and CreaterId=@CreaterId  group by STATUS",
                new { CreateTimeStart = condition.CreateTimeStart.Value, CreateTimeEnd = condition.CreateTimeEnd.Value, ItemCode = condition.ItemCode, CreaterId = condition.CreateId });
            return await Db.FetchAsync<T>(sql).ConfigureAwait(false);
        }


        public override Sql TrunConditionToSql(Sql sql, BaseCodeScanHistoryCondition condition)
        {
            if (condition != null)
            {
                if (!string.IsNullOrEmpty(condition.Code))
                {
                    sql.Append(" And Code=@Code", new { Code = condition.Code });
                }
                if (!string.IsNullOrEmpty(condition.CreaterName))
                {
                    sql.Append(" And CreaterName=@CreaterName", new { CreaterName = condition.CreaterName });
                }
                if (condition.CreateTimeStart.HasValue && condition.CreateTimeEnd.HasValue)
                {
                    sql.Append("And (@CreateTimeStart<=CreateTime And CreateTime<=@CreateTimeEnd)", new { CreateTimeStart = condition.CreateTimeStart.Value, CreateTimeEnd = condition.CreateTimeEnd.Value });
                }
                if (!string.IsNullOrEmpty(condition.ProductionCode))
                {
                    sql.Append(" And ProductionCode=@ProductionCode", new { ProductionCode = condition.ProductionCode });
                }
                if (condition.Status.HasValue)
                {
                    if (condition.Status > 0)
                    {
                        sql.Append(" And Status=@Status", new { Status = condition.Status.Value });
                    }
                }
                if (!string.IsNullOrEmpty(condition.ItemCode))
                {
                    sql.Append(" And ItemCode LIKE @ItemCode ", new { ItemCode = $"%{condition.ItemCode}%" });
                }

            }
            return sql;
        }
    }
}

