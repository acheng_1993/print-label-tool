

//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/1/15 9:53:15
using System;
using System.Collections.Generic;
using NPoco;
using System.Data.SqlClient;
using System.Threading.Tasks;

using CodePrintSystem.Core;
using CodePrintSystem.Basic.Abstractions;
using CodePrintSystem.Utility;


namespace CodePrintSystem.Repository
{
    internal class GenerateCodeTemplateRepositoryImpl : AbstractRepository<GenerateCodeTemplateEntity, BaseGenerateCodeTemplateCondition, long>, IQueryGenerateCodeTemplateRepository, ICommandGenerateCodeTemplateRepository<long>, IAutoInject
    {

        public GenerateCodeTemplateRepositoryImpl(IScopeDBFactory scopeDBFactory) : base(scopeDBFactory)
        {

        }

















        public override Sql TrunConditionToSql(Sql sql, BaseGenerateCodeTemplateCondition condition)
        {
            if (condition != null)
            {
                if (!string.IsNullOrEmpty(condition.Keyword))
                {
                    sql.Append(" And Name like @Name", new { Name =$"%{condition.Keyword }%" });
                }
                if (!string.IsNullOrEmpty(condition.Name))
                {
                    sql.Append(" And Name=@Name", new { Name = condition.Name });
                }
                if (!string.IsNullOrEmpty(condition.Code))
                {
                    sql.Append(" And Code=@Code", new { Code = condition.Code });
                }
                if (condition.IsCustom.HasValue)
                {
                    sql.Append(" And IsCustom=@IsCustom", new { IsCustom = condition.IsCustom.Value });
                }
                if (condition.IsEnabled.HasValue)
                {
                    sql.Append(" And IsEnabled=@IsEnabled", new { IsEnabled = condition.IsEnabled.Value });
                }
                if (condition.IsQuote.HasValue)
                {
                    sql.Append(" And IsQuote=@IsQuote", new { IsQuote = condition.IsQuote.Value });
                }

            }
            return sql;
        }
    }
}

