

//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/1/15 9:54:11
using System;
using System.Collections.Generic;
using NPoco;
using System.Data.SqlClient;
using System.Threading.Tasks;

using CodePrintSystem.Core;
using CodePrintSystem.Basic.Abstractions;
using CodePrintSystem.Utility;


namespace CodePrintSystem.Repository
{
    internal class GenerateCodeHistoryRepositoryImpl : AbstractRepository<GenerateCodeHistoryEntity, BaseGenerateCodeHistoryCondition, long>, IQueryGenerateCodeHistoryRepository, ICommandGenerateCodeHistoryRepository<long>, IAutoInject
    {

        public GenerateCodeHistoryRepositoryImpl(IScopeDBFactory scopeDBFactory) : base(scopeDBFactory)
        {

        }









        public override Sql TrunConditionToSql(Sql sql, BaseGenerateCodeHistoryCondition condition)
        {
            if (condition != null)
            {
                if (!string.IsNullOrEmpty(condition.Code))
                {
                    sql.Append(" And Code=@Code", new { Code = condition.Code });
                }
                if (condition.TemplateId.HasValue)
                {
                    if (condition.TemplateId > 0)
                    {
                        sql.Append(" And TemplateId=@TemplateId", new { TemplateId = condition.TemplateId.Value });
                    }
                }
                if (condition.CreateTimeStart.HasValue && condition.CreateTimeEnd.HasValue)
                {
                    sql.Append("And (@CreateTimeStart<=CreateTime And CreateTime<=@CreateTimeEnd)", new { CreateTimeStart = condition.CreateTimeStart.Value, CreateTimeEnd = condition.CreateTimeEnd.Value });
                }

            }
            return sql;
        }
    }
}

