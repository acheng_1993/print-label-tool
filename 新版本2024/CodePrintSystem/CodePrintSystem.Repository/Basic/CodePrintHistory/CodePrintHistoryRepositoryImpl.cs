

//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/1/30 14:05:50
using System;
using System.Collections.Generic;
using NPoco;
using System.Data.SqlClient;
using System.Threading.Tasks;

using CodePrintSystem.Core;
using CodePrintSystem.Basic.Abstractions;
using CodePrintSystem.Utility;
using System.Linq;

namespace CodePrintSystem.Repository
{
    internal class CodePrintHistoryRepositoryImpl : AbstractRepository<CodePrintHistoryEntity, BaseCodePrintHistoryCondition, long>, IQueryCodePrintHistoryRepository, ICommandCodePrintHistoryRepository<long>, IAutoInject
    {

        public CodePrintHistoryRepositoryImpl(IScopeDBFactory scopeDBFactory) : base(scopeDBFactory)
        {

        }


        public async Task<int> GetCountListAsync(BaseCodePrintHistoryCondition condition = null)
        {
            var sql = new Sql();
            sql.Select("count(*)");
            sql.From(PocoData.TableInfo.TableName).Where(" IsDeleted = 0 ");
            sql = TrunConditionToSql(sql, condition);
            var result = await Db.ExecuteScalarAsync<int>(sql).ConfigureAwait(false);
            return result;
        }


        public override Sql TrunConditionToSql(Sql sql, BaseCodePrintHistoryCondition condition)
        {
            if (condition != null)
            {
                if (!string.IsNullOrEmpty(condition.Code))
                {
                    sql.Append(" And Code=@Code", new { Code = condition.Code });
                }
                if (!condition.CodeList.IsListNullOrEmpty())
                {
                    var codes = condition.CodeList.Select(t => "'" + t + "'");
                    sql.Append($" And Code in ({ string.Join(",", codes) })");
                }
                if (!string.IsNullOrEmpty(condition.SupplierCode))
                {
                    sql.Append(" And SupplierCode=@SupplierCode", new { SupplierCode = condition.SupplierCode });
                }
                if (condition.CurrentDate.HasValue)
                {
                    sql.Append(" And CurrentDate=@CurrentDate", new { CurrentDate = condition.CurrentDate });
                }
                if (!string.IsNullOrEmpty(condition.ItemCode))
                {
                    sql.Append(" And ItemCode=@ItemCode", new { ItemCode = condition.ItemCode });
                }
                if (!string.IsNullOrEmpty(condition.Version))
                {
                    sql.Append(" And Version=@Version", new { Version = condition.Version });
                }
                if (!string.IsNullOrEmpty(condition.FactoryCode))
                {
                    sql.Append(" And FactoryCode=@FactoryCode", new { FactoryCode = condition.FactoryCode });
                }
                if (!string.IsNullOrEmpty(condition.Year))
                {
                    sql.Append(" And Year=@Year", new { Year = condition.Year });
                }
                if (!string.IsNullOrEmpty(condition.Week))
                {
                    sql.Append(" And Week=@Week", new { Week = condition.Week });
                }
                if (!string.IsNullOrEmpty(condition.Day))
                {
                    sql.Append(" And Day=@Day", new { Day = condition.Day });
                }
                if (!string.IsNullOrEmpty(condition.SerialNumber))
                {
                    sql.Append(" And SerialNumber=@SerialNumber", new { SerialNumber = condition.SerialNumber });
                }
                if (condition.Status.HasValue)
                {
                    if (condition.Status > 0)
                    {
                        sql.Append(" And Status=@Status", new { Status = condition.Status.Value });
                    }
                }
                if (condition.PrintTimeStart.HasValue && condition.PrintTimeEnd.HasValue)
                {
                    sql.Append("And (@PrintTimeStart<=CurrentDate And CurrentDate<=@PrintTimeEnd)", new { PrintTimeStart = condition.PrintTimeStart.Value, PrintTimeEnd = condition.PrintTimeEnd.Value });
                }
                if (!string.IsNullOrEmpty(condition.VersionCode))
                {
                    sql.Append(" And VersionCode=@VersionCode", new { VersionCode = condition.VersionCode });
                }
                if (!string.IsNullOrEmpty(condition.ProductionCode))
                {
                    sql.Append(" And ProductionCode=@ProductionCode", new { ProductionCode = condition.ProductionCode });
                }
                if (!string.IsNullOrEmpty(condition.OperateName))
                {
                    sql.Append(" And OperateName=@OperateName", new { OperateName = condition.OperateName });
                }

            }
            return sql;
        }
    }
}

