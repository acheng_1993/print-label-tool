

//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/1/30 14:08:58
using System;
using System.Collections.Generic;
using NPoco;
using System.Data.SqlClient;
using System.Threading.Tasks;

using CodePrintSystem.Core;
using CodePrintSystem.Basic.Abstractions;
using CodePrintSystem.Utility;


namespace CodePrintSystem.Repository
{
    internal class SerialNumberRepositoryImpl : AbstractRepository<SerialNumberEntity, BaseSerialNumberCondition, long>, IQuerySerialNumberRepository, ICommandSerialNumberRepository<long>, IAutoInject
    {

        public SerialNumberRepositoryImpl(IScopeDBFactory scopeDBFactory) : base(scopeDBFactory)
        {

        }




        /// <summary>
        /// 根据产品编码和打印日期查询当天流水号
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="itemCode"></param>
        /// <param name="printDayTime"></param>
        /// <returns></returns>
        public async Task<T> GetByItemCodeOfDayAsync<T>(string itemCode, DateTime printDayTime)
        {
            var sql = new Sql();
            var printDay = printDayTime.ToString("yyyy-MM-dd");
            sql.From(PocoData.TableInfo.TableName).Where(" IsDeleted = 0  AND ItemCode = @ItemCode And CurrentDate=@CurrentDate ", new { ItemCode = itemCode, CurrentDate = printDay });
            return await Db.SingleOrDefaultAsync<T>(sql).ConfigureAwait(false);
        }

        public async Task<bool> LockSerialNumberAsync(string itemCode, DateTime printDayTime)
        {
            var printDay = printDayTime.ToString("yyyy-MM-dd");
            string sql = $" update {PocoData.TableInfo.TableName} set ModifyTime = '{DateTime.Now}' where ItemCode='{itemCode}'  and CurrentDate = '{printDay}'; ";
            await Db.ExecuteAsync(sql);
            return true;
        }

        public override Sql TrunConditionToSql(Sql sql, BaseSerialNumberCondition condition)
        {
            if (condition != null)
            {
                if (!string.IsNullOrEmpty(condition.ItemCode))
                {
                    sql.Append(" And ItemCode=@ItemCode", new { ItemCode = condition.ItemCode });
                }
                if (!string.IsNullOrEmpty(condition.CurrentDate))
                {
                    sql.Append(" And CurrentDate=@CurrentDate", new { CurrentDate = condition.CurrentDate });
                }
                if (!string.IsNullOrEmpty(condition.Year))
                {
                    sql.Append(" And Year=@Year", new { Year = condition.Year });
                }
                if (!string.IsNullOrEmpty(condition.Week))
                {
                    sql.Append(" And Week=@Week", new { Week = condition.Week });
                }
                if (!string.IsNullOrEmpty(condition.Day))
                {
                    sql.Append(" And Day=@Day", new { Day = condition.Day });
                }
                if (condition.SerialNumber.HasValue)
                {
                    if (condition.SerialNumber > 0)
                    {
                        sql.Append(" And SerialNumber=@SerialNumber", new { SerialNumber = condition.SerialNumber.Value });
                    }
                }

            }
            return sql;
        }
    }
}

