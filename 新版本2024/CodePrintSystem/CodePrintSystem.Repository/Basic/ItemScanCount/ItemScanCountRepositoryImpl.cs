

//使用平台信息: ID:WinFormNoApi  描述:Winform无API
//代码版本信息: ID:None  描述:无 添加时间:2022/1/25 22:44:10
using System;
using System.Collections.Generic;
using NPoco;
using System.Data.SqlClient;
using System.Threading.Tasks;

using CodePrintSystem.Core;
using CodePrintSystem.Basic.Abstractions;
using CodePrintSystem.Utility;


namespace CodePrintSystem.Repository
{
    internal class ItemScanCountRepositoryImpl : AbstractRepository<ItemScanCountEntity, BaseItemScanCountCondition, long>, IQueryItemScanCountRepository, ICommandItemScanCountRepository<long>, IAutoInject
    {

        public ItemScanCountRepositoryImpl(IScopeDBFactory scopeDBFactory) : base(scopeDBFactory)
        {

        }



        /// <summary>
        /// 检查物料编号是否唯一
        /// </summary>
        /// <param name="itemCode"></param>
        /// <returns></returns>
        public async Task<bool> IsItemCodeUniqueAsync(string itemCode)
        {
            KeyValuePair<string, string> column = new KeyValuePair<string, string>(nameof(ItemScanCountEntity.ItemCode), itemCode);
            bool isUnique = await IsUniqueAsync<string>(column).ConfigureAwait(false);
            return isUnique;
        }

        /// <summary>
        /// 根据物料编号获取单个实体
        /// </summary>
        /// <param name="itemCode">物料编号</param>
        /// <returns></returns>
        public async Task<T> GetSingleByItemCodeAsync<T>(string itemCode)
        {
            KeyValuePair<string, string> column = new KeyValuePair<string, string>(nameof(ItemScanCountEntity.ItemCode), itemCode);
            return await GetSingleAsync<T, string>(column).ConfigureAwait(false);
        }


        public override Sql TrunConditionToSql(Sql sql, BaseItemScanCountCondition condition)
        {
            if (condition != null)
            {
                if (!string.IsNullOrEmpty(condition.ItemCode))
                {
                    sql.Append(" And ItemCode=@ItemCode", new { ItemCode = condition.ItemCode });
                }
                if (condition.UserId.HasValue)
                {
                    if (condition.UserId > 0)
                    {
                        sql.Append(" And UserId=@UserId", new { UserId = condition.UserId.Value });
                    }
                }
            }
            return sql;
        }
    }
}

