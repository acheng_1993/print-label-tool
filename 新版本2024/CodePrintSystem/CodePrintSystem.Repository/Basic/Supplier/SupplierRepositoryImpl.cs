

//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/1/30 14:09:27
using System;
using System.Collections.Generic;
using NPoco;
using System.Data.SqlClient;
using System.Threading.Tasks;

using CodePrintSystem.Core;
using CodePrintSystem.Basic.Abstractions;
using CodePrintSystem.Utility;


namespace CodePrintSystem.Repository
{
    internal class SupplierRepositoryImpl : AbstractRepository<SupplierEntity, BaseSupplierCondition, long>, IQuerySupplierRepository, ICommandSupplierRepository<long>, IAutoInject
    {

        public SupplierRepositoryImpl(IScopeDBFactory scopeDBFactory) : base(scopeDBFactory)
        {

        }



        /// <summary>
        /// 检查供应商代码是否唯一
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public async Task<bool> IsCodeUniqueAsync(string code)
        {
            KeyValuePair<string, string> column = new KeyValuePair<string, string>(nameof(SupplierEntity.Code), code);
            bool isUnique = await IsUniqueAsync<string>(column).ConfigureAwait(false);
            return isUnique;
        }

        /// <summary>
        /// 根据供应商代码获取单个实体
        /// </summary>
        /// <param name="code">供应商代码</param>
        /// <returns></returns>
        public async Task<T> GetSingleByCodeAsync<T>(string code)
        {
            KeyValuePair<string, string> column = new KeyValuePair<string, string>(nameof(SupplierEntity.Code), code);
            return await GetSingleAsync<T, string>(column).ConfigureAwait(false);
        }





        public override Sql TrunConditionToSql(Sql sql, BaseSupplierCondition condition)
        {
            if (condition != null)
            {
                if (!string.IsNullOrEmpty(condition.KeyWord))
                {
                    sql.Append(" And (Code like @KeyWord or Name like @KeyWord) ", new { KeyWord = $"%{condition.KeyWord}%" });
                }
                if (!string.IsNullOrEmpty(condition.Code))
                {
                    sql.Append(" And Code=@Code", new { Code = condition.Code });
                }
                if (!string.IsNullOrEmpty(condition.Name))
                {
                    sql.Append(" And Name=@Name", new { Name = condition.Name });
                }

            }
            return sql;
        }
    }
}

