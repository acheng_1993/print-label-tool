

//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/11/8 23:23:17
using System;
using System.Collections.Generic;
using NPoco;
using System.Data.SqlClient;
using System.Threading.Tasks;

using CodePrintSystem.Core;
using CodePrintSystem.Basic.Abstractions;
using CodePrintSystem.Utility;
using System.Linq;

namespace CodePrintSystem.Repository
{
    internal class PrintPlanRepositoryImpl : AbstractRepository<PrintPlanEntity,BasePrintPlanCondition,long>,IQueryPrintPlanRepository,ICommandPrintPlanRepository<long>, IAutoInject
    {

	     public PrintPlanRepositoryImpl (IScopeDBFactory scopeDBFactory) : base(scopeDBFactory)
        {

        }

        public async Task<List<PrintPlanBoard>> GetBoardAsync(DateTime dateTime)
        {
            //        var list =  Db.Query<PrintPlanBoard>($@" select ItemCode,PlanDate,PlanQty
            //                  ,(select count(1) from codeprinthistory b where b.itemCode=a.itemCode and LEFT(b.printTime,10)= LEFT(a.planDate,10)) ProductQty
            //          ,(select count(1) from codescanhistory b where b.itemCode=a.itemCode and LEFT(b.createTime,10)= LEFT(a.planDate,10) and status =1  ) ScanOk
            //,(select count(1) from codescanhistory b where b.itemCode=a.itemCode and LEFT(b.createTime,10)= LEFT(a.planDate,10) and status =3  ) ScanNg 
            //,(select count(1) from codescanhistory b where b.itemCode=a.itemCode and LEFT(b.createTime,10)= LEFT(a.planDate,10) and status =2  ) ScanRepeat
            //              from         
            //              printplan a where LEFT(planDate,10) = LEFT('{dateTime.ToString("yyyy-MM-dd")}',10) ");
            var boardCodeList = Db.Query<PrintBoardCodeModel>($"select itemCode,PlanQty from printplan a where LEFT(planDate,10) = LEFT('{dateTime.ToString("yyyy-MM-dd")}',10)").MapToList<PrintBoardCodeModel>();
            var sql = @$"SELECT  ItemCode, -1 Status,COUNT(0) as qty FROM  codeprinthistory  WHERE  '{dateTime.ToString("yyyy-MM-dd")}'<=CreateTime And CreateTime<='{dateTime.AddDays(1).ToString("yyyy-MM-dd")}'  
and  ItemCode in ('{string.Join("','", boardCodeList.Select(x=>x.ItemCode))}' )    group by ItemCode

UNION ALL

SELECT  ItemCode,Status,COUNT(0) qty  FROM  codescanhistory  WHERE   '{dateTime.ToString("yyyy-MM-dd")}'<=CreateTime And CreateTime<='{dateTime.AddDays(1).ToString("yyyy-MM-dd")}'   
and  ItemCode in  ('{string.Join("','", boardCodeList.Select(x => x.ItemCode))}' )    group by STATUS,ItemCode";


            var list = Db.Query<PrintBoardModel>(sql).MapToList<PrintBoardModel>();

            List<PrintPlanBoard> printPlanBoards = new List<PrintPlanBoard>();
            boardCodeList.ForEach(x =>
            {
                PrintPlanBoard printPlanBoard = new PrintPlanBoard();
                printPlanBoard.ItemCode = x.ItemCode;
                printPlanBoard.PlanDate = dateTime;
                printPlanBoard.PlanQty = x.PlanQty;
                var tempPrint = list.Find(y => y.ItemCode == x.ItemCode&&y.Status==-1);
                if (tempPrint != null) {
                    printPlanBoard.ProductQty = tempPrint.Qty;
                }

                var okScan = list.Find(y => y.ItemCode == x.ItemCode && y.Status == 1);
                if (okScan != null)
                {
                    printPlanBoard.ScanOk = okScan.Qty;
                }
                var ngScan = list.Find(y => y.ItemCode == x.ItemCode && y.Status == 3);
                if (ngScan != null)
                {
                    printPlanBoard.ScanNg = ngScan.Qty;
                }

                var repeatScan = list.Find(y => y.ItemCode == x.ItemCode && y.Status == 2);
                if (repeatScan != null)
                {
                    printPlanBoard.ScanRepeat = repeatScan.Qty;
                }
                printPlanBoards.Add(printPlanBoard);
            });

            return printPlanBoards;
        }

        public override Sql TrunConditionToSql(Sql sql,BasePrintPlanCondition condition)
        {
            if (condition != null)
            {
                        if (!string.IsNullOrEmpty(condition.ItemCode))
        {
            sql.Append(" And ItemCode=@ItemCode", new { ItemCode=condition.ItemCode });
        }
         if (condition.PlanDateStart.HasValue && condition.PlanDateEnd.HasValue)
        {
            sql.Append("And (@PlanDateStart<=PlanDate And PlanDate<=@PlanDateEnd)", new { PlanDateStart=condition.PlanDateStart.Value, PlanDateEnd=condition.PlanDateEnd.Value});
        }

            }
            return sql;
        }
    }
}

