

//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/1/30 14:16:16
using System;
using System.Collections.Generic;
using NPoco;
using System.Data.SqlClient;
using System.Threading.Tasks;

using CodePrintSystem.Core;
using CodePrintSystem.Basic.Abstractions;
using CodePrintSystem.Utility;


namespace CodePrintSystem.Repository
{
    internal class UserRepositoryImpl : AbstractRepository<UserEntity, BaseUserCondition, long>, IQueryUserRepository, ICommandUserRepository<long>, IAutoInject
    {

        public UserRepositoryImpl(IScopeDBFactory scopeDBFactory) : base(scopeDBFactory)
        {

        }
        /// <summary>
        /// 检查用户名是否唯一
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public async Task<bool> IsUserNameUniqueAsync(string userName)
        {
            KeyValuePair<string, string> column = new KeyValuePair<string, string>(nameof(UserEntity.UserName), userName);
            bool isUnique = await IsUniqueAsync<string>(column).ConfigureAwait(false);
            return isUnique;
        }












        public override Sql TrunConditionToSql(Sql sql, BaseUserCondition condition)
        {
            if (condition != null)
            {
                if (condition.IsAdmin.HasValue)
                {
                    sql.Append(" And IsAdmin=@IsAdmin", new { IsAdmin = condition.IsAdmin.Value });
                }
                if (!string.IsNullOrEmpty(condition.UserName))
                {
                    sql.Append(" And UserName=@UserName", new { UserName = condition.UserName });
                }
                if (!string.IsNullOrEmpty(condition.UserPassword))
                {
                    sql.Append(" And UserPassword=@UserPassword", new { UserPassword = condition.UserPassword });
                }
                if (condition.LastLoginTimeStart.HasValue && condition.LastLoginTimeEnd.HasValue)
                {
                    sql.Append("And (@LastLoginTimeStart<=LastLoginTime And LastLoginTime<=@LastLoginTimeEnd)", new { LastLoginTimeStart = condition.LastLoginTimeStart.Value, LastLoginTimeEnd = condition.LastLoginTimeEnd.Value });
                }
                if (condition.IsEnabled.HasValue)
                {
                    sql.Append(" And IsEnabled=@IsEnabled", new { IsEnabled = condition.IsEnabled.Value });
                }
                if (!string.IsNullOrEmpty(condition.FingePrintPasswordMD5))
                {
                    sql.Append(" And FingePrintPasswordMD5=@FingePrintPasswordMD5", new { FingePrintPasswordMD5 = condition.FingePrintPasswordMD5 });
                }
            }
            return sql;
        }
    }
}

