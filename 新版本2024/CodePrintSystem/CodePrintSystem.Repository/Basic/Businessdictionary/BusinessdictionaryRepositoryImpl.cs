

//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/1/30 14:02:11
using System;
using System.Collections.Generic;
using NPoco;
using System.Data.SqlClient;
using System.Threading.Tasks;

using CodePrintSystem.Core;
using CodePrintSystem.Basic.Abstractions;
using CodePrintSystem.Utility;


namespace CodePrintSystem.Repository
{
    internal class BusinessdictionaryRepositoryImpl : AbstractRepository<BusinessdictionaryEntity, BaseBusinessdictionaryCondition, long>, IQueryBusinessdictionaryRepository, ICommandBusinessdictionaryRepository<long>, IAutoInject
    {

        public BusinessdictionaryRepositoryImpl(IScopeDBFactory scopeDBFactory) : base(scopeDBFactory)
        {

        }



        /// <summary>
        /// 检查字典code是否唯一
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public async Task<bool> IsCodeUniqueAsync(string code)
        {
            KeyValuePair<string, string> column = new KeyValuePair<string, string>(nameof(BusinessdictionaryEntity.Code), code);
            bool isUnique = await IsUniqueAsync<string>(column).ConfigureAwait(false);
            return isUnique;
        }

        /// <summary>
        /// 根据字典code获取单个实体
        /// </summary>
        /// <param name="code">字典code</param>
        /// <returns></returns>
        public async Task<T> GetSingleByCodeAsync<T>(string code)
        {
            KeyValuePair<string, string> column = new KeyValuePair<string, string>(nameof(BusinessdictionaryEntity.Code), code);
            return await GetSingleAsync<T, string>(column).ConfigureAwait(false);
        }








        public override Sql TrunConditionToSql(Sql sql, BaseBusinessdictionaryCondition condition)
        {
            if (condition != null)
            {
                if (!string.IsNullOrEmpty(condition.Code))
                {
                    sql.Append(" And Code=@Code", new { Code = condition.Code });
                }
                if (condition.DictionaryTypeId.HasValue)
                {
                    if (condition.DictionaryTypeId > 0)
                    {
                        sql.Append(" And DictionaryTypeId=@DictionaryTypeId", new { DictionaryTypeId = condition.DictionaryTypeId.Value });
                    }
                }
                if (condition.IsDictionaryRule.HasValue)
                {
                    sql.Append(" And IsDictionaryRule=@IsDictionaryRule", new { IsDictionaryRule = condition.IsDictionaryRule.Value });
                }
                if (condition.IsEnabled.HasValue)
                {
                    sql.Append(" And IsEnabled=@IsEnabled", new { IsEnabled = condition.IsEnabled.Value });
                }
                if (condition.IsCustom.HasValue)
                {
                    sql.Append(" And IsCustom=@IsCustom", new { IsCustom = condition.IsCustom.Value });
                }
            }
            return sql;
        }
    }
}

