

//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键),不向前兼容，对应模板V3_2 添加时间:2021/1/15 9:53:44
using System;
using System.Collections.Generic;
using NPoco;
using System.Data.SqlClient;
using System.Threading.Tasks;

using CodePrintSystem.Core;
using CodePrintSystem.Basic.Abstractions;
using CodePrintSystem.Utility;


namespace CodePrintSystem.Repository
{
    internal class GenerateCodeTemplateDetailRepositoryImpl : AbstractRepository<GenerateCodeTemplateDetailEntity, BaseGenerateCodeTemplateDetailCondition, long>, IQueryGenerateCodeTemplateDetailRepository, ICommandGenerateCodeTemplateDetailRepository<long>, IAutoInject
    {

        public GenerateCodeTemplateDetailRepositoryImpl(IScopeDBFactory scopeDBFactory) : base(scopeDBFactory)
        {

        }























        public override Sql TrunConditionToSql(Sql sql, BaseGenerateCodeTemplateDetailCondition condition)
        {
            if (condition != null)
            {
                if (!condition.TemplateIds.IsListNullOrEmpty())
                {
                    sql.Append($" And TemplateId in ('{string.Join("','", condition.TemplateIds)}') " );
                  
                }
                if (condition.TemplateId.HasValue)
                {
                    if (condition.TemplateId > 0)
                    {
                        sql.Append(" And TemplateId=@TemplateId", new { TemplateId = condition.TemplateId.Value });
                    }
                }
                if (condition.RowNo.HasValue)
                {
                    if (condition.RowNo > 0)
                    {
                        sql.Append(" And RowNo=@RowNo", new { RowNo = condition.RowNo.Value });
                    }
                }
                if (condition.RuleId.HasValue)
                {
                    if (condition.RuleId > 0)
                    {
                        sql.Append(" And RuleId=@RuleId", new { RuleId = condition.RuleId.Value });
                    }
                }
                if (!string.IsNullOrEmpty(condition.RuleValue))
                {
                    sql.Append(" And RuleValue=@RuleValue", new { RuleValue = condition.RuleValue });
                }
                if (condition.IsCustom.HasValue)
                {
                    sql.Append(" And IsCustom=@IsCustom", new { IsCustom = condition.IsCustom.Value });
                }
                if (condition.IsEnabled.HasValue)
                {
                    sql.Append(" And IsEnabled=@IsEnabled", new { IsEnabled = condition.IsEnabled.Value });
                }
                if (condition.IsQuote.HasValue)
                {
                    sql.Append(" And IsQuote=@IsQuote", new { IsQuote = condition.IsQuote.Value });
                }
                if (condition.IsContain.HasValue)
                {
                    sql.Append(" And IsContain=@IsContain", new { IsContain = condition.IsContain.Value });
                }
            }
            return sql;
        }
    }
}

