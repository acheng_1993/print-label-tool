﻿using System;

namespace CodePrintSystem.Core
{
    public interface IDBTransaction : IDisposable
    {
        void Complete();
    }
}
