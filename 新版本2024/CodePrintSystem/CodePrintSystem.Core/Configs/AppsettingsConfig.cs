﻿using System;
using System.Collections.Generic;

namespace CodePrintSystem.Core
{
    /// <summary>
    /// 当前用户
    /// </summary>
    public class CurrentUserConfig
    {
        public static CurrentUser User { set; get; }
        public static bool IsPrinting { set; get; } = false;

        public static string Version { get; set; }
    }

    public class CurrentUser
    {

        public long Id { get; set; }
        /// <summary>
        ///  是否管理员
        /// </summary>
        public bool? IsAdmin { get; set; } = false;

        /// <summary>
        ///  用户名
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        ///  用户密码
        /// </summary>
        public string UserPassword { get; set; }

        /// <summary>
        ///  最近登录时间
        /// </summary>
        public DateTime? LastLoginTime { get; set; }

        /// <summary>
        ///  是否启用
        /// </summary>
        public bool? IsEnabled { get; set; }
    }


    /// <summary>
    /// 服务器配置类
    /// </summary>
    public class AppsettingsConfig
    {
        public static IServiceProvider ServiceProvider { set; get; }
        /// <summary>
        /// 默认数据库连接
        /// </summary>
        public static string DefaultConnectionString { get; set; } = "";

        /// <summary>
        /// 数据库类型SQLSERVER，MYSQL,ORACLE
        /// </summary>
        public static string DatabaseType { get; set; } = "SQLSERVER";

        /// <summary>
        /// 共享目录位置
        /// </summary>
        public static string BartenderSharePath { get; set; }

        public static int PrintInterval { set; get; }

        public static int PrintTimeout { set; get; }
        public static string PrinterName { set; get; }

        /// <summary>
        /// 导出excel地址
        /// </summary>
        public static string ExportExcelPath { get; set; }

        /// <summary>
        /// 生产主键机器码
        /// </summary>
        public static int? MachineId { get; set; } = 1;

        /// <summary>
        /// 生产主键数据中心id
        /// </summary>
        public static int? DataCenterId { get; set; } = 1;


        /// <summary>
        /// 版本长度
        /// </summary>
        public static int VersionCodeLength { get; set; }

        /// <summary>
        /// 产品长度
        /// </summary>
        public static int ItemCodeLength { get; set; }
        /// <summary>
        /// 供应商编码长度
        /// </summary>
        public static int SupplierCodeLength { get; set; }

        /// <summary>
        /// ApiService 配置
        /// </summary>
        public static List<ServiceApiHostModel> ServiceApiHosts { get; set; }
        /// <summary>
        /// 打印任务延迟
        /// </summary>
        public static int TaskDelayMillisecond { get; set; }
        public static bool IsFingerprint { get; set; }

        /// <summary>
        /// 是否可以密码解锁
        /// </summary>
        public static bool IsCanPasswordUnLock { get; set; }

        public static string SystemSetFilePath { set; get; } = @"DefaultConfig/systemSet.json";

        public static string UpdaterConfigFileAddress { get; set; } = "http://139.224.209.121:21050/updates/AutoUpdater.xml";


    }






}
