setInterval(playTime, 1000);// 时间播放

var httpHost='http://localhost:52128';//url
var changeInterval = 8000;//毫秒
var reloadInterval = 60000;//重载间隔

var contentStrArr = [];
var currentIndex = -1;
function getBoardData(){
    getAjax('/api/MyBoard/get',(data)=>{
      
        contentStrArr = [];
        currentIndex = -1;
        var k =0 ;
        let trStr = '';
        for(let i=0;i<data.length;i++){
           
            let row = data[i];
            trStr+='<tr>';
            trStr+=`<td><div class="td_div"> ${row.itemCode}</div></td>`;
            trStr+=`<td><div class="td_div"> ${row.planQty}</div></td>`;
            trStr+=`<td><div class="td_div colorGreen"> ${row.productQty}</div></td>`;
            trStr+=`<td><div class="td_div colorRed"> ${row.noProductQty}</div></td>`;
            trStr+=`<td><div class="td_div"> ${row.percent}</div></td>`;
            trStr+=`<td><div class="td_div colorGreen"> ${row.scanOk}</div></td>`;
            trStr+=`<td><div class="td_div colorRed"> ${row.scanNg}</div></td>`;
            trStr+=`<td><div class="td_div colorRed"> ${row.scanRepeat}</div></td>`;
            trStr+="</tr>";
            if(k==9||i==data.length-1){
                k=0;
                contentStrArr.push(trStr);
                trStr='';
            }else{
             k++;
            }
        }

        ChangeContent();

    })


    //每5秒刷新一次
    setTimeout(getBoardData,reloadInterval);
}

function ChangeContent(){
    currentIndex++;
if(contentStrArr.length>currentIndex){
    $('#boardContent').html('');
    $('#boardContent').append( contentStrArr[currentIndex]);

}else{
    currentIndex=-1;
}
}

setInterval(ChangeContent,changeInterval);






//var decryptKey = 'fpx4MrWHsJQQNSAs';//秘钥
function getAjax(url,func,title='获取数据'){
    $.ajax({
        url:`${httpHost}${url}`,
        type:'get',
        dataType:'json',
        // jsonp: "callback",//传递给请求处理程序或页面的，用以获得jsonp回调函数名的参数名(一般默认为:callback)
        // jsonpCallback:"message",//自定义的jsonp回调函数名称，默认为jQuery自动生成的随机函数名，也可以写"?"，jQuery会自动为你处理数据
        success:function(data){
            func(data);
        },error:function(data){
            
            layer.msg(`${title}数据获取异常`,{area:[400,200],time:6000});
        }
    });
}

getBoardData();