﻿using CodePrintSystem.Basic.Abstractions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CodePrintSystem.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MyBoardController : ControllerBase
    {
        IQueryPrintPlanService planQuery;
        IQueryCodePrintHistoryService printLogQuery;
        IQueryCodeScanHistoryService scanQuery;
        public MyBoardController(IQueryPrintPlanService planQuery, IQueryCodePrintHistoryService printLogQuery, IQueryCodeScanHistoryService scanQuery)
        {
            this.planQuery = planQuery;
            this.printLogQuery = printLogQuery;
            this.scanQuery = scanQuery;
        }
        [HttpGet]
        [Route("Get")]
        [ProducesResponseType(typeof(List<PrintPlanBoard>), 200)]
        public async Task<ActionResult> GetBoard()
        {
            var plan = await planQuery.GetBoardAsync(DateTime.Now);
            plan.ForEach(x =>
            {
                if (x.PlanQty.Value > x.ProductQty)
                    x.NoProductQty = x.PlanQty.Value - x.ProductQty;

                x.Percent = x.ProductQty * 100 / x.PlanQty.Value ;
            });
            return Ok(plan);
        }
    }
}
