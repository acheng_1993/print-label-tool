using CodePrintSystem.Core;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CodePrintSystem.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = new ConfigurationBuilder()
               .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
               .Build();
        }
        /// <summary>
        /// 获取配置文件
        /// </summary>
        private void GetJsonConfig()
        {

            AppsettingsConfig config = new AppsettingsConfig();
            Configuration.Bind("WebConfig", config);


            //var serviceApiHostConfig = Configuration.GetSection("ServiceApiHosts")?.Get<List<ServiceApiHostModel>>();

            //var serviceApiHostConfig = new List<ServiceApiHostModel>();
            //Configuration.Bind("ServiceApiHosts", serviceApiHostConfig);
        }
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            RegisterService.RegisterComponents(services);
            GetJsonConfig();
            var serviceProvider = services.BuildServiceProvider();
            //// 插入日志例子
            //LogModel logModel = new LogModel();
            //logModel.LogMessage = "test";
            //var logger = serviceProvider.GetRequiredService<TPLLogger>();
            //logger.InsertAsync(logModel);

            AppsettingsConfig.ServiceProvider = serviceProvider;
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "CodePrintSystem.Api", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "CodePrintSystem.Api v1"));
            }
            app.UseCors(builder =>
            {
                builder.AllowAnyOrigin();
                builder.AllowAnyHeader();
                builder.AllowAnyMethod();
            });
            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
