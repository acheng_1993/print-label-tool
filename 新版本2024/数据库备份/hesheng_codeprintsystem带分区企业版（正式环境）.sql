USE [master]
GO
/****** Object:  Database [hesheng_codeprintsystem]    Script Date: 2024/2/27 0:51:49 ******/
CREATE DATABASE [hesheng_codeprintsystem]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'hesheng_codeprintsystem', FILENAME = N'F:\SQLSERVERData\hesheng_codeprintsystem.mdf' , SIZE = 6889472KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB ), 
 FILEGROUP [Data2024] 
( NAME = N'Data2024_1', FILENAME = N'F:\SQLSERVERData\Data2024_1.ndf' , SIZE = 73728KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB ), 
 FILEGROUP [Data2025] 
( NAME = N'Data2025_1', FILENAME = N'F:\SQLSERVERData\Data2025_1.ndf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB ), 
 FILEGROUP [Data2026] 
( NAME = N'Data2026_1', FILENAME = N'F:\SQLSERVERData\Data2026_1.ndf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB ), 
 FILEGROUP [Data2027] 
( NAME = N'Data2027_1', FILENAME = N'F:\SQLSERVERData\Data2027_1.ndf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB ), 
 FILEGROUP [Data2028] 
( NAME = N'Data2028_1', FILENAME = N'F:\SQLSERVERData\Data2028_1.ndf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB ), 
 FILEGROUP [DataNew] 
( NAME = N'DataNew_1', FILENAME = N'F:\SQLSERVERData\DataNew_1.ndf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'hesheng_codeprintsystem_log', FILENAME = N'F:\SQLSERVERData\hesheng_codeprintsystem_log.ldf' , SIZE = 12984320KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [hesheng_codeprintsystem] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [hesheng_codeprintsystem].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [hesheng_codeprintsystem] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [hesheng_codeprintsystem] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [hesheng_codeprintsystem] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [hesheng_codeprintsystem] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [hesheng_codeprintsystem] SET ARITHABORT OFF 
GO
ALTER DATABASE [hesheng_codeprintsystem] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [hesheng_codeprintsystem] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [hesheng_codeprintsystem] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [hesheng_codeprintsystem] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [hesheng_codeprintsystem] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [hesheng_codeprintsystem] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [hesheng_codeprintsystem] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [hesheng_codeprintsystem] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [hesheng_codeprintsystem] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [hesheng_codeprintsystem] SET  DISABLE_BROKER 
GO
ALTER DATABASE [hesheng_codeprintsystem] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [hesheng_codeprintsystem] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [hesheng_codeprintsystem] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [hesheng_codeprintsystem] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [hesheng_codeprintsystem] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [hesheng_codeprintsystem] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [hesheng_codeprintsystem] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [hesheng_codeprintsystem] SET RECOVERY FULL 
GO
ALTER DATABASE [hesheng_codeprintsystem] SET  MULTI_USER 
GO
ALTER DATABASE [hesheng_codeprintsystem] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [hesheng_codeprintsystem] SET DB_CHAINING OFF 
GO
ALTER DATABASE [hesheng_codeprintsystem] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [hesheng_codeprintsystem] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [hesheng_codeprintsystem] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'hesheng_codeprintsystem', N'ON'
GO
ALTER DATABASE [hesheng_codeprintsystem] SET QUERY_STORE = OFF
GO
USE [hesheng_codeprintsystem]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
USE [hesheng_codeprintsystem]
GO
/****** Object:  Schema [hesheng_codeprintsystem]    Script Date: 2024/2/27 0:51:50 ******/
CREATE SCHEMA [hesheng_codeprintsystem]
GO
/****** Object:  PartitionFunction [TimePartition]    Script Date: 2024/2/27 0:51:50 ******/
CREATE PARTITION FUNCTION [TimePartition](datetime) AS RANGE LEFT FOR VALUES (N'2024-02-25T00:00:00.000', N'2024-12-31T00:00:00.000', N'2025-12-31T00:00:00.000', N'2026-12-31T00:00:00.000', N'2027-12-31T00:00:00.000', N'2028-12-31T00:00:00.000')
GO
/****** Object:  PartitionScheme [TimePartition]    Script Date: 2024/2/27 0:51:50 ******/
CREATE PARTITION SCHEME [TimePartition] AS PARTITION [TimePartition] TO ([PRIMARY], [Data2024], [Data2025], [Data2026], [Data2027], [Data2028], [DataNew])
GO
/****** Object:  Table [dbo].[basic_businessdictionary]    Script Date: 2024/2/27 0:51:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[basic_businessdictionary](
	[Id] [bigint] NOT NULL,
	[DictionaryTypeId] [bigint] NOT NULL,
	[IsDictionaryRule] [bit] NULL,
	[RuleDictionaryTypeId] [bigint] NULL,
	[Code] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[ShortName] [nvarchar](50) NOT NULL,
	[Remark] [nvarchar](500) NOT NULL,
	[IsCustom] [bit] NOT NULL,
	[IsEnabled] [bit] NOT NULL,
	[IsQuote] [bit] NOT NULL,
	[CreaterID] [bigint] NULL,
	[CreateTime] [datetime] NULL,
	[ModifierID] [bigint] NULL,
	[ModifyTime] [datetime] NULL,
	[IsDeleted] [bit] NULL,
	[CreaterName] [nvarchar](50) NULL,
	[ModifierName] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[basic_businessdictionarytype]    Script Date: 2024/2/27 0:51:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[basic_businessdictionarytype](
	[Id] [bigint] NOT NULL,
	[TypeName] [nvarchar](50) NULL,
	[IsCustom] [bit] NULL,
	[Code] [nvarchar](50) NULL,
	[IsBatchAttribute] [bit] NULL,
	[Remark] [nvarchar](500) NULL,
	[IsQuote] [bit] NULL,
	[CreaterID] [bigint] NULL,
	[CreateTime] [datetime] NULL,
	[ModifierID] [bigint] NULL,
	[ModifyTime] [datetime] NULL,
	[IsDeleted] [bit] NULL,
	[CreaterName] [nvarchar](50) NULL,
	[ModifierName] [nvarchar](50) NULL,
	[IsEnabled] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[codeprinthistory]    Script Date: 2024/2/27 0:51:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[codeprinthistory](
	[Id] [bigint] NOT NULL,
	[Code] [nvarchar](100) NULL,
	[SupplierCode] [nvarchar](50) NULL,
	[CurrentDate] [datetime] NULL,
	[ItemCode] [nvarchar](50) NULL,
	[Version] [nvarchar](10) NULL,
	[FactoryCode] [nvarchar](50) NULL,
	[Year] [nvarchar](5) NULL,
	[Week] [nvarchar](5) NULL,
	[Day] [nvarchar](5) NULL,
	[SerialNumber] [nvarchar](100) NULL,
	[Status] [int] NULL,
	[PrintTime] [datetime] NULL,
	[CreaterID] [bigint] NULL,
	[CreaterName] [nvarchar](50) NULL,
	[CreateTime] [datetime] NULL,
	[ModifierID] [bigint] NULL,
	[ModifierName] [nvarchar](50) NULL,
	[ModifyTime] [datetime] NULL,
	[IsDeleted] [bit] NULL,
	[VersionCode] [nvarchar](10) NULL,
	[ProductionCode] [nvarchar](255) NULL,
	[OperateName] [nvarchar](255) NULL,
PRIMARY KEY NONCLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [TimePartition]([CreateTime])
GO
/****** Object:  Table [dbo].[codescanhistory]    Script Date: 2024/2/27 0:51:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[codescanhistory](
	[Id] [bigint] NOT NULL,
	[Code] [nvarchar](50) NULL,
	[CreaterID] [bigint] NULL,
	[CreaterName] [nvarchar](50) NULL,
	[CreateTime] [datetime] NULL,
	[ModifierID] [bigint] NULL,
	[ModifierName] [nvarchar](50) NULL,
	[ModifyTime] [datetime] NULL,
	[IsDeleted] [bit] NULL,
	[ProductionCode] [nvarchar](50) NULL,
	[Status] [int] NULL,
	[ItemCode] [nvarchar](50) NULL,
	[ErrorContent] [nvarchar](200) NULL,
 CONSTRAINT [PK__codescan__3214EC0658ED52CB] PRIMARY KEY NONCLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [TimePartition]([CreateTime])
GO
/****** Object:  Table [dbo].[iteminfo]    Script Date: 2024/2/27 0:51:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[iteminfo](
	[Id] [bigint] NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Code] [nvarchar](50) NULL,
	[VersionCode] [nvarchar](50) NULL,
	[ProductType] [bigint] NULL,
	[TemplateId] [bigint] NULL,
	[Attribute1] [nvarchar](50) NULL,
	[Attribute2] [nvarchar](50) NULL,
	[Attribute3] [nvarchar](50) NULL,
	[Attribute4] [nvarchar](50) NULL,
	[CreaterID] [bigint] NULL,
	[CreaterName] [nvarchar](50) NULL,
	[CreateTime] [datetime] NULL,
	[ModifierID] [bigint] NULL,
	[ModifierName] [nvarchar](50) NULL,
	[ModifyTime] [datetime] NULL,
	[IsDeleted] [bit] NULL,
	[NumPackCount] [nvarchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[itemscancount]    Script Date: 2024/2/27 0:51:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[itemscancount](
	[Id] [bigint] NOT NULL,
	[ItemCode] [nvarchar](50) NULL,
	[MachineId] [int] NULL,
	[Count] [int] NULL,
	[UserId] [bigint] NULL,
	[CreaterID] [bigint] NULL,
	[CreaterName] [nvarchar](50) NULL,
	[CreateTime] [datetime] NULL,
	[ModifierID] [bigint] NULL,
	[ModifierName] [nvarchar](50) NULL,
	[ModifyTime] [datetime] NULL,
	[IsDeleted] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[printplan]    Script Date: 2024/2/27 0:51:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[printplan](
	[Id] [bigint] NOT NULL,
	[CreaterID] [bigint] NULL,
	[CreaterName] [nvarchar](50) NULL,
	[CreateTime] [datetime] NULL,
	[ModifierID] [bigint] NULL,
	[ModifierName] [nvarchar](50) NULL,
	[ModifyTime] [datetime] NULL,
	[IsDeleted] [bit] NULL,
	[ItemCode] [nvarchar](100) NULL,
	[PlanDate] [datetime] NULL,
	[PlanQty] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[serialnumber]    Script Date: 2024/2/27 0:51:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[serialnumber](
	[Id] [bigint] NOT NULL,
	[CurrentDate] [nvarchar](255) NULL,
	[ItemCode] [nvarchar](50) NULL,
	[Year] [nvarchar](5) NULL,
	[Week] [nvarchar](5) NULL,
	[Day] [nvarchar](5) NULL,
	[SerialNumber] [int] NULL,
	[CreaterID] [bigint] NULL,
	[CreaterName] [nvarchar](50) NULL,
	[CreateTime] [datetime] NULL,
	[ModifierID] [bigint] NULL,
	[ModifierName] [nvarchar](50) NULL,
	[ModifyTime] [datetime] NULL,
	[IsDeleted] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[supplier]    Script Date: 2024/2/27 0:51:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[supplier](
	[Id] [bigint] NOT NULL,
	[Code] [nvarchar](50) NULL,
	[Name] [nvarchar](50) NULL,
	[CreaterID] [bigint] NULL,
	[CreaterName] [nvarchar](50) NULL,
	[CreateTime] [datetime] NULL,
	[ModifierID] [bigint] NULL,
	[ModifierName] [nvarchar](50) NULL,
	[ModifyTime] [datetime] NULL,
	[IsDeleted] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sys_user]    Script Date: 2024/2/27 0:51:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_user](
	[Id] [bigint] NOT NULL,
	[IsAdmin] [bit] NULL,
	[UserName] [nvarchar](50) NULL,
	[UserPassword] [nvarchar](50) NULL,
	[LastLoginTime] [datetime] NULL,
	[IsEnabled] [bit] NULL,
	[CreaterID] [bigint] NULL,
	[CreaterName] [nvarchar](50) NULL,
	[CreateTime] [datetime] NULL,
	[ModifierID] [bigint] NULL,
	[ModifierName] [nvarchar](50) NULL,
	[ModifyTime] [datetime] NULL,
	[IsDeleted] [bit] NULL,
	[FingePrintPassword] [text] NULL,
	[FingePrintPasswordMD5] [nvarchar](255) NULL,
	[IsFingePrint] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[systemset_generatecodehistory]    Script Date: 2024/2/27 0:51:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[systemset_generatecodehistory](
	[Id] [bigint] NOT NULL,
	[Code] [nvarchar](50) NULL,
	[TemplateId] [bigint] NULL,
	[CreaterID] [bigint] NULL,
	[CreaterName] [nvarchar](50) NULL,
	[CreateTime] [datetime] NULL,
	[ModifierID] [bigint] NULL,
	[ModifierName] [nvarchar](50) NULL,
	[ModifyTime] [datetime] NULL,
	[IsDeleted] [bit] NULL,
PRIMARY KEY NONCLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [TimePartition]([CreateTime])
GO
/****** Object:  Table [dbo].[systemset_generatecodetemplate]    Script Date: 2024/2/27 0:51:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[systemset_generatecodetemplate](
	[Id] [bigint] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Code] [nvarchar](50) NOT NULL,
	[SerialNumberLength] [int] NULL,
	[TemplateJsonFilePath] [nvarchar](500) NULL,
	[TemplateJsonText] [text] NULL,
	[PrintLimitOnce] [int] NULL,
	[CodeLength] [int] NULL,
	[IsCustom] [bit] NULL,
	[IsEnabled] [bit] NULL,
	[IsQuote] [bit] NULL,
	[CreaterID] [bigint] NULL,
	[CreaterName] [nvarchar](50) NULL,
	[CreateTime] [datetime] NULL,
	[ModifierID] [bigint] NULL,
	[ModifierName] [nvarchar](50) NULL,
	[ModifyTime] [datetime] NULL,
	[IsDeleted] [bit] NULL,
	[BTWFile] [varbinary](max) NULL,
	[FileHash] [nvarchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[systemset_generatecodetemplatedetail]    Script Date: 2024/2/27 0:51:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[systemset_generatecodetemplatedetail](
	[Id] [bigint] NOT NULL,
	[TemplateId] [bigint] NULL,
	[RowNo] [int] NULL,
	[RuleId] [bigint] NULL,
	[AliasName] [nvarchar](50) NULL,
	[DictionaryTypeId] [bigint] NOT NULL,
	[RuleValue] [nvarchar](50) NULL,
	[ValueLength] [int] NULL,
	[IsAdminEdit] [bit] NULL,
	[IsCustom] [bit] NULL,
	[IsEnabled] [bit] NULL,
	[IsContain] [bit] NULL,
	[IsQuote] [bit] NULL,
	[CreaterID] [bigint] NULL,
	[CreaterName] [nvarchar](50) NULL,
	[CreateTime] [datetime] NULL,
	[ModifierID] [bigint] NULL,
	[ModifierName] [nvarchar](50) NULL,
	[ModifyTime] [datetime] NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK__systemse__3214EC0798E376E9] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[timecodeset]    Script Date: 2024/2/27 0:51:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[timecodeset](
	[Id] [bigint] NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Code] [nvarchar](50) NULL,
	[TimeCodes] [nvarchar](2000) NULL,
	[StartYear] [int] NULL,
	[Type] [bigint] NULL,
	[Attribute1] [nvarchar](50) NULL,
	[Attribute2] [nvarchar](50) NULL,
	[CreaterID] [bigint] NULL,
	[CreaterName] [nvarchar](50) NULL,
	[CreateTime] [datetime] NULL,
	[ModifierID] [bigint] NULL,
	[ModifierName] [nvarchar](50) NULL,
	[ModifyTime] [datetime] NULL,
	[IsDeleted] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user]    Script Date: 2024/2/27 0:51:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user](
	[Id] [bigint] NOT NULL,
	[IsAdmin] [bit] NULL,
	[UserName] [nvarchar](50) NULL,
	[UserPassword] [nvarchar](50) NULL,
	[LastLoginTime] [datetime] NULL,
	[IsEnabled] [bit] NULL,
	[CreaterID] [bigint] NULL,
	[CreaterName] [nvarchar](50) NULL,
	[CreateTime] [datetime] NULL,
	[ModifierID] [bigint] NULL,
	[ModifierName] [nvarchar](50) NULL,
	[ModifyTime] [datetime] NULL,
	[IsDeleted] [bit] NULL,
	[FingePrintPassword] [text] NULL,
	[FingePrintPasswordMD5] [nvarchar](255) NULL,
	[IsFingePrint] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_Code]    Script Date: 2024/2/27 0:51:50 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Code] ON [dbo].[codeprinthistory]
(
	[Code] ASC
)
INCLUDE([CreateTime]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_CodeStatus]    Script Date: 2024/2/27 0:51:50 ******/
CREATE NONCLUSTERED INDEX [IX_CodeStatus] ON [dbo].[codescanhistory]
(
	[Code] ASC,
	[Status] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_ScanCode]    Script Date: 2024/2/27 0:51:50 ******/
CREATE NONCLUSTERED INDEX [IX_ScanCode] ON [dbo].[codescanhistory]
(
	[ItemCode] ASC,
	[Code] ASC,
	[CreateTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_Code]    Script Date: 2024/2/27 0:51:50 ******/
CREATE NONCLUSTERED INDEX [IX_Code] ON [dbo].[systemset_generatecodehistory]
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_Code]    Script Date: 2024/2/27 0:51:50 ******/
CREATE NONCLUSTERED INDEX [IX_Code] ON [dbo].[systemset_generatecodetemplate]
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[basic_businessdictionary] ADD  DEFAULT ((0)) FOR [IsDictionaryRule]
GO
ALTER TABLE [dbo].[basic_businessdictionary] ADD  DEFAULT ((0)) FOR [RuleDictionaryTypeId]
GO
ALTER TABLE [dbo].[basic_businessdictionary] ADD  DEFAULT ((0)) FOR [IsCustom]
GO
ALTER TABLE [dbo].[basic_businessdictionary] ADD  CONSTRAINT [DF__basic_bus__IsEna__3C69FB99]  DEFAULT ((1)) FOR [IsEnabled]
GO
ALTER TABLE [dbo].[basic_businessdictionary] ADD  DEFAULT ((0)) FOR [IsQuote]
GO
ALTER TABLE [dbo].[basic_businessdictionary] ADD  DEFAULT ((0)) FOR [CreaterID]
GO
ALTER TABLE [dbo].[basic_businessdictionary] ADD  DEFAULT (getdate()) FOR [CreateTime]
GO
ALTER TABLE [dbo].[basic_businessdictionary] ADD  DEFAULT ((0)) FOR [ModifierID]
GO
ALTER TABLE [dbo].[basic_businessdictionary] ADD  DEFAULT (getdate()) FOR [ModifyTime]
GO
ALTER TABLE [dbo].[basic_businessdictionary] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[basic_businessdictionarytype] ADD  DEFAULT ((0)) FOR [IsCustom]
GO
ALTER TABLE [dbo].[basic_businessdictionarytype] ADD  DEFAULT ((0)) FOR [IsBatchAttribute]
GO
ALTER TABLE [dbo].[basic_businessdictionarytype] ADD  DEFAULT ((0)) FOR [IsQuote]
GO
ALTER TABLE [dbo].[basic_businessdictionarytype] ADD  DEFAULT ((0)) FOR [CreaterID]
GO
ALTER TABLE [dbo].[basic_businessdictionarytype] ADD  DEFAULT (getdate()) FOR [CreateTime]
GO
ALTER TABLE [dbo].[basic_businessdictionarytype] ADD  DEFAULT ((0)) FOR [ModifierID]
GO
ALTER TABLE [dbo].[basic_businessdictionarytype] ADD  DEFAULT (getdate()) FOR [ModifyTime]
GO
ALTER TABLE [dbo].[basic_businessdictionarytype] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[basic_businessdictionarytype] ADD  DEFAULT ((0)) FOR [IsEnabled]
GO
ALTER TABLE [dbo].[codeprinthistory] ADD  DEFAULT ((0)) FOR [Id]
GO
ALTER TABLE [dbo].[codeprinthistory] ADD  DEFAULT ((0)) FOR [Status]
GO
ALTER TABLE [dbo].[codeprinthistory] ADD  DEFAULT ((0)) FOR [CreaterID]
GO
ALTER TABLE [dbo].[codeprinthistory] ADD  DEFAULT ((0)) FOR [ModifierID]
GO
ALTER TABLE [dbo].[codeprinthistory] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[iteminfo] ADD  DEFAULT ((0)) FOR [Id]
GO
ALTER TABLE [dbo].[iteminfo] ADD  DEFAULT ((0)) FOR [TemplateId]
GO
ALTER TABLE [dbo].[iteminfo] ADD  DEFAULT ((0)) FOR [CreaterID]
GO
ALTER TABLE [dbo].[iteminfo] ADD  DEFAULT ((0)) FOR [ModifierID]
GO
ALTER TABLE [dbo].[iteminfo] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[itemscancount] ADD  DEFAULT ((1)) FOR [MachineId]
GO
ALTER TABLE [dbo].[itemscancount] ADD  DEFAULT ((0)) FOR [Count]
GO
ALTER TABLE [dbo].[itemscancount] ADD  DEFAULT ((0)) FOR [CreaterID]
GO
ALTER TABLE [dbo].[itemscancount] ADD  DEFAULT (getdate()) FOR [CreateTime]
GO
ALTER TABLE [dbo].[itemscancount] ADD  DEFAULT ((0)) FOR [ModifierID]
GO
ALTER TABLE [dbo].[itemscancount] ADD  DEFAULT (getdate()) FOR [ModifyTime]
GO
ALTER TABLE [dbo].[itemscancount] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[printplan] ADD  DEFAULT ((0)) FOR [Id]
GO
ALTER TABLE [dbo].[printplan] ADD  DEFAULT ((0)) FOR [CreaterID]
GO
ALTER TABLE [dbo].[printplan] ADD  DEFAULT ((0)) FOR [ModifierID]
GO
ALTER TABLE [dbo].[printplan] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[printplan] ADD  DEFAULT ((1)) FOR [PlanQty]
GO
ALTER TABLE [dbo].[serialnumber] ADD  DEFAULT ((0)) FOR [Id]
GO
ALTER TABLE [dbo].[serialnumber] ADD  DEFAULT ((0)) FOR [SerialNumber]
GO
ALTER TABLE [dbo].[serialnumber] ADD  DEFAULT ((0)) FOR [CreaterID]
GO
ALTER TABLE [dbo].[serialnumber] ADD  DEFAULT ((0)) FOR [ModifierID]
GO
ALTER TABLE [dbo].[serialnumber] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[supplier] ADD  DEFAULT ((0)) FOR [Id]
GO
ALTER TABLE [dbo].[supplier] ADD  DEFAULT ((0)) FOR [CreaterID]
GO
ALTER TABLE [dbo].[supplier] ADD  DEFAULT ((0)) FOR [ModifierID]
GO
ALTER TABLE [dbo].[supplier] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[sys_user] ADD  DEFAULT ((0)) FOR [Id]
GO
ALTER TABLE [dbo].[sys_user] ADD  DEFAULT ((0)) FOR [IsAdmin]
GO
ALTER TABLE [dbo].[sys_user] ADD  DEFAULT ((0)) FOR [IsEnabled]
GO
ALTER TABLE [dbo].[sys_user] ADD  DEFAULT ((0)) FOR [CreaterID]
GO
ALTER TABLE [dbo].[sys_user] ADD  DEFAULT ((0)) FOR [ModifierID]
GO
ALTER TABLE [dbo].[sys_user] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[sys_user] ADD  DEFAULT ((0)) FOR [IsFingePrint]
GO
ALTER TABLE [dbo].[systemset_generatecodehistory] ADD  DEFAULT ((0)) FOR [CreaterID]
GO
ALTER TABLE [dbo].[systemset_generatecodehistory] ADD  DEFAULT (getdate()) FOR [CreateTime]
GO
ALTER TABLE [dbo].[systemset_generatecodehistory] ADD  DEFAULT ((0)) FOR [ModifierID]
GO
ALTER TABLE [dbo].[systemset_generatecodehistory] ADD  DEFAULT (getdate()) FOR [ModifyTime]
GO
ALTER TABLE [dbo].[systemset_generatecodehistory] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[systemset_generatecodetemplate] ADD  DEFAULT ((5)) FOR [SerialNumberLength]
GO
ALTER TABLE [dbo].[systemset_generatecodetemplate] ADD  DEFAULT ((1000)) FOR [PrintLimitOnce]
GO
ALTER TABLE [dbo].[systemset_generatecodetemplate] ADD  DEFAULT ((0)) FOR [CodeLength]
GO
ALTER TABLE [dbo].[systemset_generatecodetemplate] ADD  CONSTRAINT [DF__systemset__IsCus__09A971A2]  DEFAULT ((1)) FOR [IsCustom]
GO
ALTER TABLE [dbo].[systemset_generatecodetemplate] ADD  DEFAULT ((0)) FOR [IsEnabled]
GO
ALTER TABLE [dbo].[systemset_generatecodetemplate] ADD  DEFAULT ((0)) FOR [IsQuote]
GO
ALTER TABLE [dbo].[systemset_generatecodetemplate] ADD  DEFAULT ((0)) FOR [CreaterID]
GO
ALTER TABLE [dbo].[systemset_generatecodetemplate] ADD  DEFAULT (getdate()) FOR [CreateTime]
GO
ALTER TABLE [dbo].[systemset_generatecodetemplate] ADD  DEFAULT ((0)) FOR [ModifierID]
GO
ALTER TABLE [dbo].[systemset_generatecodetemplate] ADD  DEFAULT (getdate()) FOR [ModifyTime]
GO
ALTER TABLE [dbo].[systemset_generatecodetemplate] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[systemset_generatecodetemplatedetail] ADD  CONSTRAINT [DF__systemset__Dicti__33D4B598]  DEFAULT ((0)) FOR [DictionaryTypeId]
GO
ALTER TABLE [dbo].[systemset_generatecodetemplatedetail] ADD  CONSTRAINT [DF__systemset__Value__35BCFE0A]  DEFAULT ((0)) FOR [ValueLength]
GO
ALTER TABLE [dbo].[systemset_generatecodetemplatedetail] ADD  CONSTRAINT [DF__systemset__IsAdm__37A5467C]  DEFAULT ((0)) FOR [IsAdminEdit]
GO
ALTER TABLE [dbo].[systemset_generatecodetemplatedetail] ADD  CONSTRAINT [DF__systemset__IsCus__3A81B327]  DEFAULT ((1)) FOR [IsCustom]
GO
ALTER TABLE [dbo].[systemset_generatecodetemplatedetail] ADD  CONSTRAINT [DF__systemset__IsEna__3D5E1FD2]  DEFAULT ((1)) FOR [IsEnabled]
GO
ALTER TABLE [dbo].[systemset_generatecodetemplatedetail] ADD  CONSTRAINT [DF_systemset_generatecodetemplatedetail_IsContain]  DEFAULT ((1)) FOR [IsContain]
GO
ALTER TABLE [dbo].[systemset_generatecodetemplatedetail] ADD  CONSTRAINT [DF__systemset__IsQuo__403A8C7D]  DEFAULT ((0)) FOR [IsQuote]
GO
ALTER TABLE [dbo].[systemset_generatecodetemplatedetail] ADD  CONSTRAINT [DF__systemset__Creat__4222D4EF]  DEFAULT ((0)) FOR [CreaterID]
GO
ALTER TABLE [dbo].[systemset_generatecodetemplatedetail] ADD  CONSTRAINT [DF__systemset__Creat__440B1D61]  DEFAULT (getdate()) FOR [CreateTime]
GO
ALTER TABLE [dbo].[systemset_generatecodetemplatedetail] ADD  CONSTRAINT [DF__systemset__Modif__45F365D3]  DEFAULT ((0)) FOR [ModifierID]
GO
ALTER TABLE [dbo].[systemset_generatecodetemplatedetail] ADD  CONSTRAINT [DF__systemset__Modif__47DBAE45]  DEFAULT (getdate()) FOR [ModifyTime]
GO
ALTER TABLE [dbo].[systemset_generatecodetemplatedetail] ADD  CONSTRAINT [DF__systemset__IsDel__49C3F6B7]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[timecodeset] ADD  DEFAULT ((0)) FOR [Id]
GO
ALTER TABLE [dbo].[timecodeset] ADD  DEFAULT ((0)) FOR [Type]
GO
ALTER TABLE [dbo].[timecodeset] ADD  DEFAULT ((0)) FOR [CreaterID]
GO
ALTER TABLE [dbo].[timecodeset] ADD  DEFAULT ((0)) FOR [ModifierID]
GO
ALTER TABLE [dbo].[timecodeset] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[user] ADD  DEFAULT ((0)) FOR [Id]
GO
ALTER TABLE [dbo].[user] ADD  DEFAULT ((0)) FOR [IsAdmin]
GO
ALTER TABLE [dbo].[user] ADD  DEFAULT ((0)) FOR [IsEnabled]
GO
ALTER TABLE [dbo].[user] ADD  DEFAULT ((0)) FOR [CreaterID]
GO
ALTER TABLE [dbo].[user] ADD  DEFAULT ((0)) FOR [ModifierID]
GO
ALTER TABLE [dbo].[user] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[user] ADD  DEFAULT ((0)) FOR [IsFingePrint]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'basic_businessdictionary', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'字典类型Id （TBasicDictionaryType的主键）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'basic_businessdictionary', @level2type=N'COLUMN',@level2name=N'DictionaryTypeId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否是字典规则' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'basic_businessdictionary', @level2type=N'COLUMN',@level2name=N'IsDictionaryRule'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'规则选项对应的TypeId' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'basic_businessdictionary', @level2type=N'COLUMN',@level2name=N'RuleDictionaryTypeId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'字典code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'basic_businessdictionary', @level2type=N'COLUMN',@level2name=N'Code'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'字典名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'basic_businessdictionary', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'简称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'basic_businessdictionary', @level2type=N'COLUMN',@level2name=N'ShortName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'basic_businessdictionary', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否自定义 0否 1是' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'basic_businessdictionary', @level2type=N'COLUMN',@level2name=N'IsCustom'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否可用 0 否 1是' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'basic_businessdictionary', @level2type=N'COLUMN',@level2name=N'IsEnabled'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否已引用（已引用的不能修改或删除）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'basic_businessdictionary', @level2type=N'COLUMN',@level2name=N'IsQuote'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'basic_businessdictionary', @level2type=N'COLUMN',@level2name=N'CreaterID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'basic_businessdictionary', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'basic_businessdictionary', @level2type=N'COLUMN',@level2name=N'ModifierID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'basic_businessdictionary', @level2type=N'COLUMN',@level2name=N'ModifyTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否已删除' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'basic_businessdictionary', @level2type=N'COLUMN',@level2name=N'IsDeleted'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'basic_businessdictionary', @level2type=N'COLUMN',@level2name=N'CreaterName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'basic_businessdictionary', @level2type=N'COLUMN',@level2name=N'ModifierName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'basic_businessdictionarytype', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'字典类型名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'basic_businessdictionarytype', @level2type=N'COLUMN',@level2name=N'TypeName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否自定义' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'basic_businessdictionarytype', @level2type=N'COLUMN',@level2name=N'IsCustom'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'编码 (枚举使用）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'basic_businessdictionarytype', @level2type=N'COLUMN',@level2name=N'Code'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否为批次属性' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'basic_businessdictionarytype', @level2type=N'COLUMN',@level2name=N'IsBatchAttribute'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'basic_businessdictionarytype', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否已引用（已引用的不能修改或删除）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'basic_businessdictionarytype', @level2type=N'COLUMN',@level2name=N'IsQuote'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'basic_businessdictionarytype', @level2type=N'COLUMN',@level2name=N'CreaterID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'basic_businessdictionarytype', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'basic_businessdictionarytype', @level2type=N'COLUMN',@level2name=N'ModifierID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'basic_businessdictionarytype', @level2type=N'COLUMN',@level2name=N'ModifyTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否已删除' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'basic_businessdictionarytype', @level2type=N'COLUMN',@level2name=N'IsDeleted'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'basic_businessdictionarytype', @level2type=N'COLUMN',@level2name=N'CreaterName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'basic_businessdictionarytype', @level2type=N'COLUMN',@level2name=N'ModifierName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否启用' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'basic_businessdictionarytype', @level2type=N'COLUMN',@level2name=N'IsEnabled'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'codeprinthistory', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'完整条码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'codeprinthistory', @level2type=N'COLUMN',@level2name=N'Code'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'供应商代码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'codeprinthistory', @level2type=N'COLUMN',@level2name=N'SupplierCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'当前日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'codeprinthistory', @level2type=N'COLUMN',@level2name=N'CurrentDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'物料码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'codeprinthistory', @level2type=N'COLUMN',@level2name=N'ItemCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'版本' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'codeprinthistory', @level2type=N'COLUMN',@level2name=N'Version'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'工厂代码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'codeprinthistory', @level2type=N'COLUMN',@level2name=N'FactoryCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'年' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'codeprinthistory', @level2type=N'COLUMN',@level2name=N'Year'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'周' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'codeprinthistory', @level2type=N'COLUMN',@level2name=N'Week'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'天' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'codeprinthistory', @level2type=N'COLUMN',@level2name=N'Day'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'流水号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'codeprinthistory', @level2type=N'COLUMN',@level2name=N'SerialNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0:等待打印，1：打印成功' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'codeprinthistory', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'打印时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'codeprinthistory', @level2type=N'COLUMN',@level2name=N'PrintTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'codeprinthistory', @level2type=N'COLUMN',@level2name=N'CreaterID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'codeprinthistory', @level2type=N'COLUMN',@level2name=N'CreaterName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'codeprinthistory', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'codeprinthistory', @level2type=N'COLUMN',@level2name=N'ModifierID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'codeprinthistory', @level2type=N'COLUMN',@level2name=N'ModifierName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'codeprinthistory', @level2type=N'COLUMN',@level2name=N'ModifyTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否删除' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'codeprinthistory', @level2type=N'COLUMN',@level2name=N'IsDeleted'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'版本' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'codeprinthistory', @level2type=N'COLUMN',@level2name=N'VersionCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'生产制令号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'codeprinthistory', @level2type=N'COLUMN',@level2name=N'ProductionCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'操作员' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'codeprinthistory', @level2type=N'COLUMN',@level2name=N'OperateName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'codescanhistory', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'codescanhistory', @level2type=N'COLUMN',@level2name=N'Code'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'codescanhistory', @level2type=N'COLUMN',@level2name=N'CreaterID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'codescanhistory', @level2type=N'COLUMN',@level2name=N'CreaterName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'codescanhistory', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'codescanhistory', @level2type=N'COLUMN',@level2name=N'ModifierID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'codescanhistory', @level2type=N'COLUMN',@level2name=N'ModifierName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'codescanhistory', @level2type=N'COLUMN',@level2name=N'ModifyTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否删除' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'codescanhistory', @level2type=N'COLUMN',@level2name=N'IsDeleted'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'生产制令号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'codescanhistory', @level2type=N'COLUMN',@level2name=N'ProductionCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：正常，2：重复，3：错误' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'codescanhistory', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'物料码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'codescanhistory', @level2type=N'COLUMN',@level2name=N'ItemCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'错误内容' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'codescanhistory', @level2type=N'COLUMN',@level2name=N'ErrorContent'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'iteminfo', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'iteminfo', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'iteminfo', @level2type=N'COLUMN',@level2name=N'Code'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'版本' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'iteminfo', @level2type=N'COLUMN',@level2name=N'VersionCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'物料类型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'iteminfo', @level2type=N'COLUMN',@level2name=N'ProductType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'模板id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'iteminfo', @level2type=N'COLUMN',@level2name=N'TemplateId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'扩展属性1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'iteminfo', @level2type=N'COLUMN',@level2name=N'Attribute1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'扩展属性2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'iteminfo', @level2type=N'COLUMN',@level2name=N'Attribute2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'扩展属性3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'iteminfo', @level2type=N'COLUMN',@level2name=N'Attribute3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'扩展属性4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'iteminfo', @level2type=N'COLUMN',@level2name=N'Attribute4'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'iteminfo', @level2type=N'COLUMN',@level2name=N'CreaterID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'iteminfo', @level2type=N'COLUMN',@level2name=N'CreaterName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'iteminfo', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'iteminfo', @level2type=N'COLUMN',@level2name=N'ModifierID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'iteminfo', @level2type=N'COLUMN',@level2name=N'ModifierName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'iteminfo', @level2type=N'COLUMN',@level2name=N'ModifyTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否删除' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'iteminfo', @level2type=N'COLUMN',@level2name=N'IsDeleted'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'itemscancount', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'物料编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'itemscancount', @level2type=N'COLUMN',@level2name=N'ItemCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'机器id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'itemscancount', @level2type=N'COLUMN',@level2name=N'MachineId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'itemscancount', @level2type=N'COLUMN',@level2name=N'Count'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'itemscancount', @level2type=N'COLUMN',@level2name=N'CreaterID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'itemscancount', @level2type=N'COLUMN',@level2name=N'CreaterName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'itemscancount', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'itemscancount', @level2type=N'COLUMN',@level2name=N'ModifierID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'itemscancount', @level2type=N'COLUMN',@level2name=N'ModifierName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'itemscancount', @level2type=N'COLUMN',@level2name=N'ModifyTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否删除' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'itemscancount', @level2type=N'COLUMN',@level2name=N'IsDeleted'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'printplan', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'printplan', @level2type=N'COLUMN',@level2name=N'CreaterID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'printplan', @level2type=N'COLUMN',@level2name=N'CreaterName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'printplan', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'printplan', @level2type=N'COLUMN',@level2name=N'ModifierID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'printplan', @level2type=N'COLUMN',@level2name=N'ModifierName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'printplan', @level2type=N'COLUMN',@level2name=N'ModifyTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否删除' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'printplan', @level2type=N'COLUMN',@level2name=N'IsDeleted'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'料号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'printplan', @level2type=N'COLUMN',@level2name=N'ItemCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'计划日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'printplan', @level2type=N'COLUMN',@level2name=N'PlanDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'计划数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'printplan', @level2type=N'COLUMN',@level2name=N'PlanQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'serialnumber', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'物料码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'serialnumber', @level2type=N'COLUMN',@level2name=N'ItemCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'年' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'serialnumber', @level2type=N'COLUMN',@level2name=N'Year'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'周' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'serialnumber', @level2type=N'COLUMN',@level2name=N'Week'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'天' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'serialnumber', @level2type=N'COLUMN',@level2name=N'Day'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'流水号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'serialnumber', @level2type=N'COLUMN',@level2name=N'SerialNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'serialnumber', @level2type=N'COLUMN',@level2name=N'CreaterID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'serialnumber', @level2type=N'COLUMN',@level2name=N'CreaterName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'serialnumber', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'serialnumber', @level2type=N'COLUMN',@level2name=N'ModifierID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'serialnumber', @level2type=N'COLUMN',@level2name=N'ModifierName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'serialnumber', @level2type=N'COLUMN',@level2name=N'ModifyTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否删除' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'serialnumber', @level2type=N'COLUMN',@level2name=N'IsDeleted'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'supplier', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'供应商代码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'supplier', @level2type=N'COLUMN',@level2name=N'Code'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'supplier', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'supplier', @level2type=N'COLUMN',@level2name=N'CreaterID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'supplier', @level2type=N'COLUMN',@level2name=N'CreaterName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'supplier', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'supplier', @level2type=N'COLUMN',@level2name=N'ModifierID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'supplier', @level2type=N'COLUMN',@level2name=N'ModifierName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'supplier', @level2type=N'COLUMN',@level2name=N'ModifyTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否删除' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'supplier', @level2type=N'COLUMN',@level2name=N'IsDeleted'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否管理员' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'IsAdmin'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'UserName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户密码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'UserPassword'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最近登录时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'LastLoginTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否启用' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'IsEnabled'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'CreaterID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'CreaterName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'ModifierID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'ModifierName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'ModifyTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否删除' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'IsDeleted'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'指纹密码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'FingePrintPassword'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'指纹密码MD5' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'FingePrintPasswordMD5'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'指纹密码状态' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'IsFingePrint'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'systemset_generatecodehistory', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'生成的编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'systemset_generatecodehistory', @level2type=N'COLUMN',@level2name=N'Code'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'编码生成模板id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'systemset_generatecodehistory', @level2type=N'COLUMN',@level2name=N'TemplateId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'systemset_generatecodehistory', @level2type=N'COLUMN',@level2name=N'CreaterID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'systemset_generatecodehistory', @level2type=N'COLUMN',@level2name=N'CreaterName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'systemset_generatecodehistory', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'systemset_generatecodehistory', @level2type=N'COLUMN',@level2name=N'ModifierID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'systemset_generatecodehistory', @level2type=N'COLUMN',@level2name=N'ModifierName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'systemset_generatecodehistory', @level2type=N'COLUMN',@level2name=N'ModifyTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否已删除' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'systemset_generatecodehistory', @level2type=N'COLUMN',@level2name=N'IsDeleted'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'systemset_generatecodetemplate', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'编码生成模板名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'systemset_generatecodetemplate', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'编码生成模板code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'systemset_generatecodetemplate', @level2type=N'COLUMN',@level2name=N'Code'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'流水号长度' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'systemset_generatecodetemplate', @level2type=N'COLUMN',@level2name=N'SerialNumberLength'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'模板json数据文件路径' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'systemset_generatecodetemplate', @level2type=N'COLUMN',@level2name=N'TemplateJsonFilePath'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'模板json数据' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'systemset_generatecodetemplate', @level2type=N'COLUMN',@level2name=N'TemplateJsonText'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'每次打印次数限制' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'systemset_generatecodetemplate', @level2type=N'COLUMN',@level2name=N'PrintLimitOnce'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'条码固定长度，0 代表不固定' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'systemset_generatecodetemplate', @level2type=N'COLUMN',@level2name=N'CodeLength'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否自定义 0否 1是' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'systemset_generatecodetemplate', @level2type=N'COLUMN',@level2name=N'IsCustom'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否可用 0 否 1是' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'systemset_generatecodetemplate', @level2type=N'COLUMN',@level2name=N'IsEnabled'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否已引用（已引用的不能修改或删除）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'systemset_generatecodetemplate', @level2type=N'COLUMN',@level2name=N'IsQuote'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'systemset_generatecodetemplate', @level2type=N'COLUMN',@level2name=N'CreaterID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'systemset_generatecodetemplate', @level2type=N'COLUMN',@level2name=N'CreaterName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'systemset_generatecodetemplate', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'systemset_generatecodetemplate', @level2type=N'COLUMN',@level2name=N'ModifierID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'systemset_generatecodetemplate', @level2type=N'COLUMN',@level2name=N'ModifierName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'systemset_generatecodetemplate', @level2type=N'COLUMN',@level2name=N'ModifyTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否已删除' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'systemset_generatecodetemplate', @level2type=N'COLUMN',@level2name=N'IsDeleted'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'btw文件流' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'systemset_generatecodetemplate', @level2type=N'COLUMN',@level2name=N'BTWFile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'文件hash' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'systemset_generatecodetemplate', @level2type=N'COLUMN',@level2name=N'FileHash'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'systemset_generatecodetemplatedetail', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'编码生成规则模板id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'systemset_generatecodetemplatedetail', @level2type=N'COLUMN',@level2name=N'TemplateId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'行号（不可重复）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'systemset_generatecodetemplatedetail', @level2type=N'COLUMN',@level2name=N'RowNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'条码规则id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'systemset_generatecodetemplatedetail', @level2type=N'COLUMN',@level2name=N'RuleId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'别名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'systemset_generatecodetemplatedetail', @level2type=N'COLUMN',@level2name=N'AliasName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'字典类型Id （主键）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'systemset_generatecodetemplatedetail', @level2type=N'COLUMN',@level2name=N'DictionaryTypeId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'条码规则值' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'systemset_generatecodetemplatedetail', @level2type=N'COLUMN',@level2name=N'RuleValue'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'值长度默认为0 ，表示不限制' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'systemset_generatecodetemplatedetail', @level2type=N'COLUMN',@level2name=N'ValueLength'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否需要管理员授权' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'systemset_generatecodetemplatedetail', @level2type=N'COLUMN',@level2name=N'IsAdminEdit'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否自定义 0否 1是' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'systemset_generatecodetemplatedetail', @level2type=N'COLUMN',@level2name=N'IsCustom'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否可用 0 否 1是' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'systemset_generatecodetemplatedetail', @level2type=N'COLUMN',@level2name=N'IsEnabled'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否包含在条码中' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'systemset_generatecodetemplatedetail', @level2type=N'COLUMN',@level2name=N'IsContain'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否已引用（已引用的不能修改或删除）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'systemset_generatecodetemplatedetail', @level2type=N'COLUMN',@level2name=N'IsQuote'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'systemset_generatecodetemplatedetail', @level2type=N'COLUMN',@level2name=N'CreaterID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'systemset_generatecodetemplatedetail', @level2type=N'COLUMN',@level2name=N'CreaterName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'systemset_generatecodetemplatedetail', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'systemset_generatecodetemplatedetail', @level2type=N'COLUMN',@level2name=N'ModifierID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'systemset_generatecodetemplatedetail', @level2type=N'COLUMN',@level2name=N'ModifierName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'systemset_generatecodetemplatedetail', @level2type=N'COLUMN',@level2name=N'ModifyTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否已删除' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'systemset_generatecodetemplatedetail', @level2type=N'COLUMN',@level2name=N'IsDeleted'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'timecodeset', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'timecodeset', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'timecodeset', @level2type=N'COLUMN',@level2name=N'Code'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'代号编码LIST' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'timecodeset', @level2type=N'COLUMN',@level2name=N'TimeCodes'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'类型1:比亚迪年份，2:比亚迪月份' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'timecodeset', @level2type=N'COLUMN',@level2name=N'Type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'扩展属性1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'timecodeset', @level2type=N'COLUMN',@level2name=N'Attribute1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'扩展属性2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'timecodeset', @level2type=N'COLUMN',@level2name=N'Attribute2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'timecodeset', @level2type=N'COLUMN',@level2name=N'CreaterID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'timecodeset', @level2type=N'COLUMN',@level2name=N'CreaterName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'timecodeset', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'timecodeset', @level2type=N'COLUMN',@level2name=N'ModifierID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'timecodeset', @level2type=N'COLUMN',@level2name=N'ModifierName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'timecodeset', @level2type=N'COLUMN',@level2name=N'ModifyTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否删除' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'timecodeset', @level2type=N'COLUMN',@level2name=N'IsDeleted'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'user', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否管理员' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'user', @level2type=N'COLUMN',@level2name=N'IsAdmin'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'user', @level2type=N'COLUMN',@level2name=N'UserName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户密码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'user', @level2type=N'COLUMN',@level2name=N'UserPassword'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最近登录时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'user', @level2type=N'COLUMN',@level2name=N'LastLoginTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否启用' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'user', @level2type=N'COLUMN',@level2name=N'IsEnabled'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'user', @level2type=N'COLUMN',@level2name=N'CreaterID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'user', @level2type=N'COLUMN',@level2name=N'CreaterName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'user', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'user', @level2type=N'COLUMN',@level2name=N'ModifierID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'user', @level2type=N'COLUMN',@level2name=N'ModifierName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'user', @level2type=N'COLUMN',@level2name=N'ModifyTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否删除' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'user', @level2type=N'COLUMN',@level2name=N'IsDeleted'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'指纹密码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'user', @level2type=N'COLUMN',@level2name=N'FingePrintPassword'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'指纹密码MD5' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'user', @level2type=N'COLUMN',@level2name=N'FingePrintPasswordMD5'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'指纹密码状态' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'user', @level2type=N'COLUMN',@level2name=N'IsFingePrint'
GO
USE [master]
GO
ALTER DATABASE [hesheng_codeprintsystem] SET  READ_WRITE 
GO
